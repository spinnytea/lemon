/*
	save/load configuration data
*/
const boundaries = require('./boundaries');
const utils = require('./utils');

const config = exports;

/**
	load a value from configuration

	@param {string} key - which value
	@param {JSON} value - default value if not found; must be a subset of JSON
	@return {Promise<JSON>} the value for the key
*/
config.get = async (key, value) => Promise.resolve(config.units.getValue(key, value));

/**
	save a value into configuration

	@param {string} key - which value
	@param {JSON} value - the value to save; must be a subset of JSON
	@return {Promise<JSON>} the value for the key
*/
config.set = async (key, value) => Promise.resolve(config.units.setValue(key, value));

/**
	basic properties that affect how the program behaves at runtime
*/
config.settings = {
	/* istanbul ignore next: this isn't supposed to ever actually run */
	get location() { throw new Error('must overwrite "location" (the location of the idea database)'); },
	// do we use an in_memory database (not persisted) or do we use the disk (in_memory is for unit testing)
	in_memory: false,
	// allows us to load test data from disk while we are in_memory (memory_fallback_to_location is for "unit" testing)
	memory_fallback_to_location: false,

	// REVIEW astar_max_paths is an initial seed value, can/should we adjust it at runtime? Or does this operate at too low of a level
	astar_max_paths: 100,
	// TODO add some "emotion config" to astar frontier expansion
	//  - if distFromGoal is more important, then we will charge straight for the goal (depth first)
	//  - if cost is important, then we will find the "optimal" solution (breadth first), but this takes a while
	astar_weight_cost: 1,
	astar_weight_distFromGoal: 10,

	blueprint_default_distance: 1,
	actuator_base_distance: 1,
	stub_base_distance: 2,
	serial_empty_distance: 100,
};

/**
	register a hook to call after initialization
*/
config.onInit = async (onInitCallback) => {
	config.units.onInitCallbacks.push(onInitCallback);
	if (config.units.hasInit) {
		const shouldSave = await onInitCallback(true);
		if (shouldSave) {
			return config.save();
		}
	}
	return undefined;
};

/**
	re/load settings from disk and re/run all the onInit callbacks
*/
config.init = (settings) => {
	delete config.settings.location;
	config.settings.location = undefined;

	Object.assign(config.settings, settings);

	if (settings.location) {
		config.units.data = config.units.readFileSync(`${config.settings.location}/${config.settings.filename}.json`);
	}
	else if (config.settings.in_memory) {
		Object.defineProperty(config.settings, 'location', { get() { throw new Error('must overwrite "location" (the location of the idea database)'); } });
		config.units.data = {};
	}
	else {
		throw new Error('cannot init without location or in_memory');
	}

	return Promise.all(config.units.onInitCallbacks.map((onInitCallback) => onInitCallback(!config.units.hasInit)))
		.then((shouldSave) => {
			config.units.hasInit = true;
			if (shouldSave.some((r) => !!r)) {
				return config.save();
			}
			return undefined;
		});
};

/**
	does not save immediately, will wait a moment and collect requests
	this promise will resolve once it has actually saved
*/
config.save = async () => config.units.save();

/*
	UNITS
*/

Object.defineProperty(config, 'units', { value: {} });

/** in_memory version of the data - must call init first */
config.units.data = null;

config.units.setValue = function setValue(key, value) {
	if (arguments.length !== 2) throw new Error('config must specify a key and default value');
	if (!key || typeof key !== 'string') throw new TypeError('key must be a string');
	return (config.units.data[key] = value);
};

config.units.getValue = function getValue(key, value) {
	if (arguments.length !== 2) throw new Error('config must specify a key and value');
	if (!key || typeof key !== 'string') throw new TypeError('key must be a string');
	if (!(key in config.units.data)) return value;
	return config.units.data[key];
};

config.units.hasInit = false;

config.units.onInitCallbacks = [];

config.units.$saveTimeout = null;
config.units.$writing = false;
config.units.save = utils.join(() => {
	if (config.settings.in_memory) return [Promise.resolve()];

	// XXX use _.debounce so we can throttle but still write on occasion
	boundaries.clearTimeout(config.units.$saveTimeout);
	let $resolve;
	let $reject;
	const $promise = new Promise((resolve, reject) => {
		$resolve = resolve;
		$reject = reject;
		config.units.$saveTimeout = boundaries.setTimeout(() => {
			// if we are currently writing something, redo the timeout
			// this is the same as if it were canceled, but it also re-queues
			if (config.units.$writing) {
				config.save();
				return;
			}

			config.units.$writing = true;
			config.units.writeFile(`${config.settings.location}/${config.settings.filename}.json`, config.units.data)
				.then(() => { config.units.$writing = false; resolve(); }, (err) => { config.units.$writing = false; reject(err); });
		}, 1000);
	});
	return [$promise, $resolve, $reject];
});

config.units.readFileSync = function readFileSync(filename) {
	if (!boundaries.existsSync(filename)) throw new Error(`config file does not exist: "${filename}"`);
	return JSON.parse(boundaries.readFileSync(filename, { encoding: 'utf8' }));
};

config.units.writeFile = function writeFile(filename, data) {
	return boundaries.writeFile(filename, JSON.stringify(data, null, 2), { encoding: 'utf8' });
};

/*
	INIT
*/

config.onInit(() => {
	config.units.$saveTimeout = null;
});
