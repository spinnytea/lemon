const _ = require('lodash');
const astar = require('./algorithms/astar');
const { createStartAndGoal } = require('./primitives/blueprint');
const { SerialAction } = require('./actions/serial');
const { StubAction } = require('./actions/stub');

const planner = exports;

/**
	create a plan
	@param {BlueprintState} start
	@param {BlueprintState|Array<BlueprintState>} goal - if this is an array of goals, then it will create a serial plan that passes through each one in order
	@return {BlueprintAction|undefined} the action we have found
*/
planner.create = async (start, goal) => planner.units.create(start, goal);

/*
	UNITS
*/

Object.defineProperty(planner, 'units', { value: {} });

planner.units.create = function create(start, goal) {
	if (!start || !goal) {
		return undefined;
	}

	if (_.isArray(goal)) {
		switch (goal.length) {
		case 0: return undefined;
		case 1: return planner.units.createSingle(start, goal[0]).then(({ action }) => action);
		default: return planner.units.createMultiple(start, goal);
		}
	}

	return planner.units.createSingle(start, goal).then(({ action }) => action);
};

/**
	create a plan that has a single goal

	@return {{
		action, // {BlueprintAction} - the Action we have found
		state, // {BlueprintState} - the last State of the path (in case we need to chain)
	}
*/
planner.units.createSingle = async function createSingle(start, goal) {
	const path = await planner.boundaries.search(start, goal);

	if (path === undefined) {
		return { action: undefined, state: undefined };
	}

	if (path.actions.length === 0) {
		// do a little finagling for the sake of scheduleActual
		// REVIEW is there a better way to say "the start matches the goal"?
		// XXX these are HYPER specific requirements; shouldn't we set it to `goal`?
		const sp = new SerialAction({ steps: [], requirements: start.state });
		sp.$glue = [];
		sp.$gluedState = start.state;
		sp.$gluedFinalState = start.state;
		return { action: sp, state: start };
	}

	// TODO this doesn't make sense as actions.map; it updates state, too
	// TODO this only works when there is only one stub and its the last item (these are replanned in parallel, the goals may not align in the end)
	// - e.g. lm-wumpus: stub -> right -> stub ~ this doesn't make sense, because we don't know our direction after each stub
	// - it will work if we can "live replan" whenever there's a problem
	path.actions = await Promise.all(path.actions.map(async (a, idx) => {
		if (a instanceof StubAction && a.solveAt === 'create') {
			// re-plan this step
			// XXX don't use astar.boundaries - ...but this is a continuation of the search
			if (astar.boundaries.debugLogs) astar.boundaries.log('re-plan', a.prettyNames());

			const startAndGoal = createStartAndGoal(path.states[idx], a, path.states[idx + 1]);

			const result = await createSingle(startAndGoal.start, startAndGoal.goal);
			if (result.action === undefined) return undefined;
			if (result.action instanceof SerialAction) {
				result.action.requirements = a.requirements;
			}
			path.states[idx + 1].state = result.state.state;
			return result.action;
		}
		return a;
	}));

	// after we've solved for stubs, we need to make sure that all of the actions check out
	// if one of them doesn't work, then the whole plan doesn't work
	if (path.actions.some((a) => a === undefined)) {
		return { action: undefined, state: undefined };
	}

	if (path.actions.length === 1) {
		return { action: path.actions[0], state: path.last };
	}

	const sp = new SerialAction({ steps: path.actions, requirements: start.state });
	sp.$glue = path.actions;
	sp.$gluedState = start.state;
	sp.$gluedFinalState = path.last.state;
	return { action: sp, state: path.last };
};

/**
	create a plan that goes through multiple goes

	if every plan succeeds, then return a new serial action
	if one of the plans fails, then the whole thing fails
*/
planner.units.createMultiple = async function createMultiple(start, goals) {
	const actions = [];
	let latest = start;

	const found = await goals.reduce((keepGoingPromise, goal) => (
		keepGoingPromise.then(async (keepGoing) => {
			if (!keepGoing) return false;
			const as = await planner.units.createSingle(latest, goal);
			if (as.action === undefined) return false;
			actions.push(as.action);
			latest = as.state;
			return true;
		})
	), Promise.resolve(true));

	if (!found) return undefined;

	const sp = new SerialAction({ steps: actions, requirements: start.state });
	sp.$glue = actions;
	sp.$gluedState = start.state;
	sp.$gluedFinalState = latest.state;
	return sp;
};

/*
	BOUNDARIES
*/

Object.defineProperty(planner, 'boundaries', { value: {} });

planner.boundaries.search = (start, goal) => astar.search(start, goal);
