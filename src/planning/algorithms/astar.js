const _ = require('lodash');
const PriorityQueue = require('priorityqueuejs');

const config = require('../../config');
const { StubAction } = require('../actions/stub');
const { Path, PathState } = require('../primitives/path');

/*
	Planning Forward is basically (exactly) "guess and test"
	 - calcWeight is "basically just" our intuition about how long this will take
	 - astar just builds a queue of the plans that "seem the best so far" and evaluates actions against them

	TODO Planning Backwards is just "start from the goal and work backwards"
	 - we need to ensure that all of rewrite.transactions are reversable (we need to be able to apply them backwards from the goal)
	 - we will also be _starting_ with the goal state (non-concrete) and applying actions that may have _new vertices and edges_
	   (which needs to be solved eventually anyways)

	REVIEW review/rewrite this whole file (after Blueprint, after StubAction)
*/
const astar = exports;

/**
	find a `Path` from the `start` to the `goal`

	REVIEW goals is a list, is it okay to say that these goals are equivalent and interchangable?
	 - wouldn't it always be better to consider two goals independently?
	 - like, in what situation are the two goals usefully different but identical?
	 - Wouldn't we use orData instead of two goals? Is this how we handle matching transitionable edges (that sounds aweful)

	@param {PathState} start - initial
	@param {PathState|Array<PathState>} goals - final (if this is an array of goals, then it will stop at the closest goals)
	@return {Path|undefined} the path we found, or undefined if we found none
*/
astar.search = async (start, goals) => astar.units.doSearch(start, goals);

/*
	UNITS
*/

Object.defineProperty(astar, 'units', { value: {} });

/** REVIEW is exiting early different from being unable to find a solution? should we exit differently? */
astar.units.doSearch = async function doSearch(start, goals) {
	if (!(start instanceof PathState)) throw new TypeError('start must be a type of PathState');

	if (!_.isArray(goals)) goals = [goals];
	if (!goals.length) throw new RangeError('must have at least one goal');
	goals.forEach((goal) => {
		if (!(goal instanceof PathState)) throw new TypeError('all goals must be a type of PathState');
	});

	// the current set of paths
	const frontier = astar.units.frontier();
	frontier.enq(await (new Path([start], [], goals)).getReady());
	const hasVisited = astar.units.visited();
	hasVisited(frontier.peek());

	// TODO can we save the goal->start vertex maps; this will improve Path.last.distance(goal)
	//  - I mean, this is a value-less set of matches, so it should match all possible configurations
	//  - that's the point of the distance; it finds the closest vertexMap possibility
	//  - so if we don't need to generate the map every time for something that doesn't change, that'd be great
	// ----
	//  - print out the vertex map from lm-wumpus to confirm (but it should be a hard proof, not just counterexample-less)

	// how many paths have we compared to the goal
	// (used to end early if we don't find anything)
	let numPathsExpanded = 0;

	while (!frontier.isEmpty()) {
		if (astar.boundaries.debugLogs) {
			// eslint-disable-next-line no-underscore-dangle
			astar.boundaries.log('frontier weights', _.sortBy(frontier._elements.map((p) => astar.units.calcWeight(p)), _.identity));
		}

		const path = frontier.deq();
		if (astar.boundaries.debugLogs) astar.boundaries.printActions(path);

		// do we win?
		if (path.$goalsMet) {
			if (astar.boundaries.debugLogs) astar.boundaries.finalReport('Found solution', numPathsExpanded, frontier);
			return path;
		}

		// exit early?
		numPathsExpanded += 1;
		if (numPathsExpanded > config.settings.astar_max_paths) {
			if (astar.boundaries.debugLogs) astar.boundaries.finalReport('Did not find solution, too many branches', numPathsExpanded, frontier);
			return undefined;
		}

		const beforeStepFrontierSize = frontier.size();
		await astar.units.step(path, frontier, hasVisited); // eslint-disable-line no-await-in-loop
		if (astar.boundaries.debugLogs) astar.boundaries.log(`step added ${frontier.size() - beforeStepFrontierSize} paths`);
	}

	if (astar.boundaries.debugLogs) astar.boundaries.finalReport('Did not find solution, exhausted options', numPathsExpanded, frontier);
	return undefined;
};

// REVIEW this is a utility function, it's ~~a bad~~ ~~an ok~~ a good-enough-for-now one
//  - definately come back to this
astar.units.calcWeight = function calcWeight(path) {
	const { astar_weight_cost: costWeight, astar_weight_distFromGoal: distWeight } = config.settings;
	return path.cost * costWeight + path.distFromGoal * distWeight;
};

// create a priority queue to store the current plans
astar.units.frontier = function frontier() {
	return new PriorityQueue((a, b) => {
		if (!a.isReady() || !b.isReady()) throw new Error('path is not ready');

		const ret = astar.units.calcWeight(b) - astar.units.calcWeight(a);
		if (ret !== 0) return ret;

		// if the composite is the same, then the one with the shorter distance wins
		// (the higher cost is permissible)
		// this is evident in how we value the distance from the goal more than the cost in the calc above
		return (b.distFromGoal - a.distFromGoal) * config.settings.astar_weight_distFromGoal;
	});
};

/*
	before we enque into the fronteir, filter out the visited states that are longer than previous ones

	equality is a stricter check than matches
	matches can be subbed for "distance === 0"
	therefore, if something is going to be equal, the distance must be the same
	----
	to make this faster, we can check only things that have the same distance
*/
astar.units.visited = function visited() {
	const hasVisitedByDist = new Map();
	return function hasVisited(p) {
		if (!p.isReady()) throw new Error('Path should be ready before you check visited/enq');
		const pathState = p.last;
		const pathWeight = astar.units.calcWeight(p);

		const pathDist = p.distFromGoal;
		let hasVisitedList = hasVisitedByDist.get(pathDist);
		if (!hasVisitedList) {
			hasVisitedList = [];
			hasVisitedByDist.set(pathDist, hasVisitedList);
		}

		const found = hasVisitedList.map(([state, weight]) => {
			const pathStateMatches = pathState.equals(state);

			// if the path state doesn't match, then we can use this state
			if (!pathStateMatches) return false;

			// if the weight is smaller, then we want to use it anyway (we found a better way to get here)
			if (pathWeight < weight) return false;

			return true;
		});

		if (found.length === 0 || found.every((f) => !f)) {
			hasVisitedList.push([pathState, pathWeight]);
			return false;
		}

		return true;
	};
};

// apply all of the available actions to the selected path
astar.units.step = async function step(path, frontier, hasVisited) {
	let nextActions = await path.last.actions();

	const immediateStubs = [];
	nextActions = nextActions.filter((next) => {
		if (next instanceof StubAction && next.solveAt === 'immediate') {
			// TODO StubAction.solveAt === 'immediate'
			throw new Error('StubAction.solveAt === immediate not yet implemented');
			/*
			const curr = stub.createStates(path.last, next.action, next.glue, next.action.runAbstract(path.last, next.glue));

			const action = planner.create(curr.start, curr.goal);
			if (action) {
				// save our transitions
				if (action instanceof serialplan.Action) {
					Array.prototype.push.apply(action.transitions, next.action.transitions);
					action.requirements = next.action.requirements;
				}

				// populate the list of nextActions from this action instead of the stub
				Array.prototype.push.apply(immediateStubs,
					new blueprint.State(path.last.state, [action]).actions());
			}

			// don't use the stub
			// instead, we'll append the immediateStubs list after the filter
			return false;
			*/

			/*
			// re-plan this step
			// XXX don't use astar.boundaries - ...but this is a continuation of the search
			if (astar.boundaries.debugLogs) astar.boundaries.log('re-plan', a.prettyNames());

			const startAndGoal = createStartAndGoal(path.states[idx], a, path.states[idx + 1]);

			const result = await createSingle(startAndGoal.start, startAndGoal.goal);
			if (result.action === undefined) return undefined;
			if (result.action instanceof SerialAction) {
				result.action.requirements = a.requirements;
			}
			path.states[idx + 1].state = result.state.state;
			return result.action;
			*/
		}

		return true;
	});
	Array.prototype.push.apply(nextActions, immediateStubs);

	return Promise.all(nextActions.map((next) => (
		next.runAbstract(path.last).then((state) => (
			path.add(state, next)
		)).then((p) => {
			if (astar.units.calcWeight(p) !== Infinity) {
				if (!hasVisited(p)) {
					// TODO if the last state isn't in the list OR it's smaller than the one that is
					//  ... that's a HUGE cost tho
					frontier.enq(p);
					if (astar.boundaries.debugLogs) astar.boundaries.log('frontier.enq', p.actions.length, _.last(p.actions).prettyNames());
				}
			}
		})
	)));
};

/*
	BOUNDARIES
*/

Object.defineProperty(astar, 'boundaries', { value: {} });

// this is meant for debugging
// it's left here as an example, but shouldn't be used normally
astar.boundaries.printActions = (path) => {
	astar.boundaries.log('selected weight, actions', astar.units.calcWeight(path), path.actions.map((a) => a.prettyNames()));
};

astar.boundaries.finalReport = (message, numPathsExpanded, frontier) => {
	astar.boundaries.log(`${message} (paths expanded: ${numPathsExpanded}, frontier: ${frontier.size()}).`);
};

astar.boundaries.debugLogs = false;

// XXX should we be using a logger (like log4js?)
//  - we don't want this in `boundaries.js` because we wouldn't be able to control the different sections/files independently, it'd be all or nothing
//  - this is the first time I've wanted to use the console to produce trace statements, so I don't know the right approach yet
astar.boundaries.log = (...args) => console.log(...args); // eslint-disable-line no-console
