/* eslint-disable max-classes-per-file, no-void */
const _ = require('lodash');

const path = exports;

class Path {
	/**
		sort of a standalone plan of getting from A -> B
		typically, you start with an empty Path (`new Path([start], [], goals)`) and then add actions until you reach one of the goals
		or you might have a finished path, saying "if you start here, and take these actions, you'll get to this goal"

		@param {Array<PathState>} states - (1 or more) different snapshots of the world state as we apply actions
		@param {Array<PathAction>} actions - (0 or more) how to move from one state to the next
		@param {PathState|Array<PathState>} goals - what this path will ultimately produce
		@param {Array<number>} cost - how much does this Path cost to run it? the sum of all action costs
		@param {number|Array<number>} distance - if you already know the distance(s) from goal(s), you can pass them in; otherwise, they need to be computed
	*/
	constructor(states, actions, goals, cost = undefined, distance = undefined) {
		// if (states.length !== actions.length + 1) throw new RangeError('`states` must be one longer than `actions`.');

		if (!_.isArray(goals)) goals = [goals];

		this.states = states;
		this.actions = actions;
		this.goals = goals;
		this.$goalsMet = null;
		this.cost = (actions.length ? cost : 0);

		if (_.isArray(distance)) {
			this.distFromGoal = _.min(distance);
		}
		else {
			this.distFromGoal = distance;
		}
	}

	get last() {
		const { states } = this;
		return states[states.length - 1];
	}

	/** @return {Promise<number>} */
	async $calcCost() {
		const { actions, states } = this;
		this.cost = await Promise.all(actions.map((action, idx) => action.cost(states[idx], states[idx + 1])))
			.then((cost) => _.sum(cost));
		return this.cost;
	}

	/** @return {Promise<number>} */
	async $calcDistFromGoal() {
		const { goals, last } = this;
		this.distFromGoal = await Promise.all(goals.map((g) => last.distance(g))).then((distances) => {
			const min = _.min(distances);
			// if we haven't met any goals, just return the minimum now
			if (min > 0) {
				return min;
			}

			// if any of them distances are zero, then check the matches function, too
			// XXX this may be unnecessary, but that's what it's here for
			//  - alternatively, we could skip that, and just use the distance values
			// REVIEW the distance ought never be less than zero - if it is, do we make it Infinity instead?
			return Promise.all(distances.map((d, i) => (d > 0 ? null : last.matches(goals[i]))))
				.then((met) => { this.$goalsMet = met.map((m, i) => (m ? goals[i] : null)).filter((m) => !!m); })
				.then(() => min);
		});
		return this.distFromGoal;
	}

	/** @return {boolean} false if the async values have not been computed */
	isReady() {
		return this.cost !== undefined && this.distFromGoal !== undefined;
	}

	/** @return {Promise<undefined>} resolves when all the async setup is done */
	async getReady() {
		return Promise.all([
			(this.cost === undefined ? this.$calcCost() : null),
			(this.distFromGoal === undefined ? this.$calcDistFromGoal() : null),
		]).then(() => this);
	}

	/**
	 	adds another state and action to the existing path

		@param {PathState} state
		@param {PathAction} action
		@return {Path} - a new path; a clone of this with the additional state/action
	*/
	async add(state, action) {
		const nextStepCost = await action.cost(this.last, state); // cost is cumulative, so we can simply add the next step to it
		return new Path(
			this.states.concat(state),
			this.actions.concat(action),
			this.goals,
			this.cost + nextStepCost,
			// distance is against the last state and all goals, so we need to redo that no matter what
		).getReady();
	}
}

/**
	@abstract
	@example `/example/planning/primitives/NumberSlide.js`
*/
class PathAction {
	/**
		for human consumption
		if ever we need to print out the "list of actions", we want to be able to read it
		each type of action needs different information, and might have different useful data

		@return {string|Array<any>|any}
	*/
	prettyNames() {
		throw new Error(`${this.constructor.name} must implement PathAction.prettyNames`);
	}

	/**
		how much does it cost to go from `from` to `to`
		this isn't just the distance, this is the effort of the action
		(e.g. gain an apple from store A vs store B)

		REVIEW maybe we can remove from/to from PathAction.cost - implement BlueprintAction and see how it goes

		@param {PathState} from
		@param {PathState} to
		@return {Promise<number>} MUST be greater than zero, no action has a cost of 0 or less
	*/
	async cost(from, to) {
		void from;
		void to;
		throw new Error(`${this.constructor.name} must implement PathAction.cost`);
	}

	/**
		apply the action in a theoretical sense
		this will create a new state, but not actually interact with the world.

		@alias apply
		@param {PathState} from - this action will be applied to this state, and produce a new state
		@return {Promise<PathState>}
	*/
	async runAbstract(from) {
		void from;
		throw new Error(`${this.constructor.name} must implement PathAction.runAbstract`);
	}
}

/**
	@abstract
	@example `/example/planning/primitives/NumberSlide.js`
*/
class PathState {
	constructor() {
		this.$actions = null; // compute the possible actions the first time we need them
	}

	/**
		estimate the distance to the next state
		since this is used as an A* heuristic, it's best to undershoot (get close, but air on the side of caution)
		the lowest distances will bubble to the top immediately (if dist equals zero); that means we met the goal and can just stop

		@param {PathState} to
		@return {Promise<number>} 0 if `this == to`, otherwise greater than zero
	*/
	async distance(to) {
		void to;
		throw new Error(`${this.constructor.name} must implement PathState.distance`);
	}

	/**
		returns actions can be taken from this state

		@return {Promise<Array<PathAction>>} an array of all the possible actions from this state
	*/
	async actions() {
		if (!this.$actions) {
			this.$actions = await this.$calcActions();
		}

		return Promise.resolve(this.$actions);
	}

	/**
		it's universal that our actions will be lazy loaded from this given state
		this just needs to compute them, PathState.action will call this as needed

		this function has a `$` prefix because it's supposed to be a private method (nothing outside of `path` should use it)
		but child classes DO need to implement it

		@return {Promise<Array<PathAction>>} an array of all the possible actions from this state
	*/
	async $calcActions() {
		throw new Error(`${this.constructor.name} must implement PathState.$calcActions`);
	}

	/**
		e.g. `if (state.matches(goal)) throw 'you win!';
		this should always be overriden because checking a match directly will always be faster than computing the distance

		@param {PathState} state
		@return {Promise<boolean>}
	*/
	async matches(state) {
		// this REALLY REALLY REALLY SHOULD be overriden
		// but this impl is technically valid
		return (await this.distance(state) === 0);
	}

	/**
		matches is a loose comparision, this is an exact comparison
		normally we want to know "do all the matchers agree"
		this is a stricter comparison for de-duping states

		we may have a matcher that says "any of these locations is valid" or "between 2 and 5 items is valid"
		but here we want to know "which location" and "how many exactly"
		satisfying goals is loose, but the route to get there may be more subtle

		use `matches` for "including options" (does this match the goal)
		use `exact` for "excluding options" (should we remove this option)

		@param {PathState} state
		@return {boolean}
	*/
	equals(state) {
		void state;
		throw new Error(`${this.constructor.name} must implement PathState.equals`);
	}
}

path.Path = Path;
path.PathAction = PathAction;
path.PathState = PathState;
