// this represents how much there is of something
// e.g. '13.5 meters' or '22.3124 psi'
// all numbers are a range of possible values
const ideas = require('../../database/ideas');

/**
	numbers are ALWAYS ranges
	this impl is min/max
	they are still "concrete values" (no error bars), it's just that it allows for fuzzy matching
	e.g. "greater than 0" is "(0, ∞)", greater than or equal to zero is "[0, ∞)"
	it can say, between 2 and 5 is okay "[2, 5]"
	sometimes you always want exactly one thing "[1]"
	sometimes you want a handful of things "(2, 10)"

	this is not a "precise value" that has a value / error bars (a mean + delta or mean + std deviation)
	that is another type of number that might be worth implementing
*/
const numbers = exports;
const TYPE_NAME = 'lime_numbers';

/*
	obj: {
		value: {
			bl, br: is the number bounded left or right? true = '[', false = '('
			l, r: the min/max of the value
		}
		$unit: idea.id
		$type: (auto assigned)
	}
*/
numbers.isNumber = (obj) => numbers.units.isNumber(obj);

numbers.cast = (obj) => numbers.units.cast(obj);

numbers.match = (n1, n2) => numbers.units.match(n1, n2);

/**
	construct a value object (for ease)
	(see the tests for examples)

	@param {number} l
	@param {number} r -  (optional) if this is a range
	@param {boolean} bl - (optional) bounded left
	@param {boolean} br - (optional) bounded right
*/
numbers.value = (l, r = l, bl = true, br = true) => numbers.units.value(l, r, bl, br);

numbers.combine = (n1, n2) => numbers.units.combine(n1, n2);

numbers.remove = (n1, n2) => numbers.units.remove(n1, n2);

numbers.difference = async (n1, n2) => numbers.units.difference(n1, n2);

/*
	UNITS
*/

Object.defineProperty(numbers, 'units', { value: {} });

function isValue(value) {
	if (!(value
		&& typeof value.bl === 'boolean'
		&& (typeof value.l === 'number' || value.l === null)
		&& (typeof value.r === 'number' || value.r === null)
		&& typeof value.br === 'boolean'
		&& (value.l === null || value.r === null || value.l <= value.r)
	)) {
		return false;
	}
	return true;
}

/**
	isNumber is self-correcting

	if obj looks like a number
*/
numbers.units.isNumber = function isNumber(obj) {
	if (typeof obj !== 'object') {
		return false;
	}

	if (!(
		(!obj.$type || obj.$type === TYPE_NAME)
		&& typeof obj.$unit === 'string'
		&& typeof obj.value === 'object'
		&& isValue(obj.value)
	)) {
		return false;
	}

	if (obj.value.l === null || obj.value.l === -Infinity) {
		if (obj.value.bl === true) {
			return false;
		}
	}
	if (obj.value.r === null || obj.value.r === Infinity) {
		if (obj.value.br === true) {
			return false;
		}
	}

	if (obj.value.l === null) {
		obj.value.l = -Infinity;
	}
	if (obj.value.r === null) {
		obj.value.r = Infinity;
	}

	if (obj.value.l === obj.value.r && (!obj.value.bl || !obj.value.br)) {
		return false;
	}

	obj.$type = TYPE_NAME;

	return true;
};

numbers.units.cast = function cast(obj) {
	if (numbers.units.isNumber(obj)) {
		return obj;
	}
	return undefined;
};

numbers.units.match = function match(n1, n2) {
	if (!numbers.units.isNumber(n1) || !numbers.units.isNumber(n2)) {
		return false;
	}
	if (n1.$unit !== n2.$unit) {
		return false;
	}

	return true;
};

numbers.units.value = function value(l, r = l, bl = true, br = bl) {
	if (l === null || l === undefined) l = -Infinity;
	if (r === null || r === undefined) r = Infinity;
	if (l === -Infinity) bl = false;
	if (r === Infinity) br = false;

	const val = { bl, l, r, br };

	if (!isValue(val)) {
		return undefined;
	}

	return val;
};

/**
	add these two values
	n1 + n2
*/
numbers.units.combine = function combine(n1, n2) {
	if (!numbers.units.match(n1, n2)) {
		return undefined;
	}

	const l = n1.value.l + n2.value.l;
	const r = n1.value.r + n2.value.r;

	return {
		$type: TYPE_NAME,
		value: {
			bl: ((n1.value.bl && n2.value.bl) || l === r),
			l,
			r,
			br: ((n1.value.br && n2.value.br) || l === r),
		},
		$unit: n1.$unit,
	};
};

/**
	subtract these two values
	n1 - n2
*/
numbers.units.remove = function remove(n1, n2) {
	if (!numbers.units.match(n1, n2)) {
		return undefined;
	}

	const l = n1.value.l - n2.value.r;
	const r = n1.value.r - n2.value.l;

	return {
		$type: TYPE_NAME,
		value: {
			bl: ((n1.value.bl && n2.value.br) || l === r),
			l,
			r,
			br: ((n1.value.br && n2.value.bl) || l === r),
		},
		$unit: n1.$unit,
	};
};

/**
	abs(n1 - n2)

	REVIEW should we use the mean of n2 instead of l,r ?
	 - this means n2 is a fixed value (average) we are checking, rather than the whole range

	@return {Promise<number|undefined>}
*/
numbers.units.difference = async function difference(n1, n2) {
	if (!numbers.units.match(n1, n2)) {
		return undefined;
	}

	const scale = await numbers.units.getScale(n1.$unit);

	// if n2 is entirely larger than n1
	if (n1.value.r < n2.value.l) {
		return (n2.value.l - n1.value.r) * scale;
	}

	// if n1 is entirely larger than n2
	if (n2.value.r < n1.value.l) {
		return (n1.value.l - n2.value.r) * scale;
	}

	// the effective difference is zero
	return 0;
};

/**
	@param {string} $unit - idea.id
	@return {Promise<number>}
*/
numbers.units.getScale = async function getScale($unit) {
	const data = await numbers.boundaries.loadUnitData($unit);
	let scale = 1;
	if (data && Object.prototype.hasOwnProperty.call(data, 'scale')) {
		scale = +data.scale;
	}
	return scale;
};

/*
	BOUNDARIES
*/

Object.defineProperty(numbers, 'boundaries', { value: {} });

/**
	@param {string} $unit - idea.id
	@return {Promise<JSON>}
*/
numbers.boundaries.loadUnitData = ($unit) => ideas.proxy($unit).getData();
