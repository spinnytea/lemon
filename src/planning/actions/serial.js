const _ = require('lodash');
const config = require('../../config');
const blueprint = require('../primitives/blueprint');
// const scheduler = require('../scheduler');

const { BlueprintAction } = blueprint;

const serial = exports;

/**
	this builds on a blueprint to represent a set of serial steps
	actuator is a single (concrete) step, then this is a list of steps

	you shouldn't need to use `new SerialAction` directly - instead, you can use `planner.create`
*/
class SerialAction extends BlueprintAction {
	/**
		@param {Array<BlueprintAction>} steps - the steps that make up this macro action
	*/
	constructor({ steps, ...rest }) {
		super(rest);

		this.steps = steps;
		// XXX should we remove empty serial steps?
		//  - this hasn't come up in tests, will it actually happen?
		//  - there are empty serial plans, but not yet serial plans within serial plans
		// .filter((p) => !(p instanceof SerialAction && p.steps.length === 0));

		// if there are steps in the list, my requirements are just the requirements for the first action
		// REVIEW can we disallow empty SerialAction? ApplePie doesn't encounter it
		if (steps.length > 0) {
			this.requirements = steps[0].requirements;
		}

		// transitions aren't used / can't be used by these actions, so let's not pretend
		// no empty transition array, just don't use it at all
		// (we can't "simply" combine all the transitions, because the vertexIds are dependent on the requirements)
		this.transitions = undefined;

		// when we calc/glue the start, we've already computed the end
		// save it so we can just return it for abstract planning
		this.$gluedFinalState = undefined;
	}

	/**
		@override
	*/
	prettyNames() {
		if (this.steps.length === 0) {
			return 'SerialAction (empty)';
		}
		return { 'SerialAction ->': this.steps.map((p) => p.prettyNames()) };
	}

	/**
		add the cost of all the steps
		if there is no cost (~no steps), then add a deterrent cost

		@override
		@return {Promise<number>} MUST be greater than zero, no action has a cost of 0 or less
	*/
	async $calcRunCost() {
		if (this.steps.length === 0) {
			return Promise.resolve(config.settings.serial_empty_distance);
		}

		return Promise.all(this.steps.map((p) => p.$calcRunCost()))
			.then((runCosts) => _.sum(runCosts));
	}

	/**
		Produce a copy SerialAction so that we can call `action.runAbstract` or `action.runActual`

		return [
		  new SerialAction(1a -> 2a)
		  new SerialAction(1a -> 2b)
		  new SerialAction(1a -> 2c)
		  new SerialAction(1b -> 2a)
		  new SerialAction(1b -> 2b)
		  new SerialAction(1b -> 2c)
		]

		@alias tryTransition
		@override
		@param {BlueprintState} state
		@return {Promise<Array<SerialAction>>} - an array of isGlued SerialAction (meant for runAbstract, runActual, scheduleActual)
	*/
	async calcGlue(state) {
		if (this.steps.length === 0) {
			return [];
		}

		// seed with state, empty list of glues
		// grow all the branches a list of paths through the match spaces
		// `[{ glues: [], state }]`
		// will become
		// ```
		// [
		//   { glues: ['a1', 'b1', 'c1'], state: final },
		//   { glues: ['a1', 'b1', 'c2'], state: final },
		//   { glues: ['a1', 'b2', 'c1'], state: final },
		//   { glues: ['a1', 'b2', 'c2'], state: final },
		//   { glues: ['a1', 'b1', 'c1'], state: final },
		//   { glues: ['a1', 'b1', 'c2'], state: final },
		//   { glues: ['a1', 'b2', 'c1'], state: final },
		//   { glues: ['a1', 'b2', 'c2'], state: final },
		// ]
		// ```
		let currList = [{ glues: [], state }];

		// XXX this is a breadth-first algorithm (do each layer one at a time)
		//  - is this more readable than a recursive/depth-first version?
		//  - eslint says "no-await-in-loop", but is the alternative any better?
		for (let i = 0; i < this.steps.length; i += 1) {
			const nextList = [];
			const action = this.steps[i];

			// eslint-disable-next-line no-await-in-loop
			await Promise.all(currList.map(async (curr) => {
				// noop
				// since empty SerialAction is allowed we need to handle it here
				// the default case loops over results, we need to append "no changes"
				// we can reuse curr since we know there is only one branch forward
				if (action instanceof SerialAction && action.steps.length === 0) {
					curr.glues.push([]);
					nextList.push(curr);
				}
				else {
					await Promise.all((await action.calcGlue(curr.state)).map(async (gluedAction) => (
						gluedAction.runAbstract(curr.state).then((nextState) => {
							nextList.push({ glues: curr.glues.concat(gluedAction), state: nextState });
						})
					)));
				}
			}));

			// swap curr and next
			currList = nextList;
		}

		// XXX turn our last state into a goal
		//  - we need to compute this goal for `scheduleActual` so we know when each step is finished
		// @see original
		// @see actuator

		return currList.map(({ glues, state: final }) => {
			const copy = new SerialAction(this);
			copy.$glue = glues;
			copy.$gluedState = state;
			copy.$gluedFinalState = final;
			return copy;
		});
	}

	/**
		@alias apply
		@override
		@param {BlueprintState} from - this action will be applied to this state, and produce a new state
		@return {Promise<BlueprintState>}
	*/
	async runAbstract(from) {
		if (!this.isGlued()) throw new Error('SerialAction has not been glued');
		if (from !== this.$gluedState) throw new Error('SerialAction was glued to a different state');
		if (!this.$gluedFinalState) throw new Error('SerialAction gluedAction.$gluedFinalState is missing?');

		// since we already had to runAbstract and computed this result
		// since we know from === glueState, it's going to be the same thing
		// just use the result
		return Promise.resolve(this.$gluedFinalState);
	}

	/**
		@alias runBlueprint
		@override
		@param {Subgraph} context - (will be modified)
		@return {Promise}
	*/
	async runActual(context) {
		if (!this.isGlued()) throw new Error('SerialAction has not been glued');

		return this.$glue.reduce((nextStatePromise, gluedAction) => (
			nextStatePromise.then(() => gluedAction.runActual(context))
		), Promise.resolve());
	}

	/**
		@alias scheduleBlueprint
		@param {Subgraph} context - (will be modified)
		@return {Promise} rejects if the plan ultimately fails; resolves when the goal has been met
	*/
	async scheduleActual(context) {
		if (!this.isGlued()) throw new Error('ActuatorAction has not been glued');

		// calcGlue turn our last state into a goal
		void context; // eslint-disable-line no-void

		throw new Error('not implemented yet - need a scheduler');
	}

	/**
		@override
		@return {Promise<JSON>} actionJson
	*/
	async $stringifyAction() {
		return {
			idea: this.idea,
			requirements: this.requirements.stringifySubgraph(),
			// transitions: this.transitions, // SerialAction is weird in that it doesn't use transitions; it's children do
			// it feels weird that any plan would have to be "saved" before we can use it in our stringified
			// but it's probably fine in the context of "this is only called when we are going to save the top action"
			// so why not also save the sub-actions, it makes them independently reusable
			steps: await Promise.all(this.steps.map((action) => action.save())),
		};
	}
}

serial.SerialAction = SerialAction;

/*
	UNITS
*/

Object.defineProperty(serial, 'units', { value: {} });

/*
	INIT
*/

config.onInit((first) => {
	if (first) {
		blueprint.createActionParser('SerialAction', async (actionJson) => (
			new SerialAction({
				...actionJson,
				steps: await Promise.all(actionJson.steps.map((idea) => blueprint.load(idea))),
			})
		));
	}
});
