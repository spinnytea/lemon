const _ = require('lodash');
const config = require('../../config');
const { match, rewrite, convertInnerTransitions } = require('../../database/subgraphs');
const blueprint = require('../primitives/blueprint');
// const scheduler = require('../scheduler');

const { BlueprintAction, BlueprintState } = blueprint;

const actuator = exports;

/**
	actuators represent a single action that will affect the world
	this is what we will use to update our internal thought graph
	when we create this class, we still need a callback that will affect the actual world
	 - (e.g. the "turn motor" function)

	If you want this action to interact with the world,
	you need to statically register a function with actuator.rawCallbacks
	and then set the a.rawCallback to the name of that action
	(this is how we get an outside callback to persist across save and load)
*/
class ActuatorAction extends BlueprintAction {
	/**
		@param {string} rawCallback - the name of the action to use (@alias action)
	*/
	constructor({ rawCallback = null, rawCallbackArgs = [], ...rest }) {
		super(rest);
		this.rawCallback = rawCallback;
		this.rawCallbackArgs = rawCallbackArgs;
	}

	/**
		@override
	*/
	prettyNames() {
		return `ActuatorAction -> ${this.rawCallback}`;
	}

	/**
		TODO actuator run cost needs to be influence by the weight of the actions
		 - sand != diamond
		 - 3 oz of Carbon Fiber vs 2 lbs of Cereal
		 - (is that baked into transitions, or is that something we associate with the idea that the transition is affecting)
		 - idea weight: find the vertex the transition effects, load the idea, find it's weight (not unlike numbers.scale)

		@override
		@return {Promise<number>} MUST be greater than zero, no action has a cost of 0 or less
	*/
	async $calcRunCost() {
		if (this.transitions.length === 0) {
			return Infinity;
		}

		const cost = this.transitions.reduce((sum, t) => {
			if ('cost' in t) {
				// REVIEW do we need to ensure these all have costs? is it safe to use a fallback?
				return sum + t.cost;
			}
			return sum + config.settings.actuator_base_distance;
		}, 0);

		return Promise.resolve(cost); // XXX this doesn't need to be a promise, but $calcRunCost this will be async
	}

	/**
		requirements are inner
		all the requirements must have a representation in the state
		(the entire state does not need to be contained within the requirements)
		by the transition vertexId referrs to requirements graph vertexId

		@alias tryTransition
		@override
		@param {BlueprintState} state
		@return {Promise<Array<ActuatorAction>>} - an array of isGlued ActuatorAction (meant for runAbstract, runActual, scheduleActual)
	*/
	async calcGlue(state) {
		return (await match(state.state, this.requirements, { unitsOnly: false })).map((glue) => {
			const copy = new ActuatorAction(this);
			copy.$glue = glue;
			copy.$gluedState = state;
			return copy;
		});
	}

	/**
		@alias apply
		@override
		@param {BlueprintState} from - this action will be applied to this state, and produce a new state
		@return {Promise<BlueprintState>}
	*/
	async runAbstract(from) {
		if (!this.isGlued()) throw new Error('ActuatorAction has not been glued');
		if (from !== this.$gluedState) throw new Error('ActuatorAction was glued to a different state');

		const ts = convertInnerTransitions(this.requirements, this.transitions, this.$glue);
		const nextState = await rewrite(from.state, ts, false);

		return new BlueprintState(nextState, from.availableActions);
	}

	/**
		@alias runBlueprint
		@override
		@param {Subgraph} context - (will be modified)
		@return {Promise}
	*/
	async runActual(context) {
		if (!this.isGlued()) throw new Error('ActuatorAction has not been glued');

		const ts = convertInnerTransitions(this.requirements, this.transitions, this.$glue);

		// interact with the world
		if (this.rawCallback) {
			if (this.rawCallback in actuator.rawCallbacks) {
				const args = await Promise.all(this.rawCallbackArgs.map((name) => (
					context.getData(this.$glue.innerToOuterVertexMap.get(this.requirements.$refVertex(name)))
				))).then((data) => _.zipObject(this.rawCallbackArgs, data));
				await Promise.resolve(actuator.rawCallbacks[this.rawCallback](args));
			}
			else {
				actuator.boundaries.warn(`called ActuatorAction.runActual, rawCallback "${this.rawCallback}" is not specified`);
			}
		}
		else {
			actuator.boundaries.warn('called ActuatorAction.runActual without a rawCallback defined');
		}

		// predict the outcome (update what we thing is true)
		// apply the action through to the thought graph
		const result = await rewrite(context, ts, true);
		if (result === undefined) {
			throw new Error('rewrite failed');
		}
	}

	/**
		@alias scheduleBlueprint
		@param {Subgraph} context - (will be modified)
		@return {Promise} rejects if the plan ultimately fails; resolves when the goal has been met
	*/
	async scheduleActual(context) {
		if (!this.isGlued()) throw new Error('ActuatorAction has not been glued');

		void context; // eslint-disable-line no-void

		/*
		// create a goal based on our requirements
		var goal = subgraph.createGoal(from.state, this.requirements, glue);
		// update our goal to reflect the value we expect
		goal = subgraph.rewrite(goal, this.transitions, false);

		// since the first action runs immediately, there isn't any reason for us to delay
		this.runBlueprint(from, glue);

		// wait for our goal
		return scheduler.defer(from.state, goal);
		*/

		throw new Error('not implemented yet - need a scheduler');
	}

	/**
		@override
		@return {Promise<JSON>} actionJson
	*/
	async $stringifyAction() {
		return {
			idea: this.idea,
			requirements: this.requirements.stringifySubgraph(),
			transitions: this.transitions,
			rawCallback: this.rawCallback,
			rawCallbackArgs: this.rawCallbackArgs,
		};
	}
}

actuator.ActuatorAction = ActuatorAction;

// due to serialization of javascript objects...
// all action impls must be registered here
actuator.rawCallbacks = {};

/*
	BOUNDARIES
*/

Object.defineProperty(actuator, 'boundaries', { value: {} });

actuator.boundaries.debugLogs = false;

// XXX should we be using a logger (like log4js?)
//  - we don't want this in `boundaries.js` because we wouldn't be able to control the different sections/files independently, it'd be all or nothing
//  - this is the first time I've wanted to use the console to produce trace statements, so I don't know the right approach yet
actuator.boundaries.warn = (...args) => console.warn(...args); // eslint-disable-line no-console

/*
	INIT
*/

config.onInit((first) => {
	if (first) {
		blueprint.createActionParser('ActuatorAction', async (actionJson) => (
			new ActuatorAction(actionJson)
		));
	}
});
