const config = require('../../config');
const { match, rewrite, convertInnerTransitions } = require('../../database/subgraphs');
const blueprint = require('../primitives/blueprint');

const { BlueprintAction, BlueprintState } = blueprint;

const stub = exports;

/**
	This is an "abstract" action, but it is not an "abstract class".
	This is meant to be used during planning, and can be resolved into a concrete plan sometime later.
	we plan with it (abstract) but it doesn't (actually) DO anything

	A StubAction has neither `runActual` or `scheduleActual` because it is not an actual thing that can run directly.
	And we can't "pre-approve" an action if it isn't resolved until runtime; plans must be resolved _before_ they are carried out.
*/
class StubAction extends BlueprintAction {
	/**
		@param {string} solveAt - when should this StubAction be resolved into a different Path?
	*/
	constructor({ solveAt, ...rest }) {
		super(rest);

		if (!stub.units.solveAt.includes(solveAt)) {
			// this is because the stubs aren't used directly
			// each well known solveAt has an implementation somewhere in the source
			throw new Error(`unknown solveAt: "${solveAt}"`);
		}

		this.solveAt = solveAt;
	}

	/**
		@override
	*/
	prettyNames() {
		return `StubAction (${this.solveAt})`;
	}

	/**
		@see ActuatorAction - same impl
		@override
		@return {Promise<number>} MUST be greater than zero, no action has a cost of 0 or less
	*/
	async $calcRunCost() {
		if (this.transitions.length === 0) {
			return Infinity;
		}

		const cost = this.transitions.reduce((sum, t) => {
			if ('cost' in t) {
				// REVIEW do we need to ensure these all have costs? is it safe to use a fallback?
				return sum + t.cost;
			}
			return sum + config.settings.stub_base_distance;
		}, 0);

		return Promise.resolve(cost); // XXX this doesn't need to be a promise, but $calcRunCost this will be async
	}

	/**
		@see ActuatorAction - same impl
		@alias tryTransition
		@override
		@param {BlueprintState} state
		@return {Promise<Array<StubAction>>} - an array of isGlued StubAction (meant for runAbstract)
	*/
	async calcGlue(state) {
		return (await match(state.state, this.requirements, { unitsOnly: false })).map((glue) => {
			const copy = new StubAction(this);
			copy.$glue = glue;
			copy.$gluedState = state;
			return copy;
		});
	}

	/**
		@see ActuatorAction - same impl
		@alias apply
		@override
		@param {BlueprintState} from - this action will be applied to this state, and produce a new state
		@return {Promise<BlueprintState>}
	*/
	async runAbstract(from) {
		if (!this.isGlued()) throw new Error('StubAction has not been glued');
		if (from !== this.$gluedState) throw new Error('StubAction was glued to a different state');

		const ts = convertInnerTransitions(this.requirements, this.transitions, this.$glue);
		const nextState = await rewrite(from.state, ts, false);

		return new BlueprintState(nextState, from.availableActions);
	}

	/**
		@override
		@return {Promise<JSON>} actionJson
	*/
	async $stringifyAction() {
		return {
			idea: this.idea,
			requirements: this.requirements.stringifySubgraph(),
			transitions: this.transitions,
			solveAt: this.solveAt,
		};
	}
}

stub.StubAction = StubAction;

/*
	UNITS
*/

Object.defineProperty(stub, 'units', { value: {} });

stub.units.solveAt = Object.freeze([
	/**
		this occurs during astar.units.step
		it's sort of like a greedy depth-first search
		it's like "here's a short term goal we can go for now, but we HAVE to do it right away"
		this is just a way to help manage the search space without actually deferring planning
	*/
	'immediate',

	/**
		this occurs during planner.create, after the whole plan has been constructed
		it's sort of like a greedy breadth-first search
		it's like "here's a short term goal we can acheive, work on the rest of the plan first, but this needs to be solved before we can select a plan"
	*/
	'create',
]);

/*
	INIT
*/

config.onInit((first) => {
	if (first) {
		blueprint.createActionParser('StubAction', async (actionJson) => (
			new StubAction(actionJson)
		));
	}
});
