/*
	utility functions
*/

const utils = exports;

/*
	super fast empty checking
	Object.keys(obj).length === 0 is terribly slow
*/
utils.isEmpty = function isEmpty(obj) {
	for (const key in obj) { // eslint-disable-line no-restricted-syntax
		/* istanbul ignore else */
		if (Object.prototype.hasOwnProperty.call(obj, key)) {
			return false;
		}
	}
	return true;
};

/*
	Map is design to go from key -> value
	…but what if you _need_ to go from value -> key om occasion?
	it might be better to make another map in the opposite direction, if you need it a lot
	but if it's just a one-off or something, this might be better
*/
utils.findByValue = function findByValue(map, comparator) {
	// eslint-disable-next-line no-restricted-syntax
	for (const [key, value] of map.entries()) {
		if (comparator(value)) {
			return key;
		}
	}
	return undefined;
};

/**
	chain promises together
	forces multiple calls to the same function to occur in serial
	this lets you call the same function multiple times, and guarantees that the first call finishes before the second call begins

	@example ids.next
	@param {Function} callback - the function that must run sequentially
*/
utils.transaction = function transaction(callback) {
	// single promise chain for all calls to callback
	let txnPromise = Promise.resolve();

	// return a promise so we can work with the result of a call as soon as it's ready
	// update the txnPromise so subsequent calls must wait for previous ones to finish
	// use the arguments of this function on the callback
	// (isn't it great that arrow functions don't create their own function scope)
	return (...args) => (txnPromise = txnPromise.then(() => callback(...args)));
};

/**
	when a function can be called multiple times and previous calls are canceled
	this will tie all of the outstanding promises to the same resolution
	this let's us call the same function multiple times, only actually do it once, and all the promises resolve when it does

	REVIEW this join is an anti-pattern; it requires a deferred instead of a promise
	- can we re-write this to be more elegant?

	@example config.save
	@param {*} callback
 */
utils.join = function join(callback) {
	const resolves = [];
	const rejects = [];

	function resolveAll(value) {
		resolves.forEach((r) => r(value));
		resolves.splice(0);
		rejects.splice(0);
		return value;
	}
	function rejectAll(value) {
		rejects.forEach((r) => r(value));
		resolves.splice(0);
		rejects.splice(0);
		return Promise.reject(value);
	}

	return (...args) => {
		const [promise, resolve, reject] = callback(...args);
		if (resolve) resolves.push(resolve);
		if (reject) rejects.push(reject);
		return promise.then((value) => resolveAll(value), (value) => rejectAll(value));
	};
};
