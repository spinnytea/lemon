/*
	link ideas together
	these are the edges of the graph, specifically the types of edges
	ideas take the lead role is storage, saving, and retrieval
*/
const _ = require('lodash');
const config = require('../config');

const links = exports;

/** get a type of link by name */
links.get = (name) => links.units.list[name];

// REVIEW do we even need a get and a cast? why not just validate before get?
/** validate the link before we return it */
links.cast = (name, opposite = false) => links.units.cast(name, opposite);

/** in case edges need to be normalized, this will tell you if this is an original or opposite (link vs link.opposite) */
links.isOpp = (link) => ((link?.name && link?.opposite?.name) ? link.name === `${link.opposite.name}-opp` : undefined);

/*
	UNITS
*/

Object.defineProperty(links, 'units', { value: {} });

links.units.list = {};

links.units.create = function create(name, { transitive = false, directed = true } = {}) {
	if (name in links.units.list) throw new Error(`link "${name}" is already defined`);

	const link = { name, opposite: undefined, transitive };

	if (directed) {
		link.opposite = Object.freeze({ name: `${name}-opp`, opposite: link, transitive });
		links.units.list[name] = Object.freeze(link);
		links.units.list[link.opposite.name] = link.opposite;
	}
	else {
		link.opposite = link;
		links.units.list[name] = Object.freeze(link);
	}
};

links.units.cast = function cast(name, opposite = false) {
	const link = (_.isString(name) ? links.units.list[name] : name);
	if (!link || !link.name || !links.units.list[link.name]) throw new TypeError(`"${name?.name || name}" is not a valid link`);
	if (opposite) return link.opposite;
	return link;
};

/*
	INIT
*/

config.onInit((first) => {
	if (first) {
		// apple
		//  macintosh --typeof_of-> apple
		//  gala --typeof_of-> apple
		links.units.create('typeOf', { transitive: true });

		// mark --typeof_of-> person
		// mark --has-> apple --has-> apple_stem
		// person --can_has-> apple
		links.units.create('has', { transitive: true });

		// appleInstance
		//  apple --property-> color
		//  apple --property-> dimensions
		links.units.create('property');

		// 'a' --sequence-> 'b'
		links.units.create('sequence');

		// helps identify when certain ideas are relevant
		//  idea --context-> ContextIdea
		links.units.create('context');

		// an equivalence relationship
		// white <-same_as-> blanc
		// blanc <-same_as-> shiroi
		// which also implies white <-> shiroi
		links.units.create('sameAs', { transitive: true, directed: false });

		// a link configuration that I can't think of a real life example for
		// if we can find one, then remove it
		// a <-_test__undirected_-> b
		// b <-_test__undirected_-> c
		// but this does not imply `a <-> c`
		links.units.create('_test__undirected_', { transitive: false, directed: false });
	}
});
