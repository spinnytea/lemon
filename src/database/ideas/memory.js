/*
	simple structure for keeping track of what's being held in memory

	maybe in the future it will get more complex as we try to auto-close things that haven't been used in a while
	or maybe that will become part of a "dream" state
*/
const config = require('../../config');

const memory = exports;

memory.add = (core) => memory.units.map.set(core.id, core);
memory.has = (proxy) => memory.units.map.has(proxy.id);
memory.get = (proxy) => memory.units.map.get(proxy.id);
memory.delete = (proxy) => memory.units.map.delete(proxy.id);

Object.defineProperty(memory, 'units', { value: {} });

/** ideas which have already been loaded into memory */
memory.units.map = new Map();

/*
	INIT
*/

config.onInit(() => {
	memory.units.map.clear();
});
