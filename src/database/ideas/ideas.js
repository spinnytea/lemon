const _ = require('lodash');
const config = require('../../config');
const ids = require('../../ids');
const links = require('../links');
const utils = require('../../utils');
const io = require('./io');
const memory = require('./memory');
const CoreIdea = require('./CoreIdea');

const ideas = exports;

/**
	ProxyIdea is an object that only stores the ID
	this makes it easy to pass around as a data object, to serialize, to load
	essentially, its just an object { id: 'x' }
	we can JSON.stringify; we can ideas.proxy
	The functions that are on ProxyIdea reference a singleton that stores the data
*/
class ProxyIdea {
	constructor(idea, data = undefined) {
		if (!idea) throw new TypeError('can only load ideas');
		const id = idea.id || idea;
		if (!id || typeof id !== 'string') throw new TypeError('can only load ideas');

		this.id = id;

		if (data !== undefined) throw new Error('setting data in the constructor is impossible because setData is an async operation');
	}

	/**
		@return {Promise<JSON|undefined>} a copy of the data for this this idea
	*/
	async getData() {
		// ensure that it's loaded
		return ideas.units.load(this)
			// then just return a copy of the data
			// this uses memory because ideas/ProxyIdea/CoreIdea are tightly coupled
			.then(() => _.cloneDeep(memory.get(this).data));
	}

	/**
		@param {JSON|undefined} data
		@return {Promise<JSON|undefined>} data (the original object)
	*/
	async setData(data) {
		return ideas.units.load(this)
			// this attaches a copy of the data to the CoreIdea, and returns the original object
			.then(() => ((memory.get(this).data = _.cloneDeep(data)) && data));
	}

	/**
		@param link - which type of link we are looking for
		@return {Promise<ProxyIdea[]>} the list of ideas that use this type of link
	*/
	async getLinksOfType(link) {
		// ensure that it's loaded
		return ideas.units.load(this)
			// then get the list of ids
			// this uses memory because ideas/ProxyIdea/CoreIdea are tightly coupled
			.then(() => Object.keys(memory.get(this).links[link.name] || {}))
			// upcast them to ProxyIdeas
			.then((idList) => idList.map((id) => new ProxyIdea(id)));
	}

	async addLink(link, idea) {
		// ensure the data is in memory
		idea = await ideas.units.load(idea);
		await ideas.units.load(this);

		const myCoreIdea = memory.get(this);
		myCoreIdea.links[link.name] = myCoreIdea.links[link.name] || {}; // add object if needed

		// if the link already exists, then we don't really need to do anything else
		if (Object.prototype.hasOwnProperty.call(myCoreIdea.links[link.name], idea.id)) {
			return this;
		}

		myCoreIdea.links[link.name][idea.id] = {}; // signal presence

		const otherCoreIdea = memory.get(idea);
		otherCoreIdea.links[link.opposite.name] = otherCoreIdea.links[link.opposite.name] || {}; // add object if needed
		otherCoreIdea.links[link.opposite.name][this.id] = {}; // signal presence

		return this;
	}

	async removeLink(link, idea) {
		idea = await ideas.units.load(idea);
		await ideas.units.load(this);

		// remove the idea from this
		let ls = memory.get(this).links;
		let list = ls[link.name];
		if (list) {
			delete list[idea.id];
			if (utils.isEmpty(list)) {
				delete ls[link.name];
			}
		}

		// remove this from the idea
		ls = memory.get(idea).links;
		list = ls[link.opposite.name];
		if (list) {
			delete list[this.id];
			if (utils.isEmpty(list)) {
				delete ls[link.opposite.name];
			}
		}

		return this;
	}
}

/*
	ideas
*/

/**
	@param {JSON|undefined} data - the data to store, will be stored with `JSON.stringify` and `JSON.parse`
	@return {Promise<ProxyIdea>}
*/
ideas.create = async (data) => ideas.units.create(data);

/**
	@param {ProxyIdea|string|{id:string}} idea
	@return {ProxyIdea}
*/
ideas.proxy = (idea) => (idea instanceof ProxyIdea ? idea : new ProxyIdea(idea));

/**
	@param {ProxyIdea} idea
	@return {Promise<ProxyIdea>}
*/
ideas.save = async (idea) => ideas.units.save(idea);

/**
	you can but do not need to wait for the result

	@param {ProxyIdea} idea
	@return {Promise<ProxyIdea>}
*/
ideas.close = async (idea) => ideas.units.close(idea);

/**
	this will remove:
	 - all the links pointing to the idea
	 - the idea's data
	 - all of this idea's links

	@param {ProxyIdea} idea
	@return {Promise<ProxyIdea>}
*/
ideas.delete = async (idea) => ideas.units.delete(idea);

/**
	allows for hard coded context ideas - like create, but uses a name

	@param {string} name - the id/name of the contenxt; a "well known value" that you can use to reference it again
	@return {Promise<ProxyIdea>}
*/
ideas.context = utils.transaction((name) => ideas.units.context(name));

/**
	allows for multiple hard coded context ideas

	@param {Array<string>} name - the id/name of the contenxt; a "well known value" that you can use to reference it again
	@return {Promise<Object<string, ProxyIdea>>}
*/
ideas.contexts = async (name) => ideas.units.contexts(name);

/**
	helper method to create a bunch of edges and links all in one go
	mostly useful for testing
	possibliy useful for bootstrapping a new database

	@param {{ key: any }} verts -
		`key` is the string used in edges, `value` is the data of the idea.
		this is mutated so the values point to the ideas that are created
	@param {[[src: string, link: any, dst: string]]} edges
	@return {Promise<undefined>} - since this mutates verts
*/
ideas.createGraph = async (verts, edges) => {
	// get the list of keys up front so we have a standard order
	const ideaKeysToCreate = _.keys(verts).filter((k) => !(verts[k] instanceof ProxyIdea));
	// create all the objects
	const all = await Promise.all(ideaKeysToCreate.map((k) => ideas.create(verts[k])));
	// map the ideas back onto the original verts object
	ideaKeysToCreate.forEach((k, idx) => { verts[k] = all[idx]; });

	// add all the links
	await Promise.all(edges.map(([src, link, dst]) => {
		if (!(src instanceof ProxyIdea)) src = verts[src];
		if (!(dst instanceof ProxyIdea)) dst = verts[dst];
		all.push(src);
		all.push(dst);
		link = (_.isString(link) ? links.get(link) : link);
		return src.addLink(link, dst);
	}));

	// save all the ideas (which saves the links)
	await Promise.all(_.uniq(all).map(ideas.save));
	await config.save();

	// just to make the point - use the verts input arg
	return undefined;
};

/*
	UNITS
*/

Object.defineProperty(ideas, 'units', { value: {} });

/*
	XXX I can't figure out how to separate ProxyIdea into it's own file
	 - it's tightly coupled / a circular dependency
	 - there really isn't any way around it
	 - this is just a code organization issue; helpful, not super important
*/
ideas.units.ProxyIdea = ProxyIdea;

/** ids key */
ideas.units.nextID = 'ids.ideas';

ideas.units.create = async function create(data) {
	const id = await ids.next(ideas.units.nextID, '100');
	if (data !== undefined) {
		memory.add(new CoreIdea(id, _.cloneDeep(data)));
		return ideas.units.save(id);
	}
	else { // eslint-disable-line no-else-return
		memory.add(new CoreIdea(id));
		return new ProxyIdea(id);
	}
};

ideas.units.load = async function load(idea) {
	idea = ideas.proxy(idea);

	if (!memory.has(idea)) {
		const myData = await io.load(idea.id, 'data');
		const myLinks = await io.load(idea.id, 'links');
		memory.add(new CoreIdea(idea.id, myData, myLinks));
	}

	return idea;
};

ideas.units.save = async function save(idea) {
	idea = ideas.proxy(idea);
	const core = memory.get(idea);

	if (core) {
		await io.save(idea.id, 'data', core.data);
		await io.save(idea.id, 'links', core.links);
	}

	return idea;
};

ideas.units.close = async function close(idea) {
	idea = await ideas.units.save(idea);
	memory.delete(idea);
	return idea;
};

ideas.units.delete = async function _delete(idea) {
	idea = ideas.proxy(idea);

	await ideas.units.removeAllLinks(idea);
	await io.save(idea.id, 'data', undefined);
	memory.delete(idea);

	return idea;
};

ideas.units.removeAllLinks = async function removeAllLinks(idea) {
	idea = ideas.proxy(idea);

	let myLinks;
	if (memory.has(idea)) {
		myLinks = memory.get(idea).links;
	}
	else {
		myLinks = await io.load(idea.id, 'links');
	}

	if (!myLinks || utils.isEmpty(myLinks)) return;

	const updated = new Set();
	updated.add(idea.id);
	await Promise.all(Object.keys(myLinks).map((l) => links.get(l)).map((link) => (
		Promise.all(Object.keys(myLinks[link.name] || {}).map((id) => {
			if (id !== idea.id) updated.add(id);
			return idea.removeLink(link, id);
		}))
	)));

	// may as well save all the updates right now
	await Promise.all(Array.from(updated).map((id) => ideas.units.save(id)));
};

// REVIEW `ideas.units.context` - what if we used a special character, like '_' and just made the `_${name}` == the idea.id?
//  - then we don't need to "find" it per se, we can just straight up use it, then this just needs to return the a proxy idea
//  - no need to actaully make anything with data?
//  - it'd be nice if we could make a ".context" folder at the root (or something), and then just save them there
// ----
//  - this kind of change would blow up the lm/todo database, how do we handle that?
//  - write a script to "rename this idea.id"
ideas.units.context = async function context(name) {
	if (!name) throw new RangeError('must provide a name');
	const ctx = await config.get('ideas.context', {});

	if (name in ctx) {
		return new ProxyIdea(ctx[name]);
	}

	const proxy = await ideas.units.create({ name });
	ctx[name] = proxy.id;
	await config.set('ideas.context', ctx);
	return proxy;
};

ideas.units.contexts = async function contexts(names) {
	const contextMap = {};
	await Promise.all(names.map((name) => (
		ideas.context(name).then((proxy) => { contextMap[name] = proxy; })
	)));
	return contextMap;
};
