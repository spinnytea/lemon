/*
	idea is the main api internally,
	memory is the main api externally.
*/
const _ = require('lodash');
const path = require('path');
const boundaries = require('../../boundaries');
const config = require('../../config');
const utils = require('../../utils');

const io = exports;

/**
	@param {string} id - the idea.id
	@param {string} which - a second qualifer, allows to save multiple files for a single idea (e.g. id.links.json vs id.data.json)
	@param {any} data - the data to save
	@return {Promise<JSON>} the data to save
*/
io.save = async (id, which, data) => io.units.saveFn(id, which, data);

/**
	@param {string} id - the idea.id
	@param {string} which - a second qualifer, allows to save multiple files for a single idea (e.g. id.links.json vs id.data.json)
	@return {Promise<JSON>} the loaded data
*/
io.load = async (id, which) => io.units.loadFn(id, which);

/*
	UNITS
*/

Object.defineProperty(io, 'units', { value: {} });

// create a path/filename for an idea
io.units.filepath = function filepath(id) {
	if (id.length > 2) {
		return path.join(config.settings.location, ...id
			.substr(0, (id.length - 2 + (id.length % 2)))
			.match(/../g));
	}
	return config.settings.location;
};

io.units.filename = function filename(id, which) {
	return `${io.units.filepath(id)}/${id}_${which}.json`;
};

// REVIEW async or not? throw immediate or Promise.reject?
/* istanbul ignore next: this isn't supposed to ever actually run */
io.units.saveFn = function undefinedSave() { throw new Error('must configure a save/load function'); };

/* istanbul ignore next: this isn't supposed to ever actually run */
io.units.loadFn = function undefinedLoad() { throw new Error('must configure a save/load function'); };

// for memorySave/memoryLoad
// this sort of breaks the rules
// we reference it extensively for testing that data gets written out correctly
io.units.database = { data: {}, links: {} };

/** in_memory implementation of save; data lost when program shuts down */
io.units.memorySave = async function memorySave(id, which, obj = undefined) {
	if (!id) throw new Error('id must be set');
	if (!(which in io.units.database)) throw new Error('invalid which');
	if (obj === undefined || (which === 'links' && utils.isEmpty(obj))) {
		delete io.units.database[which][id];
	}
	else {
		io.units.database[which][id] = _.cloneDeep(obj);
	}
	return Promise.resolve(obj);
};

/** in_memory implementation of load; data must be created every time the program starts */
io.units.memoryLoad = async function memoryLoad(id, which) {
	if (!id) throw new Error('id must be set');
	if (!(which in io.units.database)) throw new Error('invalid which');

	if (config.settings.memory_fallback_to_location && !(id in io.units.database[which])) {
		return io.units.fileLoad(id, which);
	}

	return Promise.resolve(io.units.database[which][id]);
};

io.units.fileSave = async function fileSave(id, which, obj = undefined) {
	if (!id) throw new Error('id must be set');
	if (!(which in io.units.database)) throw new Error('invalid which');

	const filename = io.units.filename(id, which);

	if (obj === undefined || (which === 'links' && utils.isEmpty(obj))) {
		if (boundaries.existsSync(filename)) {
			await boundaries.deleteFile(filename);
		}
	}
	else {
		const filepath = io.units.filepath(id);

		// ensure the folder structure exists
		if (!boundaries.existsSync(filepath)) {
			// we don't want to recreate the whole directory root
			// i.e. this is a check to make sure our drive is mounted
			if (boundaries.existsSync(config.settings.location)) {
				boundaries.mkdirp(filepath);
			}
			else {
				// writeFile out to throw if config.settings.location doesn't exist
				// but there are likely some edge cases that could slip through
				// i'd rather to just catch it here since this is what we want to happen
				// and not rely on writeFile to do that
				return Promise.reject(new Error(`config.settings.location is unavailable: "${config.settings.location}"`));
			}
		}

		await boundaries.writeFile(filename, JSON.stringify(obj), { encoding: 'utf8' });
	}

	return Promise.resolve(obj);
};

io.units.fileLoad = async function fileLoad(id, which) {
	if (!id) throw new Error('id must be set');
	if (!(which in io.units.database)) throw new Error('invalid which');

	const filename = io.units.filename(id, which);
	if (boundaries.existsSync(filename)) {
		return boundaries.readFile(filename, { encoding: 'utf8' }).then((data) => JSON.parse(data));
	}
	return Promise.resolve(undefined);
};

/*
	INIT
*/

config.onInit(() => {
	io.units.database = { data: {}, links: {} };
	if (config.settings.in_memory) {
		io.units.saveFn = io.units.memorySave;
		io.units.loadFn = io.units.memoryLoad;
	}
	else {
		io.units.saveFn = io.units.fileSave;
		io.units.loadFn = io.units.fileLoad;
	}
});
