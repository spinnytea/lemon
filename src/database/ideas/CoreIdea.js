/**
	this is the singleton that we will keep an internal reference to
	it's basically just a named structure
 */
class CoreIdea {
	constructor(id, data, links) {
		this.id = id;
		this.data = data;
		this.links = links || {};
	}
}

module.exports = CoreIdea;
