/*
	LazyCopyMap looks and acts like a map
	But it saves on memory/storage when you clone it, instead remembering a parent.
*/
class LazyCopyMap {
	/** @param {LazyCopyMap} parent */
	constructor(parent = undefined) {
		if (parent !== undefined && !(parent instanceof LazyCopyMap)) {
			throw new TypeError('parent must be of type LazyCopyMap');
		}

		this.data = new Map();
		this.parent = parent;
		this.children = [];

		// if the parent is empty, then we can just skip over it and use it's ancestor
		// this means the parent doesn't actually have anything useful inside of it
		if (parent && parent.data.size === 0) {
			this.parent = parent.parent;
		}
		if (this.parent) {
			this.parent.children.push(this);
		}
	}

	set(id, value) {
		// we either need the value, or we need to forward the undefined
		const v = this.get(id);
		this.children.forEach((child) => { if (!child.data.has(id)) child.data.set(id, v); });

		this.data.set(id, value);
	}

	get(id) {
		if (this.data.has(id)) {
			return this.data.get(id);
		}

		if (this.parent) {
			return this.parent.get(id);
		}

		return undefined;
	}

	flatten() {
		let p = this.parent;
		while (p) {
			p.data.forEach((value, key) => {
				if (!this.data.has(key)) {
					this.data.set(key, value);
				}
			});
			p = p.parent;
		}
		if (this.parent) {
			this.parent.children.splice(this.parent.children.indexOf(this), 1);
		}
		this.parent = undefined;

		this.children.forEach((child) => {
			this.data.forEach((value, key) => {
				if (!child.data.has(key)) child.data.set(key, value);
			});
			child.parent = undefined;
		});
		this.children = [];
	}
}

module.exports = LazyCopyMap;
