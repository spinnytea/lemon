const ideas = require('../ideas');

/**
   REVIEW should we refactor this?
	 - move it to `sg.destroy`, similar to `sg.save`
	 - move it to rewrite? `destroyEdge`, `destroyVertex`
*/

const subgraphUtils = exports;

/**
   delete the ideas under

	@param {Subgraph} sg - must be concrete
	@param {Array<number>} vertexIds - an array of verticies to delete
*/
subgraphUtils.destroy = async (sg, vertexIds) => subgraphUtils.units.destroy(sg, vertexIds);

/*
	UNITS
*/

Object.defineProperty(exports, 'units', { value: {} });

subgraphUtils.units.destroy = function destroy(sg, vertexIds) {
	if (!sg.concrete) {
		return Promise.reject(new RangeError('Subgraph must be concrete'));
	}

	// we can't delete in parallel, the links are too messy; we have to delete one at a time
	return vertexIds.reduce((promise, vertexId) => promise.then(async () => {
		// remove the actual idea
		await subgraphUtils.boundaries.deleteIdea(sg.getIdea(vertexId));

		// clean out the subgraph
		// we want to keep the edges and verticies in the subgraph
		sg.deleteData(vertexId);
		sg.deleteIdea(vertexId);
	}), Promise.resolve()).then(() => sg);
};

/*
	BOUNDARIES
*/

Object.defineProperty(subgraphUtils, 'boundaries', { value: {} });

subgraphUtils.boundaries.deleteIdea = (idea) => ideas.delete(idea);
