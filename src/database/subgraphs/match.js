const _ = require('lodash');

const subgraphs = require('./subgraphs');

// TODO when a match fails, report "what could be matched (ever) and what could not be matched (never)"
//  - this will help us diagnose when a goal doesn't map to initial conditions
//  - it will tell us "what do we need to learn next"
const match = exports;

/**
	NOTE about match structure and data
	 - match follows edges to build up a graph
	 - we can rewrite edges
	 - edges can be used to manage search spaces (disputed - we'd need to rerun astar weights on every rewrite)
	 - but it also means that we cannot store "data" by way of what edges point to
	 - (we can have a person move through a room by moving an edge - a person needs to have a location and the vertex value has to change)

	@param {subgraphs.Subgraph} outer - stand-in for ideas, the narrowed graph to search
	@param {subgraphs.Subgraph} inner - a subgraph we want to find, does not need any ideas defined (does not need any pinned nodes)
	@param {boolean} options.unitsOnly - when we check transitive verticies, do we compare the whole value, or just the unit
	@returns {Promise<Array<Map<string, string>>>} inner to outer vertexId Maps, a list of matches that map inner vertexIds to outer vertexIds
*/
match.match = async (outer, inner, { unitsOnly = false } = {}) => match.units.doMatch(outer, inner, { unitsOnly });

/**
	an object containing state info for the subgraph match
	it's a complicated process with a lot of parameters, so it's nice to have them packaged up
	it's a complicated process, so we need to compute some indexes and caches to speed it up
*/
class SubgraphMatchMetadata {
	constructor(outer, inner, innerToOuterVertexMap, unitsOnly, isClone = false) {
		// outer subgraph (concrete, the represents the world)
		this.outer = outer;
		// inner subgraph (the one we are matching)
		this.inner = inner;
		// do we use the matcher, or just match the units?
		this.unitsOnly = unitsOnly;

		// the edges in the outer subgraph, grouped by link
		this.outerEdges = new Map();
		// a list of inner edges, will be pruned as they are validated
		this.innerEdges = (isClone ? undefined : inner.allEdges().slice(0));

		// vertexes we have matched so far
		// innerVertexId -> outerVertexId
		this.innerToOuterVertexMap = new Map(innerToOuterVertexMap);
		// inverse map
		// outer -> inner
		// NOTE this only ever checks "has", but it will probably prove useful when we impl Experience and need to update plans/requirements
		this.inverseMap = new Map();
		innerToOuterVertexMap.forEach((o, i) => this.inverseMap.set(o, i));

		// edges we have mapped so far
		this.edgeMap = (isClone ? undefined : new Map());

		// a list of edges we are going to skip until we match our next edge
		this.skipThisTime = new Set();

		if (!isClone) {
			// fill outer edges, grouped by type
			outer.allEdges().forEach((edge) => {
				let list = this.outerEdges.get(edge.link.name);
				if (!list) {
					list = [];
					this.outerEdges.set(edge.link.name, list);
				}
				list.push(edge);
			});
		}
	}

	clone() {
		const c = new SubgraphMatchMetadata(this.outer, this.inner, this.innerToOuterVertexMap, this.unitsOnly, true);

		// copy the complex objects by hand
		c.edgeMap = new Map(this.edgeMap);
		c.innerEdges = this.innerEdges.slice(0);
		this.outerEdges.forEach((list, link) => {
			c.outerEdges.set(link, list.slice(0));
		});

		return c;
	}

	// pick the best inner edge
	// (this helps us reduce the number of branches)
	nextInnerEdge() {
		return this.innerEdges.reduce((prev, curr) => {
			// XXX should we only skip if the target isn't mapped? if so, then do we need 'skipThisTime'?
			if (this.skipThisTime.has(curr)) return prev;
			if (prev === null) return curr;
			if (curr.pref > prev.pref) return curr;
			return prev;
		}, null);
	}

	// find all matching outer edges
	nextOuterEdges(innerEdge) {
		const edges = this.outerEdges.get(innerEdge.link.name) || [];
		return Promise.all(edges.map((outerEdge) => (
			match.units.filterOuter(this, innerEdge, outerEdge)
		))).then((matches) => (
			// clear the list of unmatched edges
			matches.filter(_.identity)
		));
	}

	removeInnerEdge(innerEdge) {
		_.pull(this.innerEdges, innerEdge);
	}

	removeOuterEdge(outerEdge) {
		// TODO it would be more efficient to keep the list and build an inverse edge map
		// - we need to clone the inverse edge map instead of deep cloning outerEdges
		_.pull(this.outerEdges.get(outerEdge.link.name), outerEdge);
	}

	updateVertexMap(innerEdge, outerEdge) {
		this.innerToOuterVertexMap.set(innerEdge.src, outerEdge.src);
		this.innerToOuterVertexMap.set(innerEdge.dst, outerEdge.dst);
		this.inverseMap.set(outerEdge.src, innerEdge.src);
		this.inverseMap.set(outerEdge.dst, innerEdge.dst);
		this.edgeMap.set(innerEdge.edgeId, outerEdge.edgeId);
	}
}

match.SubgraphMatchMetadata = SubgraphMatchMetadata;

/*
	UNITS
*/

Object.defineProperty(exports, 'units', { value: {} });

/**
	find a list of ways to map the inner subgraph onto the outer subgraph
	returns a set of mapped edges and vertices

	this doesn't follow any particular algorithm
	it picks the "best" inner edge, and finds all matching outer edges
	it repeats that until all the inner edges have been address

	@param {Subgraph} outer - subgraph, must be concrete
	@param {Subgraph} inner - subgraph, the one we are trying to find within outer
*/
match.units.doMatch = function doMatch(outer, inner, { unitsOnly }) {
	if (!outer.concrete) {
		return Promise.reject(new RangeError('the outer subgraph must be concrete before you can match against it'));
	}

	if (inner.$vertexCount === 0) {
		return Promise.resolve([]);
	}

	// the inner must fit a subset of outer
	// if the inner is larger, then this is impossible
	if (inner.$vertexCount > outer.$vertexCount || inner.$edgeCount > outer.$edgeCount) {
		return Promise.resolve([]);
	}

	return match.units.initializeVertexMap(outer, inner).then((innerToOuterVertexMap) => {
		// if innerToOuterVertexMap doesn't exist, then there is no match
		if (!innerToOuterVertexMap) return [];

		// recurse over the edges
		return match.units.recursiveMatch(new SubgraphMatchMetadata(outer, inner, innerToOuterVertexMap, unitsOnly));
	});
};

/**
	match is the seed for the recursive function, this is the recursive case

	find an edge to expand, then expand it
*/
match.units.recursiveMatch = function recursiveMatch(metadata) {
	// are we done?
	// Note: we won't recurse if innerEdges.length === skipThisTime.size
	if (metadata.innerEdges.length === 0) {
		if (metadata.innerToOuterVertexMap.size === metadata.inner.$vertexCount) {
			return Promise.resolve([{ innerToOuterVertexMap: metadata.innerToOuterVertexMap, innerToOuterEdgeMap: metadata.edgeMap }]);
		}
		return Promise.resolve([]);
	}

	// pick the best inner edge
	const innerEdge = metadata.nextInnerEdge();

	if (innerEdge === null) {
		return Promise.resolve([]);
	}

	// find all matching outer edges
	return metadata.nextOuterEdges(innerEdge).then((matches) => {
		// 0 outer
		// - deal with vertex.pointer
		// - otherwise return no match
		if (matches.length === 0) {
			const innerSrcVertex = metadata.inner.getVertex(innerEdge.src);
			const innerDstVertex = metadata.inner.getVertex(innerEdge.dst);

			// because of indirection, we may need to skip an edge and try the next best one
			// so if our current edge uses inderection, and there are other edges to try, then, well, try again
			// but next time, don't consider this edge
			// XXX should we only skip if the target isn't mapped?
			if ((innerSrcVertex.pointer || innerDstVertex.pointer) && metadata.innerEdges.length > metadata.skipThisTime.size) {
				metadata.skipThisTime.add(innerEdge);
				return match.units.recursiveMatch(metadata);
			}

			// no matches, and we've skipped everything
			return [];
		}

		// common stuff before recursion
		// - note that when we do the many case, we don't need to do this for all the clones #winning
		metadata.removeInnerEdge(innerEdge);
		metadata.skipThisTime.clear();

		// 1 or many outer
		// - loop over matches
		// - clone (or reuse) metadata
		// - recurse
		return Promise.all(matches.map((outerEdge, idx) => {
			// don't clone if it's the last match
			const last = (idx === matches.length - 1);
			const meta = (last ? metadata : metadata.clone());
			meta.removeOuterEdge(outerEdge);
			meta.updateVertexMap(innerEdge, outerEdge);
			return match.units.recursiveMatch(meta);
		})).then(_.flatten);
	});
};

/**
	@return {Promise<Map|undefined>} innerToOuterVertexMap.get(inner vertex id) = outer vertex id;
*/
match.units.initializeVertexMap = function initializeVertexMap(outer, inner) {
	const innerToOuterVertexMap = new Map();
	const inverseOuterMap = match.units.buildInverseIdeaMap(outer.allIdeas()); // so we can get by idea
	const inverseInnerMap = match.units.buildInverseIdeaMap(inner.allIdeas()); // mostly just for deduping

	// innerIdeas is a map, not a list, so there is no map function
	const promises = [];
	// checkVertexData expects a metadata object; this is the only data we have so far
	const meta = { outer, inner };
	let keepGoing = true;
	inverseInnerMap.forEach((innerVertexId, viIdeaId) => {
		if (keepGoing) {
			const outerVertexId = inverseOuterMap.get(viIdeaId);
			if (outerVertexId !== undefined) {
				innerToOuterVertexMap.set(innerVertexId, outerVertexId);
				promises.push(match.units.checkVertexData(meta, innerVertexId, outerVertexId));
			}
			else {
				// if inner has been mapped to something that doesn't exist in outer
				// then this cannot be reconciled
				promises.push(false);
				keepGoing = false;
			}
		}
	});

	if (!keepGoing) {
		return Promise.resolve(undefined);
	}

	return Promise.all(promises).then((possible) => {
		if (possible.every((p) => p === true)) {
			return innerToOuterVertexMap;
		}
		return undefined;
	});
};

/**
	build an index (outer.idea.id -> outer.vertexId)

	REVIEW buildInverseIdeaMap has a limitation - we cannot seed the graph with duplicate `idea.id`s
	 - what this means in practice is:
	 - if outer has a vertex that does not have any edges
	 - and that vertex is mapped to an idea (which it must be)
	 - and that idea is also in another vertex somewhere else in outer
	 - then we cannot use it to see the match (to grow from that idea)
	----
	 - this is true for initializeVertexMap, and inner too
	 - if inner or outer have the same idea in multiple places, it cannot be a match seed
	 - and then if any of those vertices are orphaned, we can never build a match

	@param outerIdeas - list of all outer ideas
	@param innerCount - number of inner ideas
	@returns {Map<idea.id, vertexId>}
*/
match.units.buildInverseIdeaMap = function buildInverseIdeaMap(outerIdeas) {
	const inverseOuterMap = new Map();
	const cannotMapDups = new Set();
	outerIdeas.forEach((voIdea, outerVertexId) => {
		if (inverseOuterMap.has(voIdea.id)) {
			cannotMapDups.add(voIdea.id);
		}
		else {
			inverseOuterMap.set(voIdea.id, outerVertexId);
		}
	});
	cannotMapDups.forEach((id) => {
		inverseOuterMap.delete(id);
	});
	return inverseOuterMap;
};

/**
	check to see if the outer edge is a good match for the inner edge

	@prereq: outerEdge.link === innerEdge.link (handled by grouping outer by type)
	@prereq: outerEdge has not already been mapped (handled by removing outer from pool when matched)
	@param metadata
	@param innerEdge
	@param outerEdge
	@return outerEdge if we should use outer edge to expand, undefined otherwise, wrapped in a promise
*/
match.units.filterOuter = function filterOuter(metadata, innerEdge, outerEdge) {
	return Promise.all([
		match.units.checkVertexData(metadata, innerEdge.src, outerEdge.src),
		match.units.checkVertexData(metadata, innerEdge.dst, outerEdge.dst),
	]).then(([srcPossible, dstPossible]) => {
		if (srcPossible && dstPossible) {
			return outerEdge;
		}
		return undefined;
	});
};

/**
	check the matchers against data to make sure the edge is valid

	checkTransitionableVertexData and checkFixedVertexData are two sides of the same coin
	one checks transitionable verticies, the other checks non-transitionable vertices
	this just decides which one to call

	@return {Promise<boolean>} valid or not
*/
match.units.checkVertexData = function checkVertexData(metadata, innerVertexId, outerVertexId) {
	// skip the vertices that are mapped to something different
	if (metadata.innerToOuterVertexMap) {
		if (metadata.innerToOuterVertexMap.has(innerVertexId)) {
			if (metadata.innerToOuterVertexMap.get(innerVertexId) === outerVertexId) {
				return Promise.resolve(true);
			}
			return Promise.resolve(false);
		}
		// outerEdge src is mapped to a different inner id
		if (metadata.inverseMap.has(outerVertexId)) {
			return Promise.resolve(false);
		}
	}

	return Promise.all([
		match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId),
		match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId),
	]).then(([trans, fixed]) => (
		trans && fixed
	));
};

/**
	this function checks transitionable vertices to see if a transition is possible
	it should noop for non-transitionable vertices (returns true because it isn't determined to be invalid)
*/
match.units.checkTransitionableVertexData = async function checkTransitionableVertexData(metadata, innerVertexId, outerVertexId) {
	// only necessary to check when the inner idea has not been identified
	// because matchers are used for identifying the ideas; once ideas have been identified, the matchers don't make sense anymore
	// matchers are for the identification phase; once we have ideas, then we are in an imagination phase
	if (metadata.inner.hasIdea(innerVertexId)) return true;

	const innerVertex = metadata.inner.getVertex(innerVertexId);

	// if the inner isn't transitionable, then we don't need to check anything
	// this will be handled by checkFixedVertexData
	if (!innerVertex.transitionable) return true;
	// if this is a filler node, then of course it matches
	if (innerVertex.matcher === subgraphs.units.matchers.filler) return true;
	// the outer must be transitionable, otherwise it's a config/matcher problem
	// the outer subgraph defines up front what values it expects to change
	if (!metadata.outer.getVertex(outerVertexId).transitionable) return false;

	// make sure we have data to work with
	// REVIEW is it a bad assumption to "just returning true" if the data is null
	//  - just because they are marked as "transitionable", does it mean that "null is an acceptable value"?
	//  - do we need to check the type of transitions?
	//  - all we have right now "replace" and that's allowed, will other types be invalid, e.g. "add / combine"?
	const viData = await metadata.inner.getMatchData(innerVertexId, metadata);
	if (!viData.length) return true;
	const voData = await metadata.outer.getData(outerVertexId);
	if (!voData) return true;

	// this might be pedantic, but it'll make thing easier later
	// they must either both HAVE or NOT HAVE a unit
	if (viData.every((md) => ('$unit' in md)) !== ('$unit' in voData)) return false;

	if (metadata.unitsOnly && Object.prototype.hasOwnProperty.call(voData, '$unit')) {
		// match the units
		// REVIEW this is a special case "match only units", but the general case is "transitionable verticies need a relaxed matcher"
		//  - of which, we could write a matcher that is "units only"
		return viData.every((md) => md.$unit === voData.$unit);
	}

	// XXX we need a distance function for each kind of unit, use that instead
	// - or maybe each unit will also have an equality check
	// TODO we are comparing data - why aren't we using the matcher?
	return viData.some((md) => _.isEqual(voData, md));
};

/**
	check the matcher function against the outer data

	if a vertex is not marked as transitionable
	or if we are not checking unit only
	then we need a harder check on the value
*/
match.units.checkFixedVertexData = async function checkFixedVertexData(metadata, innerVertexId, outerVertexId) {
	// only necessary to check when the inner idea has not been identified
	// because matchers are used for identifying the ideas; once ideas have been identified, the matchers don't make sense anymore
	// matchers are for the identification phase; once we have ideas, then we are in an imagination phase
	if (metadata.inner.hasIdea(innerVertexId)) return true;

	const innerVertex = metadata.inner.getVertex(innerVertexId);

	// if the inner is transitionable, then we don't need to check anything
	// this will be handled by checkTransitionableVertexData
	if (innerVertex.transitionable) return true;
	// if this is a filler node, then of course it matches
	if (innerVertex.matcher === subgraphs.units.matchers.filler) return true;

	// if pointer, then we want to use the data we found as the matcher data
	// if !pointer, then we need to use the vertex.data on the object
	// this will also correct for subgraphs.units.matchers.id
	let viData;
	if (innerVertex.pointer) {
		viData = await metadata.inner.getMatchData(innerVertexId, metadata);
		if (!viData.length) return false;
	}
	else {
		viData = (innerVertex.orData ? innerVertex.data : [innerVertex.data]);
	}

	// outer data is simple since it's concerete
	let voData;
	if (innerVertex.matcher === subgraphs.units.matchers.id) {
		voData = metadata.outer.getIdea(outerVertexId);
	}
	else {
		voData = await metadata.outer.getData(outerVertexId);
	}

	return viData.some((md) => innerVertex.matcher(voData, md));
};
