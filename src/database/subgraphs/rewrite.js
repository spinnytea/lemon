const { Subgraph } = require('./subgraphs');
const numbers = require('../../planning/primitives/numbers');

const rewrite = exports;

/**
	@param {Subgraph} sg
	@param {Array<Object>} transitions - an array of transitions
		- { vertexId: vertexId, replace: value } - for vertex `vertexId`, set the data to `replace`
		- { vertexId: vertexId, replaceId: vertexId } - for vertex `vertexId`, set it's data to be the same as data of vertex `replaceId`
		- { vertexId: vertexId, numbersCombineId: vertexId->numbers.isNumber } - use numbers.combine on the data in the target vertex
		- { vertexId: vertexId, numbersRemoveId: vertexId->numbers.isNumber } - use numbers.remove on the data in the target vertex
		- { vertexId: vertexId, numbersCombine: numbers.isNumber } - use numbers.combine on the data
		- { vertexId: vertexId, numbersRemove: numbers.isNumber } - use numbers.remove on the data
		- { edgeId: edgeId, replaceSrc: vertexId } - for edge `edgeId`, replace the src `vertexId`
		- { edgeId: edgeId, replaceDst: vertexId } - for edge `edgeId`, replace the dst `vertexId`
		- TODO for vertex transitions, add an option to enforce units
		- TODO these transitions (as defined) are non-reversable (what if we want to plan from goal -> start, instead of start -> goal)
		   - we may be able to restrict the vertex.matcher/vertex.data to recover the other half
			- the edge swaps should be able to specify the other src/dst (we are afterall moving from someplace)
	@param {boolean} isActual
		- if true, write the updates to sg and the idea graph (write through to perceived actuality)
		- if false, write the updates to sg (in abstract memory)
	@return {Promise<Subgraph|undefined>}
		- if isActual, return sg
		- if !isActual, return a copy of sg
		- if unable to perform rewrite, return undefined
*/
rewrite.rewrite = async (sg, transitions, isActual) => rewrite.units.doRewrite(sg, transitions, isActual);

/**
	build a new transition list/map using the glue
	the transitions are based on an inner graph, but we need to apply them to the outer graph
	i.e. we matched requirements to a state, and now we want to transition that state

	@param {Subgraph} inner
	@param {Array<Object>} transitions - an array of transitions
	@return {Array<Object>} - the updated transitions
*/
rewrite.convertInnerTransitions = (inner, transitions, { innerToOuterVertexMap, innerToOuterEdgeMap }) => (
	rewrite.units.convertInnerTransitions(inner, transitions, { innerToOuterVertexMap, innerToOuterEdgeMap })
);

/*
	UNITS
*/

Object.defineProperty(rewrite, 'units', { value: {} });

rewrite.units.doRewrite = function doRewrite(sg, transitions, isActual = false) {
	// XXX is !concrete allowed if !isActual? (need a meaningful example to test against)
	if (!sg.concrete) {
		return Promise.resolve(undefined);
	}

	// validate transitions
	// check all transitions to ensure they the vertex or edge checks
	const allPass = transitions.every((t) => {
		if (!t) return false;

		if (Object.prototype.hasOwnProperty.call(t, 'vertexId')) {
			return rewrite.units.checkVertex(sg, t);
		}
		if (Object.prototype.hasOwnProperty.call(t, 'edgeId')) {
			return rewrite.units.checkEdge(sg, t);
		}
		return false;
	});
	if (!allPass) return Promise.resolve(undefined);

	// if this is the isActual transition, we apply the transitions to this object
	// if this is a theoretical transition, we apply the transitions to a copy
	if (!isActual) {
		sg = new Subgraph(sg);
	}

	return Promise.all(transitions.map((t) => {
		if (Object.prototype.hasOwnProperty.call(t, 'vertexId')) {
			return rewrite.units.transitionVertex(sg, t, isActual);
		}
		// else if (Object.prototype.hasOwnProperty.call(t, 'edgeId'))

		return rewrite.units.transitionEdge(sg, t, isActual);
	})).then(() => sg); // at long last, return sg
};

// return true if the vertex transition is valid
rewrite.units.checkVertex = function checkVertex(sg, t) {
	if (!Object.prototype.hasOwnProperty.call(t, 'vertexId')) {
		return false;
	}
	if (Object.prototype.hasOwnProperty.call(t, 'edgeId')) {
		return false;
	}

	if (!(Object.prototype.hasOwnProperty.call(t, 'replace')
		|| Object.prototype.hasOwnProperty.call(t, 'replaceId')
		|| Object.prototype.hasOwnProperty.call(t, 'numbersCombineId')
		|| Object.prototype.hasOwnProperty.call(t, 'numbersRemoveId')
		|| Object.prototype.hasOwnProperty.call(t, 'numbersCombine')
		|| Object.prototype.hasOwnProperty.call(t, 'numbersRemove')
	)) {
		return false;
	}

	const vertex = sg.getVertex(t.vertexId);
	if (!vertex?.transitionable) {
		return false;
	}

	if (Object.prototype.hasOwnProperty.call(t, 'replaceId') && !sg.getVertex(t.replaceId)) {
		return false;
	}

	return true;
};

rewrite.units.transitionVertex = async function transitionVertex(sg, t, isActual) {
	if (Object.prototype.hasOwnProperty.call(t, 'replace')) {
		sg.setData(t.vertexId, t.replace);
	}
	else if (Object.prototype.hasOwnProperty.call(t, 'replaceId')) {
		sg.setData(t.vertexId, await sg.getData(t.replaceId));
	}
	else if (Object.prototype.hasOwnProperty.call(t, 'numbersCombineId')) {
		const n = await Promise.all([
			sg.getData(t.vertexId),
			sg.getData(t.numbersCombineId),
		]).then(([n1, n2]) => numbers.combine(n1, n2));
		sg.setData(t.vertexId, n);
	}
	else if (Object.prototype.hasOwnProperty.call(t, 'numbersRemoveId')) {
		const n = await Promise.all([
			sg.getData(t.vertexId),
			sg.getData(t.numbersRemoveId),
		]).then(([n1, n2]) => numbers.remove(n1, n2));
		sg.setData(t.vertexId, n);
	}
	else if (Object.prototype.hasOwnProperty.call(t, 'numbersCombine')) {
		const n = await Promise.all([
			sg.getData(t.vertexId),
			t.numbersCombine,
		]).then(([n1, n2]) => numbers.combine(n1, n2));
		sg.setData(t.vertexId, n);
	}
	else if (Object.prototype.hasOwnProperty.call(t, 'numbersRemove')) {
		const n = await Promise.all([
			sg.getData(t.vertexId),
			t.numbersRemove,
		]).then(([n1, n2]) => numbers.remove(n1, n2));
		sg.setData(t.vertexId, n);
	}

	if (isActual) {
		await rewrite.boundaries.updateData(sg, t.vertexId);
	}
};

rewrite.units.checkEdge = function checkEdge(sg, t) {
	if (!Object.prototype.hasOwnProperty.call(t, 'edgeId')) {
		return false;
	}

	if (Object.prototype.hasOwnProperty.call(t, 'vertexId')) {
		return false;
	}

	if (!(Object.prototype.hasOwnProperty.call(t, 'replaceSrc') || Object.prototype.hasOwnProperty.call(t, 'replaceDst'))) {
		return false;
	}

	const edge = sg.getEdge(t.edgeId);
	if (!edge?.transitionable) {
		return false;
	}

	if (Object.prototype.hasOwnProperty.call(t, 'replaceSrc') && !sg.getVertex(t.replaceSrc)) {
		return false;
	}
	if (Object.prototype.hasOwnProperty.call(t, 'replaceDst') && !sg.getVertex(t.replaceDst)) {
		return false;
	}

	return true;
};

rewrite.units.transitionEdge = async function transitionEdge(sg, t, isActual) {
	const { src: prevSrc, link, dst: prevDst } = sg.getEdge(t.edgeId);
	const nextSrc = sg.$refVertex(Object.prototype.hasOwnProperty.call(t, 'replaceSrc') ? t.replaceSrc : prevSrc);
	const nextDst = sg.$refVertex(Object.prototype.hasOwnProperty.call(t, 'replaceDst') ? t.replaceDst : prevDst);

	sg.updateEdge(t.edgeId, nextSrc, nextDst);

	if (isActual && (nextSrc !== prevSrc || nextDst !== prevDst)) {
		await rewrite.boundaries.updateLink(sg, link, prevSrc, prevDst, nextSrc, nextDst);
	}
};

rewrite.units.convertInnerTransitions = function convertInnerTransitions(inner, transitions, { innerToOuterVertexMap, innerToOuterEdgeMap }) {
	return transitions.map((t) => {
		t = { ...t }; // shallow copy
		if ('vertexId' in t) {
			t.vertexId = innerToOuterVertexMap.get(inner.$refVertex(t.vertexId));
		}
		if ('replaceId' in t) {
			t.replaceId = innerToOuterVertexMap.get(inner.$refVertex(t.replaceId));
		}
		if ('numbersCombineId' in t) {
			t.numbersCombineId = innerToOuterVertexMap.get(inner.$refVertex(t.numbersCombineId));
		}
		if ('numbersRemoveId' in t) {
			t.numbersRemoveId = innerToOuterVertexMap.get(inner.$refVertex(t.numbersRemoveId));
		}
		if ('replaceSrc' in t) {
			t.replaceSrc = innerToOuterVertexMap.get(inner.$refVertex(t.replaceSrc));
		}
		if ('replaceDst' in t) {
			t.replaceDst = innerToOuterVertexMap.get(inner.$refVertex(t.replaceDst));
		}
		if ('edgeId' in t) {
			t.edgeId = innerToOuterEdgeMap.get(inner.$refEdge(t.edgeId));
		}
		return t;
	});
};

/*
	BOUNDARIES
*/

Object.defineProperty(rewrite, 'boundaries', { value: {} });

rewrite.boundaries.updateData = (sg, vertexId) => sg.getData(vertexId)
	.then((data) => sg.getIdea(vertexId).setData(data));

rewrite.boundaries.updateLink = (sg, link, prevSrc, prevDst, nextSrc, nextDst) => {
	const prevSrcIdea = sg.getIdea(prevSrc);
	const prevDstIdea = sg.getIdea(prevDst);
	const nextSrcIdea = sg.getIdea(nextSrc);
	const nextDstIdea = sg.getIdea(nextDst);

	if (nextSrcIdea.id !== prevSrcIdea.id || nextDstIdea.id !== prevDstIdea.id) {
		return Promise.all([
			prevSrcIdea.removeLink(link, prevDstIdea),
			nextSrcIdea.addLink(link, nextDstIdea),
		]);
	}

	return Promise.resolve();
};
