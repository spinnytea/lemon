const _ = require('lodash');
const subgraphs = require('./subgraphs');

const search = exports;

/**
	find a list of subgraphs in the database that matches the supplied Subgraph

	use Prim's algorithm to expand the known Subgraph
	we are trying to identify all of the vertices
	we use edges to find new vertices to attach to an already known graph

	Note: this means each lower-case-S subgraph needs to have at least 1 idea pinned
	sg does not need to be [fully] connected; every vertex must have an path to a pinned vertex

	@param {subgraphs.Subgraph} sg
	@return {Promise<Array<subgraphs.Subgraph>>}
*/
search.search = async (sg) => search.units.doSearch(sg);

/*
	UNITS
*/

Object.defineProperty(search, 'units', { value: {} });

search.units.doSearch = function doSearch(sg) {
	if (sg.isIdeaEmpty()) return Promise.reject(new Error('Subgraph must have at least one idea pinned down'));

	if (sg.concrete) return Promise.resolve([sg]);

	const edges = _.orderBy(sg.allEdges(), 'pref', 'desc');
	sg = new subgraphs.Subgraph(sg);

	return search.units.recursiveSearch(sg, edges);
};

search.units.recursiveSearch = async function recursiveSearch(sg, edges) {
	// prune and validate edges that are finished
	edges = await search.units.verifyEdges(sg, edges);
	// if the edges are invalid, then the branch is invalid
	if (!edges) return [];
	if (edges.length === 0) {
		// check all vertices to ensure they all have ideas defined
		if (!sg.isIdeaComplete()) {
			return [];
		}

		sg.concrete = true;
		return [sg];
	}

	// find the "best" list to expand next
	const selected = await search.units.bestEdge(sg, edges);
	if (!selected) return [];

	// load and check the data, generate new Subgraphs
	const nextSteps = await search.units.expandEdges(sg, selected);
	if (nextSteps.length === 0) return [];

	// do the next iteration of searches
	edges = _.pull(edges.slice(0), selected.edge);
	return Promise.all(nextSteps.map((nextSg) => search.units.recursiveSearch(nextSg, edges)))
		.then((lists) => _.flatten(lists));
};

/**
	@return {Promise<object[] | null>} the pruned array of edges if the finished ones are valid, null if the Subgraph is invalid
*/
search.units.verifyEdges = function verifyEdges(sg, edges) {
	// collect the edges that have IDs on both ends
	const done = [];
	const unfinished = [];

	edges.forEach((edge) => {
		if (sg.hasIdea(edge.src) && sg.hasIdea(edge.dst)) {
			done.push(edge);
		}
		else {
			unfinished.push(edge);
		}
	});

	// if any of the edges are invalid, then this Subgraph match is invalid
	return Promise.all(done.map((edge) => search.units.verifyEdge(sg, edge)))
		.then((allDone) => {
			if (allDone.every(_.identity)) {
				return unfinished;
			}
			return null;
		});
};

/**
	specifically for when src and dst have ideas pinned
	check to see that there is an edge between them

	`getBranches` is for finding possible new ids to pin down, based on an edge with one side on the pinned graph and one side off the pinned graph
	`verifyEdge` is for looking at edges that have an idea on both side, but we haven't verified if there is an actual link between them yet

	we could just run `getBranches(sg, edge, true).contains(edge.dst)` or `getBranches(sg, edge, false).contains(edge.src)`
	but this will be more efficient
	in the transitive case, we don't need to build a complete list, we just need to check for presence along the way

	@return {Promise<boolean>} true for a edge valid, false for an invalid edge
	@throws if either !sg.getIdea(edge.src) or !sg.getIdea(edge.dst) - so don't call call it unless they have ideas pinned
*/
search.units.verifyEdge = async function verifyEdge(sg, edge) {
	if (edge.link.transitive) {
		// since (Given a->b) A is more specific and B is more general, we will start at A and go towards B
		// (mark --typeOf-> person) we don't want to explore ALL the specific cases
		//
		// search for transitive link
		const { link } = edge; // the link to follow
		const visited = new Set(); // an index of all the places we've been
		const verified = { verified: false };

		const seed = sg.getIdea(edge.src); // a list of all the places to visit; using push and pop for speed; traversal order doesn't matter
		const targetId = sg.getIdea(edge.dst).id;
		await search.units.$verifyEdgeTransitive([seed], link, visited, verified, targetId);

		return verified.verified;
	}
	// else if (!edge.link.transitive)

	const ideas = await sg.getIdea(edge.src).getLinksOfType(edge.link);
	const targetId = sg.getIdea(edge.dst).id;
	return ideas.some((idea) => (idea.id === targetId));
};

/**
	@call `verifyEdge` instead

	this is a recursive helper function for transitive branches; use `verifyEdge` to manage it

	this implementation only works becasue Javascript is single threaded
	`visited` is shared and mutated

	@param next - (new for every call) the list of branches we need to expand
	@param link - (shared, static) the link we are following
	@param visited - (shared, mutated) the ideas we have already expanded  (link from)
	@param verified - (shared, mutated) an object with the boolean result; once any recursive branch is true, then we can stop searching other branches
	@param targetId - (shared, static)  the idea we are looking for along this chain of links
*/
search.units.$verifyEdgeTransitive = async function $verifyEdgeTransitive(next, link, visited, verified, targetId) {
	if (verified.verified) return Promise.resolve();

	return Promise.race(next.map((idea) => {
		// if we loop back to an edge we've already visited, then this circle isn't useful
		// but a different/short path might find it
		if (visited.has(idea.id)) {
			return undefined;
		}

		visited.add(idea.id);

		return idea.getLinksOfType(link).then((list) => {
			// base case: if the list is empty, don't expand any further
			if (list.length === 0) return undefined;

			// if we found things, tag them and keep going
			if (list.some((b) => b.id === targetId)) {
				verified.verified = true;
				return undefined;
			}

			return search.units.$verifyEdgeTransitive(list, link, visited, verified, targetId);
		});
	}));
};

/**
	find the next best edge to expand

	we start with the highest pref (configured on creation)
	and within those batches, pick the one with the least branches

	REVIEW any other heuristics? or is pref/branches enough
*/
search.units.bestEdge = async function bestEdge(sg, edges) {
	const edgeGroups = _.groupBy(edges, 'pref');
	const prefs = Object.keys(edgeGroups)
		.sort((a, b) => (+a > +b ? 1 : -1));

	let selected;

	// higher prefs first
	while (!selected && prefs.length) {
		const pref = prefs.pop();
		const list = await search.units.$bestEdgeOptions(sg, edgeGroups[pref]); // eslint-disable-line no-await-in-loop
		selected = _.minBy(list, 'branches.length');
	}

	return selected;
};

/**
	build the `selected` objects for multiple edges at once
	we are going to need to consider multiple edges anyway, may as well do it in batches
*/
search.units.$bestEdgeOptions = function $bestEdgeOptions(sg, edges) {
	return Promise.all(edges.map((edge) => {
		const isSrc = sg.hasIdea(edge.src);
		const isDst = sg.hasIdea(edge.dst);
		// if they are both true or both false, then we shouldn't consider this edge
		//  - they shouldn't actually be in the list anymore if they are both true
		//  - there isn't anything we can expand upon if they are both false
		if (isSrc === isDst) {
			return Promise.resolve(null);
		}

		// we can't consider this edge if the target object hasn't be identified
		// Future selves: don't keep following pointers; this matcher is based on the contents of the target idea
		const vertex = sg.getVertex(isSrc ? edge.dst : edge.src);
		if (vertex.pointer) {
			const vertexData = (vertex.orData ? vertex.data : [vertex.data]);
			if (!vertexData.some((d) => sg.hasIdea(d))) {
				return Promise.resolve(null);
			}
		}

		return search.units.getBranches(sg, edge, isSrc).then((branches) => ({
			edge,
			branches,
			isForward: isSrc,
		}));
	}));
};

/**
	follow an edge link from a known idea

	@param isForward - does src (true) or dst (false) have a defined id
*/
search.units.getBranches = async function getBranches(sg, edge, isForward) {
	if (edge.link.transitive) {
		// collect all vertices along the link
		const link = (isForward ? edge.link : edge.link.opposite); // the link to follow
		const visited = new Set(); // an index of all the verticies we've visited (link from)
		const branches = new Map(); // the return list of everything we've found (indexed to de-dup, return the values) (link to)

		const seed = sg.getIdea(isForward ? edge.src : edge.dst); // a list of all the places to visit; traversal order doesn't matter
		await search.units.$getBranchesTransitive([seed], link, visited, branches, true);

		return Array.from(branches.values());
	}
	// else if (edge.link.transitive)

	// follow the link and get the ideas
	if (isForward) {
		return sg.getIdea(edge.src).getLinksOfType(edge.link);
	}

	// else if (!isForward)
	return sg.getIdea(edge.dst).getLinksOfType(edge.link.opposite);
};

/**
	@call `getBranches` instead

	this is a recursive helper function for transitive branches; use `getBranches` to manage it

	this implementation only works becasue Javascript is single threaded
	`visited` and `branches` are shared and mutated

	@param next - (new for every call) the list of branches we need to expand
	@param link - (shared, static) the link we are following
	@param visited - (shared, mutated) the ideas we have already expanded  (link from)
	@param branches - (shared, mutated)  the ideas that could be matches (link to)
*/
search.units.$getBranchesTransitive = function $getBranchesTransitive(next, link, visited, branches, isFirst = false) {
	return Promise.all(next.map((idea) => {
		if (!isFirst) {
			// ignore ideas we've already expanded
			if (visited.has(idea.id)) {
				return null;
			}

			visited.add(idea.id);
		}

		return idea.getLinksOfType(link).then((list) => {
			// base case: if the list is empty, don't expand any further
			if (list.length === 0) return null;

			// if we found things, tag them and keep going
			list.forEach((b) => { branches.set(b.id, b); });
			return search.units.$getBranchesTransitive(list, link, visited, branches);
		});
	}));
};

search.units.expandEdges = async function expandEdges(sg, selected) {
	const targetVertexId = (selected.isForward ? selected.edge.dst : selected.edge.src);
	const vertex = sg.getVertex(targetVertexId);
	const vertexData = await sg.getMatchData(targetVertexId, { isSearch: true, useIdeaData: false });

	let matchedBranches;
	switch (vertex.matcher) {
	// id needs a matcher, but doesn't need a promise
	// we'll only really get here if we are using a pointer; otherwise the id will be identified when we create the Subgraph
	case subgraphs.units.matchers.id:
		matchedBranches = selected.branches.filter((idea) => vertexData.some((md) => vertex.matcher(idea, md)));
		break;

	// filler matches all branches
	case subgraphs.units.matchers.filler:
		matchedBranches = selected.branches;
		break;

	// the hard case gets the data, which requires a promise
	// - so we need to map all the results as a promise so we can wait for them all to resolve
	// - once that's done, we can perform the filter
	// - the map returns either the idea or undefined, so we filter on the identity
	default:
		matchedBranches = (await Promise.all(selected.branches.map((idea) => (
			idea.getData().then((d) => {
				const isMatch = vertexData.some((md) => vertex.matcher(d, md));
				return isMatch ? idea : undefined;
			})
		)))).filter(_.identity);
		break;
	}

	// build the results
	if (matchedBranches.length === 0) {
		return [];
	}
	if (matchedBranches.length === 1) {
		// we can reuse Subgraph at the next level
		sg.setIdea(targetVertexId, matchedBranches[0]);
		return [sg];
	}
	// we need to branch; create a new Subgraph instance for each match
	// sans the last one, because we don't need to
	return matchedBranches.map((idea) => {
		const s = new subgraphs.Subgraph(sg);
		s.setIdea(targetVertexId, idea);
		return s;
	});
};
