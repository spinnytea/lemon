const subgraphs = require('./subgraphs');
const rewrite = require('./rewrite');
const subgraphUtils = require('./subgraphUtils');

exports.search = require('./search').search;
exports.match = require('./match').match;

exports.Subgraph = subgraphs.Subgraph;
exports.createSubgraph = subgraphs.createSubgraph;
exports.rewrite = rewrite.rewrite;
exports.convertInnerTransitions = rewrite.convertInnerTransitions;

exports.destroy = subgraphUtils.destroy;
