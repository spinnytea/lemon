const _ = require('lodash');
const utils = require('../../utils');
const config = require('../../config');
const ideas = require('../ideas');
const links = require('../links');
const LazyCopyMap = require('./LazyCopyMap');
const $numbers = require('../../planning/primitives/numbers');

const subgraphs = exports;

class Subgraph {
	/** @param {Subgraph} sg */
	constructor(sg = undefined) {
		if (sg !== undefined && !(sg instanceof Subgraph)) {
			throw new TypeError('sg must be of type Subgraph');
		}

		// this is how we are going to match an idea in the search and match
		// this is the recipe, the way we determined if this vertex can be pinned to the world (or another Subgraph)
		this.$verticies = new LazyCopyMap(sg?.$verticies);

		// this is what we are ultimately trying to find with a Subgraph search
		// pinned context
		this.$idea = new Map(sg?.$idea);

		// theoretical state
		// this is for the rewrite, planning in general
		// if undefined, it hasn't been fetched from idea.getData()
		// set to null if there is no data (so we know not to query again)
		this.$data = new LazyCopyMap(sg?.$data);

		// how the vertices are linked together
		this.$edges = new LazyCopyMap(sg?.$edges);

		// when we generate a new vertex, we need a new key
		// we also want fast access to the number of vertices we have
		this.$vertexCount = (sg ? sg.$vertexCount : 0);

		// when we generate a new edge, we need a new key
		// we also want fast access to the number of edges we have
		this.$edgeCount = (sg ? sg.$edgeCount : 0);

		// optional names of vertices and edges
		this.$vertexAliases = (sg ? sg.$vertexAliases : new Map());
		this.$edgeAliases = (sg ? sg.$edgeAliases : new Map());

		// true
		//   does this represent a specific Subgraph
		//   all of the vertices have a specific ID
		// false
		//   is it a description of something to find
		// cache value for:
		//   sg.$verticies.every(function(v, vertexId) { return (vertexId in sg.$idea); })
		//   Object.keys(sg.$verticies).deep.equals(Object.keys(sg.$idea))
		this.concrete = (sg ? sg.concrete : true);
	}

	/** serialize the Subgraph - so it can be saved */
	stringifySubgraph() {
		this.flatten();

		const verticies = [];
		this.$verticies.data.forEach(({ matcher, data, transitionable, pointer, orData }, id) => { // eslint-disable-line object-curly-newline
			verticies.push({
				i: id,
				m: matcher.name,
				d: data,
				t: transitionable,
				p: pointer,
				od: orData,
			});
		});

		const edges = [];
		this.$edges.data.forEach(({ edgeId, src, link, dst, pref, transitionable }) => { // eslint-disable-line object-curly-newline
			edges.push({
				e: edgeId,
				s: src,
				l: link.name,
				d: dst,
				p: pref,
				t: transitionable,
			});
		});

		const va = {};
		this.$vertexAliases.forEach((id, name) => {
			va[name] = id;
		});
		const ea = {};
		this.$edgeAliases.forEach((id, name) => {
			ea[name] = id;
		});

		return JSON.stringify({
			v: verticies,
			i: Array.from(this.$idea.entries()).map(([id, idea]) => [id, idea.id]),
			d: Array.from(this.$data.data.entries()),
			e: edges,
			va,
			ea,
			c: this.concrete,
		});
	}

	/** read the serialized Subgraph - so it can be loaded from an archive */
	static parse(json) {
		json = JSON.parse(json);
		const sg = new Subgraph();

		json.v.forEach(({ i, m, d, t, p, od }) => { // eslint-disable-line object-curly-newline
			sg.$verticies.set(i, {
				matcher: subgraphs.units.matchers[m],
				data: d,
				transitionable: t,
				pointer: p,
				orData: od,
			});
		});

		json.i.forEach(([id, idea]) => {
			sg.$idea.set(id, ideas.proxy(idea));
		});

		json.d.forEach(([id, data]) => {
			sg.$data.set(id, data);
		});

		json.e.forEach(({ e, s, l, d, p, t }) => { // eslint-disable-line object-curly-newline
			sg.$edges.set(e, {
				edgeId: e,
				src: s,
				link: links.get(l),
				dst: d,
				pref: p,
				transitionable: t,
			});
		});

		_.forEach(json.va, (id, name) => {
			sg.$vertexAliases.set(name, id);
		});
		_.forEach(json.ea, (id, name) => {
			sg.$edgeAliases.set(name, id);
		});

		sg.$vertexCount = sg.$verticies.data.size;
		sg.$edgeCount = sg.$edges.data.size;
		sg.concrete = json.c;

		return sg;
	}

	/**
		given a complete subgraph, flush all the ideas to disk

		this way, we don't have to save ALL of ideas.memory,
		but we can save this specific subgraph

		e.g.
		after an actual rewrite, once the changes have been made
		we can flush our memory to disk immediately (if needed)
	*/
	async save() {
		if (!this.concrete) throw new RangeError('Subgraph must be concrete');
		return Promise.all(Array.from(this.$idea.values()).map((idea) => subgraphs.boundaries.saveIdea(idea)));
	}

	flatten() {
		this.$verticies.flatten();
		this.$data.flatten();
		this.$edges.flatten();
	}

	equals(sg) {
		if (this.$edgeCount !== sg.$edgeCount) return false;
		if (this.$vertexCount !== sg.$vertexCount) return false;

		for (let e = 0; e < this.$edgeCount; e += 1) {
			const te = this.$edges.get(e);
			const se = sg.$edges.get(e);
			if (te.src !== se.src) return false;
			if (te.link !== se.link) return false;
			if (te.dst !== se.dst) return false;
		}

		for (let v = 0; v < this.$vertexCount; v += 1) {
			const td = this.$data.get(v);
			const sd = sg.$data.get(v);
			if (!_.isEqual(td, sd)) return false;
		}

		return true;
	}

	// #setup

	$refVertex(vertexIn) {
		const vertexId = (_.isString(vertexIn) ? this.$vertexAliases.get(vertexIn) : vertexIn);
		if (!_.isNumber(vertexId)) throw new TypeError(`"${vertexIn}" (${typeof vertexIn}) is not a vertex`);
		if (vertexId >= this.$vertexCount || vertexId < 0) throw new RangeError(`id "${vertexId}" (${vertexIn}) is not a vertex`);
		return vertexId;
	}

	$refEdge(edgeIn) {
		const edgeId = (_.isString(edgeIn) ? this.$edgeAliases.get(edgeIn) : edgeIn);
		if (!_.isNumber(edgeId)) throw new TypeError(`"${edgeIn}" (${typeof edgeIn}) is not an edge`);
		if (edgeId >= this.$edgeCount || edgeId < 0) throw new RangeError(`id "${edgeId}" (${edgeIn}) is not an edge`);
		return edgeId;
	}

	/**
		add a vertex to the graph
		this only specifies match data
		the other parts (ideas / data) need to be found later

		@param {Function|string} matcher - one of subgraphs.units.matchers
		@param {any} data - passed to the matcher
		@param options
		@param {boolean} options.transitionable - if true, we are intending to change the value
			(subgraphs.rewrite, blueprints, etc; subgraphs.rewrite(transitions);)
		@param {boolean} options.pointer - if true, use sg.getIdea(vertex.data).getData() instead of vertex.data
			(data is the the ID of vertex; we use the actual data [from disk] as the matching data)
			(it doesn't make sense to use this with matchers.filler)
			(normally, we use the matcher fn + this data; instead, data is a vertexId)
			(this is basically a variable; a matcher that uses runtime data)
		@param {string} options.name - optional name for this vertex for getting/setting
		@param {string} options.orData - normally data is a single value; instead, data is an array where anything in it can match
	*/
	addVertex(matcher, data, { transitionable = false, pointer = false, name = undefined, orData = false } = {}) {
		if (_.isString(matcher)) {
			if (!(matcher in subgraphs.units.matchers)) throw new RangeError(`unkown matcher "${matcher}"`);
			matcher = subgraphs.units.matchers[matcher];
		}
		if (!matcher || matcher !== subgraphs.units.matchers[matcher.name]) {
			if (matcher?.name) throw new RangeError(`unkown matcher "${matcher.name}"`);
			throw new RangeError('invalid matcher');
		}
		if (matcher !== subgraphs.units.matchers.filler && data === undefined) throw new RangeError('vertex data must be defined');
		if (matcher === subgraphs.units.matchers.filler && pointer === true) throw new RangeError('filler matchers cannot also be options.pointer');
		if (matcher === subgraphs.units.matchers.filler && orData === true) throw new RangeError('filler matchers cannot also be options.orData');
		if (matcher === subgraphs.units.matchers.filler && data !== undefined) throw new RangeError('filler should not have vertex data');
		if (orData === true && !Array.isArray(data)) throw new RangeError('when using options.orData, data must be an array');

		// TODO upcast to orData by default to make sure everything is implemented, the remove the variable
		//  - maybe orData can be handled at the matcher level?
		//  - voData is ALWAYS singular - viData is an array in all cases
		/*
		// upcast to orData
		if (orData === false && matcher !== subgraphs.units.matchers.filler) {
			data = [data];
			orData = true;
		}
		*/

		if (pointer === true) {
			try {
				if (orData) {
					data.forEach((d) => { this.$refVertex(d); });
				}
				else {
					this.$refVertex(data);
				}
			}
			catch (e) {
				throw new RangeError('options.pointer target (vertex.data) must already be a vertex');
			}
		}

		if (!_.isBoolean(transitionable)) throw new TypeError(`invalid options.transitionable: typeof "${typeof transitionable}"`);
		if (!_.isBoolean(pointer)) throw new TypeError(`invalid options.pointer: typeof "${typeof pointer}"`);
		if (!_.isBoolean(orData)) throw new TypeError(`invalid options.orData: typeof "${typeof orData}"`);
		if (name !== undefined && this.$vertexAliases.has(name)) throw new Error(`options.name "${name}" is already used`);

		if (matcher === subgraphs.units.matchers.substring && !pointer) {
			if (orData) {
				data.forEach((d) => { d.value = d.value.toLowerCase(); });
			}
			else {
				data.value = data.value.toLowerCase();
			}
		}

		const vertexId = this.$vertexCount;
		this.$vertexCount += 1;
		if (name) this.$vertexAliases.set(name, vertexId);

		this.$verticies.set(vertexId, {
			matcher,
			data,
			transitionable,
			pointer,
			orData,
		});

		if (matcher === subgraphs.units.matchers.id && !pointer && !orData) {
			const proxy = ideas.proxy(data);
			this.$verticies.get(vertexId).data = proxy.id; // ensure vertex.data using string id (not proxy)
			this.$idea.set(vertexId, proxy); // store my idea
		}
		else {
			this.concrete = false;
		}

		return vertexId;
	}

	/**
	 	add an edge to the graph

		@param {number | string} src - source vertexId (or alias of vertex)
		@param {Object | string} link - the link from src to dst `link = links.get(link);`
		@param {number | string} dst - destination vertexId (or alias of vertex)
		@param options
		@param {number} options.pref - higher prefs will be considered first (default: 0)
		@param {boolean} options.transitionable - can the edge be changed by actions (subgraphs.rewrite)
		@param {string} options.name - optional name for this edge for getting/setting
	*/
	addEdge(src, link, dst, { pref = 0, transitionable = false, name = undefined } = {}) {
		src = this.$refVertex(src);
		dst = this.$refVertex(dst);
		link = links.cast(link);

		if (!_.isNumber(pref)) throw new TypeError('invalid options.pref');
		if (!_.isBoolean(transitionable)) throw new TypeError('invalid options.transitionable');
		if (name !== undefined && this.$edgeAliases.has(name)) throw new Error(`options.name "${name}" is already used`);

		// normalize the edge representation
		//  - this way, when we search, we don't need to check the edge direction
		//  - this way, searches will always have edges pointing in the same direction, and we don't need to flip them at search time
		if (links.isOpp(link)) {
			const tmp = src;
			src = dst;
			dst = tmp;
			link = link.opposite;
		}
		// for undirected edges, src and dst don't really matter
		// BUT, we may as well make an attempt
		else if (link === link.opposite) {
			if (src > dst) {
				const tmp = src;
				src = dst;
				dst = tmp;
			}
		}

		const edgeId = this.$edgeCount;
		this.$edgeCount += 1;
		if (name) this.$edgeAliases.set(name, edgeId);

		// rejected options
		// byIdeaLink
		//  - during subgraphs.match, instead of matching Subgraph edges uses the existing idea link
		//  - we can't do this because the Subgraph represents our imagination, we can't plan ahead if we don't let the Subgraph contain ALL the information
		this.$edges.set(edgeId, {
			edgeId,
			src,
			link,
			dst,
			pref,
			transitionable,
		});

		this.concrete = false;

		return edgeId;
	}

	/**
		add all the leaves to the graph

		given a concrete subgraph
		traverse the edges through known verticies
		add all the final ones to the graph

		@param {number | string | number[] | string[]} src - source vertexId (or alias of vertex)
		@param {Object[] | string[]} path - links to follow (must match existing edges)
		@param {Object | string} leaf - the link from a known src to an unknown dst; will add all matching edges and verticies
	*/
	async grow(src, path) {
		if (!this.concrete) throw new RangeError('Subgraph must be concrete');
		if (!Array.isArray(src)) src = [src];
		src = src.map((s) => this.$refVertex(s));
		path = path.map((l) => links.cast(l));
		const leaf = path.pop();
		if (!leaf) return;

		const vertexIds = subgraphs.units.growPath(this, src, path);
		await subgraphs.units.growLeaf(this, vertexIds, leaf);
	}

	// #ideas

	/**
		@alias getMatch
	*/
	getVertex(vertexId) {
		return this.$verticies.get(this.$refVertex(vertexId));
	}

	setIdea(vertexId, idea) {
		if (!idea) return this.deleteIdea(vertexId);
		return this.$idea.set(this.$refVertex(vertexId), idea);
	}

	getIdea(vertexId) {
		return this.$idea.get(this.$refVertex(vertexId));
	}

	hasIdea(vertexId) {
		return this.$idea.has(this.$refVertex(vertexId));
	}

	allIdeas() {
		return new Map(this.$idea);
	}

	deleteIdea(vertexId) {
		this.concrete = false;
		return this.$idea.delete(this.$refVertex(vertexId));
	}

	isIdeaEmpty() {
		return this.$idea.size === 0;
	}

	isIdeaComplete() {
		return this.$idea.size === this.$vertexCount;
	}

	// #data

	async getMatchData(vertexId, { outer = null, innerToOuterVertexMap = null, isSearch = false, useIdeaData = true } = {}) {
		const vertex = this.getVertex(vertexId);

		// filler will match anything, so don't waste time fetching data
		if (vertex.matcher === subgraphs.units.matchers.filler) return [];

		let vertexData;

		// XXX ? remove option.useIdeaData from Subgraph.getMatchData
		// - subgraph search does not (did not) need to check the sugraph data
		// - in theory, this will never hit, because it's only checking unpinned ideas
		// - is checking idea data a waste of times?
		// - is checking idea data a useful way to alter the search?
		if (useIdeaData) {
			vertexData = await this.getData(vertexId);
			// if the data is cached, always use that
			if (vertexData !== undefined) return [vertexData];
			// if the idea is mapped, then it should have data
			if (this.hasIdea(vertexId)) return [];
		}

		// otherwise, use the vertex data
		vertexData = (vertex.orData ? vertex.data : [vertex.data]);

		if (!vertex.pointer) {
			// an idea isn't pinned down, we won't have data for it yet
			// when doing a
			//  - subgraph search (tring to pin ideas)
			//  - blueprint search (check abstract goal)
			if (isSearch) {
				return vertexData;
			}

			// if this is not pointer, then there is nowhere else to look
			// when doing a
			//  - subgraph match (mapping an inner to an outer)
			return [];
		}

		// if this is a pointer...
		// (and doesn't have and idea mapped)

		return Promise.all(vertexData.map(async (d) => {
			// if our inner graph has a value cached for the target (if the target of the pointer already has data loaded)
			// REVIEW when and why do we need this? shouldn't we always use the outer subgraph for our data?
			const data = await this.getData(d);
			if (data !== undefined) {
				return data;
			}

			// NOTE we do not need to use vertex data for goals
			// the definition of a pointer is to "use the target getData"
			// any goal check will also have outer, so we should just be using that

			// if we have already mapped the target vertex, then use the outer data
			// (mapped, but the inner hasn't been updated with the idea data)
			// (note: we may not have mapped the pointer target by this point, and that's okay)
			if (outer && innerToOuterVertexMap && innerToOuterVertexMap.has(d)) {
				const outerVertexId = innerToOuterVertexMap.get(d);
				return outer.getData(outerVertexId);
			}

			// we can't find data to use (this is okay)
			return undefined;
		})).then((mds) => mds.filter((md) => md !== undefined));
	}

	async getData(vertexId) {
		vertexId = this.$refVertex(vertexId);
		const data = this.$data.get(vertexId);

		if (data === null) return Promise.resolve(undefined);
		if (data !== undefined) return Promise.resolve(data);

		const idea = this.$idea.get(vertexId);
		if (idea === undefined) return Promise.resolve(undefined);

		return idea.getData().then((d) => {
			if (d === undefined) {
				this.setData(vertexId, null);
			}
			else {
				this.setData(vertexId, d);
			}
			return d;
		});
	}

	setData(vertexId, data) {
		return this.$data.set(this.$refVertex(vertexId), data);
	}

	deleteData(...args) {
		if (args.length) {
			// only reset the ids in the arguments
			args.forEach((vertexId) => {
				this.setData(this.$refVertex(vertexId), undefined);
			});
		}
		else {
			// reset all vertices
			this.$data = new LazyCopyMap();
		}
	}

	// #edges

	getEdge(edgeId) {
		return this.$edges.get(this.$refEdge(edgeId));
	}

	updateEdge(edgeId, src, dst) {
		edgeId = this.$refEdge(edgeId);
		const { ...edge } = this.$edges.get(edgeId);
		edge.src = src;
		edge.dst = dst;
		this.$edges.set(edgeId, edge);
	}

	allEdges() {
		this.flatten();
		return Array.from(this.$edges.data.values());
	}
}

subgraphs.Subgraph = Subgraph;

/** @deprecated just do all this directly; it's not any shorter - unless we find a good reason to do it */
subgraphs.createSubgraph = (verticies, edges) => {
	const sg = new Subgraph();
	verticies.forEach((args) => sg.addVertex(...args));
	edges.forEach((args) => sg.addEdge(...args));
	return sg;
};

/**
	@param {Function} matcher - a uniquely named function
*/
subgraphs.createMatcher = (matcher) => subgraphs.units.createMatcher(matcher);

subgraphs.projectSubgraph = (outer, inner, { innerToOuterVertexMap }) => subgraphs.units.projectSubgraph(outer, inner, { innerToOuterVertexMap });

/*
	UNITS
*/

Object.defineProperty(subgraphs, 'units', { value: {} });

subgraphs.units.matchers = {
	// XXX matchers.id is a special case that does not match the standard function args
	id: function id(idea, vertexData) {
		return (vertexData.id || vertexData) === idea.id;
	},
	// filler is meant to be used when we don't care about the value of node
	// like, the vertex is simply structural, and won't have any data within it
	// comparators will always return true, distances will ALWAYS be zero
	// this is not a "relaxed matcher" this is a "non-matcher" or "everything matches don't check"
	filler: function filler() {
		return true;
	},
	substring: function substring(data, vertexData) {
		if (vertexData.path && vertexData.path.length) {
			data = _.property(vertexData.path)(data);
		}
		if (!_.isString(data)) {
			return false;
		}
		return data.toLowerCase().includes(vertexData.value);
	},
};

subgraphs.units.createMatcher = function createMatcher(matcher) {
	if (!_.isFunction(matcher) || !matcher.name) throw new Error('all matchers must be named functions');
	if (matcher.name in subgraphs.units.matchers) throw new Error(`matcher "${matcher.name}" is already defined`);
	subgraphs.units.matchers[matcher.name] = matcher;
};

/**
	@alias createGoal
*/
subgraphs.units.projectSubgraph = function projectSubgraph(outer, inner, { innerToOuterVertexMap }) {
	inner = new Subgraph(inner);
	innerToOuterVertexMap.forEach((o, i) => {
		inner.$idea.set(i, outer.$idea.get(o));
		inner.$data.set(i, outer.$data.get(o));
	});
	inner.concrete = true;
	return inner;
};

/** follow the edges from vertexId in this subgraph */
subgraphs.units.growPath = function growPath(sg, vertexIds, path) {
	if (!path.length) return vertexIds;

	const edges = sg.allEdges();
	path.forEach((link) => {
		if (!link.transitive) {
			const nextVertexIds = new Set();
			subgraphs.units.$growPathLink(link, edges, vertexIds, nextVertexIds);
			vertexIds = Array.from(nextVertexIds);
		}
		// transitive is the same as non-transitive, just in a loop with an accumulator
		else {
			const cumulativeVertexIds = new Set();

			let currVertexIds = vertexIds;
			while (currVertexIds.length > 0) {
				const nextVertexIds = new Set();
				subgraphs.units.$growPathLink(link, edges, currVertexIds, nextVertexIds);

				// remove the ones we've visited to avoid retracing
				cumulativeVertexIds.forEach((id) => { nextVertexIds.delete(id); });

				// shift
				currVertexIds = Array.from(nextVertexIds);

				// accumulate
				currVertexIds.forEach((id) => { cumulativeVertexIds.add(id); });
			}

			vertexIds = Array.from(cumulativeVertexIds);
		}
	});

	return vertexIds;
};

subgraphs.units.$growPathLink = function $growPathLink(link, edges, currVertexIds, nextVertexIds) {
	edges.forEach((edge) => {
		if (edge.link === link && currVertexIds.includes(edge.src)) {
			nextVertexIds.add(edge.dst);
		}
		// this also undirected since undirected === undirected.oppposite
		else if (edge.link === link.opposite && currVertexIds.includes(edge.dst)) {
			nextVertexIds.add(edge.src);
		}
	});
};

subgraphs.units.growLeaf = async function growLeaf(sg, vertexIds, link) {
	if (!vertexIds.length) return;

	const visited = new Set(); // prevent revisiting verticies
	const edges = sg.allEdges(); // prevent duplicate edges

	await Promise.all(vertexIds.map(async (vertexId) => (
		subgraphs.units.$growLeafLink(sg, vertexId, link, visited, edges)
	)));

	sg.concrete = true;
};

subgraphs.units.$growLeafLink = async function $growLeafLin(sg, src, link, visited, edges) {
	visited.add(src);

	await sg.$idea.get(src).getLinksOfType(link).then((list) => (
		list.map((idea) => {
			let vi = utils.findByValue(sg.$idea, (i) => i.id === idea.id);
			if (vi === undefined) vi = sg.addVertex('id', idea);
			const hasEdge = edges.some((e) => {
				if (e.src === src && e.link === link && e.dst === vi) return true;
				if (e.src === vi && e.link === link && e.dst === src) return true;
				if (e.src === src && e.link.opposite === link && e.dst === vi) return true;
				if (e.src === vi && e.link.opposite === link && e.dst === src) return true;
				return false;
			});

			if (!hasEdge) {
				sg.addEdge(src, link, vi);
				edges.push({ src, link, dst: vi });
			}

			if (link.transitive && !visited.has(vi)) {
				return subgraphs.units.$growLeafLink(sg, vi, link, visited, edges);
			}

			return undefined;
		})
	));
};

/*
	BOUNDARIES
*/

Object.defineProperty(subgraphs, 'boundaries', { value: {} });

subgraphs.boundaries.saveIdea = (idea) => ideas.save(idea);

/*
	INIT
*/

config.onInit((first) => {
	if (first) {
		/* eslint-disable prefer-arrow-callback */
		subgraphs.createMatcher(function exact(data, vertexData) {
			return _.isEqual(data, vertexData);
		});
		subgraphs.createMatcher(function similar(data, vertexData) {
			// vertexData should be contained within data
			return _.isMatch(data, vertexData);
		});
		subgraphs.createMatcher(function numbers(data, vertexData) {
			// AC: numbers.difference(voData, viData), viData could be vertexData (it will be for goals)
			// REVIEW if these vertices are over precise, we will never match
			//  - if a recipe says "i need 3 apples" and you can only get them in pairs, you'll never find the answer
			//  - numbers are always ranges; the recipe may call for exactly 3, but you can have a few extra
			// ----
			//  - I'm not sure what to do about this, when it's okay to specify one vs the other
			//  - "specification" is rigid, but "goals as means" and "goals as ends" can be more relaxed?
			return $numbers.match(data, vertexData);
		});
		/* eslint-enable prefer-arrow-callback */
	}
});
