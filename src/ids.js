/*
	generate/increment IDs
	acts the same as counting (1, 2, 3, ... 9, 10, 11, ...) but with a larger character set
*/
const config = require('./config');
const utils = require('./utils');

const ids = exports;

ids.next = utils.transaction((key, initialValue = '0') => (
	config.get(key, initialValue)
		.then((nextID) => ids.units.increment(nextID))
		.then((nextID) => config.set(key, nextID))
));

/*
	UNITS
*/

Object.defineProperty(ids, 'units', { value: {} });

ids.units.tokens = Object.freeze([
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', // numbers
	// 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', // upper case letters
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', // lower case letters
]);

ids.units.replaceAt = function replaceAt(str, index, character) {
	return str.substr(0, index) + character + str.substr(index + character.length);
};

ids.units.increment = function increment(oldID) {
	let nextID = oldID;
	if (typeof nextID !== 'string') throw new TypeError('nextID must be a string');
	const { tokens, replaceAt } = ids.units;

	let index = nextID.length - 1;
	while (index > -2) {
		if (index === -1) {
			return tokens[1] + nextID; // 10000...
		}

		// get the next token index
		const idx = tokens.indexOf(nextID.charAt(index)) + 1;

		if (idx === tokens.length) {
			// if we can't increase this index, then wrap to 0 and increase the next
			// roll over (99 -> 100)
			nextID = replaceAt(nextID, index, tokens[0]);
		}
		else {
			// if we can increase this index, then do that
			nextID = replaceAt(nextID, index, tokens[idx]);
			// we are at the end, so don't keep going
			index = -1;
		}

		index -= 1;
	}

	return nextID;
};
