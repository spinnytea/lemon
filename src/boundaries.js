const fs = require('fs');
const mkdirp = require('mkdirp');

const boundaries = exports;

/*
	Standard Javascript that we always mock during tests
*/

boundaries.setTimeout = (
	(...args) => setTimeout(...args)
);
boundaries.clearTimeout = (
	(...args) => clearTimeout(...args)
);

/*
	filesystem api
*/

boundaries.existsSync = (
	(...args) => fs.existsSync(...args)
);
boundaries.readFileSync = (
	(...args) => fs.readFileSync(...args)
);
boundaries.readFile = (
	async (...args) => fs.promises.readFile(...args)
);
boundaries.writeFile = (
	async (...args) => fs.promises.writeFile(...args)
);
boundaries.deleteFile = (
	async (...args) => fs.promises.unlink(...args)
);

// REVIEW can we use the async version or mkdirp? when we create a bunch of ideas with the same ids, will this clobber itself?
// TODO this should return a promise NOW so we don't have to work about it later
boundaries.mkdirp = (
	(...args) => mkdirp.sync(...args)
);
