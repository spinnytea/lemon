const boundaries = require('../src/boundaries');
const config = require('../src/config');

describe('config', () => {
	afterEach(() => {
		boundaries.existsSync.mockImplementationOnce(() => true);
		boundaries.readFileSync.mockImplementationOnce(() => '{}');

		config.init({ in_memory: true, location: 'test_data' });
	});

	test('config.units', () => {
		expect(Object.keys(config.units)).toEqual([
			'data',
			'setValue',
			'getValue',
			'hasInit',
			'onInitCallbacks',
			'$saveTimeout',
			'$writing',
			'save',
			'readFileSync',
			'writeFile',
		]);
	});

	test('data', () => {
		// there really isn't anything to test
		// it's just an object where we can store some data
		// it has to be initialized before we can run any tests
		expect(typeof config.units.data).toBe('object');
	});

	describe('setValue', () => {
		const key = 'someKey';
		const value1 = 'some value 1';
		const value2 = 'some value 2';

		test('value did not exist before', () => {
			expect(config.units.data).not.toHaveProperty(key);
			expect(config.units.setValue(key, value1)).toBe(value1);
			expect(config.units.data).toHaveProperty(key);
		});

		test('value did exist before', () => {
			config.units.data[key] = value1; // fake it for the test

			expect(config.units.setValue(key, value2)).toBe(value2);
			expect(config.units.data[key]).toBe(value2);
		});

		test('invalid args', () => {
			expect(() => { config.units.setValue(); }).toThrow(/^config must specify a key and default value$/);
			expect(() => { config.units.setValue(key); }).toThrow(/^config must specify a key and default value$/);
			expect(() => { config.units.setValue(key, value1); }).not.toThrow(); // this one is valid
			expect(() => { config.units.setValue(1, value1); }).toThrow(/^key must be a string$/);
			expect(() => { config.units.setValue(key, value1, value2); }).toThrow(/^config must specify a key and default value$/);
		});
	});

	describe('getValue', () => {
		const key = 'someKey';
		const value1 = 'some value 1';
		const value2 = 'some value 2';

		test('value did not exist before', () => {
			expect(config.units.data).not.toHaveProperty(key);
			expect(config.units.getValue(key, value1)).toBe(value1);
			expect(config.units.data).not.toHaveProperty(key);
		});

		test('value did exist before', () => {
			config.units.data[key] = value1; // fake it for the test

			expect(config.units.getValue(key, value2)).toBe(value1);
			expect(config.units.data[key]).toBe(value1);
		});

		test('invalid args', () => {
			expect(() => { config.units.getValue(); }).toThrow(/^config must specify a key and value$/);
			expect(() => { config.units.getValue(key); }).toThrow(/^config must specify a key and value$/);
			expect(() => { config.units.getValue(key, value1); }).not.toThrow(); // this one is valid
			expect(() => { config.units.getValue(1, value1); }).toThrow(/^key must be a string$/);
			expect(() => { config.units.getValue(key, value1, value2); }).toThrow(/^config must specify a key and value$/);
		});
	});

	test('hasInit', () => {
		expect(config.units.hasInit).toBe(true); // we have to init before anything can run
	});

	test('onInitCallbacks', () => {
		expect(config.units.onInitCallbacks).toBeInstanceOf(Array);
	});

	test('$saveTimeout', () => {
		// this shouldn't e used for anything
		// really, it should just be a local variable
		expect(config.units).toHaveProperty('$saveTimeout');
	});

	test('$writing', () => {
		// this shouldn't e used for anything
		// really, it should just be a local variable
		expect(config.units).toHaveProperty('$writing');
	});

	describe('save', () => {
		test('in_memory is a noop', async () => {
			expect(config.settings.in_memory).toBe(true);
			expect(boundaries.setTimeout).not.toHaveBeenCalled();
			expect(boundaries.clearTimeout).not.toHaveBeenCalled();
			expect(boundaries.writeFile).not.toHaveBeenCalled();

			await config.units.save();

			expect(boundaries.setTimeout).not.toHaveBeenCalled();
			expect(boundaries.clearTimeout).not.toHaveBeenCalled();
			expect(boundaries.writeFile).not.toHaveBeenCalled();
		});

		describe('fs', () => {
			beforeEach(() => {
				boundaries.existsSync.mockImplementationOnce(() => true);
				boundaries.readFileSync.mockImplementationOnce(() => '{"mock":"data"}');

				config.init({ in_memory: false, location: 'test_data' });

				boundaries.existsSync.mockReset();
				boundaries.existsSync.mockImplementation(() => { throw new Error('must re-mock "existsSync"'); });
				boundaries.readFileSync.mockReset();
				boundaries.readFileSync.mockImplementation(() => { throw new Error('must re-mock "readFileSync"'); });
			});

			test('once', async () => {
				let doSave;
				let setTimeoutCount = 0;
				boundaries.setTimeout.mockImplementation((fn) => { doSave = fn; setTimeoutCount += 1; return setTimeoutCount; });
				let doWrite;
				boundaries.writeFile.mockImplementationOnce(() => new Promise((resolve) => { doWrite = resolve; }));
				boundaries.clearTimeout.mockImplementation(() => {}); // nothing to do

				expect(boundaries.setTimeout).not.toHaveBeenCalled();
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(config.units.data).toEqual({ mock: 'data' });
				expect(config.units.$saveTimeout).toBe(null);
				expect(config.units.$writing).toBe(false);

				// start off the save
				const promise = config.units.save();

				expect(boundaries.setTimeout.mock.calls.length).toBe(1);
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(doSave).not.toBe(undefined);
				expect(doWrite).toBe(undefined);
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(false);

				doSave(); // resolve the timeout

				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(boundaries.writeFile).toHaveBeenCalledWith(
					'test_data/_settings_testing.json',
					'{\n  "mock": "data"\n}',
					{ encoding: 'utf8' },
				);
				expect(doWrite).not.toBe(undefined);
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(true);

				doWrite(); // resolve the write

				await promise;

				expect(config.units.data).toEqual({ mock: 'data' });
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(false);

				expect(boundaries.setTimeout.mock.calls.length).toBe(1);
				expect(boundaries.clearTimeout.mock.calls.length).toBe(1);
				expect(boundaries.existsSync).not.toHaveBeenCalled();
				expect(boundaries.readFileSync).not.toHaveBeenCalled();
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
			});

			test('many times', async () => {
				const doSave = [];
				boundaries.setTimeout.mockImplementation((fn) => { doSave.push(fn); return doSave.length - 1; });
				const doWrite = [];
				boundaries.writeFile.mockImplementationOnce(() => new Promise((resolve) => { doWrite.push(resolve); }));
				boundaries.clearTimeout.mockImplementation((idx) => { if (idx !== null) doSave[idx] = null; });

				expect(boundaries.setTimeout).not.toHaveBeenCalled();
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(config.units.data).toEqual({ mock: 'data' });
				expect(config.units.$saveTimeout).toBe(null);
				expect(config.units.$writing).toBe(false);

				const promise1 = config.units.save();

				expect(boundaries.setTimeout.mock.calls.length).toBe(1);
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(doSave).toEqual([expect.any(Function)]);
				expect(doWrite).toEqual([]);
				expect(config.units.$saveTimeout).toBe(0);
				expect(config.units.$writing).toBe(false);

				const promise2 = config.units.save();

				expect(boundaries.setTimeout.mock.calls.length).toBe(2);
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(doSave).toEqual([null, expect.any(Function)]);
				expect(doWrite).toEqual([]);
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(false);

				doSave[1]();

				expect(boundaries.setTimeout.mock.calls.length).toBe(2);
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(doSave).toEqual([null, expect.any(Function)]);
				expect(doWrite).toEqual([expect.any(Function)]);
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(true);

				const promise3 = config.units.save();

				expect(boundaries.setTimeout.mock.calls.length).toBe(3);
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(doSave).toEqual([null, null, expect.any(Function)]);
				expect(doWrite).toEqual([expect.any(Function)]);
				expect(config.units.$saveTimeout).toBe(2);
				expect(config.units.$writing).toBe(true);

				doSave[2](); // timeout resolves during save

				expect(boundaries.setTimeout.mock.calls.length).toBe(4);
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(doSave).toEqual([null, null, null, expect.any(Function)]);
				expect(doWrite).toEqual([expect.any(Function)]);
				expect(config.units.$saveTimeout).toBe(3);
				expect(config.units.$writing).toBe(true);

				doWrite[0]();

				expect(boundaries.setTimeout.mock.calls.length).toBe(4);
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(doSave).toEqual([null, null, null, expect.any(Function)]);
				expect(doWrite).toEqual([expect.any(Function)]);
				expect(config.units.$saveTimeout).toBe(3);
				expect(config.units.$writing).toBe(true);

				await promise1;
				await promise2;
				await promise3;

				expect(boundaries.setTimeout.mock.calls.length).toBe(4);
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(doSave).toEqual([null, null, null, expect.any(Function)]);
				expect(doWrite).toEqual([expect.any(Function)]);
				expect(config.units.$saveTimeout).toBe(3);
				expect(config.units.$writing).toBe(false);
			});

			test('error', async () => {
				let doSave;
				let setTimeoutCount = 0;
				boundaries.setTimeout.mockImplementation((fn) => { doSave = fn; setTimeoutCount += 1; return setTimeoutCount; });
				let doWrite;
				boundaries.writeFile.mockImplementationOnce(() => new Promise((resolve, reject) => { doWrite = reject; }));
				boundaries.clearTimeout.mockImplementation(() => {}); // nothing to do

				expect(boundaries.setTimeout).not.toHaveBeenCalled();
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(config.units.data).toEqual({ mock: 'data' });
				expect(config.units.$saveTimeout).toBe(null);
				expect(config.units.$writing).toBe(false);

				// start off the save
				const promise = config.units.save();

				expect(boundaries.setTimeout.mock.calls.length).toBe(1);
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(doSave).not.toBe(undefined);
				expect(doWrite).toBe(undefined);
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(false);

				doSave(); // resolve the timeout

				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(boundaries.writeFile).toHaveBeenCalledWith(
					'test_data/_settings_testing.json',
					'{\n  "mock": "data"\n}',
					{ encoding: 'utf8' },
				);
				expect(doWrite).not.toBe(undefined);
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(true);

				doWrite(new Error('pretend to fail')); // reject the write

				await promise.then(
					() => { throw new Error('should reject'); },
					(err) => { expect(err.message).toBe('pretend to fail'); },
				);

				expect(config.units.data).toEqual({ mock: 'data' });
				expect(config.units.$saveTimeout).toBe(1);
				expect(config.units.$writing).toBe(false);

				expect(boundaries.setTimeout.mock.calls.length).toBe(1);
				expect(boundaries.clearTimeout.mock.calls.length).toBe(1);
				expect(boundaries.existsSync).not.toHaveBeenCalled();
				expect(boundaries.readFileSync).not.toHaveBeenCalled();
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
			});
		});
	});

	describe('readFileSync', () => {
		test('resolve, exists', () => {
			boundaries.existsSync.mockReturnValueOnce(true);
			boundaries.readFileSync.mockReturnValueOnce('{ "some": "data" }');

			expect(config.units.readFileSync('some/file.json')).toEqual({ some: 'data' });

			expect(boundaries.existsSync).toHaveBeenCalledWith('some/file.json');
			expect(boundaries.readFileSync).toHaveBeenCalledWith('some/file.json', { encoding: 'utf8' });
		});

		test('resolve, does not exist', () => {
			boundaries.existsSync.mockReturnValueOnce(false);
			boundaries.readFileSync.mockReturnValueOnce('{ "some": "data" }');

			expect(() => config.units.readFileSync('some/file.json')).toThrow(/^config file does not exist: "some\/file.json"$/);

			expect(boundaries.existsSync).toHaveBeenCalledWith('some/file.json');
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
		});

		test('reject', () => {
			boundaries.existsSync.mockReturnValueOnce(true);
			boundaries.readFileSync.mockImplementationOnce(() => { throw new Error('mock error'); });

			expect(() => config.units.readFileSync('some/file.json')).toThrow(/^mock error$/);

			expect(boundaries.existsSync).toHaveBeenCalledWith('some/file.json');
			expect(boundaries.readFileSync).toHaveBeenCalledWith('some/file.json', { encoding: 'utf8' });
		});
	});

	describe('writeFile', () => {
		test('resolve', async () => {
			boundaries.writeFile.mockImplementationOnce(() => Promise.resolve());
			expect(boundaries.writeFile).not.toHaveBeenCalled();

			const promise = config.units.writeFile('some/file.txt', { some: 'data' });

			expect(boundaries.writeFile.mock.calls.length).toBe(1);
			expect(boundaries.writeFile).toHaveBeenCalledWith(
				'some/file.txt',
				'{\n  "some": "data"\n}',
				{ encoding: 'utf8' },
			);

			await promise.then(
				(o) => { expect(o).toBe(undefined); },
				() => { throw new Error('should resolve'); },
			);

			expect(boundaries.setTimeout).not.toHaveBeenCalled();
			expect(boundaries.clearTimeout).not.toHaveBeenCalled();
			expect(boundaries.existsSync).not.toHaveBeenCalled();
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.writeFile.mock.calls.length).toBe(1);
		});

		test('reject', async () => {
			boundaries.writeFile.mockImplementationOnce(() => Promise.reject(new Error('mock failure')));
			expect(boundaries.writeFile).not.toHaveBeenCalled();

			const promise = config.units.writeFile('some/file.txt', { some: 'data' });

			expect(boundaries.writeFile.mock.calls.length).toBe(1);
			expect(boundaries.writeFile).toHaveBeenCalledWith(
				'some/file.txt',
				'{\n  "some": "data"\n}',
				{ encoding: 'utf8' },
			);

			await promise.then(
				() => { throw new Error('should reject'); },
				(err) => { expect(err.message).toBe('mock failure'); },
			);

			expect(boundaries.setTimeout).not.toHaveBeenCalled();
			expect(boundaries.clearTimeout).not.toHaveBeenCalled();
			expect(boundaries.existsSync).not.toHaveBeenCalled();
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.writeFile.mock.calls.length).toBe(1);
		});
	});
});