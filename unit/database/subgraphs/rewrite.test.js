const links = require('../../../src/database/links');
const ideas = require('../../../src/database/ideas');
const memory = require('../../../src/database/ideas/memory');
const rewrite = require('../../../src/database/subgraphs/rewrite');
const { Subgraph } = require('../../../src/database/subgraphs/subgraphs');
const numbers = require('../../../src/planning/primitives/numbers');

describe('rewrite', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(rewrite.units).forEach((key) => {
			jest.spyOn(rewrite.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		Object.keys(rewrite.boundaries).forEach((key) => {
			jest.spyOn(rewrite.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	test('rewrite.units', () => {
		expect(Object.keys(rewrite.units)).toEqual([
			'doRewrite',
			'checkVertex',
			'transitionVertex',
			'checkEdge',
			'transitionEdge',
			'convertInnerTransitions',
		]);
	});

	describe('doRewrite', () => {
		beforeEach(() => {
			rewrite.units.doRewrite.mockRestore();
		});

		test('invalid call', () => {
			const sg = new Subgraph();
			expect(() => rewrite.units.doRewrite()).toThrow(); // cannot read property of undefined
			expect(() => rewrite.units.doRewrite(sg)).toThrow(); // cannot read property of undefined
		});

		test('subgraph not concrete', async () => {
			const sg = { concrete: false }; // @mock Subgraph
			expect(await rewrite.units.doRewrite(sg)).toBe(undefined);
		});

		test('no transitions', async () => {
			const sg = new Subgraph();
			const orig = new Subgraph(sg);

			const noActual = await rewrite.units.doRewrite(sg, [], false);
			expect(noActual).toEqual(orig);
			expect(noActual).not.toBe(sg);

			const isActual = await rewrite.units.doRewrite(sg, [], true);
			expect(isActual).toEqual(orig);
			expect(isActual).toBe(sg);
		});

		test('invalid transitions', async () => {
			rewrite.units.checkVertex.mockReturnValueOnce(false);
			rewrite.units.checkEdge.mockReturnValueOnce(false);
			const sg = new Subgraph();

			expect(await rewrite.units.doRewrite(sg, [null])).toBe(undefined);
			expect(await rewrite.units.doRewrite(sg, [undefined])).toBe(undefined);
			expect(await rewrite.units.doRewrite(sg, [{}])).toBe(undefined);
			expect(await rewrite.units.doRewrite(sg, [{ vertexId: 0 }])).toBe(undefined);
			expect(await rewrite.units.doRewrite(sg, [{ edgeId: 0 }])).toBe(undefined);

			expect(rewrite.units.checkVertex.mock.calls.length).toBe(1);
			expect(rewrite.units.checkEdge.mock.calls.length).toBe(1);
		});

		test.todo('skip transition if === undefined');

		describe('valid transitions', () => {
			test('vertex', async () => {
				rewrite.units.checkVertex.mockReturnValueOnce(true);
				rewrite.units.transitionVertex.mockReturnValueOnce(Promise.resolve());
				const sg = new Subgraph();

				const result = await rewrite.units.doRewrite(sg, [{ vertexId: 0 }]);
				expect(result).not.toBe(undefined);
				expect(result).not.toBe(sg);
				// if transitionVertex did something, then sg would have changed
				// but since it's a noop, then it should be the same
				expect(result).toEqual(sg);

				expect(rewrite.units.checkVertex.mock.calls.length).toBe(1);
				expect(rewrite.units.transitionVertex.mock.calls.length).toBe(1);
			});

			test('edge', async () => {
				rewrite.units.checkEdge.mockReturnValueOnce(true);
				rewrite.units.transitionEdge.mockReturnValueOnce(Promise.resolve());
				const sg = new Subgraph();

				const result = await rewrite.units.doRewrite(sg, [{ edgeId: 0 }]);
				expect(result).not.toBe(undefined);
				expect(result).not.toBe(sg);
				// if transitionEdge did something, then sg would have changed
				// but since it's a noop, then it should be the same
				expect(result).toEqual(sg);

				expect(rewrite.units.checkEdge.mock.calls.length).toBe(1);
				expect(rewrite.units.transitionEdge.mock.calls.length).toBe(1);
			});
		});

		describe('actual transition', () => {
			test('vertex', async () => {
				rewrite.units.checkVertex.mockReturnValueOnce(true);
				rewrite.units.transitionVertex.mockReturnValueOnce(Promise.resolve());
				const sg = new Subgraph();

				expect(await rewrite.units.doRewrite(sg, [{ vertexId: 0 }], true)).toBe(sg);

				expect(rewrite.units.checkVertex.mock.calls.length).toBe(1);
				expect(rewrite.units.transitionVertex.mock.calls.length).toBe(1);
			});

			test('edge', async () => {
				rewrite.units.checkEdge.mockReturnValueOnce(true);
				rewrite.units.transitionEdge.mockReturnValueOnce(Promise.resolve());
				const sg = new Subgraph();

				expect(await rewrite.units.doRewrite(sg, [{ edgeId: 0 }], true)).toBe(sg);

				expect(rewrite.units.checkEdge.mock.calls.length).toBe(1);
				expect(rewrite.units.transitionEdge.mock.calls.length).toBe(1);
			});
		});
	});

	describe('checkVertex', () => {
		let sg;
		beforeEach(() => {
			rewrite.units.checkVertex.mockRestore();
			sg = new Subgraph();
			jest.spyOn(sg, 'getVertex');
		});

		test('not a vertex transition', () => {
			const result = rewrite.units.checkVertex(sg, {});

			expect(result).toBe(false);
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('need different objects for vertex and edge', () => {
			const result = rewrite.units.checkVertex(sg, { vertexId: 'some vertex id', edgeId: 'some edge id' });

			expect(result).toBe(false);
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('unsupported transition', () => {
			const result = rewrite.units.checkVertex(sg, { vertexId: 'some vertex id' });

			expect(result).toBe(false);
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('vertex does not exist', () => {
			sg.getVertex.mockReturnValueOnce(undefined);

			const result = rewrite.units.checkVertex(sg, { vertexId: 'some vertex id', replace: {} });

			expect(result).toBe(false);
			expect(sg.getVertex.mock.calls).toEqual([
				['some vertex id'],
			]);
		});

		test('vertex not transitionable', () => {
			sg.getVertex.mockReturnValueOnce({ transitionable: false });

			const result = rewrite.units.checkVertex(sg, { vertexId: 'some vertex id', replace: {} });

			expect(result).toBe(false);
			expect(sg.getVertex.mock.calls).toEqual([
				['some vertex id'],
			]);
		});

		test('vertex without data', () => {
			sg.getVertex.mockReturnValueOnce({ transitionable: true });

			const result = rewrite.units.checkVertex(sg, { vertexId: 'some vertex id', replace: {} });

			expect(result).toBe(true);
			expect(sg.getVertex.mock.calls).toEqual([
				['some vertex id'],
			]);
		});

		test('replace w/o unit', () => {
			sg.getVertex.mockReturnValueOnce({ transitionable: true });

			const result = rewrite.units.checkVertex(sg, { vertexId: 'some vertex id', replace: { another: 'value' } });

			expect(result).toBe(true);
			expect(sg.getVertex.mock.calls).toEqual([
				['some vertex id'],
			]);
		});

		describe('replaceId', () => {
			test.todo('with unit');

			test('not a vertex', () => {
				sg.getVertex
					.mockReturnValueOnce({ transitionable: true })
					.mockReturnValueOnce(undefined);

				const result = rewrite.units.checkVertex(sg, { vertexId: 'vertex id w/o', replaceId: 'replace id w/o' });

				expect(result).toBe(false);
				expect(sg.getVertex.mock.calls).toEqual([
					['vertex id w/o'],
					['replace id w/o'],
				]);
			});

			test('w/o unit', () => {
				sg.getVertex
					.mockReturnValueOnce({ transitionable: true })
					.mockReturnValueOnce({ transitionable: true });

				const result = rewrite.units.checkVertex(sg, { vertexId: 'vertex id w/o', replaceId: 'replace id w/o' });

				expect(result).toBe(true);
				expect(sg.getVertex.mock.calls).toEqual([
					['vertex id w/o'],
					['replace id w/o'],
				]);
			});

			test.todo('with mismatch unit');

			test.todo('with matching unit');
		});

		test.todo('numbersCombineId');

		test.todo('numbersRemoveId');

		test.todo('numbersCombine');

		test.todo('numbersRemove');
	});

	describe('transitionVertex', () => {
		let sg;
		beforeEach(() => {
			sg = new Subgraph();
			sg.addVertex('id', '_test', { name: 'v', transitionable: true });
			sg.addVertex('id', '_test2', { name: 'v2' });
			sg.setData('v2', { value: '2' });

			rewrite.units.transitionVertex.mockRestore();
			rewrite.boundaries.updateData.mockReturnValueOnce(Promise.resolve());
		});

		test('replace', async () => {
			expect(await sg.getData('v')).toEqual(undefined);

			await rewrite.units.transitionVertex(sg, { vertexId: 'v', replace: { value: 1 } }, false);

			expect(await sg.getData('v')).toEqual({ value: 1 });
			expect(rewrite.boundaries.updateData).not.toHaveBeenCalled();
		});

		test('replaceId', async () => {
			expect(await sg.getData('v')).toEqual(undefined);

			await rewrite.units.transitionVertex(sg, { vertexId: 'v', replaceId: 'v2' }, false);

			expect(await sg.getData('v')).toEqual({ value: '2' });
			expect(rewrite.boundaries.updateData).not.toHaveBeenCalled();
		});

		test('numbersCombineId', async () => {
			sg.setData('v', numbers.cast({ value: numbers.value(10), $unit: '_test' }));
			sg.setData('v2', numbers.cast({ value: numbers.value(2, 4), $unit: '_test' }));

			await rewrite.units.transitionVertex(sg, { vertexId: 'v', numbersCombineId: 'v2' }, false);

			expect(await sg.getData('v')).toEqual({ value: { bl: true, l: 12, r: 14, br: true }, $unit: '_test', $type: 'lime_numbers' });
			expect(await sg.getData('v2')).toEqual({ value: { bl: true, l: 2, r: 4, br: true }, $unit: '_test', $type: 'lime_numbers' });
			expect(rewrite.boundaries.updateData).not.toHaveBeenCalled();
		});

		test('numbersRemoveId', async () => {
			sg.setData('v', numbers.cast({ value: numbers.value(10), $unit: '_test' }));
			sg.setData('v2', numbers.cast({ value: numbers.value(2, 4), $unit: '_test' }));

			await rewrite.units.transitionVertex(sg, { vertexId: 'v', numbersRemoveId: 'v2' }, false);

			expect(await sg.getData('v')).toEqual({ value: { bl: true, l: 6, r: 8, br: true }, $unit: '_test', $type: 'lime_numbers' });
			expect(await sg.getData('v2')).toEqual({ value: { bl: true, l: 2, r: 4, br: true }, $unit: '_test', $type: 'lime_numbers' });
			expect(rewrite.boundaries.updateData).not.toHaveBeenCalled();
		});

		test('numbersCombine', async () => {
			sg.setData('v', numbers.cast({ value: numbers.value(10), $unit: '_test' }));

			await rewrite.units.transitionVertex(sg, { vertexId: 'v', numbersCombine: numbers.cast({ value: numbers.value(2, 4), $unit: '_test' }) }, false);

			expect(await sg.getData('v')).toEqual({ value: { bl: true, l: 12, r: 14, br: true }, $unit: '_test', $type: 'lime_numbers' });
			expect(rewrite.boundaries.updateData).not.toHaveBeenCalled();
		});

		test('numbersRemove', async () => {
			sg.setData('v', numbers.cast({ value: numbers.value(10), $unit: '_test' }));

			await rewrite.units.transitionVertex(sg, { vertexId: 'v', numbersRemove: numbers.cast({ value: numbers.value(2, 4), $unit: '_test' }) }, false);

			expect(await sg.getData('v')).toEqual({ value: { bl: true, l: 6, r: 8, br: true }, $unit: '_test', $type: 'lime_numbers' });
			expect(rewrite.boundaries.updateData).not.toHaveBeenCalled();
		});

		test('isActual', async () => {
			expect(await sg.getData('v')).toEqual(undefined);

			await rewrite.units.transitionVertex(sg, { vertexId: 'v', replace: { value: 1 } }, true);

			expect(await sg.getData('v')).toEqual({ value: 1 });
			expect(rewrite.boundaries.updateData.mock.calls).toEqual([
				[sg, 'v'],
			]);
		});
	});

	describe('checkEdge', () => {
		let sg;
		beforeEach(() => {
			rewrite.units.checkEdge.mockRestore();
			sg = new Subgraph();
			jest.spyOn(sg, 'getEdge');
			jest.spyOn(sg, 'getVertex');
		});

		test('not an edge transition', () => {
			const result = rewrite.units.checkEdge(sg, {});

			expect(result).toBe(false);
			expect(sg.getEdge).not.toHaveBeenCalled();
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('need different objects for vertex and edge', () => {
			const result = rewrite.units.checkEdge(sg, { vertexId: 'some vertex id', edgeId: 'some edge id' });

			expect(result).toBe(false);
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('unsupported transition', () => {
			const result = rewrite.units.checkEdge(sg, { edgeId: 'some edge id' });

			expect(result).toBe(false);
			expect(sg.getEdge).not.toHaveBeenCalled();
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('edge does not exist', () => {
			sg.getEdge.mockReturnValueOnce(undefined);

			const result = rewrite.units.checkEdge(sg, { edgeId: 'some edge id', replaceSrc: 'some src id' });

			expect(result).toBe(false);
			expect(sg.getEdge.mock.calls).toEqual([
				['some edge id'],
			]);
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('edge not transitionable', () => {
			sg.getEdge.mockReturnValueOnce({ transitionable: false });

			const result = rewrite.units.checkEdge(sg, { edgeId: 'some edge id', replaceSrc: 'some src id' });

			expect(result).toBe(false);
			expect(sg.getEdge.mock.calls).toEqual([
				['some edge id'],
			]);
			expect(sg.getVertex).not.toHaveBeenCalled();
		});

		test('replaceSrc does not exist', () => {
			sg.getEdge.mockReturnValueOnce({ transitionable: true });
			sg.getVertex.mockReturnValueOnce(undefined);

			const result = rewrite.units.checkEdge(sg, { edgeId: 'rs dne', replaceSrc: 'some src' });

			expect(result).toBe(false);
			expect(sg.getEdge.mock.calls).toEqual([
				['rs dne'],
			]);
			expect(sg.getVertex.mock.calls).toEqual([
				['some src'],
			]);
		});

		test('replaceSrc exists', () => {
			sg.getEdge.mockReturnValueOnce({ transitionable: true });
			sg.getVertex.mockReturnValueOnce({});

			const result = rewrite.units.checkEdge(sg, { edgeId: 'rs dne', replaceSrc: 'some src' });

			expect(result).toBe(true);
			expect(sg.getEdge.mock.calls).toEqual([
				['rs dne'],
			]);
			expect(sg.getVertex.mock.calls).toEqual([
				['some src'],
			]);
		});

		test('replaceDst does not exist', () => {
			sg.getEdge.mockReturnValueOnce({ transitionable: true });
			sg.getVertex.mockReturnValueOnce(undefined);

			const result = rewrite.units.checkEdge(sg, { edgeId: 'rs dne', replaceDst: 'some dst' });

			expect(result).toBe(false);
			expect(sg.getEdge.mock.calls).toEqual([
				['rs dne'],
			]);
			expect(sg.getVertex.mock.calls).toEqual([
				['some dst'],
			]);
		});

		test('replaceDst exists', () => {
			sg.getEdge.mockReturnValueOnce({ transitionable: true });
			sg.getVertex.mockReturnValueOnce({});

			const result = rewrite.units.checkEdge(sg, { edgeId: 'rs dne', replaceDst: 'some dst' });

			expect(result).toBe(true);
			expect(sg.getEdge.mock.calls).toEqual([
				['rs dne'],
			]);
			expect(sg.getVertex.mock.calls).toEqual([
				['some dst'],
			]);
		});
	});

	describe('transitionEdge', () => {
		let sg;
		beforeEach(() => {
			sg = new Subgraph();
			sg.addVertex('id', '_startFrom', { name: 'sf' });
			sg.addVertex('id', '_startTo', { name: 'st' });
			sg.addVertex('id', '_laterFrom', { name: 'lf' });
			sg.addVertex('id', '_laterTo', { name: 'lt' });
			sg.addEdge('sf', 'has', 'st', { name: 'e', transitionable: true });

			rewrite.units.transitionEdge.mockRestore();
			rewrite.boundaries.updateLink.mockReturnValueOnce(Promise.resolve());
		});

		test('src', async () => {
			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('sf'),
				dst: sg.$refVertex('st'),
			}));

			await rewrite.units.transitionEdge(sg, { edgeId: 'e', replaceSrc: 'lf' }, false);

			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('lf'),
				dst: sg.$refVertex('st'),
			}));
			expect(rewrite.boundaries.updateLink).not.toHaveBeenCalled();
		});

		test('dst', async () => {
			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('sf'),
				dst: sg.$refVertex('st'),
			}));

			await rewrite.units.transitionEdge(sg, { edgeId: 'e', replaceDst: 'lt' }, false);

			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('sf'),
				dst: sg.$refVertex('lt'),
			}));
			expect(rewrite.boundaries.updateLink).not.toHaveBeenCalled();
		});

		test('both', async () => {
			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('sf'),
				dst: sg.$refVertex('st'),
			}));

			await rewrite.units.transitionEdge(sg, { edgeId: 'e', replaceSrc: 'lf', replaceDst: 'lt' }, false);

			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('lf'),
				dst: sg.$refVertex('lt'),
			}));
			expect(rewrite.boundaries.updateLink).not.toHaveBeenCalled();
		});

		test('isActual', async () => {
			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('sf'),
				dst: sg.$refVertex('st'),
			}));

			await rewrite.units.transitionEdge(sg, { edgeId: 'e', replaceSrc: 'lf' }, true);

			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('lf'),
				dst: sg.$refVertex('st'),
			}));
			expect(rewrite.boundaries.updateLink.mock.calls).toMatchSnapshot();
		});

		test('isActual no change', async () => {
			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('sf'),
				dst: sg.$refVertex('st'),
			}));

			await rewrite.units.transitionEdge(sg, { edgeId: 'e', replaceSrc: 'sf', replaceDst: 'st' }, true);

			expect(sg.getEdge('e')).toEqual(expect.objectContaining({
				src: sg.$refVertex('sf'),
				dst: sg.$refVertex('st'),
			}));
			expect(rewrite.boundaries.updateLink).not.toHaveBeenCalled();
		});
	});

	test.todo('convertInnerTransitions');

	test('rewrite.boundaries', () => {
		expect(Object.keys(rewrite.boundaries)).toEqual([
			'updateData',
			'updateLink',
		]);
	});

	test('updateData', async () => {
		rewrite.boundaries.updateData.mockRestore();

		const proxy = await ideas.create({ value: 1 });

		expect(memory.units.map).toMatchSnapshot();

		// setup complete

		const sg = new Subgraph();
		sg.addVertex('id', proxy, { name: 't', transitionable: true });
		sg.setData('t', { value: 2 });

		await rewrite.boundaries.updateData(sg, 't');

		expect(memory.units.map).toMatchSnapshot();
	});

	test('updateLink', async () => {
		rewrite.boundaries.updateLink.mockRestore();

		const link = links.get('sameAs');

		const proxyDog = await ideas.create({ what: 'dog' });
		const proxyCat = await ideas.create({ what: 'cat' });
		const proxyWolf = await ideas.create({ what: 'wolf' });
		await proxyDog.addLink(link, proxyCat);

		expect(memory.units.map).toMatchSnapshot();

		// setup complete

		const sg = new Subgraph();
		sg.addVertex('id', proxyDog, { name: 'd' });
		sg.addVertex('id', proxyCat, { name: 'c' });
		sg.addVertex('id', proxyWolf, { name: 'w' });
		sg.addEdge('d', link, 'c', { name: 'myEdge', transitionable: true });

		await rewrite.boundaries.updateLink(sg, link, 'd', 'c', 'd', 'w');

		expect(memory.units.map).toMatchSnapshot();
	});

	test.todo('updateLink no change');
});