const LazyCopyMap = require('../../../src/database/subgraphs/LazyCopyMap');

describe('LazyCopyMap', () => {
	test('prototype', () => {
		// eslint-disable-next-line no-proto
		expect(Object.getOwnPropertyNames(new LazyCopyMap().__proto__)).toEqual([
			'constructor',
			'set',
			'get',
			'flatten',
		]);
	});

	describe('constructor', () => {
		test('new', () => {
			const lcm = new LazyCopyMap();
			expect(lcm).toEqual({
				data: new Map(),
				parent: undefined,
				children: [],
			});
		});

		describe('copy', () => {
			test('copy has something', () => {
				const one = new LazyCopyMap();
				one.set('a', 1);
				const two = new LazyCopyMap(one);

				expect(one).toEqual({
					data: new Map([['a', 1]]),
					parent: undefined,
					children: [two],
				});
				expect(two).toEqual({
					data: new Map(),
					parent: one,
					children: [],
				});
			});

			test('copy empty has grandparent', () => {
				const one = new LazyCopyMap();
				one.set('a', 1);
				const two = new LazyCopyMap(one);
				const three = new LazyCopyMap(two);

				expect(one).toEqual({
					data: new Map([['a', 1]]),
					parent: undefined,
					children: [two, three],
				});
				expect(two).toEqual({
					data: new Map(),
					parent: one, // points to one
					children: [],
				});
				expect(three).toEqual({
					data: new Map(),
					parent: one, // points to one
					children: [],
				});
			});

			test('copy empty without grandparent', () => {
				const one = new LazyCopyMap();
				const two = new LazyCopyMap(one);

				expect(one).toEqual({
					data: new Map(),
					parent: undefined,
					children: [],
				});
				expect(two).toEqual({
					data: new Map(),
					parent: undefined,
					children: [],
				});
			});

			test('invalid', () => {
				expect(() => new LazyCopyMap(null)).toThrow(/^parent must be of type LazyCopyMap$/);
				expect(() => new LazyCopyMap(1)).toThrow(/^parent must be of type LazyCopyMap$/);
				expect(() => new LazyCopyMap({})).toThrow(/^parent must be of type LazyCopyMap$/);
			});
		});
	});

	describe('get / set', () => {
		test('get can continue up the chain', () => {
			const one = new LazyCopyMap();
			one.set('a', 1);
			one.set('x', 1);
			const two = new LazyCopyMap(one);
			two.set('b', 2);
			two.set('x', 2);
			const three = new LazyCopyMap(two);
			three.set('c', 3);
			three.set('x', 3);

			expect(one.get('a')).toBe(1);
			expect(one.get('b')).toBe(undefined);
			expect(one.get('c')).toBe(undefined);
			expect(one.get('d')).toBe(undefined);
			expect(one.get('x')).toBe(1);

			expect(two.get('a')).toBe(1);
			expect(two.get('b')).toBe(2);
			expect(two.get('c')).toBe(undefined);
			expect(two.get('d')).toBe(undefined);
			expect(two.get('x')).toBe(2);

			expect(three.get('a')).toBe(1);
			expect(three.get('b')).toBe(2);
			expect(three.get('c')).toBe(3);
			expect(three.get('d')).toBe(undefined);
			expect(three.get('x')).toBe(3);
		});

		test('if we set something in the middle of the chain', () => {
			const one = new LazyCopyMap();
			one.set('x', 1);
			const two = new LazyCopyMap(one);
			two.set('x', 2);
			const three = new LazyCopyMap(two);
			three.set('x', 3);

			two.set('d', 4);

			// we should get the same values as before, except where we set
			expect(one.get('d')).toBe(undefined);
			expect(two.get('d')).toBe(4);
			expect(three.get('d')).toBe(undefined);

			expect(Array.from(one.data.entries())).toEqual([['x', 1]]);
			expect(Array.from(two.data.entries())).toEqual([['x', 2], ['d', 4]]);
			expect(Array.from(three.data.entries())).toEqual([['x', 3], ['d', undefined]]);
		});

		test('if we set something in the middle of the chain that the child already has', () => {
			const one = new LazyCopyMap();
			one.set('x', 1);
			const two = new LazyCopyMap(one);
			two.set('x', 2);
			const three = new LazyCopyMap(two);
			three.set('x', 3);

			two.set('x', 20);

			// we should get the same values as before, except where we set
			expect(one.get('x')).toBe(1);
			expect(two.get('x')).toBe(20);
			expect(three.get('x')).toBe(3);

			expect(Array.from(one.data.entries())).toEqual([['x', 1]]);
			expect(Array.from(two.data.entries())).toEqual([['x', 20]]);
			expect(Array.from(three.data.entries())).toEqual([['x', 3]]);
		});

		test('bookeeping undefined is easier than trying to track it special', () => {
			const one = new LazyCopyMap();
			one.set('x', 1);
			const two = new LazyCopyMap(one);
			two.set('x', 2);

			expect(one.get('x')).toBe(1);
			expect(two.get('x')).toBe(2);

			two.set('x', undefined);
			expect(one.get('x')).toBe(1);
			expect(two.get('x')).toBe(undefined);

			one.set('x', undefined);
			expect(one.get('x')).toBe(undefined);
			expect(two.get('x')).toBe(undefined);

			expect(two).toMatchSnapshot();
		});
	});

	test('flatten', () => {
		const zero = new LazyCopyMap();
		zero.set('z', 0);
		zero.set('x', 0);
		const one = new LazyCopyMap(zero);
		one.set('a', 1);
		one.set('x', 1);
		const two = new LazyCopyMap(one);
		two.set('b', 2);
		two.set('x', 2);
		const three = new LazyCopyMap(two);
		three.set('c', 3);
		three.set('x', 3);
		const four = new LazyCopyMap(three);
		four.set('d', 4);
		four.set('x', 4);

		expect(zero).toEqual({
			data: new Map([['z', 0], ['x', 0]]),
			parent: undefined,
			children: [one],
		});
		expect(one).toEqual({
			data: new Map([['a', 1], ['x', 1]]),
			parent: zero,
			children: [two],
		});
		expect(two).toEqual({
			data: new Map([['b', 2], ['x', 2]]),
			parent: one,
			children: [three],
		});
		expect(three).toEqual({
			data: new Map([['c', 3], ['x', 3]]),
			parent: two,
			children: [four],
		});
		expect(four).toEqual({
			data: new Map([['d', 4], ['x', 4]]),
			parent: three,
			children: [],
		});

		two.flatten();

		// unchanged
		expect(zero).toEqual({
			data: new Map([['z', 0], ['x', 0]]),
			parent: undefined,
			children: [one],
		});
		// spliced out of children
		expect(one).toEqual({
			data: new Map([['a', 1], ['x', 1]]),
			parent: zero,
			children: [],
		});
		// copy of all data from parent
		expect(two).toEqual({
			data: new Map([['b', 2], ['x', 2], ['z', 0], ['a', 1]]),
			parent: undefined,
			children: [],
		});
		// all data put in child, parent uncut
		expect(three).toEqual({
			data: new Map([['c', 3], ['x', 3], ['b', 2], ['z', 0], ['a', 1]]),
			parent: undefined,
			children: [four],
		});
		// unchanged
		expect(four).toEqual({
			data: new Map([['d', 4], ['x', 4]]),
			parent: three,
			children: [],
		});
	});
});