const ideas = require('../../../src/database/ideas');
const { Subgraph } = require('../../../src/database/subgraphs');

describe('Subgraph', () => {
	test('prototype', () => {
		// eslint-disable-next-line no-proto
		expect(Object.getOwnPropertyNames(new Subgraph().__proto__)).toEqual([
			'constructor',
			'stringifySubgraph',
			'save',
			'flatten',
			'equals',
			// #setup
			'$refVertex',
			'$refEdge',
			'addVertex',
			'addEdge',
			'grow',
			// #ideas
			'getVertex',
			'setIdea',
			'getIdea',
			'hasIdea',
			'allIdeas',
			'deleteIdea',
			'isIdeaEmpty',
			'isIdeaComplete',
			// #data
			'getMatchData',
			'getData',
			'setData',
			'deleteData',
			// #edges
			'getEdge',
			'updateEdge',
			'allEdges',
		]);
	});

	describe('constructor', () => {
		test('empty', () => {
			expect(new Subgraph()).toMatchSnapshot();
		});

		test('copy', async () => {
			const sg = new Subgraph();
			sg.addEdge(sg.addVertex('filler'), 'property', sg.addVertex('filler'));
			sg.setIdea(0, ideas.proxy('_test'));
			const copy = new Subgraph(sg);

			expect(copy).toMatchSnapshot();
		});

		test('invalid calls', () => {
			expect(() => new Subgraph(null)).toThrow(TypeError);
			expect(() => new Subgraph(1234)).toThrow(TypeError);
			expect(() => new Subgraph({})).toThrow(TypeError);
		});
	});

	test('stringifySubgraph / parse', async () => {
		await ideas.proxy('_test').setData({ value: 'a' });

		const sg = new Subgraph();
		sg.addEdge(
			sg.addVertex('filler', undefined, { name: 'unknown' }),
			'property',
			sg.addVertex('id', ideas.proxy('_test'), { name: 'anchor' }),
		);
		sg.setIdea(0, ideas.proxy('_test'));
		await sg.getData(0);

		const string1 = sg.stringifySubgraph();
		const parse1 = Subgraph.parse(string1);
		const string2 = parse1.stringifySubgraph();
		const parse2 = Subgraph.parse(string2);

		// help verify all the data we want to see
		// so we can spot check that all the fields have values
		expect(sg).toMatchSnapshot();

		// so we can visualize / spot check the serialized string
		expect(JSON.stringify(JSON.parse(string1), null, 2)).toMatchSnapshot();

		expect(sg).toEqual(parse1);
		expect(sg).toEqual(parse2);
		expect(parse1).toEqual(parse2);
		expect(string1).toEqual(string2);
	});

	describe('save', () => {
		test.todo('not complete');

		test.todo('success');
	});

	test.todo('flatten');

	describe('equals', () => {
		test('empty, sizes', () => {
			const sg1 = new Subgraph();
			const sg2 = new Subgraph();

			expect(sg1.equals(sg2)).toBe(true);

			sg1.addVertex('filler');

			expect(sg1.equals(sg2)).toBe(false);

			sg2.addVertex('filler');

			expect(sg1.equals(sg2)).toBe(true);

			sg1.addEdge(0, 'sameAs', 0);

			expect(sg1.equals(sg2)).toBe(false);

			sg2.addEdge(0, 'sameAs', 0);

			expect(sg1.equals(sg2)).toBe(true);
		});

		test('mismatched edge', () => {
			const sg1 = new Subgraph();
			sg1.addVertex('filler');
			sg1.addEdge(0, 'sameAs', 0);
			sg1.addEdge(0, 'sameAs', 0);
			const sg2 = new Subgraph();
			sg2.addVertex('filler');
			sg2.addEdge(0, 'sameAs', 0);
			sg2.addEdge(0, 'sameAs', 0);

			expect(sg1.equals(sg2)).toBe(true);

			sg1.getEdge(1).src = 1;
			sg1.getEdge(1).dst = 0;

			expect(sg1.equals(sg2)).toBe(false);

			sg1.getEdge(1).src = 0;
			sg1.getEdge(1).dst = 1;

			expect(sg1.equals(sg2)).toBe(false);

			sg1.getEdge(1).dst = 0;
			sg1.getEdge(1).link = 'something else';

			expect(sg1.equals(sg2)).toBe(false);

			sg1.getEdge(1).link = sg1.getEdge(0).link;

			expect(sg1.equals(sg2)).toBe(true);
		});

		test('mismatched data', () => {
			const sg1 = new Subgraph();
			sg1.addVertex('filler');
			sg1.addVertex('filler');
			const sg2 = new Subgraph();
			sg2.addVertex('filler');
			sg2.addVertex('filler');

			expect(sg1.equals(sg2)).toBe(true);

			sg1.setData(1, { some: 'data' });

			expect(sg1.equals(sg2)).toBe(false);

			sg2.setData(1, { some: 'data' });

			expect(sg1.equals(sg2)).toBe(true);

			sg1.setData(1, { some: 'other data' });

			expect(sg1.equals(sg2)).toBe(false);
		});

		test.todo('non-empty equals');
	});

	describe('#setup', () => {
		describe('$refVertex', () => {
			let sg;
			let vertexId;
			beforeAll(() => {
				sg = new Subgraph();
				vertexId = sg.addVertex('filler', undefined, { name: 'unknown' });
			});

			test('preconditions', () => {
				expect(vertexId).toBe(0);
				expect(sg.$vertexCount).toBe(1);
			});

			test('valid', () => {
				expect(sg.$refVertex(vertexId)).toBe(0);
				expect(sg.$refVertex('unknown')).toBe(0);
			});

			test('invalid numbers', () => {
				expect(() => sg.$refVertex(-1)).toThrow(/^id "-1" \(-1\) is not a vertex$/);
				expect(() => sg.$refVertex(0)).not.toThrow();
				expect(() => sg.$refVertex(1)).toThrow(/^id "1" \(1\) is not a vertex$/);
				expect(() => sg.$refVertex(2)).toThrow(/^id "2" \(2\) is not a vertex$/);
			});

			test('invalid others', () => {
				expect(() => sg.$refVertex('fruit')).toThrow(/^"fruit" \(string\) is not a vertex$/);
				expect(() => sg.$refVertex()).toThrow(/^"undefined" \(undefined\) is not a vertex$/);
				expect(() => sg.$refVertex(null)).toThrow(/^"null" \(object\) is not a vertex$/);
				expect(() => sg.$refVertex({ id: 0 })).toThrow(/^"\[object Object\]" \(object\) is not a vertex$/);
			});
		});

		describe('$refEdge', () => {
			let sg;
			let edgeId;
			beforeAll(() => {
				sg = new Subgraph();
				edgeId = sg.addEdge(sg.addVertex('filler'), 'typeOf', sg.addVertex('filler'), { name: 'atob' });
			});

			test('preconditions', () => {
				expect(edgeId).toBe(0);
				expect(sg.$edgeCount).toBe(1);
			});

			test('valid', () => {
				expect(sg.$refEdge(edgeId)).toBe(0);
				expect(sg.$refEdge('atob')).toBe(0);
			});

			test('invalid numbers', () => {
				expect(() => sg.$refEdge(-1)).toThrow(/^id "-1" \(-1\) is not an edge$/);
				expect(() => sg.$refEdge(0)).not.toThrow();
				expect(() => sg.$refEdge(1)).toThrow(/^id "1" \(1\) is not an edge$/);
				expect(() => sg.$refEdge(2)).toThrow(/^id "2" \(2\) is not an edge$/);
			});

			test('invalid others', () => {
				expect(() => sg.$refEdge('fruit')).toThrow(/^"fruit" \(string\) is not an edge$/);
				expect(() => sg.$refEdge()).toThrow(/^"undefined" \(undefined\) is not an edge$/);
				expect(() => sg.$refEdge(null)).toThrow(/^"null" \(object\) is not an edge$/);
				expect(() => sg.$refEdge({ id: 0 })).toThrow(/^"\[object Object\]" \(object\) is not an edge$/);
			});
		});

		describe('addVertex', () => {
			describe('correct', () => {
				test('id', () => {
					const sg = new Subgraph();
					sg.addVertex('id', ideas.proxy('_test'));
				});

				test('filler', () => {
					const sg = new Subgraph();
					sg.addVertex('filler', undefined, { name: 'unknown' });
				});

				test('substring', () => {
					const sg = new Subgraph();
					sg.addVertex('substring', { value: 'banana' });
					sg.addVertex('substring', { value: 'some', path: 'one' });
				});

				// what would this test prove? this is used heavily in rewrite
				// test('options.transitionable');

				test('options.pointer', () => {
					const sg = new Subgraph();
					const vertexId = sg.addVertex('filler');
					sg.addVertex('id', vertexId, { pointer: true });
				});

				// what would this test prove? this is used all over the place
				// test('options.name');

				test('options.orData', () => {
					const sg = new Subgraph();
					sg.addVertex('substring', [{ value: 'one' }, { value: 'TWO' }], { orData: true });
					expect(sg).toMatchSnapshot();
				});

				test('matcher.id / options.orData', () => {
					const sg = new Subgraph();
					sg.addVertex('id', ['_test', '_test2'], { orData: true });
					expect(sg).toMatchSnapshot();
				});
			});

			describe('invalid', () => {
				test('matcher', () => {
					const sg = new Subgraph();
					expect(() => { sg.addVertex(); }).toThrow(/^invalid matcher$/);
					expect(() => { sg.addVertex('asdf'); }).toThrow(/^unkown matcher "asdf"$/);
					expect(() => { sg.addVertex(() => {}); }).toThrow(/^invalid matcher$/);
					expect(() => { sg.addVertex(function fn() {}); }).toThrow(/^unkown matcher "fn"$/); // eslint-disable-line prefer-arrow-callback
				});

				test('data', () => {
					const sg = new Subgraph();
					expect(() => { sg.addVertex('id'); })
						.toThrow(/^vertex data must be defined$/);
					expect(() => { sg.addVertex('id', 'one', { pointer: true }); })
						.toThrow(/^options.pointer target \(vertex.data\) must already be a vertex$/);
					expect(() => { sg.addVertex('filler', { value: 'one' }); })
						.toThrow(/^filler should not have vertex data$/);
				});

				test('options.transitionable', () => {
					const sg = new Subgraph();
					expect(() => { sg.addVertex('filler', undefined, { transitionable: 'banana' }); })
						.toThrow(/^invalid options.transitionable: typeof "string"$/);
				});

				test('options.pointer', () => {
					const sg = new Subgraph();
					expect(() => { sg.addVertex('id', 0, { pointer: 'banana' }); }).toThrow(/^invalid options.pointer: typeof "string"$/);
					expect(() => { sg.addVertex('filler', 0, { pointer: true }); }).toThrow(/^filler matchers cannot also be options.pointer$/);
				});

				test('options.name', () => {
					const sg = new Subgraph();
					expect(() => { sg.addVertex('filler', undefined, { name: 'banana' }); }).not.toThrow(); // the first name is fine
					expect(() => { sg.addVertex('filler', undefined, { name: 'banana' }); }).toThrow(/^options.name "banana" is already used$/);
				});

				test('options.orData', () => {
					const sg = new Subgraph();
					expect(() => { sg.addVertex('id', 0, { orData: 'banana' }); })
						.toThrow(/^invalid options.orData: typeof "string"$/);
					expect(() => { sg.addVertex('filler', 0, { orData: true }); })
						.toThrow(/^filler matchers cannot also be options.orData$/);
					expect(() => { sg.addVertex('substring', { value: 'one' }, { orData: true }); })
						.toThrow(/^when using options.orData, data must be an array$/);
				});
			});
		});

		describe('addEdge', () => {
			test('correct', () => {
				const sg = new Subgraph();
				sg.addVertex('filler');
				sg.addVertex('filler');

				sg.addEdge(0, 'property', 1);
				sg.addEdge(0, 'property', 1, { name: 'a thing', pref: 1 });
				sg.addEdge(1, 'property-opp', 0);
				sg.addEdge(1, 'sameAs', 0);

				expect(sg).toMatchSnapshot();
			});

			describe('incorrect', () => {
				test('src and dst', () => {
					const sg = new Subgraph();
					expect(() => { sg.addEdge(); }).toThrow(/^"undefined" \(undefined\) is not a vertex$/);
					expect(() => { sg.addEdge('zero'); }).toThrow(/^"zero" \(string\) is not a vertex$/);
					expect(() => { sg.addEdge(sg.addVertex('filler')); }).toThrow(/^"undefined" \(undefined\) is not a vertex$/);
					expect(() => { sg.addEdge(sg.addVertex('filler'), undefined, 'one'); }).toThrow(/^"one" \(string\) is not a vertex$/);
				});

				test('link', () => {
					const addEdge = (link) => {
						const sg = new Subgraph();
						sg.addEdge(sg.addVertex('filler'), link, sg.addVertex('filler'));
					};

					expect(() => { addEdge(undefined); }).toThrow(/^"undefined" is not a valid link$/);
					expect(() => { addEdge('banana'); }).toThrow(/^"banana" is not a valid link$/);
					expect(() => { addEdge(function apple() {}); }).toThrow(/^"apple" is not a valid link$/); // eslint-disable-line prefer-arrow-callback
					expect(() => { addEdge(() => {}); }).toThrow(/^"\(\) => \{\}" is not a valid link$/);
					expect(() => { addEdge({}); }).toThrow(/^"\[object Object\]" is not a valid link$/);
					expect(() => { addEdge(5); }).toThrow(/^"5" is not a valid link$/);
				});

				test('options.pref', () => {
					const addEdge = (pref) => {
						const sg = new Subgraph();
						sg.addEdge(sg.addVertex('filler'), 'typeOf', sg.addVertex('filler'), { pref });
					};

					expect(() => { addEdge(null); }).toThrow(/^invalid options.pref$/);
					expect(() => { addEdge('banana'); }).toThrow(/^invalid options.pref$/);
					expect(() => { addEdge(false); }).toThrow(/^invalid options.pref$/);
				});

				test('options.transitionable', () => {
					const addEdge = (transitionable) => {
						const sg = new Subgraph();
						sg.addEdge(sg.addVertex('filler'), 'typeOf', sg.addVertex('filler'), { transitionable });
					};

					expect(() => { addEdge(null); }).toThrow(/^invalid options.transitionable$/);
					expect(() => { addEdge('banana'); }).toThrow(/^invalid options.transitionable$/);
					expect(() => { addEdge(1); }).toThrow(/^invalid options.transitionable$/);
				});

				test('options.name', () => {
					const sg = new Subgraph();
					expect(() => { sg.addEdge(sg.addVertex('filler'), 'typeOf', sg.addVertex('filler'), { name: 'banana' }); })
						.not.toThrow(); // the first name is fine
					expect(() => { sg.addEdge(sg.addVertex('filler'), 'typeOf', sg.addVertex('filler'), { name: 'banana' }); })
						.toThrow(/^options.name "banana" is already used$/);
				});
			});
		});

		describe('grow', () => {
			describe('valid', () => {
				test('basic', async () => {
					const sg = new Subgraph();
					const vi = sg.addVertex('id', ideas.proxy('_test'));

					await expect(sg.grow(vi, ['has', 'property'])).resolves.toEqual(undefined);
				});

				test('multiple sources', async () => {
					const sg = new Subgraph();
					const a = sg.addVertex('id', ideas.proxy('_test'));
					const b = sg.addVertex('id', ideas.proxy('_test'));
					const c = sg.addVertex('id', ideas.proxy('_test'));

					await expect(sg.grow([a, b, c], ['has', 'property'])).resolves.toEqual(undefined);
				});
			});

			describe('invalid', () => {
				test('not concrete', async () => {
					const sg = new Subgraph();
					sg.addVertex('filler');
					await expect(sg.grow()).rejects.toThrow(/^Subgraph must be concrete$/);
				});

				test('src does not exist', async () => {
					const sg = new Subgraph();
					sg.addVertex('id', ideas.proxy('_test'));
					await expect(sg.grow('banana')).rejects.toThrow(/^"banana" \(string\) is not a vertex$/);
				});

				test('invalid path link', async () => {
					const sg = new Subgraph();
					const vi = sg.addVertex('id', ideas.proxy('_test'));

					await expect(sg.grow(vi, ['banana', 'property'])).rejects.toThrow(/^"banana" is not a valid link$/);
					await expect(sg.grow(vi, ['haz', 'property'])).rejects.toThrow(/^"haz" is not a valid link$/);
				});

				test('invalid leaf link', async () => {
					const sg = new Subgraph();
					const vi = sg.addVertex('id', ideas.proxy('_test'));

					await expect(sg.grow(vi, ['banana'])).rejects.toThrow(/^"banana" is not a valid link$/);
					await expect(sg.grow(vi, ['haz'])).rejects.toThrow(/^"haz" is not a valid link$/);
				});
			});
		});
	});

	describe('#ideas', () => {
		// these do not need to be tested
		// they are trival accessors
		// if they never get used, then they'll never have coverage, and we can remove them
		// test('getVertex');
		// test('setIdea');
		// test('getIdea');
		// test('hasIdea');
		// test('allIdeas');
		// test('deleteIdea');
		// test('isIdeaEmpty');
		// test('isIdeaComplete');
	});

	describe('#data', () => {
		describe('getMatchData', () => {
			let sg;
			let outer;
			beforeEach(() => {
				sg = new Subgraph();
				jest.spyOn(sg, 'getData');
				sg.getData.mockImplementation(() => { throw new Error('must mock "sg.getData"'); });

				outer = new Subgraph();
				jest.spyOn(outer, 'getData');
				outer.getData.mockImplementation(() => { throw new Error('must mock "outer.getData"'); });
			});

			describe('not a pointer', () => {
				test('idea mapped', async () => {
					const vertexId = sg.addVertex('id', ideas.proxy('_test'));

					// inner data
					sg.getData.mockReturnValueOnce(Promise.resolve('mock inner result'));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual(['mock inner result']);
					expect(sg.hasIdea(vertexId)).toBe(true);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
					]);
					expect(outer.getData).not.toHaveBeenCalled();
				});

				test('null data', async () => {
					const vertexId = sg.addVertex('id', ideas.proxy('_test'));

					// inner data
					sg.getData.mockReturnValueOnce(Promise.resolve(null));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual([null]);
					expect(sg.hasIdea(vertexId)).toBe(true);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
					]);
					expect(outer.getData).not.toHaveBeenCalled();
				});

				test('idea not mapped, no data', async () => {
					const vertexId = sg.addVertex('exact', 1);

					// inner data
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual([]);
					expect(sg.hasIdea(vertexId)).toBe(false);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
					]);
					expect(outer.getData).not.toHaveBeenCalled();
				});
			});

			describe('pointer', () => {
				test('already has idea', async () => {
					const vi = sg.addVertex('id', ideas.proxy('_test'));
					const vertexId = sg.addVertex('id', vi, { pointer: true });

					// has idea
					sg.setIdea(vertexId, ideas.proxy('_test'));
					sg.getData.mockReturnValueOnce(Promise.resolve('mock inner result'));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual(['mock inner result']);
					expect(sg.hasIdea(vertexId)).toBe(true);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
					]);
					expect(outer.getData).not.toHaveBeenCalled();
				});

				test('target has data', async () => {
					const vi = sg.addVertex('exact', 1);
					const vertexId = sg.addVertex('id', vi, { pointer: true });

					// inner has data
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));
					sg.getData.mockReturnValueOnce(Promise.resolve('mock inner result'));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual(['mock inner result']);
					expect(sg.hasIdea(vertexId)).toBe(false);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
						[vi],
					]);
					expect(outer.getData).not.toHaveBeenCalled();
				});

				test('target has data (orData)', async () => {
					const vi1 = sg.addVertex('exact', 1);
					const vi2 = sg.addVertex('exact', 2);
					const vertexId = sg.addVertex('id', [vi1, vi2], { pointer: true, orData: true });

					// inner has data
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));
					sg.getData.mockReturnValueOnce(Promise.resolve('mock inner result 1'));
					sg.getData.mockReturnValueOnce(Promise.resolve('mock inner result 2'));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual(['mock inner result 1', 'mock inner result 2']);
					expect(sg.hasIdea(vertexId)).toBe(false);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
						[vi1],
						[vi2],
					]);
					expect(outer.getData).not.toHaveBeenCalled();
				});

				test('target mapped to outer', async () => {
					const vi = sg.addVertex('exact', 1);
					const outerVertexId = outer.addVertex('exact', 1);
					const vertexId = sg.addVertex('id', vi, { pointer: true });

					// outer has the data
					const innerToOuterVertexMap = new Map([[vi, outerVertexId]]);
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));
					outer.getData.mockReturnValueOnce(Promise.resolve('mock outer result'));

					expect(await sg.getMatchData(vertexId, { outer, innerToOuterVertexMap })).toEqual(['mock outer result']);
					expect(sg.hasIdea(vertexId)).toBe(false);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
						[vi],
					]);
					expect(outer.getData.mock.calls).toEqual([
						[outerVertexId],
					]);
				});

				test('not ready', async () => {
					const vi = sg.addVertex('exact', 1);
					const vertexId = sg.addVertex('id', vi, { pointer: true });

					// outer is not mapped yet
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual([]);
					expect(sg.hasIdea(vertexId)).toBe(false);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
						[vi],
					]);
					expect(outer.getData.mock.calls).toEqual([]);
				});

				test('not ready (orData)', async () => {
					const vi1 = sg.addVertex('exact', 1);
					const vi2 = sg.addVertex('exact', 2);
					const vertexId = sg.addVertex('id', [vi1, vi2], { pointer: true, orData: true });

					// outer is not mapped yet
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));
					sg.getData.mockReturnValueOnce(Promise.resolve(undefined));

					expect(await sg.getMatchData(vertexId, { outer })).toEqual([]);
					expect(sg.hasIdea(vertexId)).toBe(false);
					expect(sg.getData.mock.calls).toEqual([
						[vertexId],
						[vi1],
						[vi2],
					]);
					expect(outer.getData.mock.calls).toEqual([]);
				});
			});
		});

		describe('getData', () => {
			let sg;
			let proxy;
			beforeEach(() => {
				sg = new Subgraph();
				proxy = ideas.proxy('_test');
				jest.spyOn(proxy, 'getData');

				sg.addVertex('id', proxy, { name: 'pinned' });
				proxy.getData.mockImplementation(() => { throw new Error('must mock "getData"'); });
			});

			test('no idea pinned', async () => {
				sg.setIdea('pinned', undefined); // unset the idea

				expect(await sg.getData('pinned')).toBe(undefined);

				expect(proxy.getData).not.toHaveBeenCalled();
			});

			test('idea has data', async () => {
				proxy.getData.mockImplementationOnce(() => Promise.resolve({ value: 'one' }));

				expect(sg.$data).toMatchSnapshot();
				expect(await sg.getData('pinned')).toEqual({ value: 'one' });
				expect(sg.$data).toMatchSnapshot();
				expect(await sg.getData('pinned')).toEqual({ value: 'one' });
				expect(sg.$data).toMatchSnapshot();

				expect(proxy.getData.mock.calls.length).toBe(1);
			});

			test('idea without data', async () => {
				proxy.getData.mockImplementationOnce(() => Promise.resolve(undefined));

				expect(sg.$data).toMatchSnapshot();
				expect(await sg.getData('pinned')).toEqual(undefined);
				expect(sg.$data).toMatchSnapshot();
				expect(await sg.getData('pinned')).toEqual(undefined);
				expect(sg.$data).toMatchSnapshot();

				expect(proxy.getData.mock.calls.length).toBe(1);
			});
		});

		test.todo('setData');

		test.todo('deleteData');
	});

	describe('#edges', () => {
		// these do not need to be tested
		// they are trival accessors
		// if they never get used, then they'll never have coverage, and we can remove them
		// test('getEdge');
		// test('updateEdge');
		// test('allEdges');
	});
});