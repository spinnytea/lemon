const { Subgraph } = require('../../../src/database/subgraphs/subgraphs');
const subgraphUtils = require('../../../src/database/subgraphs/subgraphUtils');

describe('subgraphUtils', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(subgraphUtils.units).forEach((key) => {
			jest.spyOn(subgraphUtils.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		Object.keys(subgraphUtils.boundaries).forEach((key) => {
			jest.spyOn(subgraphUtils.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	test('subgraphUtils.units', () => {
		expect(Object.keys(subgraphUtils.units)).toEqual([
			'destroy',
		]);
	});

	describe('destroy', () => {
		beforeEach(() => {
			subgraphUtils.units.destroy.mockRestore();
		});

		test('not concrete', async () => {
			const sg = new Subgraph();
			sg.addVertex('filler');
			expect(sg.concrete).toBe(false);

			await expect(subgraphUtils.units.destroy(sg, [0])).rejects.toThrow(/^Subgraph must be concrete$/);
		});

		test('only one', async () => {
			subgraphUtils.boundaries.deleteIdea.mockReturnValueOnce(Promise.resolve());

			const sg = new Subgraph();
			sg.addVertex('id', '_testA', { name: 'a' });
			sg.setData('a', { value: 'a' });

			expect(sg).toMatchSnapshot();

			await expect(subgraphUtils.units.destroy(sg, ['a'])).resolves.toEqual(sg);

			expect(sg).toMatchSnapshot();
			expect(subgraphUtils.boundaries.deleteIdea.mock.calls).toEqual([
				[{ id: '_testA' }],
			]);
		});

		test('few', async () => {
			subgraphUtils.boundaries.deleteIdea.mockReturnValueOnce(Promise.resolve());
			subgraphUtils.boundaries.deleteIdea.mockReturnValueOnce(Promise.resolve());

			const sg = new Subgraph();
			sg.addVertex('id', '_testA', { name: 'a' });
			sg.addVertex('id', '_testB', { name: 'b' });
			sg.addVertex('id', '_testC', { name: 'c' });
			sg.setData('b', { value: 'b' });

			expect(sg).toMatchSnapshot();

			await expect(subgraphUtils.units.destroy(sg, ['a', 'b'])).resolves.toEqual(sg);

			expect(sg).toMatchSnapshot();
			expect(subgraphUtils.boundaries.deleteIdea.mock.calls).toEqual([
				[{ id: '_testA' }],
				[{ id: '_testB' }],
			]);
		});
	});
});