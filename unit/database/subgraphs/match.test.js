const ideas = require('../../../src/database/ideas');
const match = require('../../../src/database/subgraphs/match');
const { Subgraph } = require('../../../src/database/subgraphs/subgraphs');

const { SubgraphMatchMetadata } = match;

describe('match', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(match.units).forEach((key) => {
			jest.spyOn(match.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	test('match.units', () => {
		expect(Object.keys(match.units)).toEqual([
			'doMatch',
			'recursiveMatch',
			'initializeVertexMap',
			'buildInverseIdeaMap',
			'filterOuter',
			'checkVertexData',
			'checkTransitionableVertexData',
			'checkFixedVertexData',
		]);
	});

	let metadata;
	beforeEach(() => {
		metadata = new SubgraphMatchMetadata(
			new Subgraph(),
			new Subgraph(),
			new Map(),
			false,
		);

		// the branches for these can get tricky
		// and getData sort of counts as 'boundaries'
		jest.spyOn(metadata.inner, 'getData').mockImplementation(() => { throw new Error('mock inner.getData'); });
		jest.spyOn(metadata.inner, 'getMatchData').mockImplementation(() => { throw new Error('mock inner.getMatchData'); });
		jest.spyOn(metadata.outer, 'getData').mockImplementation(() => { throw new Error('mock outer.getData'); });
		jest.spyOn(metadata.outer, 'getMatchData').mockImplementation(() => { throw new Error('outer.getMatchData should never be called'); });
	});

	describe('doMatch', () => {
		let outer;
		let inner;
		beforeEach(() => {
			match.units.doMatch.mockRestore();

			outer = new Subgraph();
			inner = new Subgraph();
		});

		test('invalid call', () => {
			expect(() => match.units.doMatch()).toThrow('Cannot destructure property');
			expect(() => match.units.doMatch(undefined, undefined, {})).toThrow("Cannot read properties of undefined (reading 'concrete')");
			expect(() => match.units.doMatch(outer, undefined, {})).toThrow("Cannot read properties of undefined (reading '$vertexCount')");
		});

		test('outer not concrete', async () => {
			const sg = { concrete: false }; // @mock Subgraph
			await expect(() => match.units.doMatch(sg, undefined, {})).rejects.toThrow(/^the outer subgraph must be concrete before you can match against it$/);
		});

		test('inner empty', async () => {
			outer.addVertex('id', ideas.proxy('_test'));
			outer.addVertex('id', ideas.proxy('_test'));

			expect(await match.units.doMatch(outer, inner, {})).toEqual([]);
		});

		test('inner bigger than outer (vertex)', async () => {
			outer.addVertex('id', ideas.proxy('_test'));
			inner.addVertex('id', ideas.proxy('_test'));
			inner.addVertex('id', ideas.proxy('_test'));

			expect(await match.units.doMatch(outer, inner, {})).toEqual([]);
		});

		test('inner bigger than outer (edge)', async () => {
			const outerVertexId = outer.addVertex('id', ideas.proxy('_test'));
			outer.addEdge(outerVertexId, 'sameAs', outerVertexId);
			outer.concrete = true;

			const innerVertexId = inner.addVertex('id', ideas.proxy('_test'));
			inner.addEdge(innerVertexId, 'sameAs', innerVertexId);
			inner.addEdge(innerVertexId, 'sameAs', innerVertexId);

			expect(await match.units.doMatch(outer, inner, {})).toEqual([]);
		});

		test('valid, cannot build innerToOuterVertexMap', async () => {
			match.units.initializeVertexMap.mockReturnValueOnce(Promise.resolve(undefined));

			outer.addVertex('id', ideas.proxy('_test'));
			inner.addVertex('id', ideas.proxy('_test'));

			expect(await match.units.doMatch(outer, inner, {})).toEqual([]);
		});

		test('valid, search', async () => {
			match.units.initializeVertexMap.mockReturnValueOnce(Promise.resolve(new Map()));
			match.units.recursiveMatch.mockReturnValueOnce(Promise.resolve([{ mock: 'result' }]));

			outer.addVertex('id', ideas.proxy('_test'));
			inner.addVertex('id', ideas.proxy('_test'));

			expect(await match.units.doMatch(outer, inner, {})).toEqual([{ mock: 'result' }]);
		});
	});

	test.todo('recursiveMatch');

	describe('initializeVertexMap', () => {
		let outer;
		let inner;
		beforeEach(() => {
			match.units.initializeVertexMap.mockRestore();
			match.units.buildInverseIdeaMap.mockRestore();

			outer = new Subgraph();
			inner = new Subgraph();

			outer.addVertex('exact', 1);
			outer.addVertex('id', ideas.proxy('_test'));
			outer.addVertex('id', ideas.proxy('_test2'));
		});

		test('simple id', async () => {
			inner.addVertex('exact', 1);
			inner.addVertex('id', ideas.proxy('_test'));
			inner.addVertex('id', ideas.proxy('_test2'));
			match.units.checkVertexData.mockReturnValueOnce(Promise.resolve(true));
			match.units.checkVertexData.mockReturnValueOnce(Promise.resolve(true));

			expect(await match.units.initializeVertexMap(outer, inner)).toMatchSnapshot();
		});

		test('simple mismatch', async () => {
			inner.addVertex('exact', 1);
			inner.addVertex('id', ideas.proxy('_test'));
			inner.addVertex('id', ideas.proxy('_test2'));
			match.units.checkVertexData.mockReturnValueOnce(Promise.resolve(false));
			match.units.checkVertexData.mockReturnValueOnce(Promise.resolve(true));

			expect(await match.units.initializeVertexMap(outer, inner)).toBe(undefined);
		});

		test('inner has other ids', async () => {
			inner.addVertex('id', ideas.proxy('_test2'));
			inner.addVertex('id', ideas.proxy('_test3'));
			match.units.checkVertexData.mockReturnValueOnce(Promise.resolve(true)); // might be called once, depends on Map

			// note: inner and outer are supposed to map to all the same ideas
			// (outer is a stand-in for / subset of the full idea graph)
			// so if inner has something that does not match outer, then this is a failure

			expect(await match.units.initializeVertexMap(outer, inner)).toBe(undefined);
		});

		test('duplicate ideas', async () => {
			outer.addVertex('id', ideas.proxy('_test'));

			inner.addVertex('exact', 1);
			inner.addVertex('id', ideas.proxy('_test'));
			inner.addVertex('id', ideas.proxy('_test2'));
			inner.addVertex('id', ideas.proxy('_test'));

			match.units.checkVertexData.mockReturnValueOnce(Promise.resolve(true));

			expect(await match.units.initializeVertexMap(outer, inner)).toMatchSnapshot();
		});

		test.todo('other practical examples');
	});

	describe('buildInverseIdeaMap', () => {
		beforeEach(() => {
			match.units.buildInverseIdeaMap.mockRestore();
		});

		test('simple', () => {
			metadata.outer.addVertex('exact', 1);
			metadata.outer.addVertex('id', ideas.proxy('_test'));
			metadata.outer.addVertex('id', ideas.proxy('_test2'));

			expect(match.units.buildInverseIdeaMap(metadata.outer.allIdeas())).toMatchSnapshot();
		});

		test('dup ideas', () => {
			metadata.outer.addVertex('exact', 1);
			metadata.outer.addVertex('id', ideas.proxy('_test'));
			metadata.outer.addVertex('id', ideas.proxy('_test2'));
			metadata.outer.addVertex('id', ideas.proxy('_test'));
			metadata.outer.addVertex('id', ideas.proxy('_test'));

			expect(match.units.buildInverseIdeaMap(metadata.outer.allIdeas())).toMatchSnapshot();
		});
	});

	test.todo('filterOuter');

	describe('checkVertexData', () => {
		let innerVertexId;
		let outerVertexId;
		beforeEach(() => {
			match.units.checkVertexData.mockRestore();

			innerVertexId = metadata.inner.addVertex('exact', 1);
			outerVertexId = metadata.outer.addVertex('exact', 1);
		});

		test('base logic without innerToOuterVertexMap (used during init)', async () => {
			delete metadata.innerToOuterVertexMap;
			delete metadata.inverseMap;

			match.units.checkTransitionableVertexData.mockReturnValueOnce(Promise.resolve(false));
			match.units.checkFixedVertexData.mockReturnValueOnce(Promise.resolve(false));

			expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);

			match.units.checkTransitionableVertexData.mockReturnValueOnce(Promise.resolve(true));
			match.units.checkFixedVertexData.mockReturnValueOnce(Promise.resolve(false));

			expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);

			match.units.checkTransitionableVertexData.mockReturnValueOnce(Promise.resolve(false));
			match.units.checkFixedVertexData.mockReturnValueOnce(Promise.resolve(true));

			expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);

			match.units.checkTransitionableVertexData.mockReturnValueOnce(Promise.resolve(true));
			match.units.checkFixedVertexData.mockReturnValueOnce(Promise.resolve(true));

			expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
		});

		describe('with innerToOuterVertexMap (standard use)', () => {
			test('innerVertexId mapped to something else', async () => {
				metadata.innerToOuterVertexMap.set(innerVertexId, 'not a valid id');
				metadata.inverseMap.set('not a valid id', innerVertexId);

				expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
				expect(match.units.checkTransitionableVertexData).not.toHaveBeenCalled();
				expect(match.units.checkFixedVertexData).not.toHaveBeenCalled();
			});

			test('outerVertexId mapped to something else', async () => {
				metadata.innerToOuterVertexMap.set('not a valid id', outerVertexId);
				metadata.inverseMap.set(outerVertexId, 'not a valid id');

				expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
				expect(match.units.checkTransitionableVertexData).not.toHaveBeenCalled();
				expect(match.units.checkFixedVertexData).not.toHaveBeenCalled();
			});

			test('already correctly mapped', async () => {
				// if this is the case, we shouldn't actually be calling this function
				metadata.innerToOuterVertexMap.set(innerVertexId, outerVertexId);
				metadata.inverseMap.set(outerVertexId, innerVertexId);

				expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
				expect(match.units.checkTransitionableVertexData).not.toHaveBeenCalled();
				expect(match.units.checkFixedVertexData).not.toHaveBeenCalled();
			});

			test('neither mapped', async () => {
				match.units.checkTransitionableVertexData.mockReturnValueOnce(Promise.resolve(false));
				match.units.checkFixedVertexData.mockReturnValueOnce(Promise.resolve(false));

				expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);

				match.units.checkTransitionableVertexData.mockReturnValueOnce(Promise.resolve(true));
				match.units.checkFixedVertexData.mockReturnValueOnce(Promise.resolve(true));

				expect(await match.units.checkVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
			});
		});
	});

	describe('checkTransitionableVertexData', () => {
		let innerVertexId;
		let outerVertexId;
		beforeEach(() => {
			match.units.checkTransitionableVertexData.mockRestore();

			outerVertexId = metadata.outer.addVertex('id', ideas.proxy('_test'), { transitionable: true });
		});

		test('inner already has idea (noop)', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1, { transitionable: true });
			jest.spyOn(metadata.inner, 'getVertex');

			metadata.inner.setIdea(innerVertexId, ideas.proxy('_test'));

			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
			expect(metadata.inner.getMatchData).not.toHaveBeenCalled();
			expect(metadata.outer.getData).not.toHaveBeenCalled();
			expect(metadata.inner.getVertex).not.toHaveBeenCalled();
		});

		test('inner is not transitionable (noop)', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1);

			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
			expect(metadata.inner.getMatchData).not.toHaveBeenCalled();
			expect(metadata.outer.getData).not.toHaveBeenCalled();
		});

		test('outer is not transitionable (noop)', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1, { transitionable: true });

			// make outer not transitionable (this isn't allowed in practice)
			metadata.outer.getVertex(outerVertexId).transitionable = false;

			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
			expect(metadata.inner.getMatchData).not.toHaveBeenCalled();
			expect(metadata.outer.getData).not.toHaveBeenCalled();
		});

		// it's unclear what should actually be tested
		test.skip('missing inner data', () => {});

		// it's unclear what should actually be tested
		test.skip('missing outer data', () => {});

		test('mismatched units existence', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1, { transitionable: true });

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ value: 1, $unit: 'banana' }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ value: 1 }));

			expect(metadata.unitsOnly).toBe(false);
			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);

			metadata.unitsOnly = true;

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ value: 1 }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ value: 1, $unit: 'apple' }));

			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
		});

		test('mismatched units', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1, { transitionable: true });

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ value: 1, $unit: 'banana' }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ value: 1, $unit: 'apple' }));

			expect(metadata.unitsOnly).toBe(false);
			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);

			metadata.unitsOnly = true;

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ value: 1, $unit: 'banana' }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ value: 1, $unit: 'apple' }));

			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
		});

		test('unitsOnly just units', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1, { transitionable: true });
			metadata.unitsOnly = true;

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ value: 1, $unit: 'banana' }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ value: 1, $unit: 'banana' }));

			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ value: 1, $unit: 'banana' }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ value: 2, $unit: 'banana' }));

			expect(await match.units.checkTransitionableVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
		});

		// it's unclear if this should compare the data or run the matcher
		test.skip('unitsOnly but no units (equality)', () => {});

		// it's unclear if this should compare the data or run the matcher
		test.skip('equality', () => {});
	});

	describe('checkFixedVertexData', () => {
		let innerVertexId;
		let outerVertexId;
		beforeEach(() => {
			match.units.checkFixedVertexData.mockRestore();

			outerVertexId = metadata.outer.addVertex('id', ideas.proxy('_test'));
		});

		test('inner already has idea (noop)', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1);
			jest.spyOn(metadata.inner, 'getVertex');

			metadata.inner.setIdea(innerVertexId, ideas.proxy('_test'));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
			expect(metadata.inner.getData).not.toHaveBeenCalled();
			expect(metadata.outer.getData).not.toHaveBeenCalled();
			expect(metadata.inner.getVertex).not.toHaveBeenCalled();
		});

		test('inner is transitionable (noop)', async () => {
			innerVertexId = metadata.inner.addVertex('exact', 1, { transitionable: true });

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);
			expect(metadata.inner.getData).not.toHaveBeenCalled();
			expect(metadata.outer.getData).not.toHaveBeenCalled();
		});

		test('inner pointer, id matcher', async () => {
			const iv = metadata.inner.addVertex('exact', 1);
			innerVertexId = metadata.inner.addVertex('id', iv, { pointer: true });

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve(['_test']));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve(['_test2']));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
		});

		test('inner pointer, other matcher', async () => {
			const iv = metadata.inner.addVertex('exact', 1);
			innerVertexId = metadata.inner.addVertex('exact', iv, { pointer: true });

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ thing1: 3.14, thing2: 2.71 }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ thing1: 3.14, thing2: 2.71 }));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);

			metadata.inner.getMatchData.mockReturnValueOnce(Promise.resolve([{ thing1: 3.14, thing2: 2.71 }]));
			metadata.outer.getData.mockReturnValueOnce(Promise.resolve({ thing1: 3, thing2: 2 }));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
		});

		/** these probably should have already been mapped, but just in case... */
		test('inner not pointer, id matcher', async () => {
			innerVertexId = metadata.inner.addVertex('id', ideas.proxy('_test'));

			// remove the mapping
			metadata.inner.deleteIdea(innerVertexId);

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);

			// outer is somewhere else
			metadata.outer.setIdea(outerVertexId, ideas.proxy('_test2'));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
		});

		test('inner not pointer, other matcher', async () => {
			innerVertexId = metadata.inner.addVertex('substring', { value: 'banana' });

			metadata.outer.getData.mockReturnValueOnce(Promise.resolve('bananas'));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(true);

			metadata.outer.getData.mockReturnValueOnce(Promise.resolve('apple'));

			expect(await match.units.checkFixedVertexData(metadata, innerVertexId, outerVertexId)).toBe(false);
		});
	});
});