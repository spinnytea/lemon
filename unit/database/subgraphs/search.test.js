const _ = require('lodash');
const config = require('../../../src/config');
const io = require('../../../src/database/ideas/io');
const ideas = require('../../../src/database/ideas');
const subgraphs = require('../../../src/database/subgraphs/subgraphs');

const search = require('../../../src/database/subgraphs/search');

const { Subgraph } = subgraphs;

function makeEdges(prefs) {
	return prefs.map((pref) => ({ pref }));
}

describe('search', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(search.units).forEach((key) => {
			jest.spyOn(search.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	/*
		typeOf
			square -> rectangle -> parallelogram -> quadrilateral
			square -> rhombus -> parallelogram
		property
			rectangle -> height
			rectangle -> width

		sameAs
			dog -> cat -> wolf -> dog
		typeOf
			dog -> cat -> wolf -> dog
	*/
	let IDEA_MAP;
	let BACKUP_DATABASE;
	let BACKUP_CONFIG;
	beforeEach(async () => {
		if (!BACKUP_DATABASE) {
			IDEA_MAP = {
				square: { value: 'square' },
				rectangle: { value: 'rectangle' },
				rhombus: { value: 'rhombus' },
				parallelogram: { value: 'parallelogram' },
				quadrilateral: { value: 'quadrilateral' },
				width: { value: 'width' },
				height: { value: 'height' },

				dog: { value: 'dog' },
				cat: { value: 'cat' },
				wolf: { value: 'wolf' },

				matchPointer1: null,
				matchPointer2: null,
			};
			await ideas.createGraph(IDEA_MAP, [
				['square', 'typeOf', 'rectangle'],
				['square', 'typeOf', 'rhombus'],
				['rectangle', 'typeOf', 'parallelogram'],
				['rhombus', 'typeOf', 'parallelogram'],
				['parallelogram', 'typeOf', 'quadrilateral'],
				['rectangle', 'property', 'width'],
				['rectangle', 'property', 'height'],

				['dog', 'typeOf', 'cat'],
				['cat', 'typeOf', 'wolf'],
				['wolf', 'typeOf', 'dog'],

				['dog', 'sameAs', 'cat'],
				['cat', 'sameAs', 'wolf'],
				['wolf', 'sameAs', 'dog'],
			]);
			BACKUP_DATABASE = io.units.database;
			BACKUP_CONFIG = config.units.data;
		}
		else {
			io.units.database = BACKUP_DATABASE;
			config.units.data = BACKUP_CONFIG;
		}
	});

	test('search.units', () => {
		expect(Object.keys(search.units)).toEqual([
			'doSearch',
			'recursiveSearch',
			'verifyEdges',
			'verifyEdge',
			'$verifyEdgeTransitive',
			'bestEdge',
			'$bestEdgeOptions',
			'getBranches',
			'$getBranchesTransitive',
			'expandEdges',
		]);
		expect(io.units.database).toMatchSnapshot();
	});

	describe('doSearch', () => {
		let sg; // @mock Subgraph
		beforeEach(() => {
			search.units.doSearch.mockRestore();
			search.units.recursiveSearch.mockImplementationOnce(() => Promise.resolve());

			jest.spyOn(subgraphs, 'Subgraph').mockImplementation(() => ({ a: 'copy' }));

			sg = {
				$idea: { size: 1 },
				allEdges: jest.fn().mockImplementationOnce(() => makeEdges([1, 0, 2, 0, 3, 0, 4])),
				concrete: false,
				isIdeaEmpty: jest.fn().mockImplementation(() => (sg.$idea.size === 0)),
			};
		});

		test('must have some ideas defined', async () => {
			sg.$idea.size = 0;

			await expect(() => search.units.doSearch(sg)).rejects.toThrow(/^Subgraph must have at least one idea pinned down$/);

			expect(search.units.recursiveSearch).not.toHaveBeenCalled();
		});

		test('noop concrete', async () => {
			sg.concrete = true;

			const results = await search.units.doSearch(sg);

			expect(results).toEqual([sg]);
			expect(results[0]).toBe(sg);

			expect(sg.allEdges).not.toHaveBeenCalled();
			expect(subgraphs.Subgraph).not.toHaveBeenCalled();
			expect(search.units.recursiveSearch).not.toHaveBeenCalled();
		});

		test('edge order', async () => {
			expect(search.units.recursiveSearch).not.toHaveBeenCalled();

			await search.units.doSearch(sg);

			expect(search.units.recursiveSearch.mock.calls.length).toBe(1);
			expect(search.units.recursiveSearch).toHaveBeenCalledWith({ a: 'copy' }, makeEdges([4, 3, 2, 1, 0, 0, 0]));
		});
	});

	describe('recursiveSearch', () => {
		beforeEach(() => {
			search.units.recursiveSearch.mockRestore();
			jest.spyOn(search.units, 'recursiveSearch');
		});

		describe('ways to not find a match', () => {
			test('null from verifyEdges', async () => {
				search.units.verifyEdges.mockReturnValueOnce(null);

				expect(await search.units.recursiveSearch('a', ['1', '2', '3'])).toEqual([]);

				expect(search.units.verifyEdges.mock.calls).toEqual([
					['a', ['1', '2', '3']],
				]);
				expect(search.units.bestEdge.mock.calls).toEqual([]);
				expect(search.units.expandEdges.mock.calls).toEqual([]);
				expect(search.units.recursiveSearch.mock.calls).toEqual([
					['a', ['1', '2', '3']],
				]);
			});

			test('no edges left and not finished', async () => {
				search.units.verifyEdges.mockReturnValueOnce([]);
				const sg = new Subgraph();
				sg.addVertex('filler');

				expect(await search.units.recursiveSearch(sg, ['1', '2', '3'])).toEqual([]);

				expect(search.units.verifyEdges.mock.calls).toEqual([
					[sg, ['1', '2', '3']],
				]);
				expect(search.units.bestEdge.mock.calls).toEqual([]);
				expect(search.units.expandEdges.mock.calls).toEqual([]);
				expect(search.units.recursiveSearch.mock.calls).toEqual([
					[sg, ['1', '2', '3']],
				]);
			});

			test('null from bestEdge', async () => {
				search.units.verifyEdges.mockReturnValueOnce(['1', '2']);
				search.units.bestEdge.mockReturnValueOnce(null);

				expect(await search.units.recursiveSearch('a', ['1', '2', '3'])).toEqual([]);

				expect(search.units.verifyEdges.mock.calls).toEqual([
					['a', ['1', '2', '3']],
				]);
				expect(search.units.bestEdge.mock.calls).toEqual([
					['a', ['1', '2']],
				]);
				expect(search.units.expandEdges.mock.calls).toEqual([]);
				expect(search.units.recursiveSearch.mock.calls).toEqual([
					['a', ['1', '2', '3']],
				]);
			});

			test('empty from expandEdges', async () => {
				search.units.verifyEdges.mockReturnValueOnce(['1', '2']);
				search.units.bestEdge.mockReturnValueOnce({ edge: '2' });
				search.units.expandEdges.mockReturnValueOnce([]);

				expect(await search.units.recursiveSearch('a', ['1', '2', '3'])).toEqual([]);

				expect(search.units.verifyEdges.mock.calls).toEqual([
					['a', ['1', '2', '3']],
				]);
				expect(search.units.bestEdge.mock.calls).toEqual([
					['a', ['1', '2']],
				]);
				expect(search.units.expandEdges.mock.calls).toEqual([
					['a', { edge: '2' }],
				]);
				expect(search.units.recursiveSearch.mock.calls).toEqual([
					['a', ['1', '2', '3']],
				]);
			});

			test('successful expand, but ultimately, failed', async () => {
				search.units.verifyEdges.mockReturnValueOnce(['1', '2']);
				search.units.verifyEdges
					.mockReturnValueOnce(null)
					.mockReturnValueOnce(null)
					.mockReturnValueOnce(null);
				search.units.bestEdge.mockReturnValueOnce({ edge: '2' });
				search.units.expandEdges.mockReturnValueOnce(['X', 'Y', 'Z']);

				expect(await search.units.recursiveSearch('a', ['1', '2', '3'])).toEqual([]);

				expect(search.units.verifyEdges.mock.calls).toEqual([
					['a', ['1', '2', '3']],
					['X', ['1']],
					['Y', ['1']],
					['Z', ['1']],
				]);
				expect(search.units.bestEdge.mock.calls).toEqual([
					['a', ['1', '2']],
				]);
				expect(search.units.expandEdges.mock.calls).toEqual([
					['a', { edge: '2' }],
				]);
				expect(search.units.recursiveSearch.mock.calls).toEqual([
					['a', ['1', '2', '3']],
					['X', ['1']],
					['Y', ['1']],
					['Z', ['1']],
				]);
			});
		});

		describe('ways to succeed', () => {
			let sg;
			beforeEach(() => {
				sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square);
			});

			test('already complete', async () => {
				search.units.verifyEdges.mockReturnValueOnce([]);
				expect(sg.$vertexCount).toBe(1);
				expect(sg.$idea.size).toBe(1);

				expect(await search.units.recursiveSearch(sg, [])).toEqual([sg]);

				expect(search.units.verifyEdges.mock.calls).toEqual([
					[sg, []],
				]);
				expect(search.units.bestEdge.mock.calls).toEqual([]);
				expect(search.units.expandEdges.mock.calls).toEqual([]);
				expect(search.units.recursiveSearch.mock.calls).toEqual([
					[sg, []],
				]);
			});

			test('successful expand, and list of complete', async () => {
				search.units.verifyEdges.mockReturnValueOnce(['1', '2']);
				search.units.verifyEdges
					.mockReturnValueOnce([])
					.mockReturnValueOnce([])
					.mockReturnValueOnce([]);
				search.units.bestEdge.mockReturnValueOnce({ edge: '2' });
				const x = new Subgraph(sg);
				x.setIdea(0, IDEA_MAP.dog); // add variation so we can tell them apart
				const y = new Subgraph(sg);
				y.setIdea(0, IDEA_MAP.cat); // add variation so we can tell them apart
				const z = new Subgraph(sg);
				z.setIdea(0, IDEA_MAP.wolf); // add variation so we can tell them apart
				search.units.expandEdges.mockReturnValueOnce([x, y, z]);

				expect(await search.units.recursiveSearch(sg, ['1', '2', '3'])).toEqual([x, y, z]);

				expect(search.units.verifyEdges.mock.calls).toEqual([
					[sg, ['1', '2', '3']],
					[x, ['1']],
					[y, ['1']],
					[z, ['1']],
				]);
				expect(search.units.bestEdge.mock.calls).toEqual([
					[sg, ['1', '2']],
				]);
				expect(search.units.expandEdges.mock.calls).toEqual([
					[sg, { edge: '2' }],
				]);
				expect(search.units.recursiveSearch.mock.calls).toEqual([
					[sg, ['1', '2', '3']],
					[x, ['1']],
					[y, ['1']],
					[z, ['1']],
				]);
			});
		});
	});

	test('verifyEdges', async () => {
		search.units.verifyEdges.mockRestore();

		const sg = new Subgraph();
		sg.addVertex('filler');
		sg.addVertex('filler');
		sg.addVertex('filler');
		sg.addEdge(0, 'property', 1);
		sg.addEdge(0, 'property', 2);
		sg.addEdge(1, 'property', 2);
		sg.setIdea(0, IDEA_MAP.square);
		sg.setIdea(1, IDEA_MAP.rectangle);

		const edges = sg.allEdges();
		const original = [sg.getEdge(0), sg.getEdge(1), sg.getEdge(2)];
		expect(edges).toEqual(original);

		// one edge is "done" and two edges are "unfinished"
		// if that one edge is valid, we get the two unfinished ones

		search.units.verifyEdge.mockImplementationOnce(() => true);

		expect(await search.units.verifyEdges(sg, edges)).toEqual([sg.getEdge(1), sg.getEdge(2)]);

		expect(search.units.verifyEdge.mock.calls.length).toBe(1);
		expect(search.units.verifyEdge.mock.calls[0]).toEqual([sg, sg.getEdge(0)]);

		// reset between tests
		expect(edges).toEqual(original);
		search.units.verifyEdge.mockReset();
		expect(search.units.verifyEdge).not.toHaveBeenCalled();

		// if that one edge is invalid, then the whole branch is a dead end

		search.units.verifyEdge.mockImplementationOnce(() => false);

		expect(await search.units.verifyEdges(sg, edges)).toBe(null);

		expect(search.units.verifyEdge.mock.calls.length).toBe(1);
		expect(search.units.verifyEdge.mock.calls[0]).toEqual([sg, sg.getEdge(0)]);

		// edges should not be changed during this
		expect(edges).toEqual(original);
	});

	describe('verifyEdge', () => {
		describe('not transitive', () => {
			let sg;
			let edge;
			beforeEach(() => {
				search.units.verifyEdge.mockRestore();
				sg = new Subgraph();
				sg.addEdge(sg.addVertex('filler'), 'property', sg.addVertex('filler'));
				sg.setIdea(0, IDEA_MAP.rectangle);
				sg.setIdea(1, IDEA_MAP.width);
				edge = sg.getEdge(0);
			});

			test('preconditions', () => {
				expect(edge.link.transitive).toBe(false);
			});

			test('valid use', async () => {
				expect(await search.units.verifyEdge(sg, edge)).toBe(true);
			});

			test('invalid: src not defined', () => {
				expect.assertions(1);

				sg.setIdea(0, undefined);

				return search.units.verifyEdge(sg, edge).catch((err) => {
					expect(err.message).toBe("Cannot read properties of undefined (reading 'getLinksOfType')");
				});
			});

			test('invalid: dst not defined', () => {
				expect.assertions(1);

				sg.setIdea(1, undefined);

				return search.units.verifyEdge(sg, edge).catch((err) => {
					expect(err.message).toBe("Cannot read properties of undefined (reading 'id')");
				});
			});
		});

		describe('$verifyEdgeTransitive', () => {
			beforeEach(() => {
				search.units.verifyEdge.mockRestore();
				search.units.$verifyEdgeTransitive.mockRestore();
			});

			describe('directed', () => {
				test('immediate', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.parallelogram),
						'typeOf',
						sg.addVertex('id', IDEA_MAP.quadrilateral),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(true);
				});

				test('few', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.rectangle),
						'typeOf',
						sg.addVertex('id', IDEA_MAP.quadrilateral),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(true);
				});

				test('branched', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.square),
						'typeOf',
						sg.addVertex('id', IDEA_MAP.parallelogram),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(true);
				});

				test('edge incorrect', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.width),
						'typeOf',
						sg.addVertex('id', IDEA_MAP.quadrilateral),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(false);
				});
			});

			describe('directed circular', () => {
				test('found', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.dog),
						'typeOf',
						sg.addVertex('id', IDEA_MAP.wolf),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(true);
				});

				test('edge incorrect', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.dog),
						'typeOf',
						sg.addVertex('id', IDEA_MAP.square),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(false);
				});
			});

			describe('undirected', () => {
				test('found', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.dog),
						'sameAs',
						sg.addVertex('id', IDEA_MAP.wolf),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(true);
				});

				test('edge incorrect', async () => {
					const sg = new Subgraph();
					const edge = sg.getEdge(sg.addEdge(
						sg.addVertex('id', IDEA_MAP.dog),
						'sameAs',
						sg.addVertex('id', IDEA_MAP.square),
					));

					expect(await search.units.verifyEdge(sg, edge)).toBe(false);
				});
			});
		});
	});

	describe('bestEdge', () => {
		beforeEach(() => {
			search.units.bestEdge.mockRestore();
		});

		test('handles one pref', async () => {
			search.units.$bestEdgeOptions.mockReturnValueOnce([null, null]);

			const edges = [
				{ pref: 0, name: '1' },
				{ pref: 0, name: '2' },
			];
			expect(await search.units.bestEdge('a', edges)).toEqual(undefined);

			expect(search.units.$bestEdgeOptions.mock.calls).toEqual([
				['a', [{ pref: 0, name: '1' }, { pref: 0, name: '2' }]],
			]);
		});

		test('checks by decreasing pref', async () => {
			search.units.$bestEdgeOptions
				.mockReturnValueOnce([null])
				.mockReturnValueOnce([null, null])
				.mockReturnValueOnce([null, null])
				.mockReturnValueOnce([null])
				.mockReturnValueOnce([null, null])
				.mockReturnValueOnce([null]);

			const edges = [
				{ pref: -1, name: '0' },
				{ pref: 0, name: '1' },
				{ pref: 1, name: '2' },
				{ pref: 2, name: '3' },
				{ pref: 3, name: '4' },
				{ pref: 2, name: '5' },
				{ pref: 1, name: '6' },
				{ pref: -2, name: '7' },
				{ pref: -1, name: '8' },
			];
			expect(await search.units.bestEdge('a', edges)).toEqual(undefined);

			expect(search.units.$bestEdgeOptions.mock.calls).toEqual([
				['a', [{ pref: 3, name: '4' }]],
				['a', [{ pref: 2, name: '3' }, { pref: 2, name: '5' }]],
				['a', [{ pref: 1, name: '2' }, { pref: 1, name: '6' }]],
				['a', [{ pref: 0, name: '1' }]],
				['a', [{ pref: -1, name: '0' }, { pref: -1, name: '8' }]],
				['a', [{ pref: -2, name: '7' }]],
			]);
		});

		test('pick least branches', async () => {
			search.units.$bestEdgeOptions.mockReturnValueOnce([
				{ edge: { pref: 0, name: '3' }, branches: [1, 2, 3] },
				{ edge: { pref: 0, name: '1' }, branches: [1] },
				{ edge: { pref: 0, name: '2' }, branches: [1, 2] },
			]);

			const edges = [
				{ pref: 0, name: '3' },
				{ pref: 0, name: '1' },
				{ pref: 0, name: '2' },
			];
			expect(await search.units.bestEdge('a', edges)).toEqual({ edge: { pref: 0, name: '1' }, branches: [1] });

			expect(search.units.$bestEdgeOptions.mock.calls).toEqual([
				['a', [{ pref: 0, name: '3' }, { pref: 0, name: '1' }, { pref: 0, name: '2' }]],
			]);
		});

		test('handles empty list', async () => {
			expect(await search.units.bestEdge('a', [])).toEqual(undefined);

			expect(search.units.$bestEdgeOptions).not.toHaveBeenCalled();
		});
	});

	describe('$bestEdgeOptions', () => {
		beforeEach(() => {
			search.units.$bestEdgeOptions.mockRestore();
		});

		// this should have already been pruned, this edge shouldn't have made it this far
		test('both sides have an idea pinned', async () => {
			const sg = new Subgraph();
			const edge = sg.getEdge(sg.addEdge(
				sg.addVertex('id', IDEA_MAP.parallelogram),
				'typeOf',
				sg.addVertex('id', IDEA_MAP.quadrilateral),
			));

			expect(await search.units.$bestEdgeOptions(sg, [edge])).toEqual([null]);

			expect(search.units.getBranches).not.toHaveBeenCalled();
		});

		test('neither side has an idea pinned', async () => {
			const sg = new Subgraph();
			const edge = sg.getEdge(sg.addEdge(
				sg.addVertex('filler'),
				'typeOf',
				sg.addVertex('filler'),
			));

			expect(await search.units.$bestEdgeOptions(sg, [edge])).toEqual([null]);

			expect(search.units.getBranches).not.toHaveBeenCalled();
		});

		test('edge checks out', async () => {
			search.units.getBranches.mockReturnValueOnce(Promise.resolve([IDEA_MAP.width, IDEA_MAP.height]));

			const sg = new Subgraph();
			const edge = sg.getEdge(sg.addEdge(
				sg.addVertex('id', IDEA_MAP.rectangle),
				'property',
				sg.addVertex('filler'),
			));

			expect(await search.units.$bestEdgeOptions(sg, [edge])).toEqual([
				{ edge, branches: [IDEA_MAP.width, IDEA_MAP.height], isForward: true },
			]);

			expect(search.units.getBranches.mock.calls).toEqual([
				[sg, edge, true],
			]);
		});

		test('multiple edges', async () => {
			search.units.getBranches
				.mockReturnValueOnce(Promise.resolve([IDEA_MAP.width, IDEA_MAP.height]))
				.mockReturnValueOnce(Promise.resolve([IDEA_MAP.parallelogram, IDEA_MAP.quadrilateral]));

			const sg = new Subgraph();
			sg.addVertex('id', IDEA_MAP.rectangle, { name: 'rectangle' });
			const edge1 = sg.getEdge(sg.addEdge(
				'rectangle',
				'property',
				sg.addVertex('filler'),
			));
			const edge2 = sg.getEdge(sg.addEdge(
				'rectangle',
				'typeOf',
				sg.addVertex('filler'),
			));

			expect(await search.units.$bestEdgeOptions(sg, [edge1, edge2])).toEqual([
				{ edge: edge1, branches: [IDEA_MAP.width, IDEA_MAP.height], isForward: true },
				{ edge: edge2, branches: [IDEA_MAP.parallelogram, IDEA_MAP.quadrilateral], isForward: true },
			]);

			expect(search.units.getBranches.mock.calls).toEqual([
				[sg, edge1, true],
				[sg, edge2, true],
			]);
		});

		describe('pointer', () => {
			test('valid', async () => {
				await IDEA_MAP.matchPointer1.setData(IDEA_MAP.width.id);
				search.units.getBranches.mockReturnValueOnce(Promise.resolve([IDEA_MAP.width, IDEA_MAP.height]));

				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.matchPointer1, { name: 'matchPointer1' });
				const edge = sg.getEdge(sg.addEdge(
					sg.addVertex('id', IDEA_MAP.rectangle),
					'property',
					sg.addVertex('id', 'matchPointer1', { pointer: true }),
				));

				expect(await search.units.$bestEdgeOptions(sg, [edge])).toEqual([
					{ edge, branches: [IDEA_MAP.width, IDEA_MAP.height], isForward: true },
				]);

				expect(search.units.getBranches.mock.calls).toEqual([
					[sg, edge, true],
				]);
			});

			test('not defined', async () => {
				search.units.getBranches.mockReturnValueOnce(Promise.resolve([IDEA_MAP.width, IDEA_MAP.height]));

				const sg = new Subgraph();
				sg.addVertex('filler', undefined, { name: 'matchPointer1' });
				const edge = sg.getEdge(sg.addEdge(
					sg.addVertex('id', IDEA_MAP.rectangle),
					'property',
					sg.addVertex('id', 'matchPointer1', { pointer: true }),
				));

				expect(await search.units.$bestEdgeOptions(sg, [edge])).toEqual([null]);

				expect(search.units.getBranches).not.toHaveBeenCalled();
			});

			test('orData and valid', async () => {
				await IDEA_MAP.matchPointer1.setData(IDEA_MAP.width.id);
				await IDEA_MAP.matchPointer2.setData(IDEA_MAP.height.id);
				search.units.getBranches.mockReturnValueOnce(Promise.resolve([IDEA_MAP.width, IDEA_MAP.height]));

				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.matchPointer1, { name: 'matchPointer1' });
				sg.addVertex('id', IDEA_MAP.matchPointer2, { name: 'matchPointer2' });
				const edge = sg.getEdge(sg.addEdge(
					sg.addVertex('id', IDEA_MAP.rectangle),
					'property',
					sg.addVertex('id', ['matchPointer1', 'matchPointer2'], { pointer: true, orData: true }),
				));

				expect(await search.units.$bestEdgeOptions(sg, [edge])).toEqual([
					{ edge, branches: [IDEA_MAP.width, IDEA_MAP.height], isForward: true },
				]);

				expect(search.units.getBranches.mock.calls).toEqual([
					[sg, edge, true],
				]);
			});

			test('orData but none defined', async () => {
				search.units.getBranches.mockReturnValueOnce(Promise.resolve([IDEA_MAP.width, IDEA_MAP.height]));

				const sg = new Subgraph();
				sg.addVertex('filler', undefined, { name: 'matchPointer1' });
				sg.addVertex('filler', undefined, { name: 'matchPointer2' });
				const edge = sg.getEdge(sg.addEdge(
					sg.addVertex('id', IDEA_MAP.rectangle),
					'property',
					sg.addVertex('id', ['matchPointer1', 'matchPointer2'], { pointer: true, orData: true }),
				));

				expect(await search.units.$bestEdgeOptions(sg, [edge])).toEqual([null]);

				expect(search.units.getBranches).not.toHaveBeenCalled();
			});
		});
	});

	describe('getBranches', () => {
		describe('not transitive', () => {
			let sg;
			let edge;
			beforeEach(() => {
				search.units.getBranches.mockRestore();

				sg = new Subgraph();
				sg.addVertex('exact', { value: 'rectangle' }, { name: 'rectangle' });
				sg.addVertex('exact', { value: 'width' }, { name: 'width' });
				sg.addEdge('rectangle', 'property', 'width', { name: 'r_w' });
				edge = sg.getEdge('r_w');
			});

			test('forward', async () => {
				sg.setIdea('rectangle', IDEA_MAP.rectangle);

				const branches = await search.units.getBranches(sg, edge, true);

				expect(_.sortBy(branches, 'id')).toEqual([IDEA_MAP.width, IDEA_MAP.height]);
			});

			test('reverse', async () => {
				sg.setIdea('width', IDEA_MAP.width);

				const branches = await search.units.getBranches(sg, edge, false);

				expect(_.sortBy(branches, 'id')).toEqual([IDEA_MAP.rectangle]);
			});
		});

		describe('$getBranchesTransitive', () => {
			beforeEach(() => {
				search.units.getBranches.mockRestore();
				search.units.$getBranchesTransitive.mockRestore();
			});

			describe('directed', () => {
				let sg;
				beforeEach(() => {
					sg = new Subgraph();
					sg.addVertex('filler', undefined, { name: 'square' });
					sg.addVertex('id', IDEA_MAP.rectangle, { name: 'rectangle' });
					sg.addVertex('filler', undefined, { name: 'parallelogram' });
					sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
					sg.addEdge('rectangle', 'typeOf', 'parallelogram', { name: 'r_p' });
				});

				test('forward, few', async () => {
					const edge = sg.getEdge('r_p');

					const branches = await search.units.getBranches(sg, edge, true);

					expect(_.sortBy(branches, 'id')).toEqual([IDEA_MAP.parallelogram, IDEA_MAP.quadrilateral]);
				});

				test('reverse, one', async () => {
					const edge = sg.getEdge('s_r');

					const branches = await search.units.getBranches(sg, edge, false);

					expect(_.sortBy(branches, 'id')).toEqual([IDEA_MAP.square]);
				});
			});

			test('directed circular', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.dog, { name: 'dog' });
				sg.addVertex('filler', undefined, { name: 'unknown' });
				const edge = sg.getEdge(sg.addEdge('dog', 'typeOf', 'unknown'));

				const branches = await search.units.getBranches(sg, edge, true);

				expect(_.sortBy(branches, 'id')).toEqual([IDEA_MAP.dog, IDEA_MAP.cat, IDEA_MAP.wolf]);
			});

			test('undirected', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.dog, { name: 'dog' });
				sg.addVertex('filler', undefined, { name: 'unknown' });
				const edge = sg.getEdge(sg.addEdge('dog', 'sameAs', 'unknown'));

				const branches = await search.units.getBranches(sg, edge, true);

				expect(_.sortBy(branches, 'id')).toEqual([IDEA_MAP.dog, IDEA_MAP.cat, IDEA_MAP.wolf]);
			});
		});
	});

	// test all matchers, pointer true/false
	describe('expandEdges', () => {
		beforeEach(() => {
			search.units.expandEdges.mockRestore();
		});

		describe('branches do not match', () => {
			test('id', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('id', IDEA_MAP.rectangle, { name: 'rectangle' });
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				sg.deleteIdea('rectangle');
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [IDEA_MAP.cat],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([]);
				expect(sg.getIdea('rectangle')).toBe(undefined);
			});

			test('filler', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('filler', undefined, { name: 'rectangle' });
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([]);
				expect(sg.getIdea('rectangle')).toBe(undefined);
			});

			test('substring', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('substring', { value: 'rectangle', path: 'value' }, { name: 'rectangle' });
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [IDEA_MAP.cat],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([]);
				expect(sg.getIdea('rectangle')).toBe(undefined);
			});
		});

		describe('branch matches', () => {
			test('id', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('id', IDEA_MAP.rectangle, { name: 'rectangle' });
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				sg.deleteIdea('rectangle');
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [IDEA_MAP.rectangle, IDEA_MAP.cat],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([sg]);
				expect(nextSteps[0]).toBe(sg);
				expect(sg.getIdea('rectangle')).toEqual(IDEA_MAP.rectangle);
			});

			test('filler', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('filler', undefined, { name: 'rectangle' });
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [IDEA_MAP.rectangle],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([sg]);
				expect(nextSteps[0]).toBe(sg);
				expect(sg.getIdea('rectangle')).toEqual(IDEA_MAP.rectangle);
			});

			test('substring', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('substring', { value: 'rectangle', path: 'value' }, { name: 'rectangle' });
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [IDEA_MAP.rectangle, IDEA_MAP.cat],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([sg]);
				expect(nextSteps[0]).toBe(sg);
				expect(sg.getIdea('rectangle')).toEqual(IDEA_MAP.rectangle);
			});

			test('options.orData', async () => {
				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex(
					'substring',
					[{ value: 'rectangle', path: 'value' }, { value: 'parallelogram', path: 'value' }],
					{ name: 'rectangle', orData: true },
				);
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [IDEA_MAP.rectangle, IDEA_MAP.cat],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([sg]);
				expect(nextSteps[0]).toBe(sg);
				expect(sg.getIdea('rectangle')).toEqual(IDEA_MAP.rectangle);
			});
		});

		test('multiple branches match', async () => {
			const sg = new Subgraph();
			sg.addVertex('filler', undefined, { name: 'shapes' });
			sg.addVertex('id', IDEA_MAP.quadrilateral, { name: 'quadrilateral' });
			sg.addEdge('shapes', 'typeOf', 'quadrilateral', { name: 's_q' });
			const selected = {
				edge: sg.getEdge('s_q'),
				branches: [IDEA_MAP.rectangle, IDEA_MAP.rhombus, IDEA_MAP.parallelogram],
				isForward: false,
			};

			expect(sg.getIdea('shapes')).toBe(undefined);

			const nextSteps = await search.units.expandEdges(sg, selected);

			expect(nextSteps).toEqual([
				expect.any(Subgraph),
				expect.any(Subgraph),
				expect.any(Subgraph),
			]);
			expect(sg.getIdea('shapes')).toBe(undefined);
			expect(nextSteps[0].getIdea('shapes')).toBe(IDEA_MAP.rectangle);
			expect(nextSteps[1].getIdea('shapes')).toBe(IDEA_MAP.rhombus);
			expect(nextSteps[2].getIdea('shapes')).toBe(IDEA_MAP.parallelogram);
		});

		describe('pointer', () => {
			test('substring', async () => {
				await IDEA_MAP.matchPointer1.setData({ value: 'rectangle', path: 'value' });

				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.matchPointer1, { name: 'matchPointer1' });
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('substring', 'matchPointer1', { pointer: true, name: 'rectangle' });
				sg.addEdge('square', 'typeOf', 'rectangle', { name: 's_r' });
				const selected = {
					edge: sg.getEdge('s_r'),
					branches: [IDEA_MAP.rectangle, IDEA_MAP.cat],
					isForward: true,
				};

				expect(sg.getIdea('rectangle')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([sg]);
				expect(nextSteps[0]).toBe(sg);
				expect(sg.getIdea('rectangle')).toEqual(IDEA_MAP.rectangle);
			});

			test('options.orData', async () => {
				await IDEA_MAP.matchPointer1.setData({ value: 'rectangle', path: 'value' });
				await IDEA_MAP.matchPointer2.setData({ value: 'rhombus', path: 'value' });

				const sg = new Subgraph();
				sg.addVertex('id', IDEA_MAP.matchPointer1, { name: 'matchPointer1' });
				sg.addVertex('id', IDEA_MAP.matchPointer2, { name: 'matchPointer2' });
				sg.addVertex('id', IDEA_MAP.square, { name: 'square' });
				sg.addVertex('substring', ['matchPointer1', 'matchPointer2'], { pointer: true, name: 'shapes', orData: true });
				sg.addEdge('square', 'typeOf', 'shapes', { name: 's_s' });
				const selected = {
					edge: sg.getEdge('s_s'),
					branches: [IDEA_MAP.rectangle, IDEA_MAP.rhombus, IDEA_MAP.cat],
					isForward: true,
				};

				expect(sg.getIdea('shapes')).toBe(undefined);

				const nextSteps = await search.units.expandEdges(sg, selected);

				expect(nextSteps).toEqual([
					expect.any(Subgraph),
					expect.any(Subgraph),
				]);
				expect(sg.getIdea('shapes')).toBe(undefined);
				expect(nextSteps[0].getIdea('shapes')).toBe(IDEA_MAP.rectangle);
				expect(nextSteps[1].getIdea('shapes')).toBe(IDEA_MAP.rhombus);
			});
		});
	});
});