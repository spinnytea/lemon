const _ = require('lodash');
const ideas = require('../../../src/database/ideas');
const links = require('../../../src/database/links');
const subgraphs = require('../../../src/database/subgraphs/subgraphs');
const numbers = require('../../../src/planning/primitives/numbers');

describe('subgraphs', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		/*
		Object.keys(subgraphs.units).forEach((key) => {
			jest.spyOn(subgraphs.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		*/
		Object.keys(subgraphs.boundaries).forEach((key) => {
			jest.spyOn(subgraphs.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	test('subgraphs.units', () => {
		expect(Object.keys(subgraphs.units)).toEqual([
			'matchers',
			'createMatcher',
			'projectSubgraph',
			'growPath',
			'$growPathLink',
			'growLeaf',
			'$growLeafLink',
		]);
	});

	describe('matchers', () => {
		test('the list of them', () => {
			expect(Object.keys(subgraphs.units.matchers)).toEqual([
				'id',
				'filler',
				'substring',
				'exact',
				'similar',
				'numbers',
			]);
		});

		test('id', () => {
			// idea is special, it takes the idea as a first argument
			expect(subgraphs.units.matchers.id({ id: '_test' }, '_test')).toBe(true);
			expect(subgraphs.units.matchers.id({ id: '_test' }, '_abcd')).toBe(false);
		});

		test('filler', () => {
			// filler is special, it always returns true
			// it's probably also never actually called
			expect(subgraphs.units.matchers.filler()).toBe(true);
		});

		describe('substring', () => {
			test('undefined', () => {
				const data = undefined;
				expect(subgraphs.units.matchers.substring(data, { value: 'some' })).toBe(false);
			});

			test('string', () => {
				const data = 'some STRING';
				expect(subgraphs.units.matchers.substring(data, { value: 'some' })).toBe(true);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: [] })).toBe(true);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: '' })).toBe(true);
				expect(subgraphs.units.matchers.substring(data, { value: 'string' })).toBe(true);
			});

			test('shallow object', () => {
				const data = { one: 'some STRING' };
				expect(subgraphs.units.matchers.substring(data, { value: 'some' })).toBe(false);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: ['one'] })).toBe(true);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: 'one' })).toBe(true);
			});

			test('deep object', () => {
				const data = { two: { one: 'some STRING' } };
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: 'two' })).toBe(false);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: 'one' })).toBe(false);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: ['two', 'one'] })).toBe(true);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: 'two.one' })).toBe(true);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: 'two.one.three' })).toBe(false);
				expect(subgraphs.units.matchers.substring(data, { value: 'some', path: 'three.two.one' })).toBe(false);
				expect(subgraphs.units.matchers.substring(data, { value: 'string', path: 'two.one' })).toBe(true);
			});
		});

		test('exact', () => {
			const data = { thing1: 3.14, thing2: 2.71 };

			expect(subgraphs.units.matchers.exact(data, { thing1: 3.14, thing2: 2.71 })).toBe(true);
			expect(subgraphs.units.matchers.exact(data, { thing1: 6.28, thing2: 2.71 })).toBe(false);
			expect(subgraphs.units.matchers.exact(data, { thing1: 3.14 })).toBe(false);
			expect(subgraphs.units.matchers.exact(data, {})).toBe(false);
		});

		test('similar', () => {
			const data = { thing1: 3.14, thing2: 2.71 };
			const original = { ...data };

			expect(subgraphs.units.matchers.similar(data, { thing1: 3.14 })).toBe(true);
			expect(subgraphs.units.matchers.similar(data, { thing2: 2.71 })).toBe(true);
			expect(subgraphs.units.matchers.similar(data, {})).toBe(true);
			expect(subgraphs.units.matchers.similar(data, undefined)).toBe(true);
			expect(subgraphs.units.matchers.similar(data, { thing2: 42 })).toBe(false);
			expect(subgraphs.units.matchers.similar(data, { others: 42 })).toBe(false);

			// the data shouldn't change when similar is called
			expect(data).toEqual(original);
		});

		test('numbers', () => {
			const data = numbers.cast({ value: numbers.value(10), $unit: '_test' });

			expect(subgraphs.units.matchers.numbers(data, { $unit: '_test' })).toBe(false);
			expect(subgraphs.units.matchers.numbers(data, { value: numbers.value(0), $unit: '_test' })).toBe(true);
			expect(subgraphs.units.matchers.numbers(data, { value: numbers.value(0), $unit: '_test2' })).toBe(false);
		});
	});

	describe('createMatcher', () => {
		test('existing', () => {
			// this is redundant, and they are tested above
			// but the point is that this was called to create some of them during init
			expect(Object.keys(subgraphs.units.matchers).sort()).toEqual([
				'exact',
				'filler',
				'id',
				'numbers',
				'similar',
				'substring',
			]);
		});

		test('valid', () => {
			// this shouldn't actually be used anywhere
			delete subgraphs.units.matchers.SOME_TEST_MATCHER;
			expect(subgraphs.units.matchers).not.toHaveProperty('SOME_TEST_MATCHER');

			subgraphs.units.createMatcher(function SOME_TEST_MATCHER() { return false; }); // eslint-disable-line prefer-arrow-callback

			expect(subgraphs.units.matchers).toHaveProperty('SOME_TEST_MATCHER');
			expect(subgraphs.units.matchers.SOME_TEST_MATCHER).toEqual(expect.any(Function));
			expect(subgraphs.units.matchers.SOME_TEST_MATCHER.name).toEqual('SOME_TEST_MATCHER');

			// cleanup
			delete subgraphs.units.matchers.SOME_TEST_MATCHER;
			expect(subgraphs.units.matchers).not.toHaveProperty('SOME_TEST_MATCHER');
		});

		test('invalid', () => {
			expect(() => subgraphs.units.createMatcher()).toThrow(/^all matchers must be named functions$/);
			expect(() => subgraphs.units.createMatcher({})).toThrow(/^all matchers must be named functions$/);
			expect(() => subgraphs.units.createMatcher({ name: 'asdf' })).toThrow(/^all matchers must be named functions$/);
			expect(() => subgraphs.units.createMatcher(() => false)).toThrow(/^all matchers must be named functions$/);
			expect(() => subgraphs.units.createMatcher(function () { return false; })) // eslint-disable-line prefer-arrow-callback, func-names
				.toThrow(/^all matchers must be named functions$/);
			expect(() => subgraphs.units.createMatcher(function filler() { return false; })) // eslint-disable-line prefer-arrow-callback, func-names
				.toThrow(/^matcher "filler" is already defined$/);
		});
	});

	test('projectSubgraph', async () => {
		const inner = new subgraphs.Subgraph();
		inner.addVertex('filler');
		inner.addVertex('filler');
		const outer = new subgraphs.Subgraph();
		outer.addVertex('filler');
		outer.addVertex('filler');
		outer.addVertex('filler');
		outer.setData(0, 'a');
		outer.setData(1, 'b');
		outer.setData(2, 'c');
		const innerToOuterVertexMap = new Map([[0, 2], [1, 0]]);

		const goal = subgraphs.units.projectSubgraph(outer, inner, { innerToOuterVertexMap });

		expect(goal).not.toBe(inner);
		expect(goal).toMatchSnapshot();
		expect(await goal.getData(0)).toBe('c');
		expect(await goal.getData(1)).toBe('a');
	});

	describe('growPath', () => {
		let sg;
		let vertexMap;
		beforeEach(() => {
			sg = new subgraphs.Subgraph();
			vertexMap = {};
			vertexMap.fruit = sg.addVertex('id', ideas.proxy('_test_fruit'));
			vertexMap.apple = sg.addVertex('id', ideas.proxy('_test_apple'));
			vertexMap.red = sg.addVertex('id', ideas.proxy('_test_red'));
			vertexMap.banana = sg.addVertex('id', ideas.proxy('_test_banana'));
			vertexMap.yellow = sg.addVertex('id', ideas.proxy('_test_yellow'));
			sg.addEdge(vertexMap.apple, 'typeOf', vertexMap.fruit);
			sg.addEdge(vertexMap.apple, 'property', vertexMap.red);
			sg.addEdge(vertexMap.banana, 'typeOf', vertexMap.fruit);
			sg.addEdge(vertexMap.banana, 'property', vertexMap.yellow);
		});

		test('no paths', () => {
			expect(subgraphs.units.growPath(sg, [vertexMap.fruit], [])).toEqual([vertexMap.fruit]);
		});

		test('one path', () => {
			expect(subgraphs.units.growPath(sg, [vertexMap.apple], [links.get('property')]))
				.toEqual([vertexMap.red]);
			expect(subgraphs.units.growPath(sg, [vertexMap.apple, vertexMap.banana], [links.get('property')]))
				.toEqual([vertexMap.red, vertexMap.yellow]);
		});

		describe('link types', () => {
			test('opposite', () => {
				expect(subgraphs.units.growPath(sg, [vertexMap.fruit], [links.get('typeOf').opposite]))
					.toEqual([vertexMap.apple, vertexMap.banana]);
			});

			test('transitive', () => {
				vertexMap.food = sg.addVertex('id', ideas.proxy('_test_food'));
				sg.addEdge(vertexMap.fruit, 'typeOf', vertexMap.food);

				expect(subgraphs.units.growPath(sg, [vertexMap.fruit], [links.get('typeOf')]))
					.toEqual([vertexMap.food]);
				expect(subgraphs.units.growPath(sg, [vertexMap.apple], [links.get('typeOf')]))
					.toEqual([vertexMap.fruit, vertexMap.food]);
			});

			test('undirected', () => {
				vertexMap.a = sg.addVertex('id', ideas.proxy('_test_undir-a'));
				vertexMap.b = sg.addVertex('id', ideas.proxy('_test_undir-b'));
				vertexMap.c = sg.addVertex('id', ideas.proxy('_test_undir-c'));
				sg.addEdge(vertexMap.apple, '_test__undirected_', vertexMap.a);
				sg.addEdge(vertexMap.apple, '_test__undirected_', vertexMap.b);
				sg.addEdge(vertexMap.apple, '_test__undirected_', vertexMap.c);

				expect(subgraphs.units.growPath(sg, [vertexMap.apple], [links.get('_test__undirected_')]).sort())
					.toEqual([vertexMap.a, vertexMap.b, vertexMap.c]);
				expect(subgraphs.units.growPath(sg, [vertexMap.apple], [links.get('_test__undirected_').opposite]).sort())
					.toEqual([vertexMap.a, vertexMap.b, vertexMap.c]);
				expect(subgraphs.units.growPath(sg, [vertexMap.a], [links.get('_test__undirected_')]).sort())
					.toEqual([vertexMap.apple]);
				expect(subgraphs.units.growPath(sg, [vertexMap.a], [links.get('_test__undirected_').opposite]).sort())
					.toEqual([vertexMap.apple]);
			});

			test('circular', () => {
				vertexMap.food = sg.addVertex('id', ideas.proxy('_test_food'));
				sg.addEdge(vertexMap.fruit, 'typeOf', vertexMap.food);

				// uuuhm, just for testing
				sg.addEdge(vertexMap.food, 'typeOf', vertexMap.apple);
				sg.addEdge(vertexMap.food, 'typeOf', vertexMap.banana);

				expect(subgraphs.units.growPath(sg, [vertexMap.fruit], [links.get('typeOf')]).sort())
					.toEqual([vertexMap.fruit, vertexMap.apple, vertexMap.banana, vertexMap.food]);
				expect(subgraphs.units.growPath(sg, [vertexMap.apple], [links.get('typeOf')]).sort())
					.toEqual([vertexMap.fruit, vertexMap.apple, vertexMap.banana, vertexMap.food]);
				expect(subgraphs.units.growPath(sg, [vertexMap.banana], [links.get('typeOf')]).sort())
					.toEqual([vertexMap.fruit, vertexMap.apple, vertexMap.banana, vertexMap.food]);
				expect(subgraphs.units.growPath(sg, [vertexMap.food], [links.get('typeOf')]).sort())
					.toEqual([vertexMap.fruit, vertexMap.apple, vertexMap.banana, vertexMap.food]);
				expect(subgraphs.units.growPath(sg, [vertexMap.food, vertexMap.banana], [links.get('typeOf')]).sort())
					.toEqual([vertexMap.fruit, vertexMap.apple, vertexMap.banana, vertexMap.food]);
			});

			test('transitive undirected circular', () => {
				vertexMap.ringo = sg.addVertex('id', ideas.proxy('_test_ringo'));
				vertexMap.manzana = sg.addVertex('id', ideas.proxy('_test_manzana'));
				vertexMap.pomme = sg.addVertex('id', ideas.proxy('_test_pomme'));
				sg.addEdge(vertexMap.apple, 'sameAs', vertexMap.ringo);
				sg.addEdge(vertexMap.apple, 'sameAs', vertexMap.manzana);
				sg.addEdge(vertexMap.apple, 'sameAs', vertexMap.pomme);

				expect(subgraphs.units.growPath(sg, [vertexMap.apple], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.ringo], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.manzana], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.pomme], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.apple, vertexMap.ringo], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);

				sg.addEdge(vertexMap.ringo, 'sameAs', vertexMap.manzana);
				sg.addEdge(vertexMap.ringo, 'sameAs', vertexMap.pomme);
				sg.addEdge(vertexMap.manzana, 'sameAs', vertexMap.pomme);

				expect(subgraphs.units.growPath(sg, [vertexMap.apple], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.ringo], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.manzana], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.pomme], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.apple, vertexMap.ringo], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
				expect(subgraphs.units.growPath(sg, [vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme], [links.get('sameAs')]).sort())
					.toEqual([vertexMap.apple, vertexMap.ringo, vertexMap.manzana, vertexMap.pomme]);
			});
		});

		test('many path', () => {
			vertexMap.color = sg.addVertex('id', ideas.proxy('_test_color'));
			sg.addEdge(vertexMap.red, 'typeOf', vertexMap.color);
			sg.addEdge(vertexMap.yellow, 'typeOf', vertexMap.color);

			expect(subgraphs.units.growPath(sg, [vertexMap.fruit], [links.get('typeOf').opposite, links.get('property')]))
				.toEqual([vertexMap.red, vertexMap.yellow]);
			expect(subgraphs.units.growPath(sg, [vertexMap.fruit], [links.get('typeOf').opposite, links.get('property'), links.get('typeOf')]))
				.toEqual([vertexMap.color]);
		});

		test('no paths match', () => {
			expect(subgraphs.units.growPath(sg, [vertexMap.fruit], [links.cast('has', true)])).toEqual([]);
		});
	});

	test.todo('$growPathLink');

	describe('growLeaf', () => {
		let ideaMap;
		let sg;
		let vertexMap;
		beforeEach(async () => {
			ideaMap = {
				fruit: { value: 'fruit' },
				apple: { value: 'apple' },
				banana: { value: 'banana' },
				red: { value: 'red' },
				yellow: { value: 'yellow' },
			};
			const edges = [
				['apple', 'typeOf', 'fruit'],
				['banana', 'typeOf', 'fruit'],
				['apple', 'property', 'red'],
				['banana', 'property', 'yellow'],
			];
			await ideas.createGraph(ideaMap, edges);

			sg = new subgraphs.Subgraph();
			vertexMap = {};
			vertexMap.apple = sg.addVertex('id', ideaMap.apple);
			vertexMap.banana = sg.addVertex('id', ideaMap.banana);
		});

		test('exists', async () => {
			expect(sg.$vertexCount).toBe(2);
			expect(sg.getIdea(0)).toEqual(ideaMap.apple);
			expect(sg.getIdea(1)).toEqual(ideaMap.banana);
			expect(sg.$edgeCount).toBe(0);
			expect(sg.concrete).toBe(true);

			await subgraphs.units.growLeaf(sg, [vertexMap.apple], links.get('property'));

			expect(sg.$vertexCount).toBe(3);
			expect(sg.getIdea(2)).toEqual(ideaMap.red);
			expect(sg.$edgeCount).toBe(1);

			await subgraphs.units.growLeaf(sg, [vertexMap.apple], links.get('typeOf'));

			expect(sg.$vertexCount).toBe(4);
			expect(sg.getIdea(3)).toEqual(ideaMap.fruit);
			expect(sg.$edgeCount).toBe(2);
			expect(sg.concrete).toBe(true);
		});

		test('do not add duplicate verticies', async () => {
			sg.addEdge(vertexMap.apple, 'property', sg.addVertex('id', ideaMap.red));

			expect(sg.$vertexCount).toBe(3);
			expect(sg.getIdea(0)).toEqual(ideaMap.apple);
			expect(sg.getIdea(1)).toEqual(ideaMap.banana);
			expect(sg.getIdea(2)).toEqual(ideaMap.red);
			expect(sg.$edgeCount).toBe(1);

			await subgraphs.units.growLeaf(sg, [vertexMap.apple, vertexMap.banana], links.get('property'));

			expect(sg.$vertexCount).toBe(4);
			expect(sg.getIdea(3)).toEqual(ideaMap.yellow);
			expect(sg.$edgeCount).toBe(2);
		});

		test('no vertexIds', async () => {
			expect(sg.$vertexCount).toBe(2);
			expect(sg.$edgeCount).toBe(0);

			await subgraphs.units.growLeaf(sg, [], links.get('property'));

			expect(sg.$vertexCount).toBe(2);
			expect(sg.$edgeCount).toBe(0);
		});

		test('no leaf match', async () => {
			expect(sg.$vertexCount).toBe(2);
			expect(sg.$edgeCount).toBe(0);

			await subgraphs.units.growLeaf(sg, [vertexMap.apple], links.get('has'));

			expect(sg.$vertexCount).toBe(2);
			expect(sg.$edgeCount).toBe(0);
		});

		describe('link types', () => {
			test('opposite', async () => {
				ideaMap.color = { value: 'color' };
				const edges = [
					['red', 'typeOf', 'color'],
					['yellow', 'typeOf', 'color'],
				];
				await ideas.createGraph(ideaMap, edges);
				vertexMap.color = sg.addVertex('id', ideaMap.color);

				expect(sg.$vertexCount).toBe(3);
				expect(sg.getIdea(0)).toEqual(ideaMap.apple);
				expect(sg.getIdea(1)).toEqual(ideaMap.banana);
				expect(sg.getIdea(2)).toEqual(ideaMap.color);
				expect(sg.$edgeCount).toBe(0);

				await subgraphs.units.growLeaf(sg, [vertexMap.color], links.get('typeOf').opposite);

				expect(sg.$vertexCount).toBe(5);
				expect(sg.getIdea(3)).toEqual(ideaMap.red);
				expect(sg.getIdea(4)).toEqual(ideaMap.yellow);
				expect(sg.$edgeCount).toBe(2);

				await subgraphs.units.growLeaf(sg, [3, 4], links.get('property').opposite);

				expect(sg.$vertexCount).toBe(5);
				expect(sg.$edgeCount).toBe(4);
			});

			test('transitive', async () => {
				ideaMap.food = { value: 'food' };
				const edges = [
					['fruit', 'typeOf', 'food'],
				];
				await ideas.createGraph(ideaMap, edges);

				expect(sg.$vertexCount).toBe(2);
				expect(sg.getIdea(0)).toEqual(ideaMap.apple);
				expect(sg.getIdea(1)).toEqual(ideaMap.banana);
				expect(sg.$edgeCount).toBe(0);

				await subgraphs.units.growLeaf(sg, [vertexMap.apple], links.get('typeOf'));

				expect(sg.$vertexCount).toBe(4);
				expect(sg.getIdea(2)).toEqual(ideaMap.fruit);
				expect(sg.getIdea(3)).toEqual(ideaMap.food);
				expect(sg.$edgeCount).toBe(2);
			});

			test('undirected', async () => {
				ideaMap.a = { value: 'a' };
				ideaMap.b = { value: 'b' };
				const edges = [
					['apple', '_test__undirected_', 'a'],
					['a', '_test__undirected_', 'b'],
				];
				await ideas.createGraph(ideaMap, edges);
				vertexMap.a = sg.addVertex('id', ideaMap.a);

				expect(sg.$vertexCount).toBe(3);
				expect(sg.getIdea(0)).toEqual(ideaMap.apple);
				expect(sg.getIdea(1)).toEqual(ideaMap.banana);
				expect(sg.getIdea(2)).toEqual(ideaMap.a);
				expect(sg.$edgeCount).toBe(0);

				await subgraphs.units.growLeaf(sg, [vertexMap.a], links.get('_test__undirected_'));

				expect(sg.$vertexCount).toBe(4);
				expect(sg.getIdea(3)).toEqual(ideaMap.b);
				expect(sg.$edgeCount).toBe(2);
			});

			describe('transitive undirected circular', () => {
				test('circle', async () => {
					ideaMap.ringo = { value: 'ringo' };
					ideaMap.manzana = { value: 'manzana' };
					ideaMap.pomme = { value: 'pomme' };
					const edges = [
						['apple', 'sameAs', 'ringo'],
						['ringo', 'sameAs', 'manzana'],
						['manzana', 'sameAs', 'pomme'],
						['pomme', 'sameAs', 'apple'],
					];
					await ideas.createGraph(ideaMap, edges);

					expect(sg.$vertexCount).toBe(2);
					expect(sg.getIdea(0)).toEqual(ideaMap.apple);
					expect(sg.getIdea(1)).toEqual(ideaMap.banana);
					expect(sg.$edgeCount).toBe(0);

					await subgraphs.units.growLeaf(sg, [vertexMap.apple], links.get('sameAs'));

					expect(sg.$vertexCount).toBe(5);
					expect(sg.getIdea(2)).toEqual(ideaMap.ringo);
					expect(sg.getIdea(3)).toEqual(ideaMap.pomme);
					expect(sg.getIdea(4)).toEqual(ideaMap.manzana);
					const allEdges = _.chain(sg.allEdges())
						.map(({ src, link, dst }) => ({ src, link: link.name, dst }))
						.sortBy('src', 'dst')
						.value();
					expect(allEdges).toMatchSnapshot();
					expect(sg.$edgeCount).toBe(4);
				});

				test('complete', async () => {
					ideaMap.ringo = { value: 'ringo' };
					ideaMap.manzana = { value: 'manzana' };
					ideaMap.pomme = { value: 'pomme' };
					const edges = [
						['apple', 'sameAs', 'ringo'],
						['apple', 'sameAs', 'manzana'],
						['apple', 'sameAs', 'pomme'],
						['ringo', 'sameAs', 'manzana'],
						['ringo', 'sameAs', 'pomme'],
						['manzana', 'sameAs', 'pomme'],
					];
					await ideas.createGraph(ideaMap, edges);

					expect(sg.$vertexCount).toBe(2);
					expect(sg.getIdea(0)).toEqual(ideaMap.apple);
					expect(sg.getIdea(1)).toEqual(ideaMap.banana);
					expect(sg.$edgeCount).toBe(0);

					await subgraphs.units.growLeaf(sg, [vertexMap.apple], links.get('sameAs'));

					expect(sg.$vertexCount).toBe(5);
					expect(sg.getIdea(2)).toEqual(ideaMap.ringo);
					expect(sg.getIdea(3)).toEqual(ideaMap.manzana);
					expect(sg.getIdea(4)).toEqual(ideaMap.pomme);
					const allEdges = _.chain(sg.allEdges())
						.map(({ src, link, dst }) => ({ src, link: link.name, dst }))
						.sortBy('src', 'dst')
						.value();
					expect(allEdges).toMatchSnapshot();
					expect(sg.$edgeCount).toBe(6);
				});
			});
		});

		test.todo('with vertex options');

		test.todo('with edge options');
	});

	test.todo('$growLeafLink');
});