const _ = require('lodash');
const links = require('../../src/database/links');

describe('links', () => {
	test('links.units', () => {
		expect(Object.keys(links.units)).toEqual([
			'list',
			'create',
			'cast',
		]);
	});

	test('list', () => {
		expect(links.units.list).toBeInstanceOf(Object);
		expect(Object.keys(links.units.list).sort()).toEqual([
			'_test__undirected_',
			'context', 'context-opp',
			'has', 'has-opp',
			'property', 'property-opp',
			'sameAs',
			'sequence', 'sequence-opp',
			'typeOf', 'typeOf-opp',
		]);
	});

	describe('create', () => {
		test('!transitive & directed (standard)', () => {
			const linkProperty = links.get('property');
			expect(_.omit(linkProperty, 'opposite')).toEqual({ name: 'property', transitive: false });
			expect(_.omit(linkProperty.opposite, 'opposite')).toEqual({ name: 'property-opp', transitive: false });

			// the normal way of writing this statement will cause the test to explode and not recover
			// which is why the next test is written weird
			//
			// expect(linkProperty.opposite.opposite).not.toBe(linkProperty);
			//
			// the opposite of the opposite is myself
			expect(linkProperty.opposite.opposite === linkProperty).toBeTruthy();
			// my opposite is different than me
			expect(linkProperty.opposite !== linkProperty).toBeTruthy();
		});

		test('transitive & directed', () => {
			const linkTypeOf = links.get('typeOf');
			expect(_.omit(linkTypeOf, 'opposite')).toEqual({ name: 'typeOf', transitive: true });
			expect(_.omit(linkTypeOf.opposite, 'opposite')).toEqual({ name: 'typeOf-opp', transitive: true });

			// the opposite of the opposite is myself
			expect(linkTypeOf.opposite.opposite === linkTypeOf).toBeTruthy();
			// my opposite is different than me
			expect(linkTypeOf.opposite !== linkTypeOf).toBeTruthy();
		});

		test('transitive & !directed', () => {
			const linkSameAs = links.get('sameAs');
			expect(_.omit(linkSameAs, 'opposite')).toEqual({ name: 'sameAs', transitive: true });
			expect(_.omit(linkSameAs.opposite, 'opposite')).toEqual({ name: 'sameAs', transitive: true });

			// the opposite of the opposite is myself
			expect(linkSameAs.opposite.opposite === linkSameAs).toBeTruthy();
			// i am my own opposite
			expect(linkSameAs.opposite === linkSameAs).toBeTruthy();
		});

		test('!transitive & !directed', () => {
			const linkTest = links.get('_test__undirected_');
			expect(_.omit(linkTest, 'opposite')).toEqual({ name: '_test__undirected_', transitive: false });
			expect(_.omit(linkTest.opposite, 'opposite')).toEqual({ name: '_test__undirected_', transitive: false });

			// the opposite of the opposite is myself
			expect(linkTest.opposite.opposite === linkTest).toBeTruthy();
			// i am my own opposite
			expect(linkTest.opposite === linkTest).toBeTruthy();
		});

		test('cannot have name collisions', () => {
			expect(() => links.units.create('property')).toThrow(/^link "property" is already defined$/);
			expect(() => links.units.create('property-opp')).toThrow(/^link "property-opp" is already defined$/);
		});
	});

	describe('cast', () => {
		test('valid link', () => {
			expect.assertions(3);
			const linkHas = links.units.cast('has');
			expect(_.omit(linkHas, 'opposite')).toEqual({ name: 'has', transitive: true });
			expect(_.omit(linkHas.opposite, 'opposite')).toEqual({ name: 'has-opp', transitive: true });

			// the normal way of writing this statement will cause the test to explode and not recover
			// which is why the next test is written weird
			//
			// expect(linkHas.opposite.opposite).not.toBe(linkHas);
			//
			// the opposite of the opposite is myself
			expect(linkHas.opposite.opposite === linkHas).toBeTruthy();
		});

		test('valid opposite', () => {
			expect.assertions(3);
			const linkHasOpp = links.units.cast('has', true);
			expect(_.omit(linkHasOpp, 'opposite')).toEqual({ name: 'has-opp', transitive: true });
			expect(_.omit(linkHasOpp.opposite, 'opposite')).toEqual({ name: 'has', transitive: true });
			expect(linkHasOpp.opposite.opposite === linkHasOpp).toBeTruthy();
		});

		test('invalid link', () => {
			expect.assertions(3);
			expect(() => links.units.cast('thisLinkIsInvalidLink')).toThrow(/^"thisLinkIsInvalidLink" is not a valid link$/);
			expect(() => links.units.cast()).toThrow(/^"undefined" is not a valid link$/);
			expect(() => links.units.cast({ name: 'alsoNotAValidLink' })).toThrow(/^"alsoNotAValidLink" is not a valid link$/);
		});
	});
});