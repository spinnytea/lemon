const io = require('../../../src/database/ideas/io');
const boundaries = require('../../../src/boundaries');
const config = require('../../../src/config');

describe('io', () => {
	beforeEach(() => {
		boundaries.existsSync.mockImplementationOnce(() => true);
		boundaries.readFileSync.mockImplementationOnce(() => '{}');

		config.init({ in_memory: true, location: 'test_data' });

		boundaries.existsSync.mockReset();
		boundaries.readFileSync.mockReset();
	});

	afterEach(() => {
		config.init({ in_memory: true, memory_fallback_to_location: false });
	});

	test('io.units', () => {
		expect(Object.keys(io.units)).toEqual([
			'filepath',
			'filename',
			'saveFn',
			'loadFn',
			'database',
			'memorySave',
			'memoryLoad',
			'fileSave',
			'fileLoad',
		]);
	});

	test('filepath', () => {
		const prefix = config.settings.location;
		expect(io.units.filepath('')).toBe(`${prefix}`);
		expect(io.units.filepath('1')).toBe(`${prefix}`);
		expect(io.units.filepath('12')).toBe(`${prefix}`);
		expect(io.units.filepath('123')).toBe(`${prefix}/12`);
		expect(io.units.filepath('1234')).toBe(`${prefix}/12`);
		expect(io.units.filepath('12345')).toBe(`${prefix}/12/34`);
		expect(io.units.filepath('123456')).toBe(`${prefix}/12/34`);
		expect(io.units.filepath('1234567')).toBe(`${prefix}/12/34/56`);
	});

	test('filename', () => {
		const prefix = config.settings.location;
		expect(io.units.filename('1', 'data')).toBe(`${prefix}/1_data.json`);
		expect(io.units.filename('123', 'links')).toBe(`${prefix}/12/123_links.json`);
	});

	test('saveFn', () => {
		expect([
			io.units.memorySave,
			io.units.fileSave,
		]).toContain(io.units.saveFn);
	});

	test('loadFn', () => {
		expect([
			io.units.memoryLoad,
			io.units.fileLoad,
		]).toContain(io.units.loadFn);
	});

	test('database', () => {
		expect(io.units.database).toMatchSnapshot();
	});

	describe('memorySave', () => {
		beforeEach(() => {
			config.init({ in_memory: true });
		});

		afterEach(() => {
			expect(boundaries.existsSync).not.toHaveBeenCalled();
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.writeFile).not.toHaveBeenCalled();
		});

		test('set data', async () => {
			expect(io.units.database).toMatchSnapshot();

			expect(await io.units.memorySave('_a', 'data', { value: 1 })).toEqual({ value: 1 });

			expect(io.units.database).toMatchSnapshot();

			expect(await io.units.memorySave('_b', 'links', { has: { _c: {} } })).toEqual({ has: { _c: {} } });

			expect(io.units.database).toMatchSnapshot();
		});

		test('delete data', async () => {
			io.units.database = { data: { _a: { value: 1 }, _c: { value: 2 } }, links: { _b: { has: { _c: {} } } } };
			expect(io.units.database).toMatchSnapshot();

			expect(await io.units.memorySave('_a', 'data', undefined)).toEqual(undefined);

			expect(io.units.database).toMatchSnapshot();

			expect(await io.units.memorySave('_b', 'links', null)).toEqual(null);

			expect(io.units.database).toMatchSnapshot();
		});

		test('invalid', async () => {
			await expect(() => io.units.memorySave(undefined, 'data')).rejects.toThrow(/^id must be set$/);
			await expect(() => io.units.memorySave('_a')).rejects.toThrow(/^invalid which$/);
			await expect(() => io.units.memorySave('_a', 'potato')).rejects.toThrow(/^invalid which$/);
		});
	});

	describe('memoryLoad', () => {
		beforeEach(() => {
			jest.spyOn(io.units, 'fileLoad').mockImplementation(() => Promise.resolve('mock fileLoad'));
		});

		describe('standard', () => {
			beforeEach(() => {
				config.init({ in_memory: true, memory_fallback_to_location: false });
			});

			afterEach(() => {
				expect(boundaries.existsSync).not.toHaveBeenCalled();
				expect(boundaries.readFileSync).not.toHaveBeenCalled();
				expect(boundaries.writeFile).not.toHaveBeenCalled();
			});

			test('no data', async () => {
				io.units.database = { data: {}, links: {} }; // just doing this for symmetry

				expect(await io.units.memoryLoad('_a', 'data')).toEqual(undefined);
				expect(await io.units.memoryLoad('_b', 'links')).toEqual(undefined);

				expect(io.units.fileLoad).not.toHaveBeenCalled();
			});

			test('has data', async () => {
				io.units.database = { data: { _a: { value: 1 }, _c: { value: 2 } }, links: { _b: { has: { _c: {} } } } };

				expect(await io.units.memoryLoad('_a', 'data')).toEqual({ value: 1 });
				expect(await io.units.memoryLoad('_b', 'links')).toEqual({ has: { _c: {} } });

				expect(io.units.fileLoad).not.toHaveBeenCalled();
			});

			test('invalid', async () => {
				await expect(() => io.units.memoryLoad(undefined, 'data')).rejects.toThrow(/^id must be set$/);
				await expect(() => io.units.memoryLoad('_a')).rejects.toThrow(/^invalid which$/);
				await expect(() => io.units.memoryLoad('_a', 'potato')).rejects.toThrow(/^invalid which$/);
			});
		});

		describe('memory_fallback_to_location', () => {
			beforeEach(() => {
				boundaries.existsSync.mockImplementationOnce(() => true);
				boundaries.readFileSync.mockImplementationOnce(() => '{"mock":"data"}');

				config.init({ in_memory: true, location: 'test_data', memory_fallback_to_location: true });
			});

			test('no data', async () => {
				io.units.database = { data: {}, links: {} }; // just doing this for symmetry

				expect(await io.units.memoryLoad('_a', 'data')).toEqual('mock fileLoad');
				expect(await io.units.memoryLoad('_b', 'links')).toEqual('mock fileLoad');

				expect(io.units.fileLoad.mock.calls).toEqual([
					['_a', 'data'],
					['_b', 'links'],
				]);
			});

			test('has data', async () => {
				io.units.database = { data: { _a: { value: 1 }, _c: { value: 2 } }, links: { _b: { has: { _c: {} } } } };

				expect(await io.units.memoryLoad('_a', 'data')).toEqual({ value: 1 });
				expect(await io.units.memoryLoad('_b', 'links')).toEqual({ has: { _c: {} } });

				expect(io.units.fileLoad).not.toHaveBeenCalled();
			});
		});
	});

	describe('fileSave', () => {
		beforeEach(() => {
			boundaries.existsSync.mockImplementationOnce(() => true);
			boundaries.readFileSync.mockImplementationOnce(() => '{"mock":"data"}');

			config.init({ in_memory: false, location: 'test_data' });

			boundaries.existsSync.mockReset();
			boundaries.existsSync.mockImplementation(() => { throw new Error('must re-mock "existsSync"'); });
			boundaries.readFileSync.mockReset();
			boundaries.readFileSync.mockImplementation(() => { throw new Error('must re-mock "readFileSync"'); });
		});

		describe('save data', () => {
			test('path exists', async () => {
				boundaries.existsSync.mockImplementationOnce(() => true);
				boundaries.writeFile.mockImplementationOnce(() => Promise.resolve());

				await io.units.fileSave('_abcd', 'data', { value: 1 }).then(
					() => {},
					() => { throw new Error('should resolve'); },
				);

				expect(boundaries.existsSync.mock.calls.length).toBe(1);
				expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc');
				expect(boundaries.readFileSync).not.toHaveBeenCalled();
				expect(boundaries.readFile).not.toHaveBeenCalled();
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(boundaries.writeFile).toHaveBeenCalledWith(
					'test_data/_a/bc/_abcd_data.json',
					'{"value":1}',
					{ encoding: 'utf8' },
				);
				expect(boundaries.deleteFile).not.toHaveBeenCalled();
				expect(boundaries.mkdirp).not.toHaveBeenCalled();
			});

			test('path missing', async () => {
				boundaries.existsSync
					.mockImplementationOnce(() => false)
					.mockImplementationOnce(() => true);
				boundaries.writeFile.mockImplementationOnce(() => Promise.resolve());
				boundaries.mkdirp.mockImplementationOnce(() => Promise.resolve());

				await io.units.fileSave('_abcd', 'data', { value: 1 }).then(
					() => {},
					() => { throw new Error('should resolve'); },
				);

				expect(boundaries.existsSync.mock.calls.length).toBe(2);
				expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc');
				expect(boundaries.existsSync).toHaveBeenCalledWith('test_data');
				expect(boundaries.readFileSync).not.toHaveBeenCalled();
				expect(boundaries.readFile).not.toHaveBeenCalled();
				expect(boundaries.writeFile.mock.calls.length).toBe(1);
				expect(boundaries.writeFile).toHaveBeenCalledWith(
					'test_data/_a/bc/_abcd_data.json',
					'{"value":1}',
					{ encoding: 'utf8' },
				);
				expect(boundaries.deleteFile).not.toHaveBeenCalled();
				expect(boundaries.mkdirp.mock.calls.length).toBe(1);
				expect(boundaries.mkdirp).toHaveBeenCalledWith('test_data/_a/bc');
			});

			test('volume missing', async () => {
				boundaries.existsSync
					.mockImplementationOnce(() => false)
					.mockImplementationOnce(() => false);

				await io.units.fileSave('_abcd', 'data', { value: 1 }).then(
					() => { throw new Error('should reject'); },
					(err) => { expect(err.message).toBe('config.settings.location is unavailable: "test_data"'); },
				);

				expect(boundaries.existsSync.mock.calls.length).toBe(2);
				expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc');
				expect(boundaries.existsSync).toHaveBeenCalledWith('test_data');
				expect(boundaries.readFileSync).not.toHaveBeenCalled();
				expect(boundaries.readFile).not.toHaveBeenCalled();
				expect(boundaries.writeFile).not.toHaveBeenCalled();
				expect(boundaries.deleteFile).not.toHaveBeenCalled();
				expect(boundaries.mkdirp).not.toHaveBeenCalled();
			});
		});

		test('delete data', async () => {
			boundaries.existsSync.mockImplementationOnce(() => true);
			boundaries.deleteFile.mockImplementationOnce(() => Promise.resolve());

			await io.units.fileSave('_abcd', 'data', undefined).then(
				() => {},
				() => { throw new Error('should resolve'); },
			);

			expect(boundaries.existsSync.mock.calls.length).toBe(1);
			expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json');
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.readFile).not.toHaveBeenCalled();
			expect(boundaries.writeFile).not.toHaveBeenCalled();
			expect(boundaries.deleteFile.mock.calls.length).toBe(1);
			expect(boundaries.deleteFile).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json');
			expect(boundaries.mkdirp).not.toHaveBeenCalled();
		});

		test('nothing to delete', async () => {
			boundaries.existsSync.mockImplementationOnce(() => false);

			await io.units.fileSave('_abcd', 'data', undefined).then(
				() => {},
				() => { throw new Error('should resolve'); },
			);

			expect(boundaries.existsSync.mock.calls.length).toBe(1);
			expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json');
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.readFile).not.toHaveBeenCalled();
			expect(boundaries.writeFile).not.toHaveBeenCalled();
			expect(boundaries.deleteFile).not.toHaveBeenCalled();
			expect(boundaries.mkdirp).not.toHaveBeenCalled();
		});

		test('invalid', async () => {
			await expect(() => io.units.fileSave(undefined, 'data')).rejects.toThrow(/^id must be set$/);
			await expect(() => io.units.fileSave('_a')).rejects.toThrow(/^invalid which$/);
			await expect(() => io.units.fileSave('_a', 'potato')).rejects.toThrow(/^invalid which$/);
		});
	});

	describe('fileLoad', () => {
		beforeEach(() => {
			boundaries.existsSync.mockImplementationOnce(() => true);
			boundaries.readFileSync.mockImplementationOnce(() => '{"mock":"data"}');

			config.init({ in_memory: false, location: 'test_data' });

			boundaries.existsSync.mockReset();
			boundaries.existsSync.mockImplementation(() => { throw new Error('must re-mock "existsSync"'); });
			boundaries.readFileSync.mockReset();
			boundaries.readFileSync.mockImplementation(() => { throw new Error('must re-mock "readFileSync"'); });
		});

		test('no data', async () => {
			boundaries.existsSync.mockImplementationOnce(() => false);

			await io.units.fileLoad('_abcd', 'data').then(
				(data) => { expect(data).toEqual(undefined); },
				() => { throw new Error('should resolve'); },
			);

			expect(boundaries.existsSync.mock.calls.length).toBe(1);
			expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json');
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.readFile).not.toHaveBeenCalled();
			expect(boundaries.writeFile).not.toHaveBeenCalled();
			expect(boundaries.deleteFile).not.toHaveBeenCalled();
			expect(boundaries.mkdirp).not.toHaveBeenCalled();
		});

		test('has data', async () => {
			boundaries.existsSync.mockImplementationOnce(() => true);
			boundaries.readFile.mockImplementationOnce(() => Promise.resolve('{"value":1}'));

			await io.units.fileLoad('_abcd', 'data').then(
				(data) => { expect(data).toEqual({ value: 1 }); },
				() => { throw new Error('should resolve'); },
			);

			expect(boundaries.existsSync.mock.calls.length).toBe(1);
			expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json');
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.readFile.mock.calls.length).toBe(1);
			expect(boundaries.readFile).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json', { encoding: 'utf8' });
			expect(boundaries.writeFile).not.toHaveBeenCalled();
			expect(boundaries.deleteFile).not.toHaveBeenCalled();
			expect(boundaries.mkdirp).not.toHaveBeenCalled();
		});

		test('read failure', async () => {
			boundaries.existsSync.mockImplementationOnce(() => true);
			boundaries.readFile.mockImplementationOnce(() => Promise.reject(new Error('mock failure')));

			await io.units.fileLoad('_abcd', 'data').then(
				() => { throw new Error('should reject'); },
				(err) => { expect(err.message).toBe('mock failure'); },
			);

			expect(boundaries.existsSync.mock.calls.length).toBe(1);
			expect(boundaries.existsSync).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json');
			expect(boundaries.readFileSync).not.toHaveBeenCalled();
			expect(boundaries.readFile.mock.calls.length).toBe(1);
			expect(boundaries.readFile).toHaveBeenCalledWith('test_data/_a/bc/_abcd_data.json', { encoding: 'utf8' });
			expect(boundaries.writeFile).not.toHaveBeenCalled();
			expect(boundaries.deleteFile).not.toHaveBeenCalled();
			expect(boundaries.mkdirp).not.toHaveBeenCalled();
		});

		test('invalid', async () => {
			await expect(() => io.units.fileLoad(undefined, 'data')).rejects.toThrow(/^id must be set$/);
			await expect(() => io.units.fileLoad('_a')).rejects.toThrow(/^invalid which$/);
			await expect(() => io.units.fileLoad('_a', 'potato')).rejects.toThrow(/^invalid which$/);
		});
	});
});