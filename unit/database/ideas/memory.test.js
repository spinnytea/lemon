const memory = require('../../../src/database/ideas/memory');

describe('memory', () => {
	test('memory.units', () => {
		expect(Object.keys(memory.units)).toEqual([
			'map',
		]);
	});

	test('map', () => {
		expect(memory.units.map).toBeInstanceOf(Map);
	});
});