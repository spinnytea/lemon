const config = require('../../../src/config');
const links = require('../../../src/database/links');
const ideas = require('../../../src/database/ideas');
const io = require('../../../src/database/ideas/io');
const memory = require('../../../src/database/ideas/memory');
const CoreIdea = require('../../../src/database/ideas/CoreIdea');

describe('ideas', () => {
	// the only reason to use JSON.parse is is to make the linter happy
	const appleLinks = () => JSON.parse('{ "property": { "_red": {} } }');
	const redData = () => JSON.parse('{ "value": "red" }');
	const redLinks = () => JSON.parse('{ "property-opp": { "_apple": {} } }');

	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(ideas.units).forEach((key) => {
			if (key === 'ProxyIdea') return;
			if (key === 'nextID') return;
			jest.spyOn(ideas.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	test('ideas.units', () => {
		expect(Object.keys(ideas.units)).toEqual([
			'ProxyIdea',
			'nextID',
			'create',
			'load',
			'save',
			'close',
			'delete',
			'removeAllLinks',
			'context',
			'contexts',
		]);
	});

	test('ProxyIdea', () => {
		// this gets it's own file for testing
		expect(ideas.units.ProxyIdea.name).toBe('ProxyIdea');
	});

	test('nextID', () => {
		expect(ideas.units.nextID).toBe('ids.ideas');
	});

	describe('create', () => {
		const id = '101'; // XXX 101 just so happens to be the first id we generate
		beforeEach(() => {
			ideas.units.create.mockRestore();
		});

		test('empty', async () => {
			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			const proxy = await ideas.units.create();

			expect(memory.get({ id })).toEqual({ id: '101', data: undefined, links: {} });
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			expect(proxy).toEqual({ id: '101' });
		});

		test('with data', async () => {
			ideas.units.save.mockRestore();

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			const proxy = await ideas.units.create({ value: 1 });

			expect(memory.get({ id })).toEqual({ id: '101', data: { value: 1 }, links: {} });
			expect(io.units.database.data).toHaveProperty(id);
			expect(io.units.database.data[id]).toEqual({ value: 1 });
			expect(io.units.database.links).not.toHaveProperty(id);

			expect(proxy).toEqual({ id: '101' });
		});
	});

	describe('load', () => {
		beforeEach(() => {
			ideas.units.load.mockRestore();
		});

		test('empty', async () => {
			const id = '_test';

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			const proxy = await ideas.units.load(id);

			expect(memory.get({ id })).toEqual({ id: '_test', data: undefined, links: {} });
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			expect(proxy).toEqual({ id: '_test' });
		});

		test('with data and links', async () => {
			const red = ideas.proxy('_red');
			io.units.database.data[red.id] = redData();

			expect(memory.has(red)).toBe(false);
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links).not.toHaveProperty(red.id);

			await ideas.units.load(red);

			expect(memory.get(red)).toEqual({ id: '_red', data: redData(), links: {} });
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links).not.toHaveProperty(red.id);
		});

		test('empty', async () => {
			const proxy = ideas.proxy('_test');
			io.units.database.data[proxy.id] = {};

			expect(memory.has(proxy)).toBe(false);
			expect(io.units.database.data[proxy.id]).toEqual({});
			expect(io.units.database.links).not.toHaveProperty(proxy.id);

			await ideas.units.load(proxy);

			expect(memory.get(proxy)).toEqual({ id: '_test', data: {}, links: {} });
			expect(io.units.database.data[proxy.id]).toEqual({});
			expect(io.units.database.links).not.toHaveProperty(proxy.id);
		});

		test('links only', async () => {
			const apple = ideas.proxy('_apple');
			io.units.database.links[apple.id] = appleLinks();

			expect(memory.has(apple)).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(apple.id);
			expect(io.units.database.links[apple.id]).toEqual(appleLinks());

			await ideas.units.load(apple);

			expect(memory.get(apple)).toEqual({ id: '_apple', data: undefined, links: appleLinks() });
			expect(io.units.database.data).not.toHaveProperty(apple.id);
			expect(io.units.database.links[apple.id]).toEqual(appleLinks());
		});

		test('with data and links', async () => {
			const red = ideas.proxy('_red');
			io.units.database.data[red.id] = redData();
			io.units.database.links[red.id] = redLinks();

			expect(memory.has(red)).toBe(false);
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links[red.id]).toEqual(redLinks());

			await ideas.units.load(red);

			expect(memory.get(red)).toEqual({ id: '_red', data: redData(), links: redLinks() });
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links[red.id]).toEqual(redLinks());
		});

		test('already loaded', async () => {
			const red = ideas.proxy('_red');

			memory.add(new CoreIdea('_red', redData(), redLinks()));
			io.units.database.data[red.id] = { value: 'blue' };
			io.units.database.links[red.id] = appleLinks();

			expect(memory.has(red)).toBe(true);

			await ideas.units.load(red);

			expect(memory.get(red)).toEqual({ id: '_red', data: redData(), links: redLinks() });
			expect(io.units.database.data[red.id]).toEqual({ value: 'blue' });
			expect(io.units.database.links[red.id]).toEqual(appleLinks());
		});
	});

	describe('save', () => {
		beforeEach(() => {
			ideas.units.save.mockRestore();
			ideas.units.load.mockRestore();
		});

		test('empty data', async () => {
			const proxyL = await ideas.units.load('_test');

			expect(memory.get(proxyL)).toEqual({ id: '_test', data: undefined, links: {} });
			expect(io.units.database.data).not.toHaveProperty(proxyL.id);
			expect(io.units.database.links).not.toHaveProperty(proxyL.id); // note that empty links are note saved

			const proxyS = await ideas.units.save(proxyL);
			expect(proxyL).toBe(proxyS);

			expect(memory.get(proxyS)).toEqual({ id: '_test', data: undefined, links: {} });
			expect(io.units.database.data).not.toHaveProperty(proxyS.id);
			expect(io.units.database.links).not.toHaveProperty(proxyS.id);
		});

		test('data only', async () => {
			const red = ideas.proxy('_red');
			await red.setData({ value: 'red' });

			await ideas.units.save(red);

			expect(memory.get(red)).toEqual({ id: '_red', data: redData(), links: {} });
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links).not.toHaveProperty(red.id);
		});

		test('empty data', async () => {
			const proxy = ideas.proxy('_test');
			await proxy.setData({});

			await ideas.units.save(proxy);

			expect(memory.get(proxy)).toEqual({ id: '_test', data: {}, links: {} });
			expect(io.units.database.data[proxy.id]).toEqual({}); // we want to ensure it saves the object data as is
			expect(io.units.database.links).not.toHaveProperty(proxy.id);
		});

		test('links only', async () => {
			const apple = ideas.proxy('_apple');
			await apple.addLink(links.get('property'), ideas.proxy('_red'));

			await ideas.units.save(apple);

			expect(memory.get(apple)).toEqual({ id: '_apple', data: undefined, links: appleLinks() });
			expect(io.units.database.data).not.toHaveProperty(apple.id);
			expect(io.units.database.links[apple.id]).toEqual(appleLinks());
		});

		test('with data and links', async () => {
			const red = ideas.proxy('_red');
			red.setData({ value: 'red' });
			await ideas.proxy('_apple').addLink(links.get('property'), red);

			await ideas.units.save(red);

			expect(memory.get(red)).toEqual({ id: '_red', data: redData(), links: redLinks() });
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links[red.id]).toEqual(redLinks());
		});

		test('not yet loaded', async () => {
			const id = '_test';

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			const proxy = await ideas.units.save(id);

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			expect(proxy).toEqual({ id: '_test' });
		});
	});

	describe('close', () => {
		beforeEach(() => {
			ideas.units.close.mockRestore();
			ideas.units.save.mockRestore();
		});

		test('loaded', async () => {
			const red = ideas.proxy('_red');

			memory.add(new CoreIdea('_red', redData(), redLinks()));
			io.units.database.data[red.id] = { value: 'blue' };
			io.units.database.links[red.id] = appleLinks();

			expect(memory.has(red)).toBe(true);

			await ideas.units.close(red);

			expect(memory.has(red)).toBe(false);
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links[red.id]).toEqual(redLinks());
		});

		test('not yet loaded', async () => {
			const red = ideas.proxy('_red');

			expect(memory.has(red)).toBe(false);
			io.units.database.data[red.id] = redData();
			io.units.database.links[red.id] = redLinks();

			const proxy = await ideas.units.close(red);

			expect(memory.has(red)).toBe(false);
			expect(io.units.database.data[red.id]).toEqual(redData());
			expect(io.units.database.links[red.id]).toEqual(redLinks());

			expect(proxy).toBe(red);
		});

		test('does not exist', async () => {
			const id = '_test';

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			const proxy = await ideas.units.close(id);

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			expect(proxy).toEqual({ id: '_test' });
		});
	});

	describe('delete', () => {
		beforeEach(() => {
			// XXX consider not restoring everything?
			//  - currently, delete is a composition of all these things
			ideas.units.delete.mockRestore();

			// since we are going to let removeAllLinks do it's thing, we also need load/save
			ideas.units.removeAllLinks.mockRestore();
			ideas.units.load.mockRestore();
			ideas.units.save.mockRestore();
		});

		test('loaded', async () => {
			const red = ideas.proxy('_red');

			memory.add(new CoreIdea('_red', redData(), redLinks()));
			io.units.database.data[red.id] = { value: 'blue' };
			io.units.database.links[red.id] = appleLinks();

			expect(memory.has(red)).toBe(true);

			await ideas.units.delete(red);

			expect(memory.has(red)).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(red.id);
			expect(io.units.database.links).not.toHaveProperty(red.id);
		});

		test('not yet loaded', async () => {
			const red = ideas.proxy('_red');

			expect(memory.has(red)).toBe(false);
			io.units.database.data[red.id] = redData();
			io.units.database.links[red.id] = redLinks();

			const proxy = await ideas.units.delete(red);

			expect(memory.has(red)).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(red.id);
			expect(io.units.database.links).not.toHaveProperty(red.id);

			expect(proxy).toBe(red);
		});

		test('does not exist', async () => {
			const id = '_test';

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			const proxy = await ideas.units.delete(id);

			expect(memory.has(proxy)).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			expect(proxy).toEqual({ id: '_test' });
		});
	});

	describe('removeAllLinks', () => {
		beforeEach(() => {
			ideas.units.removeAllLinks.mockRestore();
		});

		test('basic', async () => {
			ideas.units.load.mockRestore();
			ideas.units.save.mockReturnValueOnce(Promise.resolve());
			ideas.units.save.mockReturnValueOnce(Promise.resolve());
			ideas.units.save.mockReturnValueOnce(Promise.resolve());

			const link = links.get('sameAs');

			const proxyA = ideas.proxy('_testA');
			const proxyB = ideas.proxy('_testB');
			const proxyC = ideas.proxy('_testC');
			await proxyA.addLink(link, proxyB);
			await proxyB.addLink(link, proxyB);
			await proxyC.addLink(link, proxyB);
			await proxyA.addLink(link, proxyC);

			expect(await proxyA.getLinksOfType(link)).toEqual([proxyB, proxyC]);
			expect(await proxyB.getLinksOfType(link)).toEqual([proxyA, proxyB, proxyC]);
			expect(await proxyC.getLinksOfType(link)).toEqual([proxyB, proxyA]);

			await ideas.units.removeAllLinks(proxyB);

			expect(await proxyA.getLinksOfType(link)).toEqual([proxyC]);
			expect(await proxyB.getLinksOfType(link)).toEqual([]);
			expect(await proxyC.getLinksOfType(link)).toEqual([proxyA]);

			expect(ideas.units.save.mock.calls).toEqual([
				['_testB'],
				['_testA'],
				['_testC'],
			]);
		});

		test('no links', async () => {
			ideas.units.load.mockRestore();

			const proxy = await ideas.units.load('_test');

			expect(memory.get(proxy).links).toEqual({});

			await ideas.units.removeAllLinks(proxy);

			expect(ideas.units.save).not.toHaveBeenCalled();
		});

		test('does not exist', async () => {
			const id = '_test';

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			await ideas.units.removeAllLinks(id);

			expect(memory.has({ id })).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(id);
			expect(io.units.database.links).not.toHaveProperty(id);

			expect(ideas.units.load).not.toHaveBeenCalled();
			expect(ideas.units.save).not.toHaveBeenCalled();
		});
	});

	test('context', async () => {
		ideas.units.context.mockRestore();
		ideas.units.create.mockRestore();
		ideas.units.save.mockRestore();

		expect(config.units.data['ideas.context']).toEqual(undefined);

		const c1 = await ideas.units.context('c1');

		expect(memory.has(c1)).toBeTruthy();
		expect(config.units.data['ideas.context']).toEqual({ c1: c1.id });

		await ideas.units.context('c1');
		await ideas.units.context('c1');
		await ideas.units.context('c1');

		expect(memory.has(c1)).toBeTruthy();
		expect(config.units.data['ideas.context']).toEqual({ c1: c1.id });

		const c2 = await ideas.units.context('c2');

		expect(memory.has(c1)).toBeTruthy();
		expect(memory.has(c2)).toBeTruthy();
		expect(config.units.data['ideas.context']).toEqual({ c1: c1.id, c2: c2.id });
	});

	describe('contexts', () => {
		beforeEach(() => {
			ideas.units.contexts.mockRestore();
			ideas.units.context.mockRestore();
			ideas.units.create.mockRestore();
			ideas.units.save.mockRestore();
		});

		test('just one', async () => {
			expect(config.units.data['ideas.context']).toEqual(undefined);

			const contextMap = await ideas.units.contexts(['c1']);

			expect(Object.keys(contextMap).sort()).toEqual(['c1']);
			expect(Object.keys(config.units.data['ideas.context']).sort()).toEqual(['c1']);

			await expect(ideas.units.contexts(['c1'])).resolves.toEqual(contextMap);

			expect(Object.keys(config.units.data['ideas.context']).sort()).toEqual(['c1']);
		});

		test('a few', async () => {
			expect(config.units.data['ideas.context']).toEqual(undefined);

			const contextMap = await ideas.units.contexts(['c1', 'c2', 'c3']);

			expect(Object.keys(contextMap).sort()).toEqual(['c1', 'c2', 'c3']);
			expect(Object.keys(config.units.data['ideas.context']).sort()).toEqual(['c1', 'c2', 'c3']);

			await ideas.units.contexts(['c1', 'c2', 'c3']);
			await ideas.units.contexts(['c4', 'c5', 'c6']);

			await expect(ideas.units.contexts(['c1', 'c2', 'c3'])).resolves.toEqual(contextMap);
			expect(Object.keys(config.units.data['ideas.context']).sort()).toEqual(['c1', 'c2', 'c3', 'c4', 'c5', 'c6']);
		});
	});
});