const links = require('../../../src/database/links');
const CoreIdea = require('../../../src/database/ideas/CoreIdea');
const memory = require('../../../src/database/ideas/memory');
const { ProxyIdea } = require('../../../src/database/ideas').units;

describe('ProxyIdea', () => {
	// the only reason to use JSON.parse is is to make the linter happy
	const appleLinks = () => JSON.parse('{ "property": { "_red": {} } }');
	const redData = () => JSON.parse('{ "value": "red" }');
	const redLinks = () => JSON.parse('{ "property-opp": { "_apple": {} } }');

	test('prototype', () => {
		// eslint-disable-next-line no-proto
		expect(Object.getOwnPropertyNames(new ProxyIdea('a').__proto__)).toEqual([
			'constructor',
			'getData',
			'setData',
			'getLinksOfType',
			'addLink',
			'removeLink',
		]);
	});

	describe('constructor', () => {
		test('id only', () => {
			expect(new ProxyIdea('_test')).toEqual({ id: '_test' });
			expect(new ProxyIdea({ id: '_test' })).toEqual({ id: '_test' });
			expect(new ProxyIdea(new ProxyIdea('_test'))).toEqual({ id: '_test' });
			expect(new ProxyIdea('_test')).toEqual(new ProxyIdea('_test'));
		});

		test('with data', async () => {
			expect(() => new ProxyIdea('_test', { value: 'Some Pig' }))
				.toThrow(/^setting data in the constructor is impossible because setData is an async operation$/);
			// besides, you we should be using `ideas.create` anyway
		});

		test('invalid calls', () => {
			expect(() => new ProxyIdea()).toThrow(TypeError);
			expect(() => new ProxyIdea(1234)).toThrow(TypeError);
			expect(() => new ProxyIdea({})).toThrow(TypeError);
		});
	});

	test('getData', async () => {
		const red = new ProxyIdea('_red');
		const redDataOrig = { value: 'red' };

		expect(await red.getData()).toEqual(undefined);

		await red.setData(redDataOrig);

		const redDataGet = await red.getData();
		expect(redDataGet).toEqual(redDataOrig);
		expect(redDataGet).not.toBe(redDataOrig);

		const redDataGet2 = await red.getData();
		expect(redDataGet2).toEqual(redDataGet);
		expect(redDataGet2).not.toBe(redDataGet);

		expect(memory.has(red)).toBe(true);
		expect(memory.get(red)).toBeInstanceOf(CoreIdea);
		expect(memory.get(red)).toEqual({ id: '_red', data: redData(), links: {} });
		expect(memory.get(red).data).toEqual(redDataGet);
		expect(memory.get(red).data).not.toBe(redDataGet);
	});

	test('setData', async () => {
		const red = new ProxyIdea('_red');
		const redDataOrig = { value: 'red' };

		expect(memory.has(red)).toBe(false);

		const redDataSet = await red.setData(redDataOrig);

		expect(red).toEqual({ id: '_red' });
		expect(redDataSet).toBe(redDataOrig);
		expect(memory.has(red)).toBe(true);
		expect(memory.get(red)).toBeInstanceOf(CoreIdea);
		expect(memory.get(red)).toEqual({ id: '_red', data: redData(), links: {} });
		expect(memory.get(red).data).toEqual(redDataOrig);
		expect(memory.get(red).data).not.toBe(redDataOrig);
	});

	test('getLinksOfType', async () => {
		const apple = new ProxyIdea('_apple');
		const linkProperty = links.get('property');
		const red = new ProxyIdea('_red');
		await apple.addLink(linkProperty, red);

		expect(await apple.getLinksOfType(linkProperty)).toEqual([red]);
		expect(await apple.getLinksOfType(linkProperty.opposite)).toEqual([]);
		expect(await apple.getLinksOfType(links.get('has'))).toEqual([]);
		expect(await red.getLinksOfType(linkProperty)).toEqual([]);
		expect(await red.getLinksOfType(linkProperty.opposite)).toEqual([apple]);
		expect(await red.getLinksOfType(links.get('has'))).toEqual([]);
	});

	describe('addLink', () => {
		test('basic', async () => {
			const apple = new ProxyIdea('_apple');
			const linkProperty = links.get('property');
			const red = new ProxyIdea('_red');

			expect(memory.has(apple)).toBe(false);
			expect(memory.has(red)).toBe(false);

			await apple.addLink(linkProperty, red.id); // using the id is non-standard, but should still work

			expect(memory.has(apple)).toBe(true);
			expect(memory.get(apple)).toEqual({ id: '_apple', data: undefined, links: appleLinks() });
			expect(memory.has(red)).toBe(true);
			expect(memory.get(red)).toEqual({ id: '_red', data: undefined, links: redLinks() });

			// adding multiple times doesn't really do anything
			await apple.addLink(linkProperty, red);
			await apple.addLink(linkProperty, red);
			await apple.addLink(linkProperty, red);

			expect(memory.has(apple)).toBe(true);
			expect(memory.get(apple)).toEqual({ id: '_apple', data: undefined, links: appleLinks() });
			expect(memory.has(red)).toBe(true);
			expect(memory.get(red)).toEqual({ id: '_red', data: undefined, links: redLinks() });
		});
	});

	describe('removeLink', () => {
		test('not loaded / not linked', async () => {
			const apple = new ProxyIdea('_apple');
			const linkProperty = links.get('property');
			const red = new ProxyIdea('_red');

			expect(memory.has(apple)).toBe(false);
			expect(memory.has(red)).toBe(false);

			await apple.removeLink(linkProperty, red.id); // using the id is non-standard, but should still work

			expect(memory.has(apple)).toBe(true);
			expect(memory.get(apple)).toEqual({ id: '_apple', data: undefined, links: {} });
			expect(memory.has(red)).toBe(true);
			expect(memory.get(red)).toEqual({ id: '_red', data: undefined, links: {} });
		});

		test('links removed', async () => {
			const apple = new ProxyIdea('_apple');
			const linkProperty = links.get('property');
			const red = new ProxyIdea('_red');
			await apple.addLink(linkProperty, red);

			expect(memory.get(apple)).toEqual({ id: '_apple', data: undefined, links: appleLinks() });
			expect(memory.get(red)).toEqual({ id: '_red', data: undefined, links: redLinks() });

			await apple.removeLink(linkProperty, red);

			expect(memory.get(apple)).toEqual({ id: '_apple', data: undefined, links: {} });
			expect(memory.get(red)).toEqual({ id: '_red', data: undefined, links: {} });
		});

		test('links remain', async () => {
			const dog = new ProxyIdea('_dog');
			const cat = new ProxyIdea('_cat');
			const wolf = new ProxyIdea('_wolf');
			const linkSameAs = links.get('sameAs');
			await dog.addLink(linkSameAs, cat);
			await dog.addLink(linkSameAs, wolf);
			await cat.addLink(linkSameAs, wolf);

			expect(memory.get(dog)).toEqual({ id: '_dog', data: undefined, links: JSON.parse('{ "sameAs": { "_cat": {}, "_wolf": {} } }') });
			expect(memory.get(cat)).toEqual({ id: '_cat', data: undefined, links: JSON.parse('{ "sameAs": { "_dog": {}, "_wolf": {} } }') });
			expect(memory.get(wolf)).toEqual({ id: '_wolf', data: undefined, links: JSON.parse('{ "sameAs": { "_cat": {}, "_dog": {} } }') });

			await dog.removeLink(linkSameAs, cat);

			expect(memory.get(dog)).toEqual({ id: '_dog', data: undefined, links: JSON.parse('{ "sameAs": { "_wolf": {} } }') }); // link remains
			expect(memory.get(cat)).toEqual({ id: '_cat', data: undefined, links: JSON.parse('{ "sameAs": { "_wolf": {} } }') }); // link removed
			expect(memory.get(wolf)).toEqual({ id: '_wolf', data: undefined, links: JSON.parse('{ "sameAs": { "_cat": {}, "_dog": {} } }') }); // unchanged
		});
	});
});