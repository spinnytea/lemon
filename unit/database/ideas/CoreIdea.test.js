const CoreIdea = require('../../../src/database/ideas/CoreIdea');

describe('CoreIdea', () => {
	test('prototype', () => {
		expect(Object.getOwnPropertyNames(new CoreIdea('a').__proto__)).toEqual([
			'constructor',
		]);
	});

	test('constructor', () => {
		expect(new CoreIdea('_a'))
			.toEqual({ id: '_a', data: undefined, links: {} });

		expect(new CoreIdea('_a', { value: true }))
			.toEqual({ id: '_a', data: { value: true }, links: {} });

		expect(new CoreIdea('_a', { value: true }, { property: { _b: {} } }))
			.toEqual({ id: '_a', data: { value: true }, links: { property: { _b: {} } } });
	});
});