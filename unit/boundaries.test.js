const boundaries = require('../src/boundaries');

describe('boundaries', () => {
	test('boundaries.units', () => {
		expect(boundaries.units).toBe(undefined); // by definition, this shouldn't have any units
	});
});