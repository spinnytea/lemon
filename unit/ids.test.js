const ids = require('../src/ids');

describe('ids', () => {
	test('ids.units', () => {
		expect(Object.keys(ids.units)).toEqual([
			'tokens',
			'replaceAt',
			'increment',
		]);
	});

	test('tokens', () => {
		// 36 doesn't really matter, but it's good to know
		expect(ids.units.tokens.length).toBe(36);

		// not all file systems are case sensitive
		// since our primary way of storing data will be to write files, we need to limit our character set
		expect(ids.units.tokens.map((s) => s.toLowerCase())).toEqual(ids.units.tokens);

		// I like to use underscores for special IDs, so we need to make sure the normally generated IDs can't overlap with them
		expect(ids.units.tokens).toContain('a'); // make sure our search works
		expect(ids.units.tokens).not.toContain('_'); // perform the test

		// the array should be immutable
		expect(() => { ids.units.tokens.push('%'); }).toThrow(TypeError);
	});

	test('replaceAt', () => {
		expect(ids.units.replaceAt('1234', 0, 'a')).toBe('a234');
		expect(ids.units.replaceAt('1234', 1, 'a')).toBe('1a34');
		expect(ids.units.replaceAt('1234', 2, 'a')).toBe('12a4');
		expect(ids.units.replaceAt('1234', 3, 'a')).toBe('123a');
	});

	describe('increment', () => {
		const { increment, tokens } = ids.units;
		beforeAll(() => { ids.units.tokens = ['0', '1', '2']; });
		afterAll(() => { ids.units.tokens = tokens; });

		test('initial', () => {
			expect(increment('')).toBe('1');
		});

		test('normal', () => {
			expect(increment('0')).toBe('1');
			expect(increment('1')).toBe('2');
			expect(increment('10')).toBe('11');
			expect(increment('11')).toBe('12');
		});

		test('rollover', () => {
			expect(increment('2')).toBe('10');
			expect(increment('22')).toBe('100');
		});

		test('type', () => {
			// I've made this mistake once or twice
			expect(() => { increment(0); }).toThrow(TypeError);
			expect(() => { increment('0'); }).not.toThrow();
		});
	});

	describe('unsupported', () => {
		test('replaceAt: invalid indexes', () => {
			expect(ids.units.replaceAt('1234', -9, 'a')).toBe('a1234');
			expect(ids.units.replaceAt('1234', 9, 'a')).toBe('1234a');
		});

		test('increment: invalid characters', () => {
			expect(ids.units.tokens).not.toContain('%'); // invalid char
			expect(ids.units.increment('%')).toEqual('0');
			expect(ids.units.increment('22%')).toEqual('220');
			expect(ids.units.increment('2%1')).toEqual('2%2');
		});

		test('increment: invalid character wrap', () => {
			expect(ids.units.tokens).not.toContain('%'); // invalid char
			expect(ids.units.tokens.slice(-1)).toEqual(['z']); // the last index is z
			expect(ids.units.increment('z')).toEqual('10'); // so it should rollover

			// this is what happens if we wrap against a bad character
			expect(ids.units.increment('2%z')).toEqual('200');
		});
	});
});