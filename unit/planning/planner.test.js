const { SerialAction } = require('../../src/planning/actions/serial');
const planner = require('../../src/planning/planner');

describe('planner', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(planner.units).forEach((key) => {
			jest.spyOn(planner.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		Object.keys(planner.boundaries).forEach((key) => {
			jest.spyOn(planner.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	test('planner.units', () => {
		expect(Object.keys(planner.units)).toEqual([
			'create',
			'createSingle',
			'createMultiple',
		]);
	});

	describe('create', () => {
		beforeEach(() => {
			planner.units.create.mockRestore();
		});

		test('invalid', async () => {
			expect(await planner.units.create()).toBe(undefined);
			expect(await planner.units.create('start')).toBe(undefined);
			expect(await planner.units.create('start', [])).toBe(undefined);

			expect(planner.units.createSingle).not.toHaveBeenCalled();
			expect(planner.units.createMultiple).not.toHaveBeenCalled();
		});

		test('single', async () => {
			planner.units.createSingle.mockReturnValueOnce(Promise.resolve({ action: 'a' }));
			expect(await planner.units.create('start', 'goal1')).toBe('a');

			planner.units.createSingle.mockReturnValueOnce(Promise.resolve({ action: 'b' }));
			expect(await planner.units.create('start', ['goal2'])).toBe('b');

			expect(planner.units.createSingle.mock.calls).toEqual([
				['start', 'goal1'],
				['start', 'goal2'],
			]);
			expect(planner.units.createMultiple).not.toHaveBeenCalled();
		});

		test('multiple', async () => {
			planner.units.createMultiple.mockReturnValueOnce(Promise.resolve('c'));
			expect(await planner.units.create('start', ['goal1', 'goal2'])).toBe('c');

			expect(planner.units.createSingle).not.toHaveBeenCalled();
			expect(planner.units.createMultiple.mock.calls).toEqual([
				['start', ['goal1', 'goal2']],
			]);
		});
	});

	describe('createSingle', () => {
		let p; // @mock Path
		const start = { state: new Set(['mock', 'subgraph', 'start']) }; // @mock BlueprintAction, Subgraph
		const goal = { state: new Set(['mock', 'subgraph', 'goal']) }; // @mock BlueprintAction, Subgraph
		beforeEach(() => {
			planner.units.createSingle.mockRestore();
			p = { actions: [], last: 'last' };
		});

		test('search not found', async () => {
			planner.boundaries.search.mockReturnValueOnce(Promise.resolve(undefined));

			expect(await planner.units.createSingle(start, goal)).toEqual({ action: undefined, state: undefined });
		});

		test('empty path', async () => {
			planner.boundaries.search.mockReturnValueOnce(Promise.resolve(p));

			expect(await planner.units.createSingle(start, goal)).toMatchSnapshot();
		});

		test('single path', async () => {
			p.actions = ['mock action'];
			planner.boundaries.search.mockReturnValueOnce(Promise.resolve(p));

			expect(await planner.units.createSingle(start, goal)).toEqual({ action: 'mock action', state: 'last' });
		});

		test('multi path', async () => {
			p.actions = [{ requirements: 'mock action 1' }, 'mock action 2'];
			planner.boundaries.search.mockReturnValueOnce(Promise.resolve(p));

			const result = await planner.units.createSingle(start, goal);
			expect(result).toEqual({ action: expect.any(SerialAction), state: 'last' });
			expect(result.action.steps).toEqual([{ requirements: 'mock action 1' }, 'mock action 2']);
		});

		test('bad plans', async () => {
			p.actions = ['mock action', undefined, 'mock action'];
			planner.boundaries.search.mockReturnValueOnce(Promise.resolve(p));

			expect(await planner.units.createSingle(start, goal)).toEqual({ action: undefined, state: undefined });
		});
	});

	test.todo('createMultiple');
});