const numbers = require('../../../src/planning/primitives/numbers');

function numbersIdeaData(...args) {
	return {
		value: numbers.units.value(...args),
		$unit: '_test',
		$type: 'lime_numbers',
	};
}

describe('numbers', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(numbers.units).forEach((key) => {
			jest.spyOn(numbers.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		Object.keys(numbers.boundaries).forEach((key) => {
			jest.spyOn(numbers.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});

		numbers.units.isNumber.mockRestore();
		numbers.units.match.mockRestore();
		numbers.units.value.mockRestore();
	});

	test('numbers.units', () => {
		expect(Object.keys(numbers.units)).toEqual([
			'isNumber',
			'cast',
			'match',
			'value',
			'combine',
			'remove',
			'difference',
			'getScale',
		]);
	});

	describe('isNumber', () => {
		beforeEach(() => {
			// numbers.units.isNumber.mockRestore();
		});

		test('invalid', () => {
			expect(numbers.units.isNumber()).toBe(false);
			expect(numbers.units.isNumber(1)).toBe(false);
			expect(numbers.units.isNumber({ $type: 'invalid', value: { bl: true, l: 1, r: 1, br: true }, $unit: '_test' })).toBe(false);
			expect(numbers.units.isNumber({ $type: 'lime_numbers', value: { bl: true, l: 2, r: 1, br: true }, $unit: '_test' })).toBe(false);
			expect(numbers.units.isNumber({ $type: 'lime_numbers', value: { bl: true, l: null, r: null, br: false }, $unit: '_test' })).toBe(false);
			expect(numbers.units.isNumber({ $type: 'lime_numbers', value: { bl: false, l: null, r: null, br: true }, $unit: '_test' })).toBe(false);
			expect(numbers.units.isNumber({ $type: 'lime_numbers', value: { bl: false, l: 1, r: 1, br: true }, $unit: '_test' })).toBe(false);
			expect(numbers.units.isNumber({ value: { bl: false, l: 2, r: 1, br: true }, $unit: '_test' })).toBe(false);
			expect(numbers.units.isNumber({ value: null, $unit: '_test' })).toBe(false);
		});

		test('valid', () => {
			expect(numbers.units.isNumber({ value: { bl: false, l: null, r: null, br: false }, $unit: '_test' })).toBe(true);
			expect(numbers.units.isNumber({ $type: 'lime_numbers', value: { bl: false, l: null, r: null, br: false }, $unit: '_test' })).toBe(true);
			expect(numbers.units.isNumber({ $type: 'lime_numbers', value: { bl: true, l: 1, r: 1, br: true }, $unit: '_test' })).toBe(true);
			expect(numbers.units.isNumber(numbersIdeaData(5))).toBe(true);
		});

		test('stringified', () => {
			const val1 = numbersIdeaData(-Infinity, Infinity);
			expect(val1).toMatchSnapshot();

			const str1 = JSON.stringify(val1);
			expect(str1).toMatchSnapshot();

			const val2 = JSON.parse(str1);
			expect(val2).toMatchSnapshot();
			expect(val2).not.toEqual(val1);
			expect(numbers.units.isNumber(val2)).toBe(true);
			expect(val2).toEqual(val1);

			const str2 = JSON.stringify(val2);
			expect(str2).toBe(str1);
		});
	});

	describe('cast', () => {
		beforeEach(() => {
			numbers.units.cast.mockRestore();
		});

		test('basic', () => {
			const n = { value: numbers.units.value(0), $unit: '_test' };
			expect(n).not.toHaveProperty('$type');
			expect(n).toMatchSnapshot('before cast');

			numbers.units.cast(n);

			expect(n).toHaveProperty('$type');
			expect(n).toMatchSnapshot('after cast');
		});

		test('not a number', () => {
			expect(numbers.units.cast()).toBe(undefined);
			expect(numbers.units.cast({})).toBe(undefined);
			expect(numbers.units.cast({ value: 'only' })).toBe(undefined);
			expect(numbers.units.cast({ $unit: '_only' })).toBe(undefined);
		});
	});

	test('match', () => {
		const n1 = { value: numbers.value(10), $unit: '_test' };
		expect(numbers.units.match(n1, { $unit: '_test' })).toBe(false);
		expect(numbers.units.match(n1, { value: numbers.value(0), $unit: '_test' })).toBe(true);
		expect(numbers.units.match(n1, { value: numbers.value(0), $unit: '_test2' })).toBe(false);
	});

	describe('value', () => {
		test('some stuff', () => {
			expect(numbers.units.value(0)).toEqual({ bl: true, l: 0, r: 0, br: true });
			expect(numbers.units.value(-3)).toEqual({ bl: true, l: -3, r: -3, br: true });
			expect(numbers.units.value(0, 0, false)).toEqual({ bl: false, l: 0, r: 0, br: false });
			expect(numbers.units.value(0, 0, true, false)).toEqual({ bl: true, l: 0, r: 0, br: false });
			expect(numbers.units.value(0, 0, false, true)).toEqual({ bl: false, l: 0, r: 0, br: true });

			expect(numbers.units.value(0, 1)).toEqual({ bl: true, l: 0, r: 1, br: true });
			expect(numbers.units.value(0, 1, false)).toEqual({ bl: false, l: 0, r: 1, br: false });
			expect(numbers.units.value(0, 1, true, false)).toEqual({ bl: true, l: 0, r: 1, br: false });
			expect(numbers.units.value(0, 1, false, true)).toEqual({ bl: false, l: 0, r: 1, br: true });
		});

		test('sugar', () => {
			expect(numbers.units.value()).toEqual({ bl: false, l: -Infinity, r: Infinity, br: false });
			expect(numbers.units.value(null)).toEqual({ bl: false, l: -Infinity, r: Infinity, br: false });
		});

		test('auto unbound Infinity', () => {
			expect(numbers.units.value(-Infinity, Infinity, true, true)).toEqual({ bl: false, l: -Infinity, r: Infinity, br: false });
		});

		test('invalid', () => {
			expect(numbers.units.value(-2, true)).toBe(undefined);
		});
	});

	describe('combine', () => {
		beforeEach(() => {
			numbers.units.combine.mockRestore();
		});

		test('invalid', () => {
			expect(numbers.units.combine()).toBe(undefined);
			expect(numbers.units.combine(numbersIdeaData(1))).toBe(undefined);
			expect(numbers.units.combine(numbersIdeaData(1), { value: numbers.units.value(1), $unit: '_mismatch' })).toBe(undefined);
		});

		test('simple number', () => {
			expect(numbers.units.combine(numbersIdeaData(1), numbersIdeaData(1))).toEqual(numbersIdeaData(2));
		});

		test('complicated', () => {
			expect(numbers.units.combine(numbersIdeaData(0), numbersIdeaData(3, 4, true, false))).toEqual(numbersIdeaData(3, 4, true, false));
			expect(numbers.units.combine(numbersIdeaData(1), numbersIdeaData(4, 5, false, true))).toEqual(numbersIdeaData(5, 6, false, true));
			expect(numbers.units.combine(numbersIdeaData(1, 2, true, false), numbersIdeaData(3, 4, false, true))).toEqual(numbersIdeaData(4, 6, false));

			expect(numbers.units.combine(numbersIdeaData(0), numbersIdeaData(-4, -3, true, false))).toEqual(numbersIdeaData(-4, -3, true, false));
		});

		test('strafe across zero', () => {
			expect(numbers.units.combine(numbersIdeaData(-6, -2), numbersIdeaData(-6, -2))).toEqual(numbersIdeaData(-12, -4));
			expect(numbers.units.combine(numbersIdeaData(-6, -2), numbersIdeaData(-2, 2))).toEqual(numbersIdeaData(-8, 0));
			expect(numbers.units.combine(numbersIdeaData(-6, -2), numbersIdeaData(2, 6))).toEqual(numbersIdeaData(-4, 4));

			expect(numbers.units.combine(numbersIdeaData(-2, 2), numbersIdeaData(-6, -2))).toEqual(numbersIdeaData(-8, 0));
			expect(numbers.units.combine(numbersIdeaData(-2, 2), numbersIdeaData(-2, 2))).toEqual(numbersIdeaData(-4, 4));
			expect(numbers.units.combine(numbersIdeaData(-2, 2), numbersIdeaData(2, 6))).toEqual(numbersIdeaData(0, 8));

			expect(numbers.units.combine(numbersIdeaData(2, 6), numbersIdeaData(-6, -2))).toEqual(numbersIdeaData(-4, 4));
			expect(numbers.units.combine(numbersIdeaData(2, 6), numbersIdeaData(-2, 2))).toEqual(numbersIdeaData(0, 8));
			expect(numbers.units.combine(numbersIdeaData(2, 6), numbersIdeaData(2, 6))).toEqual(numbersIdeaData(4, 12));
		});
	});

	describe('remove', () => {
		beforeEach(() => {
			numbers.units.remove.mockRestore();
		});

		test('invalid', () => {
			expect(numbers.units.remove()).toBe(undefined);
			expect(numbers.units.remove(numbersIdeaData(1))).toBe(undefined);
			expect(numbers.units.remove(numbersIdeaData(1), { value: numbers.units.value(1), $unit: '_mismatch' })).toBe(undefined);
		});

		test('simple number', () => {
			expect(numbers.units.remove(numbersIdeaData(3), numbersIdeaData(2))).toEqual(numbersIdeaData(1));
		});

		test('complicated', () => {
			expect(numbers.units.remove(numbersIdeaData(0), numbersIdeaData(3, 4, true, false))).toEqual(numbersIdeaData(-4, -3, false, true));
			expect(numbers.units.remove(numbersIdeaData(1), numbersIdeaData(4, 5, false, true))).toEqual(numbersIdeaData(-4, -3, true, false));
			expect(numbers.units.remove(numbersIdeaData(1, 2, true, false), numbersIdeaData(3, 4, false, true))).toEqual(numbersIdeaData(-3, -1, true, false));
		});

		test('fixed some weirdess about subtracting a range', () => {
			const sub = numbersIdeaData(2, 4);
			let curr = numbersIdeaData(10);

			curr = numbers.units.remove(curr, sub);

			// this is fine
			expect(curr).toEqual(numbersIdeaData(6, 8));

			curr = numbers.units.remove(curr, sub);

			expect(curr).toEqual(numbersIdeaData(2, 6));

			curr = numbers.units.remove(curr, sub);
			expect(curr).toEqual(numbersIdeaData(-2, 4));
			curr = numbers.units.remove(curr, sub);
			expect(curr).toEqual(numbersIdeaData(-6, 2));
		});

		test('strafe across zero', () => {
			expect(numbers.units.remove(numbersIdeaData(-6, -2), numbersIdeaData(-6, -2))).toEqual(numbersIdeaData(-4, 4));
			expect(numbers.units.remove(numbersIdeaData(-6, -2), numbersIdeaData(-2, 2))).toEqual(numbersIdeaData(-8, 0));
			expect(numbers.units.remove(numbersIdeaData(-6, -2), numbersIdeaData(2, 6))).toEqual(numbersIdeaData(-12, -4));

			expect(numbers.units.remove(numbersIdeaData(-2, 2), numbersIdeaData(-6, -2))).toEqual(numbersIdeaData(0, 8));
			expect(numbers.units.remove(numbersIdeaData(-2, 2), numbersIdeaData(-2, 2))).toEqual(numbersIdeaData(-4, 4));
			expect(numbers.units.remove(numbersIdeaData(-2, 2), numbersIdeaData(2, 6))).toEqual(numbersIdeaData(-8, 0));

			expect(numbers.units.remove(numbersIdeaData(2, 6), numbersIdeaData(-6, -2))).toEqual(numbersIdeaData(4, 12));
			expect(numbers.units.remove(numbersIdeaData(2, 6), numbersIdeaData(-2, 2))).toEqual(numbersIdeaData(0, 8));
			expect(numbers.units.remove(numbersIdeaData(2, 6), numbersIdeaData(2, 6))).toEqual(numbersIdeaData(-4, 4));
		});
	});

	test('combine/remove uncertainty 1', () => {
		numbers.units.combine.mockRestore();
		numbers.units.remove.mockRestore();

		// notice that our "uncertainty" increases each time
		// or, since there is a range of acceptible values, working on those only increases the range by that much

		expect(6 - 2).toBe(4); // our initial number
		expect(5 - 3).toBe(2); // the value we will keep oporating on

		expect(numbers.units.combine(numbersIdeaData(2, 6), numbersIdeaData(3, 5))).toEqual(numbersIdeaData(5, 11));
		expect((6 - 2) + (5 - 3)).toBe(6); // mixing the two numbers together spreads the allowable value
		expect(4 + 2).toBe(6); // mixing by this much
		expect(11 - 5).toBe(6); // the result spread

		expect(numbers.units.remove(numbersIdeaData(5, 11), numbersIdeaData(3, 5))).toEqual(numbersIdeaData(0, 8));
		expect((11 - 5) + (5 - 3)).toBe(8); // mix the same number again, and we see it increase by that much
		expect(4 + 2 + 2).toBe(8); // our original spread, oporated on twice
		expect(8 - 0).toBe(8); // the result spread
	});

	test('combine/remove uncertainty 2', () => {
		numbers.units.combine.mockRestore();
		numbers.units.remove.mockRestore();

		// in contrast, if there is no "uncertainty" in one of the numbers, then it doesn't change as you maths

		expect(numbers.units.combine(numbersIdeaData(1), numbersIdeaData(3))).toEqual(numbersIdeaData(4));
		expect(numbers.units.remove(numbersIdeaData(4), numbersIdeaData(3))).toEqual(numbersIdeaData(1));

		expect(numbers.units.combine(numbersIdeaData(2, 6), numbersIdeaData(3))).toEqual(numbersIdeaData(5, 9));
		expect(numbers.units.remove(numbersIdeaData(5, 9), numbersIdeaData(3))).toEqual(numbersIdeaData(2, 6));
	});

	describe('difference', () => {
		beforeEach(() => {
			numbers.units.difference.mockRestore();
			numbers.units.getScale.mockReturnValue(Promise.resolve(1));
		});

		test('invalid calls', async () => {
			expect(await numbers.units.difference()).toBe(undefined);
			expect(await numbers.units.difference(numbersIdeaData(1))).toBe(undefined);
			expect(await numbers.units.difference(numbersIdeaData(1), { value: numbers.units.value(1), $unit: '_mismatch' })).toBe(undefined);
		});

		test('simple numbers', async () => {
			expect(await numbers.units.difference(numbersIdeaData(1), numbersIdeaData(1))).toBe(0);
			expect(await numbers.units.difference(numbersIdeaData(3), numbersIdeaData(1))).toBe(2);
			expect(await numbers.units.difference(numbersIdeaData(1), numbersIdeaData(10))).toBe(9);
		});

		test('boundary strafing', async () => {
			expect(await numbers.units.difference(numbersIdeaData(1, 3), numbersIdeaData(-1))).toBe(2);
			expect(await numbers.units.difference(numbersIdeaData(1, 3), numbersIdeaData(0))).toBe(1);
			expect(await numbers.units.difference(numbersIdeaData(1, 3), numbersIdeaData(1))).toBe(0);
			expect(await numbers.units.difference(numbersIdeaData(1, 3), numbersIdeaData(2))).toBe(0);
			expect(await numbers.units.difference(numbersIdeaData(1, 3), numbersIdeaData(3))).toBe(0);
			expect(await numbers.units.difference(numbersIdeaData(1, 3), numbersIdeaData(4))).toBe(1);
			expect(await numbers.units.difference(numbersIdeaData(1, 3), numbersIdeaData(5))).toBe(2);
		});

		test('-Infinity', async () => {
			expect(await numbers.units.difference(numbersIdeaData(-Infinity, 3), numbersIdeaData(-1))).toBe(0);
			expect(await numbers.units.difference(numbersIdeaData(-Infinity, 3), numbersIdeaData(-10000))).toBe(0);
			expect(await numbers.units.difference(numbersIdeaData(-Infinity, 3), numbersIdeaData(5))).toBe(2);
			expect(await numbers.units.difference(numbersIdeaData(-Infinity, 3), numbersIdeaData(5000))).toBe(4997);
		});

		test('Infinity', async () => {
			expect(await numbers.units.difference(numbersIdeaData(3, Infinity), numbersIdeaData(-1))).toBe(4);
			expect(await numbers.units.difference(numbersIdeaData(3, Infinity), numbersIdeaData(-10000))).toBe(10003);
			expect(await numbers.units.difference(numbersIdeaData(3, Infinity), numbersIdeaData(5))).toBe(0);
			expect(await numbers.units.difference(numbersIdeaData(3, Infinity), numbersIdeaData(5000))).toBe(0);
		});

		test('alt scale', async () => {
			numbers.units.getScale.mockReturnValueOnce(Promise.resolve(3));
			expect(await numbers.units.difference(numbersIdeaData(1), numbersIdeaData(3))).toEqual(6);

			numbers.units.getScale.mockReturnValueOnce(Promise.resolve(3));
			expect(await numbers.units.difference(numbersIdeaData(1), numbersIdeaData(11))).toEqual(30);

			numbers.units.getScale.mockReturnValueOnce(Promise.resolve(0.5));
			expect(await numbers.units.difference(numbersIdeaData(1), numbersIdeaData(3))).toEqual(1);

			numbers.units.getScale.mockReturnValueOnce(Promise.resolve(0.5));
			expect(await numbers.units.difference(numbersIdeaData(1), numbersIdeaData(11))).toEqual(5);
		});
	});

	describe('getScale', () => {
		beforeEach(() => {
			numbers.units.getScale.mockRestore();
		});

		test('no data', async () => {
			numbers.boundaries.loadUnitData.mockReturnValueOnce(Promise.resolve(null));

			expect(await numbers.units.getScale('_test')).toBe(1);

			expect(numbers.boundaries.loadUnitData.mock.calls).toEqual([
				['_test'],
			]);
		});

		test('no scale', async () => {
			numbers.boundaries.loadUnitData.mockReturnValueOnce(Promise.resolve({ some: 'thing' }));

			expect(await numbers.units.getScale('_test')).toBe(1);

			expect(numbers.boundaries.loadUnitData.mock.calls).toEqual([
				['_test'],
			]);
		});

		test('has scale', async () => {
			numbers.boundaries.loadUnitData.mockReturnValueOnce(Promise.resolve({ scale: 2 }));

			expect(await numbers.units.getScale('_test')).toBe(2);

			expect(numbers.boundaries.loadUnitData.mock.calls).toEqual([
				['_test'],
			]);
		});
	});
});