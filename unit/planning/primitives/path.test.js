/* eslint-disable max-classes-per-file */
const _ = require('lodash');
const { Path, PathAction, PathState } = require('../../../src/planning/primitives/path');

class MockAction extends PathAction {
	cost() { return 1; }
}

class MockState extends PathState {
	distance() { return 2; }
}

describe('path', () => {
	describe('Path', () => {
		test('prototype', () => {
			expect(Object.getOwnPropertyNames(new Path(undefined, []).__proto__)).toEqual([
				'constructor',
				'last',
				'$calcCost',
				'$calcDistFromGoal',
				'isReady',
				'getReady',
				'add',
			]);
		});

		test('constructor', () => {
			expect(new Path(['s1'], [], 'g')).toMatchSnapshot();
			expect(new Path(['s1', 's2'], ['a1'], 'g')).toMatchSnapshot();
			expect(new Path(['s1', 's2'], ['a1'], 'g', 1, 2)).toMatchSnapshot();
			expect(new Path(['s1', 's2'], ['a1'], ['g1', 'g2'], 1, [3, 4])).toMatchSnapshot();
		});

		test('last', () => {
			const path = new Path(['a'], []);
			expect(path.last).toBe('a');
		});

		describe('$calcCost', () => {
			test('empty', async () => {
				const path = new Path(undefined, []);
				expect(path.cost).toBe(0);

				expect(await path.$calcCost()).toBe(0);

				expect(path.cost).toBe(0);
			});

			test('non-empty', async () => {
				const path = new Path(['a', 'b', 'c'], [
					{ cost: jest.fn().mockReturnValueOnce(Promise.resolve(1)) },
					{ cost: jest.fn().mockReturnValueOnce(Promise.resolve(2)) },
				]);
				expect(path.cost).toBe(undefined);

				expect(await path.$calcCost()).toBe(3);

				expect(path.cost).toBe(3);
				expect(path.actions[0].cost.mock.calls).toEqual([['a', 'b']]);
				expect(path.actions[1].cost.mock.calls).toEqual([['b', 'c']]);
			});
		});

		describe('$calcDistFromGoal', () => {
			let state; // @mock PathState
			beforeEach(() => {
				state = {
					distance: jest.fn(),
					matches: jest.fn(),
				};
			});

			test('non-zero distance', async () => {
				const path = new Path([state, state], ['a'], ['g1', 'g2', 'g3']);
				expect(path.distFromGoal).toBe(undefined);
				state.distance.mockReturnValueOnce(Promise.resolve(2));
				state.distance.mockReturnValueOnce(Promise.resolve(3));
				state.distance.mockReturnValueOnce(Promise.resolve(4));

				expect(await path.$calcDistFromGoal()).toBe(2);

				expect(path.distFromGoal).toBe(2);
				expect(path.$goalsMet).toBe(null);
				expect(state.distance.mock.calls).toEqual([['g1'], ['g2'], ['g3']]);
				expect(state.matches.mock.calls).toEqual([]);
			});

			test('zero distance', async () => {
				const path = new Path([state, state], ['a'], ['g1', 'g2', 'g3']);
				expect(path.distFromGoal).toBe(undefined);
				state.distance.mockReturnValueOnce(Promise.resolve(2));
				state.distance.mockReturnValueOnce(Promise.resolve(0));
				state.distance.mockReturnValueOnce(Promise.resolve(0));
				state.matches.mockReturnValueOnce(Promise.resolve(true));
				state.matches.mockReturnValueOnce(Promise.resolve(false));

				expect(await path.$calcDistFromGoal()).toBe(0);

				expect(path.distFromGoal).toBe(0);
				expect(path.$goalsMet).toEqual(['g2']);
				expect(state.distance.mock.calls).toEqual([['g1'], ['g2'], ['g3']]);
				expect(state.matches.mock.calls).toEqual([['g2'], ['g3']]);
			});
		});

		test('isReady', () => {
			const path = new Path(undefined, []);
			expect(path.isReady()).toBe(false);

			path.cost = 0;
			expect(path.isReady()).toBe(false);

			path.distFromGoal = 0;
			expect(path.isReady()).toBe(true);

			path.cost = undefined;
			expect(path.isReady()).toBe(false);
		});

		test('getReady', async () => {
			const action = new MockAction();
			const state = new MockState();
			const path = new Path([state, state], [action], [state]);

			expect(path.isReady()).toBe(false);

			expect(await path.getReady()).toBe(path);

			expect(path.isReady()).toBe(true);
			expect(await path.getReady()).toBe(path);
			expect(_.omit(path, ['states', 'actions', 'goals'])).toMatchSnapshot();
		});

		test('add', async () => {
			const state = new MockState();
			const action = new MockAction();
			const path = new Path([state, state], [action], [state]);
			await path.getReady();
			expect(path.cost).toBe(1);

			const next = await path.add(state, action);

			expect(next).not.toBe(path);
			expect(next.isReady()).toBe(true);
			expect(next.cost).toBe(2);

			next.states = `(${next.states.length})`;
			next.actions = `(${next.actions.length})`;
			next.goals = `(${next.goals.length})`;
			expect(next).toMatchSnapshot();
		});
	});

	describe('PathAction', () => {
		let action;
		beforeEach(() => {
			action = new PathAction();
		});

		test('prototype', () => {
			expect(Object.getOwnPropertyNames(action.__proto__)).toEqual([
				'constructor',
				'prettyNames',
				'cost',
				'runAbstract',
			]);
		});

		test('constructor', () => {
			expect(action).toMatchSnapshot();
		});

		test('prettyNames', () => {
			expect(() => action.prettyNames()).toThrow(/^PathAction must implement PathAction.prettyNames$/);
		});

		test('cost', async () => {
			await expect(action.cost()).rejects.toThrow(/^PathAction must implement PathAction.cost$/);
		});

		test('runAbstract', async () => {
			await expect(action.runAbstract()).rejects.toThrow(/^PathAction must implement PathAction.runAbstract$/);
		});
	});

	describe('PathState', () => {
		let state;
		beforeEach(() => {
			state = new PathState();
		});

		test('prototype', () => {
			expect(Object.getOwnPropertyNames(state.__proto__)).toEqual([
				'constructor',
				'distance',
				'actions',
				'$calcActions',
				'matches',
				'equals',
			]);
		});

		test('constructor', () => {
			expect(state).toMatchSnapshot();
		});

		test('distance', async () => {
			await expect(state.distance()).rejects.toThrow(/^PathState must implement PathState.distance$/);
		});

		test('actions', async () => {
			jest.spyOn(state, '$calcActions').mockReturnValueOnce(Promise.resolve(['a', 'b']));

			expect(await state.actions()).toEqual(['a', 'b']);
			expect(await state.actions()).toEqual(['a', 'b']);
			expect(await state.actions()).toEqual(['a', 'b']);
			expect(state.$calcActions.mock.calls).toEqual([
				[], // called once with no args
			]);
			state.$calcActions.mockRestore();
			expect(state).toMatchSnapshot(); // actions are cached
		});

		test('$calcActions', async () => {
			await expect(state.$calcActions()).rejects.toThrow(/^PathState must implement PathState.\$calcActions$/);
		});

		describe('matches (default impl)', () => {
			beforeEach(() => {
				jest.spyOn(state, 'distance');
			});

			test('yes', async () => {
				state.distance.mockReturnValueOnce(Promise.resolve(0));
				expect(await state.matches(null)).toBe(true);
			});

			test('no', async () => {
				state.distance.mockReturnValueOnce(Promise.resolve(1000));
				expect(await state.matches(null)).toBe(false);
			});
		});

		test('equals', async () => {
			expect(() => state.equals()).toThrow(/^PathState must implement PathState.equals$/);
		});
	});
});