const ideas = require('../../../src/database/ideas');
const subgraphs = require('../../../src/database/subgraphs/subgraphs'); // TODO try to remove this dep
const { Subgraph } = require('../../../src/database/subgraphs');
const numbers = require('../../../src/planning/primitives/numbers');
const blueprint = require('../../../src/planning/primitives/blueprint');

const { BlueprintAction, BlueprintState } = blueprint;

class MockAction extends BlueprintAction {}

describe('blueprint', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(blueprint.units).forEach((key) => {
			if (key === 'actionParsers') return;
			jest.spyOn(blueprint.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		Object.keys(blueprint.boundaries).forEach((key) => {
			if (key === 'context') return;
			if (key === 'debugLogs') return;
			jest.spyOn(blueprint.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	describe('BlueprintAction', () => {
		let action;
		beforeEach(() => {
			action = new BlueprintAction();
		});

		test('prototype', () => {
			// eslint-disable-next-line no-proto
			expect(Object.getOwnPropertyNames(action.__proto__)).toEqual([
				'constructor',
				'$calcRunCost',
				'cost',
				'isGlued',
				'calcGlue',
				'runActual',
				'scheduleActual',
				'save',
				'$stringifyAction',
				'shallowEquals',
			]);
		});

		test('constructor', () => {
			expect(action).toMatchSnapshot();
		});

		test('$calcRunCost', async () => {
			await expect(action.$calcRunCost()).rejects.toThrow(/^BlueprintAction must implement BlueprintAction.\$calcRunCost$/);
		});

		describe('cost', () => {
			let from;
			let to;
			beforeEach(() => {
				from = new BlueprintState(new Subgraph(), []);
				to = new BlueprintState(new Subgraph(), []);

				jest.spyOn(from, 'distance').mockReturnValue(Promise.resolve(5));
				jest.spyOn(action, '$calcRunCost').mockReturnValue(Promise.resolve(12));
			});

			test('from does not match requirements', async () => {
				expect(await action.cost(from, to)).toBe(Infinity);

				expect(from.distance).not.toHaveBeenCalled();
				expect(action.$calcRunCost).not.toHaveBeenCalled();
			});

			test('success', async () => {
				from.state.addVertex('id', ideas.proxy('_test'));
				action.requirements.addVertex('id', ideas.proxy('_test'));

				expect(await action.cost(from, to)).toBe(17);

				expect(from.distance.mock.calls).toEqual([
					[to],
				]);
				expect(action.$calcRunCost.mock.calls).toEqual([
					[],
				]);
			});
		});

		test('isGlued', () => {
			expect(action.isGlued()).toBe(false);

			action.$glue = 'mock glue';
			action.$gluedState = undefined;

			expect(action.isGlued()).toBe(false);

			action.$glue = undefined;
			action.$gluedState = 'mock state';

			expect(action.isGlued()).toBe(false);

			action.$glue = 'mock glue';
			action.$gluedState = 'mock state';

			expect(action.isGlued()).toBe(true);
		});

		test('calcGlue', async () => {
			await expect(action.calcGlue()).rejects.toThrow(/^BlueprintAction must implement BlueprintAction.calcGlue$/);
		});

		test('runAbstract', async () => {
			await expect(action.runAbstract()).rejects.toThrow(/^BlueprintAction must implement PathAction.runAbstract$/);
		});

		test('runActual', async () => {
			await expect(action.runActual()).rejects.toThrow(/^BlueprintAction must implement BlueprintAction.runActual$/);
		});

		test('scheduleActual', async () => {
			await expect(action.scheduleActual()).rejects.toThrow(/^BlueprintAction must implement BlueprintAction.scheduleActual$/);
		});

		test.todo('save');

		test('$stringifyAction / actionParser', async () => {
			await expect(action.$stringifyAction()).rejects.toThrow(/^BlueprintAction must implement BlueprintAction.\$stringifyAction$/);
			expect(blueprint.units.actionParsers.get('BlueprintAction')).toBe(undefined);
		});

		test('shallowEquals', () => {
			const a = new MockAction();

			expect(a.shallowEquals()).toBe(false);
			expect(a.shallowEquals({})).toBe(false);
			expect(a.shallowEquals(a)).toBe(true);
			expect(a.shallowEquals(new MockAction())).toBe(false); // NOTE this is shallow equals - they are deep equal
			expect(a.shallowEquals(new MockAction(a))).toBe(true); // since one is derrived from the other

			const b = new MockAction(a);
			expect(a.shallowEquals(b)).toBe(true);

			// these "private" props are ignored
			a.$somethingIgnored = ['a'];
			b.$somethingIgnored = ['b'];
			expect(a.shallowEquals(b)).toBe(true);

			a.somethingImportant = ['a'];
			b.somethingImportant = ['b'];
			expect(a.shallowEquals(b)).toBe(false);
			b.somethingImportant = a.somethingImportant;
			expect(a.shallowEquals(b)).toBe(true);
		});
	});

	describe('BlueprintState', () => {
		let state;
		beforeEach(() => {
			state = new BlueprintState(new Subgraph(), []);
		});

		test('prototype', () => {
			// eslint-disable-next-line no-proto
			expect(Object.getOwnPropertyNames(state.__proto__)).toEqual([
				'constructor',
				'distance',
				'$calcActions',
				'matches',
				'equals',
			]);
		});

		// break down BlueprintState.distance into smaller units to make it easier to understand and test
		describe('distance', () => {
			test('not concrete', async () => {
				state.state.addVertex('filler');
				expect(state.state.concrete).toBe(false);

				await expect(state.distance()).rejects.toThrow(/^BlueprintState must be concrete to plan from$/);
			});

			test('empty states', async () => {
				const goal = new BlueprintState(new Subgraph(), []);

				expect(await state.distance(goal)).toBe(Infinity);
			});

			describe('match found', () => {
				let goal;
				beforeEach(() => {
					goal = new BlueprintState(new Subgraph(), []);
					state.state.addEdge(
						state.state.addVertex('id', ideas.proxy('_test')),
						'has',
						state.state.addVertex('id', ideas.proxy('_test2')),
					);
					state.state.concrete = true;
					goal.state.addEdge(
						goal.state.addVertex('id', ideas.proxy('_test')),
						'has',
						goal.state.addVertex('filler'),
					);
				});

				test('success', async () => {
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 1' }));
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 2' }));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(1));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(1));

					expect(await state.distance(goal)).toBe(2);

					expect(blueprint.units.getVertexDataForDiff.mock.calls).toEqual([
						[expect.any(Map), state, goal, 0, 0],
						[expect.any(Map), state, goal, 1, 1],
					]);
					expect(blueprint.units.calcDiffFromVertexData.mock.calls).toEqual([
						[{ innerVertex: {}, viData: 'mock dataForDiff 1' }],
						[{ innerVertex: {}, viData: 'mock dataForDiff 2' }],
					]);
				});

				test('equal', async () => {
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 1' }));
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 2' }));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(0));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(0));

					expect(await state.distance(goal)).toBe(0);

					expect(blueprint.units.getVertexDataForDiff.mock.calls).toEqual([
						[expect.any(Map), state, goal, 0, 0],
						[expect.any(Map), state, goal, 1, 1],
					]);
					expect(blueprint.units.calcDiffFromVertexData.mock.calls).toEqual([
						[{ innerVertex: {}, viData: 'mock dataForDiff 1' }],
						[{ innerVertex: {}, viData: 'mock dataForDiff 2' }],
					]);
				});

				test('bad diff', async () => {
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve(null));
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 2' }));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(1));

					expect(await state.distance(goal)).toBe(Infinity);

					expect(blueprint.units.getVertexDataForDiff.mock.calls).toEqual([
						[expect.any(Map), state, goal, 0, 0],
						[expect.any(Map), state, goal, 1, 1],
					]);
					expect(blueprint.units.calcDiffFromVertexData.mock.calls).toEqual([
						[{ innerVertex: {}, viData: 'mock dataForDiff 2' }],
					]);
				});

				test('multiple matches', async () => {
					state.state.addEdge(
						0,
						'has',
						state.state.addVertex('id', ideas.proxy('_test3')),
					);
					state.state.concrete = true;

					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 1' }));
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 2' }));
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 3' }));
					blueprint.units.getVertexDataForDiff.mockReturnValueOnce(Promise.resolve({ innerVertex: {}, viData: 'mock dataForDiff 4' }));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(1));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(4));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(2));
					blueprint.units.calcDiffFromVertexData.mockReturnValueOnce(Promise.resolve(8));

					expect(await state.distance(goal)).toBe(5);

					expect(blueprint.units.getVertexDataForDiff.mock.calls).toEqual([
						[expect.any(Map), state, goal, 0, 0],
						[expect.any(Map), state, goal, 1, 1],
						[expect.any(Map), state, goal, 0, 0],
						[expect.any(Map), state, goal, 2, 1],
					]);
					expect(blueprint.units.calcDiffFromVertexData.mock.calls).toEqual([
						[{ innerVertex: {}, viData: 'mock dataForDiff 1' }],
						[{ innerVertex: {}, viData: 'mock dataForDiff 2' }],
						[{ innerVertex: {}, viData: 'mock dataForDiff 3' }],
						[{ innerVertex: {}, viData: 'mock dataForDiff 4' }],
					]);
				});
			});
		});

		describe('$calcActions', () => {
			beforeEach(() => {
				// this should have been in the constructor, but... this is a test
				state.availableActions = [new BlueprintAction(), new BlueprintAction()];
				state.availableActions.forEach((a) => { jest.spyOn(a, 'calcGlue').mockReturnValue(Promise.resolve([])); });
			});

			test('not concrete', async () => {
				state.state.addVertex('filler');
				expect(state.state.concrete).toBe(false);

				await expect(state.$calcActions()).rejects.toThrow(/^BlueprintState must be concrete to plan from$/);

				expect(state.availableActions[0].calcGlue).not.toHaveBeenCalled();
				expect(state.availableActions[1].calcGlue).not.toHaveBeenCalled();
			});

			test('no actions', async () => {
				expect(state.state.concrete).toBe(true);

				expect(await state.$calcActions()).toEqual([]);

				expect(state.availableActions[0].calcGlue.mock.calls).toEqual([
					[state],
				]);
				expect(state.availableActions[1].calcGlue.mock.calls).toEqual([
					[state],
				]);
			});

			test('success', async () => {
				expect(state.state.concrete).toBe(true);
				// @mock BlueprintAction
				function createGluedAction(num) {
					const gluedAction = new BlueprintAction();
					gluedAction.requirements = `mocked null ${num}`;
					gluedAction.$glue = `somedata ${num}`;
					gluedAction.$gluedState = 'arguments[0]';
					return gluedAction;
				}

				state.availableActions[0].calcGlue.mockReturnValue(Promise.resolve([
					createGluedAction('0-1'),
				]));
				state.availableActions[1].calcGlue.mockReturnValue(Promise.resolve([
					createGluedAction('1-1'),
					createGluedAction('1-2'),
				]));

				expect(await state.$calcActions()).toMatchSnapshot();

				expect(state.availableActions[0].calcGlue.mock.calls).toEqual([
					[state],
				]);
				expect(state.availableActions[1].calcGlue.mock.calls).toEqual([
					[state],
				]);
			});
		});

		describe('matches', () => {
			test('not concrete', async () => {
				state.state.addVertex('filler');

				await expect(state.matches(null)).rejects.toThrow(/^BlueprintState must be concrete to plan from$/);
			});

			test('empty states', async () => {
				const goal = new BlueprintState(new Subgraph(), []);

				expect(await state.matches(goal)).toBe(false);
			});

			test('equal', async () => {
				const goal = new BlueprintState(new Subgraph(), []);

				state.state.addVertex('id', ideas.proxy('_test'));
				goal.state.addVertex('id', ideas.proxy('_test'));

				expect(await state.matches(goal)).toBe(true);
			});

			test('not equal', async () => {
				const goal = new BlueprintState(new Subgraph(), []);

				state.state.addVertex('id', ideas.proxy('_test'));
				goal.state.addVertex('id', ideas.proxy('_test2'));

				expect(await state.matches(goal)).toBe(false);
			});
		});

		test.todo('equals');
	});

	/*
		UNITS
	*/

	test('blueprint.units', () => {
		expect(Object.keys(blueprint.units)).toEqual([
			'getVertexDataForDiff',
			'calcDiffFromVertexData',
			'actionParsers',
			'load',
			'fromContext',
			'createStartAndGoal',
		]);
	});

	describe('getVertexDataForDiff', () => {
		let innerToOuterVertexMap;
		let from;
		let to;
		let outerVertexId;
		let innerVertexId;
		beforeEach(() => {
			blueprint.units.getVertexDataForDiff.mockRestore();
			innerToOuterVertexMap = new Map([[0, 0], [1, 1]]);

			from = new BlueprintState(new Subgraph(), []);
			from.state.addEdge(
				from.state.addVertex('id', ideas.proxy('_test')),
				'has',
				from.state.addVertex('id', ideas.proxy('_test2')),
			);
			from.state.concrete = true;
			from.state.setData(0, { value: 'mock data from' });

			to = new BlueprintState(new Subgraph(), []);
			to.state.addEdge(
				to.state.addVertex('id', ideas.proxy('_test')),
				'has',
				to.state.addVertex('filler'),
			);
			to.state.setData(0, { value: 'mock data to' });

			outerVertexId = 0;
			innerVertexId = 0;
		});

		test('initial setup works', async () => {
			expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).toMatchSnapshot();
		});

		test('transitionable states', async () => {
			from.state.getVertex(outerVertexId).transitionable = false;
			to.state.getVertex(innerVertexId).transitionable = false;
			// nobody can transition
			expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).not.toBe(null);

			from.state.getVertex(outerVertexId).transitionable = true;
			to.state.getVertex(innerVertexId).transitionable = false;
			// there inner is more restrictive than the outer
			expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).not.toBe(null);

			from.state.getVertex(outerVertexId).transitionable = false;
			to.state.getVertex(innerVertexId).transitionable = true;
			// this one is invalid
			// the outer must say that it can be transition because the inner may try to act on it
			expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).toBe(null);

			from.state.getVertex(outerVertexId).transitionable = true;
			to.state.getVertex(innerVertexId).transitionable = true;
			// both can transition
			expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).not.toBe(null);
		});

		describe('viData not found', () => {
			beforeEach(() => {
				to.state.deleteData(0);
			});

			test('nowhere else to look', async () => {
				expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).toMatchSnapshot();
			});

			test('check pointer', async () => {
				innerVertexId = to.state.addVertex('id', 0, { pointer: true });

				expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).toMatchSnapshot();
			});

			// this test is just a copy of the one above, it's setup wrong
			// i think i need to add complexity to the test data to be sure of the results
			test.skip('check pointer, ensure mismatched vertex', async () => {
				innerVertexId = to.state.addVertex('id', 0, { pointer: true });
				const innerVertex = to.state.getVertex(innerVertexId);
				expect(innerVertex.data).not.toBe(innerToOuterVertexMap.get(innerVertex.data)); // AC

				expect(await blueprint.units.getVertexDataForDiff(innerToOuterVertexMap, from, to, outerVertexId, innerVertexId)).toMatchSnapshot();
			});
		});
	});

	describe('calcDiffFromVertexData', () => {
		let dataForDiff;
		beforeEach(() => {
			blueprint.units.calcDiffFromVertexData.mockRestore();
			dataForDiff = {
				innerVertex: { matcher: subgraphs.units.matchers.exact }, // @mock Subgraph
				outerVertex: { transitionable: true }, // @mock Subgraph
				viData: [undefined],
				voData: undefined,
			};
		});

		test('similar matcher', async () => {
			dataForDiff.innerVertex.matcher = subgraphs.units.matchers.similar;

			dataForDiff.viData = [{ thing1: 3.1 }];
			dataForDiff.voData = { thing1: 3.1, thing2: 2.7 };
			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(0);

			dataForDiff.viData = [{ thing1: 42 }];
			dataForDiff.voData = { thing1: 3.1, thing2: 2.7 };
			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(1);

			dataForDiff.viData = [{ thing1: 3.1, thing2: 2.7 }];
			dataForDiff.voData = { thing1: 3.1 };
			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(1);
		});

		test('exact matcher', async () => {
			dataForDiff.innerVertex.matcher = subgraphs.units.matchers.exact;

			dataForDiff.viData = [undefined];
			dataForDiff.voData = undefined;
			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(0);

			dataForDiff.viData = [1];
			dataForDiff.voData = 1;
			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(0);

			dataForDiff.viData = [1];
			dataForDiff.voData = 50;
			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(1);
		});

		test('id matcher', async () => {
			dataForDiff.innerVertex.matcher = subgraphs.units.matchers.id;

			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(0);
		});

		describe('isNumber', () => {
			beforeEach(() => {
				dataForDiff.viData = [numbers.cast({ value: numbers.value(10), $unit: '_test' })];
				dataForDiff.voData = numbers.cast({ value: numbers.value(14), $unit: '_test' });
			});

			test('both are numbers', async () => {
				expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(4);
			});

			test('viData is not a number', async () => {
				dataForDiff.viData = [{}];
				expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(Infinity);
			});

			// skipped because: this was an extra check before viData was a list; i'm not sure how important the check actually is
			//  - it's probably better to just ignore the whole "is a number" thing, and always rely on the number matcher
			test.skip('voData is not a number', async () => {
				dataForDiff.voData = {};
				expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(Infinity);
			});
		});

		test('equal', async () => {
			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(0);
		});

		test('not equal', async () => {
			dataForDiff.voData = {};

			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(1);
		});

		test.todo('new equal, inner uses orData');

		test('not equal, outer not transitionable', async () => {
			dataForDiff.outerVertex.transitionable = false;
			dataForDiff.voData = {};

			expect(await blueprint.units.calcDiffFromVertexData(dataForDiff)).toBe(Infinity);
		});
	});

	test('actionParsers', () => {
		expect(blueprint.units.actionParsers).toMatchSnapshot();
	});

	test.todo('load');

	test.todo('fromContext');

	describe('createStartAndGoal', () => {
		let start; // @mock BlueprintState, Subgraph
		let action;
		let goal; // @mock BlueprintState, Subgraph

		beforeEach(() => {
			blueprint.units.createStartAndGoal.mockRestore();
			jest.spyOn(subgraphs, 'projectSubgraph').mockImplementation(() => { throw new Error('must mock behavior for "projectSubgraph"'); });

			action = new MockAction({ requirements: { mock: 'requirements', concrete: false } });
			start = { state: { mock: 'start', concrete: true }, availableActions: ['mock action a', new MockAction(action), 'mock action b'] };
			goal = { state: { mock: 'goal', concrete: true }, availableActions: start.availableActions };

			action.$glue = { innerToOuterVertexMap: new Map([['mock', 'vertex']]), innerToOuterEdgeMap: new Map([['mock', 'edge']]) };
			action.$gluedState = start;
		});

		test('invalid', () => {
			goal.state.concrete = false;
			expect(() => blueprint.units.createStartAndGoal(start, action, goal)).toThrow(/^BlueprintState must be concrete to check equality \(goal\)$/);

			action.$gluedState = undefined;
			expect(() => blueprint.units.createStartAndGoal(start, action, goal)).toThrow(/^MockAction has not been glued$/);

			start.state.concrete = false;
			expect(() => blueprint.units.createStartAndGoal(start, action, goal)).toThrow(/^BlueprintState must be concrete to check equality \(start\)$/);

			expect(subgraphs.projectSubgraph).not.toHaveBeenCalled();
		});

		test('success', () => {
			subgraphs.projectSubgraph.mockReturnValueOnce({ mock: 'goal-requirements', concrete: false });

			expect(blueprint.units.createStartAndGoal(start, action, goal)).toMatchSnapshot();

			expect(subgraphs.projectSubgraph.mock.calls).toEqual([
				[goal.state, action.requirements, action.$glue],
			]);
		});
	});
});