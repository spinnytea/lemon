const _ = require('lodash');
const ideas = require('../../../src/database/ideas');
const { Subgraph } = require('../../../src/database/subgraphs');
const stub = require('../../../src/planning/actions/stub');
const { BlueprintState } = require('../../../src/planning/primitives/blueprint');

const { StubAction } = stub;

describe('stub', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(stub.units).forEach((key) => {
			if (key === 'solveAt') return;
			jest.spyOn(stub.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	describe('StubAction', () => {
		let action;
		beforeEach(() => {
			action = new StubAction({ solveAt: 'immediate' });
		});

		test('prototype', () => {
			// eslint-disable-next-line no-proto
			expect(Object.getOwnPropertyNames(action.__proto__)).toEqual([
				'constructor',
				'prettyNames',
				'$calcRunCost',
				'calcGlue',
				'runAbstract',
				'$stringifyAction',
			]);
		});

		test('constructor', () => {
			expect(() => new StubAction()).toThrow(/^Cannot destructure property 'solveAt' of 'undefined' as it is undefined.$/);
			expect(() => new StubAction({})).toThrow(/^unknown solveAt: "undefined"$/);
			expect(() => new StubAction({ solveAt: 'banana' })).toThrow(/^unknown solveAt: "banana"$/);

			stub.units.solveAt.forEach((solveAt) => {
				expect(new StubAction({ solveAt })).toMatchSnapshot(solveAt);
			});
		});

		test('prettyNames', () => {
			expect(new StubAction({ solveAt: 'create' }).prettyNames()).toEqual('StubAction (create)');
			expect(new StubAction({ solveAt: 'immediate' }).prettyNames()).toEqual('StubAction (immediate)');
		});

		describe('$calcRunCost', () => {
			test('no transitions', async () => {
				action.transitions = [];

				// there shouldn't actually be any of these
				// but like, we don't want to pick actions that don't _do_ anything
				// neutral jing (waiting with purpose) is different than total inaction
				expect(await action.$calcRunCost()).toBe(Infinity);
			});

			test('no costs', async () => {
				action.transitions = [
					{ vertexId: 0 },
					{ edgeId: 0 },
				];

				expect(await action.$calcRunCost()).toBe(4);
			});

			test('with costs', async () => {
				action.transitions = [
					{ vertexId: 0, cost: 3 },
					{ edgeId: 0, cost: 4 },
				];

				expect(await action.$calcRunCost()).toBe(7);
			});
		});

		describe('calcGlue', () => {
			let state;
			beforeEach(() => {
				action.requirements.addEdge(
					action.requirements.addVertex('id', ideas.proxy('_test')),
					'has',
					action.requirements.addVertex('filler'),
				);

				state = new BlueprintState(new Subgraph(), []);
				state.state.addEdge(
					state.state.addVertex('id', ideas.proxy('_test')),
					'has',
					state.state.addVertex('id', ideas.proxy('_test2')),
				);
				state.state.concrete = true;
			});

			// this is important because it throws an error
			test.todo('state is not concrete');

			test('single glue', async () => {
				expect(await action.calcGlue(state)).toMatchSnapshot();

				expect(action).toMatchSnapshot('original action is unchanged');
			});

			test('multiple glues', async () => {
				state.state.addEdge(
					0,
					'has',
					state.state.addVertex('id', ideas.proxy('_test3')),
				);
				state.state.concrete = true;

				const gluedActions = await action.calcGlue(state);
				expect(_.map(gluedActions, '$glue')).toMatchSnapshot();

				expect(action).toMatchSnapshot('original action is unchanged');
			});
		});

		test.todo('runAbstract');

		test('runActual', async () => {
			// this should never be implemented, it's supposed to be replaced instead of run
			await expect(action.runActual()).rejects.toThrow(/^StubAction must implement BlueprintAction.runActual$/);
		});

		test('scheduleActual', async () => {
			// this should never be implemented, it's supposed to be replaced instead of run
			await expect(action.scheduleActual()).rejects.toThrow(/^StubAction must implement BlueprintAction.scheduleActual$/);
		});

		test.todo('$stringifyAction / actionParser');
	});

	/*
		UNITS
	*/

	test('stub.units', () => {
		expect(Object.keys(stub.units)).toEqual([
			'solveAt',
		]);
	});

	test('solveAt', () => {
		expect(stub.units.solveAt).toEqual(['immediate', 'create']);
	});
});