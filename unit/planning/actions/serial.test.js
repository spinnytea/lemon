const config = require('../../../src/config');
const { createSubgraph } = require('../../../src/database/subgraphs');
const serial = require('../../../src/planning/actions/serial');
const blueprint = require('../../../src/planning/primitives/blueprint');

const { SerialAction } = serial;
const { BlueprintAction } = blueprint;

class MockAction extends BlueprintAction {
	constructor({ name, ...rest }) { super(rest); this.name = name; }

	prettyNames() { return `MockAction -> ${this.name}`; }

	$calcRunCost() { return 10; }
}

describe('serial', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(serial.units).forEach((key) => {
			jest.spyOn(serial.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	describe('SerialAction', () => {
		describe('has steps', () => {
			let action;
			beforeEach(() => {
				const one = new MockAction({
					name: 'one',
					requirements: createSubgraph([['id', '_test']], []),
					transitions: [{ vertexId: 0, replace: 'one' }],
				});

				const two = new MockAction({
					name: 'two',
					transitions: [{ vertexId: 0, replace: 'two' }],
				});

				action = new SerialAction({ steps: [one, two] });
			});

			test('prototype', () => {
				// eslint-disable-next-line no-proto
				expect(Object.getOwnPropertyNames(action.__proto__)).toEqual([
					'constructor',
					'prettyNames',
					'$calcRunCost',
					'calcGlue',
					'runAbstract',
					'runActual',
					'scheduleActual',
					'$stringifyAction',
				]);
			});

			test('constructor', () => {
				expect(action).toMatchSnapshot();
			});

			test('prettyNames', () => {
				expect(action.prettyNames()).toMatchSnapshot();

				const action2 = new SerialAction({
					steps: [
						new MockAction({ name: 'a' }),
						new MockAction({ name: 'b' }),
						action,
						new MockAction({ name: 'c' }),
					],
				});
				expect(action2.prettyNames()).toMatchSnapshot();
			});

			test('$calcRunCost', async () => {
				expect(await action.$calcRunCost()).toBe(20);
			});

			test.todo('calcGlue');

			test.todo('runAbstract');

			test.todo('runActual');

			test.todo('scheduleActual');

			test('$stringifyAction / actionParser', async () => {
				jest.spyOn(action.steps[0], 'save').mockReturnValueOnce(Promise.resolve('mock save, idea one'));
				jest.spyOn(action.steps[1], 'save').mockReturnValueOnce(Promise.resolve('mock save, idea two'));
				jest.spyOn(blueprint, 'load').mockReturnValueOnce(Promise.resolve(action.steps[0]));
				jest.spyOn(blueprint, 'load').mockReturnValueOnce(Promise.resolve(action.steps[1]));

				const stringifyAction = await action.$stringifyAction();
				expect(stringifyAction).toMatchSnapshot();

				const actionParser = blueprint.units.actionParsers.get('SerialAction');
				const parsed = await actionParser(stringifyAction);

				expect(parsed).toEqual(action);

				expect(blueprint.load.mock.calls).toEqual([
					['mock save, idea one'],
					['mock save, idea two'],
				]);
			});
		});

		// empty gets it's whole describe block because ... REVIEW maybe we should just throw an error in the constructor
		describe('empty', () => {
			let action;
			beforeEach(() => {
				action = new SerialAction({ steps: [] });
			});

			test('prototype', () => {
				// eslint-disable-next-line no-proto
				expect(Object.getOwnPropertyNames(action.__proto__)).toEqual([
					'constructor',
					'prettyNames',
					'$calcRunCost',
					'calcGlue',
					'runAbstract',
					'runActual',
					'scheduleActual',
					'$stringifyAction',
				]);
			});

			test('constructor', () => {
				expect(action).toMatchSnapshot();
			});

			test('prettyNames', () => {
				expect(action.prettyNames()).toMatchSnapshot();
			});

			test('$calcRunCost', async () => {
				expect(await action.$calcRunCost()).toBe(100);
				expect(await action.$calcRunCost()).toBe(config.settings.serial_empty_distance);
			});

			test('calcGlue', async () => {
				expect(await action.calcGlue()).toEqual([]);
			});

			// even if this is a noop, it's still a waypoint
			// we still need to validate the input before we can let it pass through
			test('runAbstract', async () => {
				await expect(action.runAbstract()).rejects.toThrow(/^SerialAction has not been glued$/);

				action.$glue = []; // calcGlue returns empty array
				action.$gluedState = { mock: 'state' };

				await expect(action.runAbstract({ mock: 'state' })).rejects.toThrow(/^SerialAction was glued to a different state$/);
				await expect(action.runAbstract(action.$gluedState)).rejects.toThrow(/^SerialAction gluedAction.\$gluedFinalState is missing\?$/);

				action.$gluedFinalState = action.$gluedState;

				expect(await action.runAbstract(action.$gluedState)).toBe(action.$gluedState);
			});

			test('runActual', async () => {
				await expect(action.runActual()).rejects.toThrow(/^SerialAction has not been glued$/);

				action.$glue = []; // calcGlue returns empty array
				action.$gluedState = { mock: 'state' };
				action.$gluedFinalState = action.$gluedState;

				// there isn't a whole lot to check here
				// no actions are in the list, so there's nothing to confirm was called
				// and we don't have a way of saying "this wasn't called" - because there's nothing there
				const context = { mock: 'subgraph' };
				expect(await action.runActual(context)).toBe(undefined);
				expect(context).toEqual({ mock: 'subgraph' });
			});

			test.todo('scheduleActual');

			test('$stringifyAction / actionParser', async () => {
				const stringifyAction = await action.$stringifyAction();
				expect(stringifyAction).toMatchSnapshot();

				const actionParser = blueprint.units.actionParsers.get('SerialAction');
				const parsed = await actionParser(stringifyAction);

				expect(parsed).toEqual(action);
			});
		});
	});

	/*
		UNITS
	*/

	test('serial.units', () => {
		expect(Object.keys(serial.units)).toEqual([
		]);
	});
});