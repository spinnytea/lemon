const _ = require('lodash');
const ideas = require('../../../src/database/ideas');
const { Subgraph } = require('../../../src/database/subgraphs');
const actuator = require('../../../src/planning/actions/actuator');
const blueprint = require('../../../src/planning/primitives/blueprint');

const { ActuatorAction } = actuator;
const { BlueprintState } = blueprint;

describe('actuator', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		/*
		Object.keys(actuator.units).forEach((key) => {
			jest.spyOn(actuator.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		*/
		Object.keys(actuator.boundaries).forEach((key) => {
			if (key === 'debugLogs') return;
			jest.spyOn(actuator.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	describe('ActuatorAction', () => {
		let action;
		beforeEach(() => {
			action = new ActuatorAction({ rawCallback: 'mock callback', rawCallbackArgs: ['a', 'b'] });
		});

		test('prototype', () => {
			// eslint-disable-next-line no-proto
			expect(Object.getOwnPropertyNames(action.__proto__)).toEqual([
				'constructor',
				'prettyNames',
				'$calcRunCost',
				'calcGlue',
				'runAbstract',
				'runActual',
				'scheduleActual',
				'$stringifyAction',
			]);
		});

		test.todo('constructor');

		test('prettyNames', () => {
			expect(action.prettyNames()).toBe('ActuatorAction -> mock callback');
		});

		describe('$calcRunCost', () => {
			test('no transitions', async () => {
				action.transitions = [];

				// there shouldn't actually be any of these
				// but like, we don't want to pick actions that don't _do_ anything
				// neutral jing (waiting with purpose) is different than total inaction
				expect(await action.$calcRunCost()).toBe(Infinity);
			});

			test('no costs', async () => {
				action.transitions = [
					{ vertexId: 0 },
					{ edgeId: 0 },
				];

				expect(await action.$calcRunCost()).toBe(2);
			});

			test('with costs', async () => {
				action.transitions = [
					{ vertexId: 0, cost: 2 },
					{ edgeId: 0, cost: 4 },
				];

				expect(await action.$calcRunCost()).toBe(6);
			});
		});

		describe('calcGlue', () => {
			let state;
			beforeEach(() => {
				action.requirements.addEdge(
					action.requirements.addVertex('id', ideas.proxy('_test')),
					'has',
					action.requirements.addVertex('filler'),
				);

				state = new BlueprintState(new Subgraph(), []);
				state.state.addEdge(
					state.state.addVertex('id', ideas.proxy('_test')),
					'has',
					state.state.addVertex('id', ideas.proxy('_test2')),
				);
				state.state.concrete = true;
			});

			// this is important because it throws an error
			test.todo('state is not concrete');

			test('single glue', async () => {
				expect(await action.calcGlue(state)).toMatchSnapshot();

				expect(action).toMatchSnapshot('original action is unchanged');
			});

			test('multiple glues', async () => {
				state.state.addEdge(
					0,
					'has',
					state.state.addVertex('id', ideas.proxy('_test3')),
				);
				state.state.concrete = true;

				const gluedActions = await action.calcGlue(state);
				expect(_.map(gluedActions, '$glue')).toMatchSnapshot();

				expect(action).toMatchSnapshot('original action is unchanged');
			});
		});

		describe('run', () => {
			test.todo('runAbstract');

			test.todo('runActual');

			test.todo('scheduleActual');
		});

		test('$stringifyAction / actionParser', async () => {
			const stringifyAction = await action.$stringifyAction();
			expect(stringifyAction).toMatchSnapshot();

			const actionParser = blueprint.units.actionParsers.get('ActuatorAction');
			const parsed = await actionParser(stringifyAction);

			expect(parsed).toEqual(action);
		});
	});
});