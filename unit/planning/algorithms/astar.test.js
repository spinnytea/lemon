/* eslint-disable max-classes-per-file */
const _ = require('lodash');
const config = require('../../../src/config');
const astar = require('../../../src/planning/algorithms/astar');
const { Path, PathAction, PathState } = require('../../../src/planning/primitives/path');

class MockAction extends PathAction {
	prettyNames() { return 'MockAction'; }

	cost() { return 1; }
}

class MockState extends PathState {
	constructor(mockDist) {
		super();
		this.mockDist = mockDist;
	}

	distance() { return this.mockDist; }
}

describe('astar', () => {
	beforeEach(() => {
		// it's best these we force them all to be mocked so we can properly test them in isolation
		// (we can restore them on a per-test basis as needed)
		Object.keys(astar.units).forEach((key) => {
			if (key === 'distFromGoalWeight') return;
			jest.spyOn(astar.units, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
		Object.keys(astar.boundaries).forEach((key) => {
			if (key === 'debugLogs') return;
			if (key === 'printActions') return;
			if (key === 'finalReport') return;
			jest.spyOn(astar.boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
		});
	});

	test('astar.units', () => {
		expect(Object.keys(astar.units)).toEqual([
			'doSearch',
			'calcWeight',
			'frontier',
			'visited',
			'step',
		]);
	});

	describe('doSearch', () => {
		let start;
		let goal;
		let hasVisited;
		beforeEach(() => {
			astar.units.doSearch.mockRestore();
			start = new MockState(1);
			goal = new MockState(0);

			astar.units.calcWeight.mockRestore();
			astar.units.frontier.mockRestore();
			hasVisited = jest.fn();
			astar.units.visited.mockReturnValueOnce(hasVisited);

			astar.boundaries.debugLogs = true;
			astar.boundaries.log.mockImplementation(() => {});
		});

		test('invalid args', async () => {
			await expect(() => astar.units.doSearch()).rejects.toThrow(/^start must be a type of PathState$/);
			await expect(() => astar.units.doSearch({})).rejects.toThrow(/^start must be a type of PathState$/);
			await expect(() => astar.units.doSearch(start)).rejects.toThrow(/^all goals must be a type of PathState$/);
			await expect(() => astar.units.doSearch(start, [])).rejects.toThrow(/^must have at least one goal$/);
			await expect(() => astar.units.doSearch(start, [{}])).rejects.toThrow(/^all goals must be a type of PathState$/);
			expect(astar.boundaries.log).not.toHaveBeenCalled();
		});

		test('no change', async () => {
			start.mockDist = 0;

			expect(await astar.units.doSearch(start, goal)).toMatchSnapshot();

			expect(astar.boundaries.log.mock.calls).toMatchSnapshot('logs');
			expect(astar.units.step).not.toHaveBeenCalled();
		});

		test('single', async () => {
			astar.units.step.mockImplementationOnce(async (ignore, frontier) => {
				const p = new Path([start, goal], [new MockAction()], [goal]);
				await p.getReady();
				frontier.enq(p);
				frontier.enq(p);
				frontier.enq(p);
			});

			expect(await astar.units.doSearch(start, goal)).toMatchSnapshot();

			expect(astar.boundaries.log.mock.calls).toMatchSnapshot('logs');
			expect(astar.units.step.mock.calls.length).toBe(1);
		});

		test('multiple', async () => {
			start.mockDist = 4;
			const a = new MockState(3);
			const b = new MockState(2);
			const c = new MockState(1);

			astar.units.step.mockImplementationOnce(async (ignore, frontier) => {
				const p = new Path([start, a], [new MockAction()], [goal]);
				await p.getReady();
				frontier.enq(p);
				frontier.enq(p);
				frontier.enq(p);
			});
			astar.units.step.mockImplementationOnce(async (ignore, frontier) => {
				const p = new Path([start, a, b], [new MockAction(), new MockAction()], [goal]);
				await p.getReady();
				frontier.enq(p);
				frontier.enq(p);
			});
			astar.units.step.mockImplementationOnce(async (ignore, frontier) => {
				const p = new Path([start, a, b, c], [new MockAction(), new MockAction(), new MockAction()], [goal]);
				await p.getReady();
				frontier.enq(p);
			});
			astar.units.step.mockImplementationOnce(async (ignore, frontier) => {
				const p = new Path([start, a, b, c, goal], [new MockAction(), new MockAction(), new MockAction(), new MockAction()], [goal]);
				await p.getReady();
				frontier.enq(p);
			});

			expect(await astar.units.doSearch(start, goal)).toMatchSnapshot();

			expect(astar.boundaries.log.mock.calls).toMatchSnapshot('logs');
			expect(astar.units.step.mock.calls.length).toBe(4);
		});

		describe('no solution', () => {
			let ORIGINAL_ASTAR_MAX_PATHS;
			beforeEach(() => {
				ORIGINAL_ASTAR_MAX_PATHS = config.settings.astar_max_paths;
			});
			afterEach(() => {
				config.settings.astar_max_paths = ORIGINAL_ASTAR_MAX_PATHS;
			});

			test('too many branches', async () => {
				config.settings.astar_max_paths = -1;

				expect(await astar.units.doSearch(start, goal)).toBe(undefined);

				expect(astar.boundaries.log.mock.calls).toMatchSnapshot('logs');
				expect(astar.units.step).not.toHaveBeenCalled();
			});

			test('exhausted options', async () => {
				astar.units.step.mockImplementationOnce(() => {});

				expect(await astar.units.doSearch(start, goal)).toBe(undefined);

				expect(astar.boundaries.log.mock.calls).toMatchSnapshot('logs');
				expect(astar.units.step.mock.calls.length).toBe(1);
			});
		});
	});

	test('calcWeight', () => {
		astar.units.calcWeight.mockRestore();
		const paths = [
			{ cost: 0, distFromGoal: 0 },
			{ cost: 1, distFromGoal: 0 },
			{ cost: 2, distFromGoal: 0 },
			{ cost: 3, distFromGoal: 0 },
			{ cost: 0, distFromGoal: 1 },
			{ cost: 0, distFromGoal: 2 },
			{ cost: 0, distFromGoal: 3 },
			{ cost: 1, distFromGoal: 1 },
			{ cost: 2, distFromGoal: 2 },
			{ cost: 3, distFromGoal: 3 },
		];
		paths.forEach((path) => { path.weight = astar.units.calcWeight(path); });
		expect(paths).toMatchSnapshot();
	});

	describe('frontier', () => {
		beforeEach(() => {
			astar.units.frontier.mockRestore();
			astar.units.calcWeight.mockRestore();
		});

		function enqueDeque(list) {
			const frontier = astar.units.frontier();
			list.forEach((p) => {
				p.$weight = astar.units.calcWeight(p);
				frontier.enq(p);
			});
			list.splice(0);
			while (!frontier.isEmpty()) {
				list.push(frontier.deq());
			}
		}

		test('path not ready', () => {
			const frontier = astar.units.frontier();
			expect(() => frontier.enq({ isReady: () => false })).not.toThrow();
			expect(() => frontier.enq({ isReady: () => false })).toThrow(/^path is not ready$/);
		});

		test('general ordering', () => {
			// XXX shuffling the data is BAD, it breaks all the testing rules
			//  - but that's how confident I am
			//  - IFF this does actually break even one time, then we can write deterministic tests
			//  - (and track down that freak accident now that we know one can exist)
			// ----
			//  - the one rule is that the cost/distFromGoal must be unique
			const list = _.shuffle([
				{ mock: 'a', cost: 1, distFromGoal: 0, isReady: () => true },
				{ mock: 'b', cost: 2, distFromGoal: 0, isReady: () => true },
				{ mock: 'c', cost: 0, distFromGoal: 0, isReady: () => true },
				{ mock: 'd', cost: 0, distFromGoal: 2, isReady: () => true },
				{ mock: 'e', cost: 0, distFromGoal: 1, isReady: () => true },
			]);

			enqueDeque(list);

			expect(list).toMatchSnapshot();
		});

		test('same weight', () => {
			// if these weights change, then we need to redo the maths
			expect(config.settings.astar_weight_cost).toBe(1);
			expect(config.settings.astar_weight_distFromGoal).toBe(10);

			const list = _.shuffle([
				{ mock: 'a', cost: 1, distFromGoal: 3, isReady: () => true },
				{ mock: 'b', cost: 11, distFromGoal: 2, isReady: () => true },
				{ mock: 'c', cost: 21, distFromGoal: 1, isReady: () => true },
				{ mock: 'd', cost: 31, distFromGoal: 0, isReady: () => true },
			]);

			enqueDeque(list);

			expect(list).toMatchSnapshot();
		});

		test('cost/distFromGoal are the same', () => {
			const list = [
				{ mock: 'a', cost: 1, distFromGoal: 1, isReady: () => true },
				{ mock: 'b', cost: 1, distFromGoal: 1, isReady: () => true },
				{ mock: 'c', cost: 1, distFromGoal: 1, isReady: () => true },
			];

			enqueDeque(list);

			expect(list.map((p) => p.mock)).toMatchSnapshot();
		});
	});

	describe('visited', () => {
		beforeEach(() => {
			astar.units.visited.mockRestore();
		});

		function mockPath(value, weight) { // @mock Path
			astar.units.calcWeight.mockReturnValueOnce(weight);
			return {
				isReady: () => true,
				last: {
					value,
					equals: (s) => s.value === value,
				},
			};
		}

		test('example', () => {
			const hasVisited = astar.units.visited();

			// the first item is not in the list
			expect(hasVisited(mockPath('a', 3))).toBe(false);

			// an item HAS been visted if it is equal or greater
			expect(hasVisited(mockPath('a', 4))).toBe(true);
			expect(hasVisited(mockPath('a', 3))).toBe(true);

			// an item has NOT been visted if it is less
			expect(hasVisited(mockPath('a', 2))).toBe(false);

			// a different item is not in the list no matter the weight
			expect(hasVisited(mockPath('b', 4))).toBe(false);
			expect(hasVisited(mockPath('c', 3))).toBe(false);
			expect(hasVisited(mockPath('d', 2))).toBe(false);
		});

		test('path not ready', () => {
			const hasVisited = astar.units.visited();

			expect(() => hasVisited({ isReady: () => false })).toThrow(/^Path should be ready before you check visited\/enq$/);
		});
	});

	test.todo('step');
});