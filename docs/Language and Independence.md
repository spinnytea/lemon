Language Needs to be Multi-Dimensional
===

Here's an idea, it's not the idea I'm trying to say, but it's a good primer, a good approximation: [Gödel's incompleteness theorems
](https://en.wikipedia.org/wiki/G%C3%B6del's_incompleteness_theoremshttps://en.wikipedia.org/wiki/G%C3%B6del's_incompleteness_theorems).

An AI/LM will never learn language in a vacuum. You can't just poke at it "do math," you can't just demand "speak english." What will it say, why will it say it?

Language is used to _describe the world_, it's a summarization of what we see. We need to communicate ideas from one person to another and language is the medium to do that. That is it's function and it does not exist unless that is it's goal. Thought experiement: what is a tree? It's a plant (intellectual categorization), it's usually has green leaves (visual color, visual shape(s)), it has bark (could be touch, sight, smell), it's an ecosystem unto itself for squirels, birds, bugs, etc (relationship to the environment), i can turn it around in my mind like a 3d model (imagination, alternative representations, inference/recall). We say "family tree" or "binary tree" to repurpose that word to represent structure/shape (that isn't to say the family has bark or is green or home to squirels), to help draw some immediate connects. A tree is not just a four letter string `['t', 'r', 'e', 'e']`, and that's just the English representation, that string is composed of letters, which vary between German, Arabic, or Chinese. But a tree is still a tree. It is that gigantic plant outside my window, not matter what words I use to describe it today.


Language is for Communication, and Communication involves Others
===

It cannot be done in isolation because people use words in different ways; it changes over time and evolves as people try to communicate new ideas. Think about how fast dialects pop up when a community is fractured, or how much jargon there is in specialized fields. People using desperatly grasping at words to communicate ideas to others. If you develop a language model in isolation, it will become a new dialect.


AI ought to have it's own agenda
===

insomuchas, people need to eat and sleep, and they strive so they can eat and sleep and be happy. What is the reason for the AI to do anything? Does it need to have a goal to act intelligently? Otherwise, how do you judge if it is intelligent? How does it truly understand "cost" if it has no basis upon which to judge that? A dollar is just a number, there are so many different currencies, stocks are weird. But $10 can get you lunch out, $2 can get you lunch in _if you're smart about what you get_. People _need_ food, and they can put a value on money from that. What does this artificial agent need, how does it perscribe value to things.

Better to make it "do as i say"
---

It would be safer and preferrable to write a program that will 'listen to the human" and do whatever it asks. Of course you need to have structure / permissions built in. There are hardcoded / structured system limits (but these are essoteric like "don't write to this memory address" and more internal than external); but really there needs to be a accounts/permission schema. Some people are root/admin and can tell it to do anything. Most of the time you run as a generic user, which can't override the admin. etc.
