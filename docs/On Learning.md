Learning
========

These are the axioms that the code is built upon.

One of the goals is to create a learning machine; one of the goals is to make a planner that can handle a complex world.
But what does that mean? And how does that work.

<pre>
              +--------> [Action] -----------+
              |                              |
              |                              v
        [Knowledge]      [Context]      [Experience]
          //  ^                              |
[    Logic ]  |                              |
[Simulation]  +--------- [Patterns ] <-------+
                         [Intuition]
</pre>

<!-- TODO finish explanation of each part (see: notes/The Five Elements) -->
