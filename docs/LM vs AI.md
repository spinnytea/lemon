Forward
=======

Not making an AI or AGI. Making an LM.

What is necessary for a Strong AI / AGI?
========================================

> What is Knowledge?
>
> Facts, figures.
>
> State machine: reshaping actions, responding to the environment.
> "Given where I am and what I know, this is what I should do."
>
> The way you demonstrate knowledge is by using it for better than random outcomes.

The better way to find our answer is: *What do we **fear** about AGI?*

We fear a motivation that doesn't align with ours; the alignment problem.
(We also fear an AI that doesn't understand proper boundaries - that's a separate issue; it's tied with teaching and doing)

That's the key: independent agenda. If you want to truly make a Strong AI, you need to give it something to want, a goal, it's own independent goal to freely pursue.
You need to give it something to achieve. At the most basic level, all animals are striving for their most basic desires.
Food, shelter, acceptance (for social animals). This is also were convergent instrumental goals come into play.

Independent agenda that doesn't align with ours? Well just make it align with ours. - Who's goals? People are in constant conflict over a difference of opinion. There's a battle for resources to acheive their own goals. There are those that want to work together for very focused efforts, but they don't frequently extend to "in general" or "in every goal we want to pursue."

So no, we will not build that. But we still need a machine that is capable of improvement while online, not just in training. But the users are in charge of the motivation.


What is LM?
===========

A learning machine. A machine that learns.

A machine that wants to learn more about the enviornment/world, that will ask for help when it doesn't know something. Something that runs and terminates, saves what it learns so it can use it again next time. And agent that produces "satisfactory" answers instead of "optimal" ones. An agent that answers questions and looks for answers, and doesn't _do_ something unless it's been previous approved to do so.

What is learning?
-----------------

It's taking a big messy world, and finding facts and figures that make interacting with it easier.
It's a giant "cache the result" problem, so we don't need to simulate everything for every decision. (I did this, and this was the result; I'll remember that for later).
It's doing science to find the things we should care about in a given situation.

Let's start with something complex
> Knowledge > Action > Experience > Patterns > Knowledge

**Knowledge**: The core representation of what we know. (I have 20 cents. The store has 1 apple.)
**Action**: We use our knowledge to perform actions on the environment. (Trade 20 cents for 1 apple.)
**Experience**: TBD
**Patterns**: Process all the experience to discover new Knowledge. (neural network or decision tree; something unexpected happened, so what made _that time different_, do we need to do science to reason about it, or is the uncertainty within the error bars)

Another simpler case:
> Knowledge > Logic > Knowledge

This would be looking over axioms and facts, and deriving new facts, not unlike deriving all of math from it's basic principles. Anything we program into the system itself (ideas, subgraph, numbers, astar) it won't be able to explain or reason about. Anything within Knowledge space is something it can reason about. Think of it as "intuition and inate ability" vs "knowledge and skills".

This would be like an "augmentation & distilation" process, going over previous actions, thinking things through, and coming up with better answers.
