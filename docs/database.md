Database
========

This represents Knowledge in the learning process.

The lowest level (ideas) is an infinite graph.

Subgraph represents a subset of the infinite graph, and is used in a few ways.

- Find sections of the infinite graph.
- Find sections of a subgraph.
- Explore possibility spaces.


The infinite graph (ideas) represents _What **Is**_. It represents facts that the machine knows about the world. The current state of things.
This can be directly manipulated by actuators when carrying out plans and read by sensors.

A subgraph is used to identify a section of the idea graph that we care about at the moment. This is often called a context, and maps directly to the idea graph (it is concrete). This is sort of like "working memory", things that we are holding in our head so we can narrow the search space and actually function.

A subgraph can also map to another subgraph (goals, action requirements; typically not concrete), allowing for more abstract representation. These are used for planning, imagination, or any other kind of possibility space exploration.

<pre>
                +---------------+  > Subgraph
               /               /
          +---/               /-+  > Subgraph (context, concrete)
         /   +---------------+ /
    +---/                     /-+  > Ideas
   /   +---------------------+ /
  /                           /
 /                           /
+---------------------------+
</pre>
