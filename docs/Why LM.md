<!-- TODO this file is a bit awkward, try turning it in an essay or whitepaper or something -->

Shorthand / analogy: The goal is to more or less build Jarvis.
1. You say "this is what I want".
2. LM helps you find out how to get there.
3. LM presents a solution for you to vett.
4. "Ok, build it."

What is special about these steps
1. _You_ define a _short term_ goal.
    - LM will come up with instrumental goals and steps, but the user is dictating the goals and can change them on a whim.
	 - You can define longer sweeping goals like "cure cancer" but it's likely to come up short and say "I don't know.". These need to be acheivable goals. You probably already have an idea of how to do it. You are just directing and expanding the LM as you go, making more and more things automatable.
2. This is all mental arithmetic. It's just planning would needs to be done, how it might do it.
3. The solution is not automatically carried it. It presents the plan and tries to help you understand what's involved.
4. _You_ confirm _this_ action.
    - Again, it's not LM's goal. it's _your_ goal. If LM is a one shot program, it's something you give it before it runs. If LM is a long running program, then it's sort of an idle loop waiting for something new.

This is not going to be the final form. This is a stepping stone. It's an automation tool that runs in "stable" environments where "stop" is always acceptible (i.e. it won't be driving any cars, but it can make plans that require you to drive. If we do have self driving cars, then it may say "this is the step where the car gets me there" but won't be directly in control of that stage, just planning/replanning as necessary.)

<!-- TODO this "paragraph" is a mess - i was brain dumping too early in the morning -->
LM is an attempt to "model the mind". Not so much "make LM do exactly what a human does" nor "make an exact replica". More like, "well how do people deal with this". LM is no an independent agent that will carry out it's goals, it's a computer program (starts, runs, terminates) that helps find solutions to goals. It doesn't carry them out in the real world (actually run them), until every given plan has been previously approved, and any time it starts a new plan it asks if it's acceptible. It needs to imagine and reason about any given plan all the way through completion to see if it's viable.

A lot of problems arrise when an AGI has long term, unwavering goals; when it tries to min-max and perfectly optimize. Why do this at all? This LM will have goals that it tries to acheive, but they are short lived and change constantly (today we get to the store to by apples, tomorrow we hunt the wumpus, the next day we play mario). This LM won't try to find the "absolute best" way to do anything, just "this gets me approximately the right number of apples (not exactly 3, but between 3 and 5)" Once it's done so and the plan has been "proven", it doesn't need to plan again, until the initial states / goals have change significantly, until the old plan can no longer be used.

In a lot of ways LM is re-implementing an OS. There's memory, every plan is a function, you start off by saying "here i am, this is were we need to go" and it's turtles all the way down (every action is a function that starts at some prequisite state, and runs to produce some deltas). Why I'm not straight up re-tooling an os is because there needs to be enough metadata about those rereqs and deltas. Eventually I'll try to get LM to help me "compile to hardware" or something, but for now it's a re-impl.
