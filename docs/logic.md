Logic
=====

I haven't gotten to experience yet, so I don't have a clear picture of how probability will be integrated. But logic is based on them so it's worth having a rough picture of something that will use them.

Prereq/Background
---

Sensors detect information about the world (think, microphone to pick up audio, object recogniction to detect an apple, terminal i/o to speak with a person). Experience collects the information about "here's what I did and what i expected, here's what acctually happened and how they differ. From that we can use Patterns to distill that infromation and update Knowledge. Somewhere in there is "this is probably what happened and why." You "can't be 100% certain that it was an apple, but it smelled, sounded, tasted, and looked like one, and the person accepted it when you gave it."

Probabilities
---

None of our learned probabilities can ever be >= 100% or <= 0%. They can be epsilon, or 1-epsilon, but never equal or beyond. It's bounded to `(0, 1)`.

However, there IS a class of probabilities that is always exactly 0 or 1 that can be reasoned about. For example, Math is built on top of a few axioms that we "assume is fact" and then we can derive everything else. E.g. 1 + 1 = 2, 2 - 1 = 1. From there we can derive the whole number line from -∞ to +∞, we can derive multiplication and division, and then all of algebra, and so forth. These are Facts. 1 + 1 = 2 is 100% because we defined it and 1 + 2 = 3 is derived from that and also true. 1 + 3 != 5, that is 0% correct.

This is also why our learned probabilities can never be 1 or 0. Those are absolutes, and once you hit an absolute, you can build an entire field from that.

We can, however, define probabilities that are fixed. For example, we can, losely speaking, make a rule in the system that says "humans are important, this is 99% true and you cannot change that".
