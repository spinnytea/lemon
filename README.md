
# To Do List

```
\b(TODO|FIXME)\b
\b(XXX|TODO|FIXME|REVIEW|IDEA|NOTE)\b
```

- TODO test via in_memory = false
- TODO index.js? build?
- TODO how do we make this available as a dep? `npm link`
- TODO it's time to review lime/notes
- TODO review `await` in all non-test files
  - this was ported from sync to async "in haste"
  - some promise loops have been spread to Promise.all, but in general the control is very linear
  - some of that can't change, it is more or less linear in nature, but maybe there are improvements to be made
- IDEA StubActions are our "learned actions", the knowledge we distill
  - they can start of super simple (like the applie pie go actuator), just "move from one place to another costs this much"
  - you can get more precise "moving by car" has a different cost, "move by bike" as another
  - in general, you can say "moving is probably this cost" or you can get more specific with different requirements (like a plane is a fixed high cost no matter the distance, a bike is a scaled cost based on distance)
- TODO rules about the world state that cannot be violated, contect boundaries
  - e.g. anything that is a typeOf money cannot be negative (you _cannot_ spend too much money, that is a dead end)
- TODO make the maze traversal _again_, and see if I can steal the algorithm (instead of astar)
- REVIEW "List All TODOs" doesn't see tasks in /** jsdocs */


# Project Structure

* `/src` source code
	* `top-level function` - higher-order functions that tie units together. should have as few branches as possible - if you have to write more than 2 or 3 tests for it to be fully tested, then it's too complex. ideally you need exactly 1 test
		* top-level functions contains all the internal dependencies (simple functions that run without branches)
		* top level functions: these are on the exports object directly, they are the public methods, and the only ones that should be used outside of the file
	* `units function` - this is where all the logic goes. the value is the boundary. it accepts a single type of input, and pruduces a single type of output, and can do whatever it needs to do to get between them. It's okay to break up code into multiple units, but they should not have any external dependencies (these function should be fully self contained), other those in `boundaries`
		* units contain all the decisions
		* these are just to break out the code into - ya' know - functions; placed on an object to test individually; think of them as private methods
		* they ought to be one-liners that are just aliases for external functions
	* `boundary functions` - these are very simple pass-through functions that either manipulate state or call out to other libs. similar to top level functions, but the nitty gritty low levels. if you can't inspect it and know that it's correct; if you can't call it exactly once or O(1), to know if it works or not, then it should be broken down into one or more units and boundaries.
* `/test` integration tests
	* do not mock anything unless absolutely necessary
	* test the top level functions
	* used to test the boundaries themselves - as in, by running the tests, you are utilizing the boundaries
	* examples of how to use the functions
* `/unit` unit tests
	* fully test every unit
	* no need to test any of the boundaries (they should be simple / passthroughs)
	* always mock units and boundaries
	* mock out literally anything that makes sense to do so; if you need to mock it and it's not a unit or boundary, it should be
	* if relevant, demonstrate some unsupported cases, just to see what happens


# Source File Structure

Throw errors when it's a developer's mistake, do not throw errors if it's something that happens at runtime.
If a function's arguments are wrong or a function is misused, then throw an error when something is wrong.
If the data is bad, if the runtime logic hits a dead end, then return empty/bad values.

```javascript
const example = exports;

example.fn = () => {};


Object.defineProperty(example, 'units', { value: {} });

example.units.unitFn = function unitFn() {};
```
