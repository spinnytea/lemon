const _ = require('lodash');
const boundaries = require('../src/boundaries');
const config = require('../src/config');

describe('config', () => {
	beforeEach(() => {
		boundaries.setTimeout.mockRestore();
		boundaries.clearTimeout.mockRestore();
		boundaries.existsSync.mockRestore();
		boundaries.readFileSync.mockRestore();
		boundaries.writeFile.mockRestore();

		config.init({ in_memory: true, location: 'test_data' });
	});

	afterEach(() => {
		config.init({ in_memory: true });
	});

	test('config.integration', () => {
		expect.assertions(1);
		expect(Object.keys(config)).toEqual([
			'get',
			'set',
			'settings',
			'onInit',
			'init',
			'save',
		]);
	});

	const key = 'someKey';
	const value1 = 'some value 1';
	const value2 = 'some value 2';

	describe('get', () => {
		test('empty', async () => {
			expect.assertions(3);
			expect(config.units.data).not.toHaveProperty(key);
			await expect(config.get(key, value1)).resolves.toEqual(value1);
			expect(config.units.data).not.toHaveProperty(key);
		});

		test('has a value', async () => {
			expect.assertions(2);
			config.units.data[key] = value1;
			await expect(config.get(key, value2)).resolves.toEqual(value1);
			expect(config.units.data[key]).toEqual(value1);
		});
	});

	test('set', async () => {
		expect.assertions(5);
		expect(config.units.data).not.toHaveProperty(key);
		await expect(config.set(key, value1)).resolves.toEqual(value1);
		expect(config.units.data[key]).toEqual(value1);
		await expect(config.set(key, value2)).resolves.toEqual(value2);
		expect(config.units.data[key]).toEqual(value2);
	});

	test('settings', () => {
		expect.assertions(5);
		expect(config.settings).toBeInstanceOf(Object);
		expect(Object.keys(config.settings).sort()).toEqual([
			'actuator_base_distance',
			'astar_max_paths',
			'astar_weight_cost',
			'astar_weight_distFromGoal',
			'blueprint_default_distance',
			'filename',
			'in_memory',
			'location',
			'memory_fallback_to_location',
			'serial_empty_distance',
			'stub_base_distance',
		]);
		expect(typeof config.settings.filename).toBe('string');
		expect(typeof config.settings.in_memory).toBe('boolean');
		expect(typeof config.settings.location).toBe('string');
	});

	describe('onInit', () => {
		test('not hasInit', async () => {
			expect.assertions(3);
			config.units.hasInit = false; // never actually do this!

			let called = false;
			config.onInit(() => { called = true; });
			expect(called).toBe(false);

			await config.init({ in_memory: true, location: 'test_data' }); // don't change the test settings

			expect(called).toBe(true);
			expect(config.units.hasInit).toBe(true); // make sure it's back!
		});

		test('hasInit', () => {
			expect.assertions(2);
			// this has to be true for us to be able to run the unit tests
			expect(config.units.hasInit).toBe(true);

			let called = false;
			config.onInit(() => { called = true; });
			expect(called).toBe(true);
		});
	});

	describe('init', () => {
		test('in_memory', () => {
			expect.assertions(2);

			// init isn't actually all that complicated
			// and it has to be run before we can start the tests
			// and lots of things rely on it
			// so
			// not much to test
			// #integration test done right
			expect(config.units.hasInit).toBe(true);

			expect(config.settings).toEqual({
				in_memory: true,
				location: 'test_data',
				filename: '_settings_testing',
				memory_fallback_to_location: false,
				astar_max_paths: 100,
				astar_weight_cost: 1,
				astar_weight_distFromGoal: 10,
				blueprint_default_distance: 1,
				actuator_base_distance: 1,
				stub_base_distance: 2,
				serial_empty_distance: 100,
			});
		});

		test('without location', () => {
			expect.assertions(4);

			config.init({ in_memory: true });

			const keys = Object.keys(config.settings).sort();
			expect(keys).toEqual([
				'actuator_base_distance',
				'astar_max_paths',
				'astar_weight_cost',
				'astar_weight_distFromGoal',
				'blueprint_default_distance',
				'filename',
				'in_memory',
				'location',
				'memory_fallback_to_location',
				'serial_empty_distance',
				'stub_base_distance',
			]);
			_.pull(keys, 'location');
			expect(() => config.settings.location).toThrow('must overwrite "location" (the location of the idea database)');
			expect(() => _.pick(config.settings, 'location')).toThrow('must overwrite "location" (the location of the idea database)');

			expect(_.pick(config.settings, keys)).toEqual({
				in_memory: true,
				filename: '_settings_testing',
				memory_fallback_to_location: false,
				astar_max_paths: 100,
				astar_weight_cost: 1,
				astar_weight_distFromGoal: 10,
				blueprint_default_distance: 1,
				actuator_base_distance: 1,
				stub_base_distance: 2,
				serial_empty_distance: 100,
			});
		});

		test('files', () => {
			expect.assertions(2);

			config.init({ in_memory: false, location: 'test_data' });

			expect(config.settings).toEqual({
				in_memory: false,
				location: 'test_data',
				filename: '_settings_testing',
				memory_fallback_to_location: false,
				astar_max_paths: 100,
				astar_weight_cost: 1,
				astar_weight_distFromGoal: 10,
				blueprint_default_distance: 1,
				actuator_base_distance: 1,
				stub_base_distance: 2,
				serial_empty_distance: 100,
			});
			expect(config.units.data).toEqual({ peanutbutter: 'jelly' });
		});

		test('invalid memory/location', () => {
			expect.assertions(1);

			expect(() => config.init({ in_memory: false })).toThrow('cannot init without location or in_memory');
		});
	});

	test('save', async () => {
		expect.assertions(2);

		config.init({ in_memory: false, location: 'test_data' });
		expect(config.units.data).toEqual({ peanutbutter: 'jelly' });

		// notable in that it does not throw an error
		// notable in that the resultant file in git does not change
		await config.save();

		expect(config.units.data).toEqual({ peanutbutter: 'jelly' });
	});
});