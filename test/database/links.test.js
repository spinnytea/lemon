const _ = require('lodash');
const links = require('../../src/database/links');

describe('links', () => {
	test('links.integration', () => {
		expect.assertions(1);
		expect(Object.keys(links)).toEqual([
			'get',
			'cast',
			'isOpp',
		]);
	});

	describe('get', () => {
		test('valid link', () => {
			expect.assertions(5);
			const linkHas = links.get('has');
			expect(_.omit(linkHas, 'opposite')).toEqual({ name: 'has', transitive: true });
			expect(_.omit(linkHas.opposite, 'opposite')).toEqual({ name: 'has-opp', transitive: true });

			// the opposite of the opposite is myself
			expect(linkHas.opposite.opposite).toBe(linkHas);

			// sameAs points to itself
			expect(links.get('sameAs').opposite).toBe(links.get('sameAs'));
			expect(links.get('sameAs').opposite.opposite).toBe(links.get('sameAs'));
		});

		test('invalid link', () => {
			expect.assertions(1);
			// for now, there isn't any reason to do anything special or cute
			// we can change always this in the future
			const linkInvalid = links.get('thisLinkIsInvalidLink');
			expect(linkInvalid).toBe(undefined);
		});
	});

	test('cast', () => {
		expect.assertions(1);
		const linkHas = links.cast('has');
		const linkHasOpp = links.cast('has', true);
		expect(linkHas.opposite).toBe(linkHasOpp);
	});

	describe('isOpp', () => {
		test('valid link', () => {
			expect.assertions(4);

			expect(links.isOpp(links.get('has'))).toBe(false);
			expect(links.isOpp(links.get('has').opposite)).toBe(true);
			expect(links.isOpp(links.get('has-opp'))).toBe(true);
			expect(links.isOpp(links.get('has').opposite.opposite)).toBe(false);
		});

		test('invalid link', () => {
			expect.assertions(2);

			expect(links.isOpp('has')).toBe(undefined);
			expect(links.isOpp(links.get('thisLinkIsInvalidLink'))).toBe(undefined);
		});
	});
});