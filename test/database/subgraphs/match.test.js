const ideas = require('../../../src/database/ideas');
const { match, Subgraph, search } = require('../../../src/database/subgraphs');

describe('match', () => {
	test('match.integration', () => {
		expect.assertions(1);
		expect(Object.keys(match)).toEqual([
			// this just exports the function
		]);
	});

	describe('fruit demo', () => {
		test('single', async () => {
			expect.assertions(12);

			const verts = {
				fruit: { value: 'fruit' },
				apple: { value: 'apple' },
				banana: { value: 'banana' },
				red: { value: 'red' },
				yellow: { value: 'yellow' },
			};
			const edges = [
				['apple', 'typeOf', 'fruit'],
				['banana', 'typeOf', 'fruit'],
				['apple', 'property', 'red'],
				['banana', 'property', 'yellow'],
			];

			await ideas.createGraph(verts, edges);

			const sg = new Subgraph();
			sg.addVertex('id', verts.fruit, { name: 'fruit' });
			sg.addVertex('id', verts.apple, { name: 'apple' });
			sg.addVertex('id', verts.red, { name: 'red' });
			sg.addEdge('apple', 'typeOf', 'fruit');
			sg.addEdge('apple', 'property', 'red');

			const results = await search(sg);
			expect(results.length).toBe(1);
			const outer = results[0];
			expect(outer).toMatchSnapshot('outer');

			const inner = new Subgraph();
			inner.addVertex('filler', undefined, { name: 'fruit' });
			inner.addVertex('filler', undefined, { name: 'unknown' });
			inner.addVertex('exact', { value: 'red' }, { name: 'red' });
			inner.addEdge('unknown', 'typeOf', 'fruit');
			inner.addEdge('unknown', 'property', 'red');

			expect(inner.getIdea('fruit')).toEqual(undefined); // we don't need any pinned ideas
			expect(inner.getIdea('unknown')).toEqual(undefined); // this is what we are looking for
			expect(inner.getIdea('red')).toEqual(undefined); // we have match data, but not a specific idea
			expect(inner).toMatchSnapshot('inner before');

			const matches = await match(outer, inner);
			expect(matches.length).toBe(1);
			expect(matches[0]).toMatchSnapshot();

			// original inner hasn't changed
			expect(inner.getIdea('fruit')).toEqual(undefined);
			expect(inner.getIdea('unknown')).toEqual(undefined);
			expect(inner.getIdea('red')).toEqual(undefined);
			expect(inner).toMatchSnapshot('inner after');
		});

		test('multiple', async () => {
			expect.assertions(10);

			const verts = {
				fruit: { value: 'fruit' },
				apple: { value: 'apple' },
				banana: { value: 'banana' },
				red: { value: 'red' },
				yellow: { value: 'yellow' },
			};
			const edges = [
				['apple', 'typeOf', 'fruit'],
				['banana', 'typeOf', 'fruit'],
				['apple', 'property', 'red'],
				['banana', 'property', 'yellow'],
			];

			await ideas.createGraph(verts, edges);

			const sg = new Subgraph();
			sg.addVertex('id', verts.fruit, { name: 'fruit' });
			sg.addVertex('id', verts.apple, { name: 'apple' });
			sg.addVertex('id', verts.red, { name: 'red' });
			sg.addVertex('id', verts.banana, { name: 'banana' });
			sg.addVertex('id', verts.yellow, { name: 'yellow' });
			sg.addEdge('apple', 'typeOf', 'fruit');
			sg.addEdge('apple', 'property', 'red');
			sg.addEdge('banana', 'typeOf', 'fruit');
			sg.addEdge('banana', 'property', 'yellow');

			const results = await search(sg);
			expect(results.length).toBe(1);
			const outer = results[0];
			expect(outer).toMatchSnapshot('outer');

			const inner = new Subgraph();
			inner.addVertex('id', verts.fruit, { name: 'fruit' });
			inner.addVertex('filler', undefined, { name: 'unknown-fruit' });
			inner.addVertex('filler', undefined, { name: 'unknown-color' });
			inner.addEdge('unknown-fruit', 'typeOf', 'fruit');
			inner.addEdge('unknown-fruit', 'property', 'unknown-color');
			expect(inner).toMatchSnapshot('inner before');

			const matches = await match(outer, inner);
			expect(matches.length).toBe(2);
			expect(matches[0]).toMatchSnapshot('apple');
			expect(matches[1]).toMatchSnapshot('banana');

			// original inner hasn't changed
			expect(inner.getIdea('fruit')).toEqual(verts.fruit);
			expect(inner.getIdea('unknown-fruit')).toEqual(undefined);
			expect(inner.getIdea('unknown-color')).toEqual(undefined);
			expect(inner).toMatchSnapshot('inner after');
		});
	});

	// copy the search.test.js and plum
	test.todo('square demo (transitive)');

	test('status enum (orData)', async () => {
		expect.assertions(4);

		const verts = {
			status: { value: 'status' },
			todo: { value: 'todo' },
			open: { value: 'open' },
			closed: { value: 'closed' },
			item: { value: 'item' },
		};
		const edges = [
			['todo', 'typeOf', 'status'],
			['open', 'typeOf', 'status'],
			['closed', 'typeOf', 'status'],
			['item', 'property', 'todo'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new Subgraph();
		sg.addVertex('id', verts.status, { name: 'status' });
		sg.addVertex('id', verts.todo, { name: 'todo' });
		sg.addVertex('id', verts.open, { name: 'open' });
		sg.addVertex('id', verts.closed, { name: 'closed' });
		sg.addVertex('id', verts.item, { name: 'item' });
		sg.addEdge('todo', 'typeOf', 'status');
		sg.addEdge('open', 'typeOf', 'status');
		sg.addEdge('closed', 'typeOf', 'status');
		sg.addEdge('item', 'property', 'todo');

		const results = await search(sg);
		expect(results.length).toBe(1);
		const outer = results[0];
		// expect(outer).toMatchSnapshot('outer');

		const inner = new Subgraph();
		inner.addVertex('id', verts.status, { name: 'status' });
		inner.addVertex('exact', [{ value: 'open' }, { value: 'todo' }], { name: 'astatus', orData: true });
		inner.addEdge('astatus', 'typeOf', 'status');

		const matches = await match(outer, inner);
		expect(matches.length).toBe(2);
		expect(matches[0]).toMatchSnapshot('todo');
		expect(matches[1]).toMatchSnapshot('open');
	});

	test('status enum (orData w/ id)', async () => {
		expect.assertions(4);

		const verts = {
			status: { value: 'status' },
			todo: { value: 'todo' },
			open: { value: 'open' },
			closed: { value: 'closed' },
			item: { value: 'item' },
		};
		const edges = [
			['todo', 'typeOf', 'status'],
			['open', 'typeOf', 'status'],
			['closed', 'typeOf', 'status'],
			['item', 'property', 'todo'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new Subgraph();
		sg.addVertex('id', verts.status, { name: 'status' });
		sg.addVertex('id', verts.todo, { name: 'todo' });
		sg.addVertex('id', verts.open, { name: 'open' });
		sg.addVertex('id', verts.closed, { name: 'closed' });
		sg.addVertex('id', verts.item, { name: 'item' });
		sg.addEdge('todo', 'typeOf', 'status');
		sg.addEdge('open', 'typeOf', 'status');
		sg.addEdge('closed', 'typeOf', 'status');
		sg.addEdge('item', 'property', 'todo');

		const results = await search(sg);
		expect(results.length).toBe(1);
		const outer = results[0];
		// expect(outer).toMatchSnapshot('outer');

		const inner = new Subgraph();
		inner.addVertex('id', verts.status, { name: 'status' });
		inner.addVertex('id', [verts.open, verts.todo.id], { name: 'astatus', orData: true });
		inner.addEdge('astatus', 'typeOf', 'status');

		const matches = await match(outer, inner);
		expect(matches.length).toBe(2);
		expect(matches[0]).toMatchSnapshot('todo');
		expect(matches[1]).toMatchSnapshot('open');
	});

	test.todo('unitsOnly');

	describe('integration bugs', () => {
		/**
			here's the real question: why make two vertices that map to the same idea?
			why not just use the one vertex? it's a simpler search space anyways
			@see match.units.buildInverseIdeaMap
		*/
		test.skip('two vertices match same idea', async () => {
			expect.assertions(2);

			// setup base ideas, just one vertex
			const verts = { fruit: { value: 'fruit' } };
			const edges = [];
			await ideas.createGraph(verts, edges);

			// setup sugraph
			const outer = new Subgraph();
			outer.addVertex('id', verts.fruit);
			// no need to search, it's already concrete

			// make our inner subgaph, this is where the trouble starts
			const inner = new Subgraph();
			inner.addVertex('id', verts.fruit);

			// we can match the subgraph when there is exactly one match
			expect((await match(outer, inner)).length).toBe(1);

			// but it fails when there are two vertices that map to the same place
			// this is _still_ a concrete graph with no edges, so it isn't in the edge matching
			inner.addVertex('id', verts.fruit);
			expect((await match(outer, inner)).length).toBe(1); // this matches zero
		});

		// this bug was because in the original impl I normalized the edged when I added them to the graph `links.isOpp.(link)`
		//  - i removed that because is seemed silly
		//  - but the search needs to match the edges; if we don't normalize, then we have to constantly check/flip when we search/match
		//  - it's better to just do it once up front
		describe('has/has-opp', () => {
			test('hh', async () => {
				expect.assertions(1);

				const outer = new Subgraph();
				outer.addEdge(
					outer.addVertex('id', ideas.proxy('_test')),
					'has',
					outer.addVertex('id', ideas.proxy('_test2'), { name: 'theValue', transitionable: true }),
				);
				outer.setData('theValue', { material: 'lead' });
				outer.concrete = true;

				const inner = new Subgraph();
				inner.addEdge(
					inner.addVertex('id', ideas.proxy('_test')),
					'has',
					inner.addVertex('exact', { material: 'lead' }, { name: 'theValue', transitionable: true }),
				);

				expect(await match(outer, inner, { unitsOnly: false })).toMatchSnapshot('match');
			});

			test('ho', async () => {
				expect.assertions(1);

				const outer = new Subgraph();
				outer.addEdge(
					outer.addVertex('id', ideas.proxy('_test')),
					'has',
					outer.addVertex('id', ideas.proxy('_test2'), { name: 'theValue', transitionable: true }),
				);
				outer.setData('theValue', { material: 'lead' });
				outer.concrete = true;

				const inner = new Subgraph();
				inner.addEdge(
					inner.addVertex('exact', { material: 'lead' }, { name: 'theValue', transitionable: true }),
					'has-opp',
					inner.addVertex('id', ideas.proxy('_test')),
				);

				expect(await match(outer, inner, { unitsOnly: false })).toMatchSnapshot('match');
			});

			test('oh', async () => {
				expect.assertions(1);

				const outer = new Subgraph();
				outer.addEdge(
					outer.addVertex('id', ideas.proxy('_test2'), { name: 'theValue', transitionable: true }),
					'has-opp',
					outer.addVertex('id', ideas.proxy('_test')),
				);
				outer.setData('theValue', { material: 'lead' });
				outer.concrete = true;

				const inner = new Subgraph();
				inner.addEdge(
					inner.addVertex('id', ideas.proxy('_test')),
					'has',
					inner.addVertex('exact', { material: 'lead' }, { name: 'theValue', transitionable: true }),
				);

				expect(await match(outer, inner, { unitsOnly: false })).toMatchSnapshot('match');
			});

			test('oo', async () => {
				expect.assertions(1);

				const outer = new Subgraph();
				outer.addEdge(
					outer.addVertex('id', ideas.proxy('_test2'), { name: 'theValue', transitionable: true }),
					'has-opp',
					outer.addVertex('id', ideas.proxy('_test')),
				);
				outer.setData('theValue', { material: 'lead' });
				outer.concrete = true;

				const inner = new Subgraph();
				inner.addEdge(
					inner.addVertex('exact', { material: 'lead' }, { name: 'theValue', transitionable: true }),
					'has-opp',
					inner.addVertex('id', ideas.proxy('_test')),
				);

				expect(await match(outer, inner, { unitsOnly: false })).toMatchSnapshot('match');
			});
		});
	});
});