const ideas = require('../../../src/database/ideas');
const { Subgraph, search } = require('../../../src/database/subgraphs');

describe('search', () => {
	test('search.integration', () => {
		expect.assertions(1);
		expect(Object.keys(search)).toEqual([
			// this just exports the function
		]);
	});

	test('fruit demo', async () => {
		expect.assertions(15);

		const verts = {
			fruit: { value: 'fruit' },
			apple: { value: 'apple' },
			banana: { value: 'banana' },
			red: { value: 'red' },
			yellow: { value: 'yellow' },
		};
		const edges = [
			['apple', 'typeOf', 'fruit'],
			['banana', 'typeOf', 'fruit'],
			['apple', 'property', 'red'],
			['banana', 'property', 'yellow'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new Subgraph();
		sg.addVertex('id', verts.fruit, { name: 'fruit' });
		sg.addVertex('filler', undefined, { name: 'unknown' });
		sg.addVertex('exact', { value: 'red' }, { name: 'red' });
		sg.addEdge('unknown', 'typeOf', 'fruit');
		sg.addEdge('unknown', 'property', 'red');

		expect(sg.getIdea('fruit')).toEqual(verts.fruit); // this is our starting point
		expect(sg.getIdea('unknown')).toEqual(undefined); // this is what we are looking for
		expect(sg.getIdea('red')).toEqual(undefined); // we have match data, but not a specific idea
		expect(sg).toMatchSnapshot('search params');

		const results = await search(sg);
		const result = results[0];

		// we have a match!
		expect(results.length).toBe(1);
		expect(result).not.toBe(sg);
		expect(result).not.toEqual(sg);
		expect(result.getIdea('fruit')).toEqual(verts.fruit);
		expect(result.getIdea('unknown')).toEqual(verts.apple);
		expect(result.getIdea('red')).toEqual(verts.red);
		expect(result).toMatchSnapshot('result');

		// original Subgraph hasn't changed
		expect(sg.getIdea('fruit')).toEqual(verts.fruit);
		expect(sg.getIdea('unknown')).toEqual(undefined);
		expect(sg.getIdea('red')).toEqual(undefined);
		expect(sg).toMatchSnapshot('search params');
	});

	/*
		typeOf
			square -> rectangle -> parallelogram -> quadrilateral
			square -> rhombus -> parallelogram
	*/
	test('square demo (transitive)', async () => {
		expect.assertions(5);

		const verts = {
			square: { value: 'square' },
			rectangle: { value: 'rectangle' },
			rhombus: { value: 'rhombus' },
			parallelogram: { value: 'parallelogram' },
			quadrilateral: { value: 'quadrilateral' },
		};
		const edges = [
			['square', 'typeOf', 'rectangle'],
			['square', 'typeOf', 'rhombus'],
			['rectangle', 'typeOf', 'parallelogram'],
			['rhombus', 'typeOf', 'parallelogram'],
			['parallelogram', 'typeOf', 'quadrilateral'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new Subgraph();
		sg.addVertex('id', verts.quadrilateral, { name: 'quadrilateral' });
		sg.addVertex('filler', undefined, { name: 'unknown' });
		sg.addEdge('unknown', 'typeOf', 'quadrilateral');

		const results = await search(sg);

		expect(results.length).toBe(4);
		results.sort((a, b) => (a.getIdea('unknown').id > b.getIdea('unknown').id ? 1 : -1));
		expect(results[0].getIdea('unknown')).toEqual(verts.square);
		expect(results[1].getIdea('unknown')).toEqual(verts.rectangle);
		expect(results[2].getIdea('unknown')).toEqual(verts.rhombus);
		expect(results[3].getIdea('unknown')).toEqual(verts.parallelogram);
	});

	test('status enum (orData)', async () => {
		expect.assertions(5);

		const verts = {
			status: { value: 'status' },
			todo: { value: 'todo' },
			open: { value: 'open' },
			closed: { value: 'closed' },
			item: { value: 'item' },
		};
		const edges = [
			['todo', 'typeOf', 'status'],
			['open', 'typeOf', 'status'],
			['closed', 'typeOf', 'status'],
			['item', 'property', 'todo'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new Subgraph();
		sg.addVertex('id', verts.status, { name: 'status' });
		sg.addVertex('exact', [{ value: 'open' }, { value: 'todo' }], { name: 'astatus', orData: true });
		sg.addEdge('astatus', 'typeOf', 'status');

		let results = await search(sg);
		expect(results.length).toBe(2);
		expect(results[0].getIdea('astatus')).toEqual(verts.todo);
		expect(results[1].getIdea('astatus')).toEqual(verts.open);

		sg.addEdge(
			sg.addVertex('id', verts.item),
			'property',
			'astatus',
		);

		results = await search(sg);
		expect(results.length).toBe(1);
		expect(results[0].getIdea('astatus')).toEqual(verts.todo);
	});

	test('status enum (orData w/ ids)', async () => {
		expect.assertions(10);

		const verts = {
			status: { value: 'status' },
			todo: { value: 'todo' },
			open: { value: 'open' },
			closed: { value: 'closed' },
			item_t1: { value: 'item_t1' },
			item_t2: { value: 'item_t2' },
			item_o: { value: 'item_o' },
			item_c: { value: 'item_c' },
		};
		const edges = [
			['todo', 'typeOf', 'status'],
			['open', 'typeOf', 'status'],
			['closed', 'typeOf', 'status'],
			['item_t1', 'property', 'todo'],
			['item_t2', 'property', 'todo'],
			['item_o', 'property', 'open'],
			['item_c', 'property', 'closed'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new Subgraph();
		sg.addVertex('id', verts.status, { name: 'status' });
		sg.addVertex('id', [verts.open, verts.todo.id], { name: 'astatus', orData: true });
		sg.addEdge('astatus', 'typeOf', 'status');

		let results = await search(sg);
		expect(results.length).toBe(2);
		expect(results[0].getIdea('astatus')).toEqual(verts.todo);
		expect(results[1].getIdea('astatus')).toEqual(verts.open);

		sg.addEdge(
			sg.addVertex('filler', undefined, { name: 'item' }),
			'property',
			'astatus',
		);

		results = await search(sg);
		expect(results.length).toBe(3);
		expect(results[0].getIdea('astatus')).toEqual(verts.todo);
		expect(results[0].getIdea('item')).toEqual(verts.item_t1);
		expect(results[1].getIdea('astatus')).toEqual(verts.todo);
		expect(results[1].getIdea('item')).toEqual(verts.item_t2);
		expect(results[2].getIdea('astatus')).toEqual(verts.open);
		expect(results[2].getIdea('item')).toEqual(verts.item_o);
	});

	// tags or character sets (e.g. decimal)
	test('counter-point: lexigrams (multiple)', async () => {
		expect.assertions(5);

		const verts = {
			tags: { value: 'tags' },
			tasks: { value: 'tasks' },
			task: { value: 'task' },
			tagA: { value: 'tagA' },
			tagAProp: { value: 'tagAProp' },
			tagB: { value: 'tagB' },
			tagBProp: { value: 'tagBProp' },
			enums: { value: 'enums' },
			enumA: { value: 'enumA' },
		};
		const edges = [
			['task', 'context', 'tasks'],
			['tagA', 'context', 'tags'],
			['tagB', 'context', 'tags'],
			['task', 'property', 'tagA'],
			['task', 'property', 'tagB'],
			['tagA', 'property', 'tagAProp'],
			['tagB', 'property', 'tagBProp'],
			['task', 'property', 'enumA'],
			['enumA', 'context', 'enums'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new Subgraph();
		sg.addVertex('id', verts.tasks, { name: 'task-context' });
		sg.addVertex('id', verts.tags, { name: 'tags-context' });
		sg.addVertex('filler', undefined, { name: 'task' });
		sg.addVertex('filler', undefined, { name: 'tag' });
		sg.addVertex('filler', undefined, { name: 'tagProp' });
		sg.addEdge('task', 'context', 'task-context');
		sg.addEdge('tag', 'context', 'tags-context');
		sg.addEdge('task', 'property', 'tag', { multiple: true }); // we want to be able to match ALL tags with a single search
		sg.addEdge('tag', 'property', 'tagProp');

		const results = await search(sg);

		// we want this to be 1 (this search matched all the tags)
		expect(results.length).toBe(2);

		// it's not hard to consider how we get the tags directly
		// instead of looking across multiple results, a single vertex name could return multiple ideas
		// we can expand all task --property-> tag
		// the hard (but solvable) part is ensuring that ALL tag --context-> tags (filter out enums)
		expect(results[0].getIdea('tag')).toEqual(verts.tagA);
		expect(results[1].getIdea('tag')).toEqual(verts.tagB);

		// but it gets really complicated if we want to start describing those idea
		// how do we know that tagBProp aligns with tagB?
		// does this edge need a multiple as well (even tho it's 1-1, what if there are multiple here too?)
		// we can't flatten this search; it's fractal or at least hyperbolic.
		expect(results[0].getIdea('tagProp')).toEqual(verts.tagAProp);
		expect(results[1].getIdea('tagProp')).toEqual(verts.tagBProp);
	});
});