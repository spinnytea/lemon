const ideas = require('../../../src/database/ideas/ideas');
const memory = require('../../../src/database/ideas/memory');
const io = require('../../../src/database/ideas/io');
const { Subgraph, search } = require('../../../src/database/subgraphs');
const subgraphUtils = require('../../../src/database/subgraphs/subgraphUtils');

describe('subgraphUtils', () => {
	test('subgraphUtils.integration', () => {
		expect.assertions(1);
		expect(Object.keys(subgraphUtils)).toEqual([
			'destroy',
		]);
	});

	test('destroy', async () => {
		expect.assertions(12);

		const verts = {
			fruit: { value: 'fruit' },
			apple: { value: 'apple' },
			banana: { value: 'banana' },
			red: { value: 'red' },
			yellow: { value: 'yellow' },
		};
		const edges = [
			['apple', 'typeOf', 'fruit'],
			['banana', 'typeOf', 'fruit'],
			['apple', 'property', 'red'],
			['banana', 'property', 'yellow'],
		];

		await ideas.createGraph(verts, edges);

		let sg = new Subgraph();
		sg.addVertex('id', verts.fruit, { name: 'fruit' });
		sg.addVertex('id', verts.apple, { name: 'apple' });
		sg.addVertex('id', verts.red, { name: 'red' });
		sg.addEdge('apple', 'typeOf', 'fruit');
		sg.addEdge('apple', 'property', 'red');
		([sg] = (await search(sg)));

		const appleId = sg.getIdea('apple').id;
		const redId = sg.getIdea('red').id;

		expect(sg.hasIdea('apple')).toBe(true);
		expect(sg.hasIdea('red')).toBe(true);
		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();
		expect(io.units.database.data).toHaveProperty(appleId);
		expect(io.units.database.data).toHaveProperty(redId);

		await subgraphUtils.destroy(sg, ['apple', 'red']);

		expect(sg.hasIdea('apple')).toBe(false);
		expect(sg.hasIdea('red')).toBe(false);
		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();
		expect(io.units.database.data).not.toHaveProperty(appleId);
		expect(io.units.database.data).not.toHaveProperty(redId);
	});
});