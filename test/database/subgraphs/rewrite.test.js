const _ = require('lodash');
const links = require('../../../src/database/links');
const ideas = require('../../../src/database/ideas');
const memory = require('../../../src/database/ideas/memory');
const io = require('../../../src/database/ideas/io');
const { Subgraph, search, rewrite } = require('../../../src/database/subgraphs');

describe('rewrite', () => {
	test('rewrite.integration', () => {
		expect.assertions(1);
		expect(Object.keys(rewrite)).toEqual([
			// just exports the function
		]);
	});

	test('simple data', async () => {
		expect.assertions(7);

		const proxy = await ideas.create({ value: 1 });

		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();

		// setup complete

		const sg = new Subgraph();
		sg.addVertex('id', proxy, { name: 't', transitionable: true });

		expect(await rewrite(sg, [{ vertexId: 't', replace: { value: 2 } }], true)).not.toBe(undefined);

		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();

		await sg.save();

		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();
	});

	test('simple link', async () => {
		expect.assertions(7);

		const link = links.get('sameAs');

		const proxyDog = await ideas.create({ what: 'dog' });
		const proxyCat = await ideas.create({ what: 'cat' });
		const proxyWolf = await ideas.create({ what: 'wolf' });
		await proxyDog.addLink(link, proxyCat);
		await ideas.save(proxyDog);
		await ideas.save(proxyCat);

		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();

		// setup complete

		let sg = new Subgraph();
		sg.addVertex('id', proxyDog, { name: 'd' });
		sg.addVertex('id', proxyCat, { name: 'c' });
		sg.addVertex('id', proxyWolf, { name: 'w' });
		sg.addEdge('d', link, 'c', { name: 'myEdge', transitionable: true });
		([sg] = (await search(sg)));

		expect(await rewrite(sg, [{ edgeId: 'myEdge', replaceDst: 'w' }], true)).not.toBe(undefined);

		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();

		await sg.save();

		expect(memory.units.map).toMatchSnapshot();
		expect(io.units.database).toMatchSnapshot();
	});

	test('mark and his fruit', async () => {
		expect.assertions(22);

		/*
			apple_bundle --typeOf-> apple
			             \-property-> (count) --typeOf-> bundle_count
			banana_bundle --typeOf-> banana
			             \-property-> (count) --typeOf-> bundle_count

			mark --has-> apple_bundle
		*/
		const ideaMap = {
			mark: { name: 'mark' },
			apple: { name: 'apple' },
			banana: { name: 'banana' },
			bundle_count: { name: 'bundle_count' },

			// instances
			apple_bundle: {},
			banana_bundle: {},
			apple_count: { count: 1 },
			banana_count: { count: 2 },
		};

		await ideas.createGraph(ideaMap, [
			['apple_bundle', 'typeOf', 'apple'],
			['apple_bundle', 'property', 'apple_count'],
			['apple_count', 'typeOf', 'bundle_count'],

			['banana_bundle', 'typeOf', 'banana'],
			['banana_bundle', 'property', 'banana_count'],
			['banana_count', 'typeOf', 'bundle_count'],

			['mark', 'has', 'apple_bundle'],
		]);

		const sg = new Subgraph();
		sg.addVertex('id', ideaMap.mark.id, { name: 'mark' });
		sg.addVertex('id', ideaMap.apple.id, { name: 'apple' });
		sg.addVertex('id', ideaMap.banana.id, { name: 'banana' });
		sg.addVertex('id', ideaMap.bundle_count.id, { name: 'bundle_count' });
		sg.addVertex('filler', undefined, { name: 'ab' });
		sg.addVertex('filler', undefined, { name: 'ac', transitionable: true });
		sg.addVertex('filler', undefined, { name: 'bb' });
		sg.addVertex('filler', undefined, { name: 'bc', transitionable: true });
		sg.addEdge('ab', 'typeOf', 'apple');
		sg.addEdge('ab', 'property', 'ac');
		sg.addEdge('ac', 'typeOf', 'bundle_count');
		sg.addEdge('bb', 'typeOf', 'banana');
		sg.addEdge('bb', 'property', 'bc');
		sg.addEdge('bc', 'typeOf', 'bundle_count');
		sg.addEdge('mark', 'has', 'ab', { name: 'mark_has_bundle', transitionable: true });

		const searchResults = await search(sg);
		expect(searchResults).toEqual(expect.any(Array));
		expect(searchResults.length).toBe(1);
		const sgResult = searchResults[0];
		expect(sgResult.concrete).toBe(true);

		// okay, setup done
		// do a transition

		const sgTryAppleCount = await rewrite(sgResult, [{ vertexId: 'ac', replace: { count: 4 } }]);
		expect(sgTryAppleCount).not.toBe(undefined);
		expect(sgTryAppleCount).not.toBe(sgResult);
		expect(await sgResult.getData('ac')).toEqual({ count: 1 });
		expect(await sgTryAppleCount.getData('ac')).toEqual({ count: 4 });
		expect(await ideaMap.apple_count.getData()).toEqual({ count: 1 });

		const sgTrySwapBundle = await rewrite(sgTryAppleCount, [{ edgeId: 'mark_has_bundle', replaceDst: 'bb' }]);
		expect(sgTrySwapBundle).not.toBe(undefined);
		expect(sgTrySwapBundle).not.toBe(sgTryAppleCount);
		expect(_.pick(sgTryAppleCount.getEdge('mark_has_bundle'), ['src', 'dst'])).toEqual({ src: sg.$refVertex('mark'), dst: sg.$refVertex('ab') });
		expect(_.pick(sgTrySwapBundle.getEdge('mark_has_bundle'), ['src', 'dst'])).toEqual({ src: sg.$refVertex('mark'), dst: sg.$refVertex('bb') });
		expect(await ideaMap.mark.getLinksOfType(links.get('has'))).toEqual([ideaMap.apple_bundle]);
		expect(await ideaMap.apple_bundle.getLinksOfType(links.get('has').opposite)).toEqual([ideaMap.mark]);
		expect(await ideaMap.banana_bundle.getLinksOfType(links.get('has').opposite)).toEqual([]);

		// though experiment done
		// apply the transitions

		const sgActual = await rewrite(sgResult, [
			{ vertexId: 'ac', replace: { count: 4 } },
			{ edgeId: 'mark_has_bundle', replaceDst: 'bb' },
		], true);
		expect(sgActual).toBe(sgResult);
		expect(await sgResult.getData('ac')).toEqual({ count: 4 });
		expect(_.pick(sgResult.getEdge('mark_has_bundle'), ['src', 'dst'])).toEqual({ src: sg.$refVertex('mark'), dst: sg.$refVertex('bb') });
		expect(await ideaMap.apple_count.getData()).toEqual({ count: 4 });
		expect(await ideaMap.mark.getLinksOfType(links.get('has'))).toEqual([ideaMap.banana_bundle]);
		expect(await ideaMap.apple_bundle.getLinksOfType(links.get('has').opposite)).toEqual([]);
		expect(await ideaMap.banana_bundle.getLinksOfType(links.get('has').opposite)).toEqual([ideaMap.mark]);
	});
});