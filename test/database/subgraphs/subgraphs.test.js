const ideas = require('../../../src/database/ideas');
const links = require('../../../src/database/links');
const subgraphs = require('../../../src/database/subgraphs/subgraphs');

describe('subgraphs', () => {
	test('subgraphs.integration', () => {
		expect.assertions(1);
		expect(Object.keys(subgraphs)).toEqual([
			'Subgraph',
			'createSubgraph',
			'createMatcher',
			'projectSubgraph',
		]);
	});

	test('Subgraph demo', () => {
		expect.assertions(1);

		const sg = new subgraphs.Subgraph();
		sg.addVertex('id', ideas.proxy('_fruit'), { name: 'fruit' });
		sg.addVertex('filler', undefined, { name: 'unknown' });
		sg.addVertex('exact', { value: 'red' }, { name: 'red' });
		sg.addEdge('unknown', 'typeOf', 'fruit');
		sg.addEdge('unknown', 'property', 'red');

		expect(sg).toMatchSnapshot();
	});

	test('Subgraph grow demo', async () => {
		expect.assertions(3);

		const fruit = await ideas.create({ value: 'fruit' });
		const verts = {
			fruit,
			apple: { value: 'apple' },
			banana: { value: 'banana' },
			red: { value: 'red' },
			yellow: { value: 'yellow' },
		};
		const edges = [
			['apple', 'typeOf', 'fruit'],
			['banana', 'typeOf', 'fruit'],
			['apple', 'property', 'red'],
			['banana', 'property', 'yellow'],
		];

		await ideas.createGraph(verts, edges);

		const sg = new subgraphs.Subgraph();
		const vertexId = sg.addVertex('id', fruit);

		await sg.grow(vertexId, [links.get('typeOf').opposite]);
		await sg.grow(vertexId, [links.get('typeOf').opposite, 'property']);

		// you shouldn't actually do this; it's just to make sure it doesn't break
		// it won't do anything. grow nowhere
		await sg.grow(vertexId, []);

		expect(sg.$vertexCount).toBe(5);
		expect(sg.$edgeCount).toBe(4);
		expect(sg).toMatchSnapshot();
	});

	test.todo('createSubgraph');

	test.todo('createMatcher');

	test.todo('projectSubgraph');
});