const config = require('../../../src/config');
const boundaries = require('../../../src/boundaries');
const io = require('../../../src/database/ideas/io');

describe('io', () => {
	beforeEach(() => {
		boundaries.existsSync.mockRestore();
		boundaries.readFileSync.mockRestore();

		config.init({ in_memory: false, location: 'test_data' });

		boundaries.readFile.mockRestore();
		boundaries.writeFile.mockRestore();
		boundaries.mkdirp.mockRestore();
	});

	afterEach(() => {
		config.init({ in_memory: true });
	});

	test('io.integration', () => {
		expect.assertions(1);
		expect(Object.keys(io)).toEqual([
			'save',
			'load',
		]);
	});

	test('save', () => {
		expect.assertions(1);
		return io.save('_io_test', 'data', { value: 1 }).then(
			(data) => { expect(data).toEqual({ value: 1 }); },
			() => { throw new Error('should resolve'); },
		);
	});

	test('load', () => {
		expect.assertions(1);
		return io.load('_io_test', 'data').then(
			(data) => { expect(data).toEqual({ value: 1 }); },
			() => { throw new Error('should resolve'); },
		);
	});
});