const ideas = require('../../../src/database/ideas');
const links = require('../../../src/database/links');
const io = require('../../../src/database/ideas/io');
const memory = require('../../../src/database/ideas/memory');
const CoreIdea = require('../../../src/database/ideas/CoreIdea');
const { ProxyIdea } = require('../../../src/database/ideas').units;

describe('ideas', () => {
	test('ideas.integration', () => {
		expect.assertions(1);
		expect(Object.keys(ideas)).toEqual([
			'create',
			'proxy',
			'save',
			'close',
			'delete',
			'context',
			'contexts',
			'createGraph',
		]);
	});

	describe('create', () => {
		test('empty', async () => {
			expect.assertions(6);

			const proxy = await ideas.create();

			expect(proxy).toBeInstanceOf(ProxyIdea);
			expect(Object.keys(proxy)).toEqual(['id']);
			expect(typeof proxy.id).toBe('string');
			expect(memory.has(proxy)).toBe(true);
			expect(memory.get(proxy)).toBeInstanceOf(CoreIdea);
			expect(io.units.database.data).not.toHaveProperty(proxy.id);
		});

		test('with data', async () => {
			expect.assertions(6);

			const proxy = await ideas.create({ key: 'some data' });

			expect(proxy).toBeInstanceOf(ProxyIdea);
			expect(Object.keys(proxy)).toEqual(['id']);
			expect(typeof proxy.id).toBe('string');
			expect(memory.has(proxy)).toBe(true);
			expect(memory.get(proxy)).toBeInstanceOf(CoreIdea);
			expect(io.units.database.data).toHaveProperty(proxy.id);
		});
	});

	test.skip('load', async () => {
		expect.assertions(3);

		expect(memory.has({ id: '_test' })).toBe(false);

		const proxy = await ideas.load('_test');

		expect(proxy).toBeInstanceOf(ProxyIdea);
		expect(memory.has({ id: '_test' })).toBe(true);
	});

	describe('proxy', () => {
		test('basic', () => {
			expect.assertions(3);

			const proxy = ideas.proxy('_test');

			expect(proxy).toBeInstanceOf(ProxyIdea); // for all other tests, this should be sufficient
			expect(Object.keys(proxy)).toEqual(['id']);
			expect(proxy.id).toBe('_test');
		});

		test('de-dups', () => {
			expect.assertions(1);
			const proxyA = ideas.proxy('_test');
			const proxyB = ideas.proxy(proxyA);

			expect(proxyA).toBe(proxyB);
		});
	});

	test('save', async () => {
		expect.assertions(5);
		await ideas.proxy('_test').setData({});

		expect(memory.has({ id: '_test' })).toBe(true);
		expect(memory.get({ id: '_test' })).toBeInstanceOf(CoreIdea);
		expect(io.units.database.data).not.toHaveProperty('_test');

		const proxy = await ideas.save('_test');

		expect(memory.has(proxy)).toBe(true);
		expect(io.units.database.data).toHaveProperty(proxy.id);
	});

	test('close', async () => {
		expect.assertions(5);
		await ideas.proxy('_test').setData({});

		expect(memory.has({ id: '_test' })).toBe(true);
		expect(memory.get({ id: '_test' })).toBeInstanceOf(CoreIdea);
		expect(io.units.database.data).not.toHaveProperty('_test');

		const proxy = await ideas.close('_test');

		expect(memory.has(proxy)).toBe(false);
		expect(io.units.database.data).toHaveProperty(proxy.id);
	});

	describe('delete', () => {
		test('data', async () => {
			expect.assertions(7);
			const proxy = ideas.proxy('_test');
			await proxy.setData({});
			await ideas.save(proxy);

			expect(proxy).toBeInstanceOf(ProxyIdea);
			expect(memory.has(proxy)).toBe(true);
			expect(memory.get(proxy)).toBeInstanceOf(CoreIdea);
			expect(io.units.database.data).toHaveProperty('_test');

			const proxyD = await ideas.delete('_test');

			expect(memory.has(proxy)).toBe(false);
			expect(io.units.database.data).not.toHaveProperty(proxy.id);

			expect(proxy).toEqual(proxyD);
		});

		test('links', async () => {
			expect.assertions(8);

			const link = links.get('sameAs');

			const proxyA = ideas.proxy('_testA');
			const proxyB = ideas.proxy('_testB');
			const proxyC = ideas.proxy('_testC');
			await proxyA.addLink(link, proxyB);
			await proxyB.addLink(link, proxyB);
			await proxyC.addLink(link, proxyB);

			expect([memory.get(proxyA), memory.get(proxyB), memory.get(proxyC)]).toMatchSnapshot();
			expect(await proxyA.getLinksOfType(link)).toEqual([proxyB]);
			expect(await proxyB.getLinksOfType(link)).toEqual([proxyA, proxyB, proxyC]);
			expect(await proxyC.getLinksOfType(link)).toEqual([proxyB]);

			await ideas.delete(proxyB);

			expect([memory.get(proxyA), memory.get(proxyB), memory.get(proxyC)]).toMatchSnapshot();
			expect(await proxyA.getLinksOfType(link)).toEqual([]);
			expect(await proxyB.getLinksOfType(link)).toEqual([]);
			expect(await proxyC.getLinksOfType(link)).toEqual([]);
		});
	});

	test('context', async () => {
		expect.assertions(5);

		const c1 = await ideas.context('c1');

		expect(c1).toBeInstanceOf(ProxyIdea);
		expect(memory.has(c1)).toBe(true);
		expect(memory.get(c1)).toBeInstanceOf(CoreIdea);

		expect(c1).toEqual(await ideas.context('c1'));

		await ideas.context('c2');
		await ideas.context('c3');
		expect(c1).toEqual(await ideas.context('c1'));
	});

	test('contexts', async () => {
		expect.assertions(3);

		const contextMap = await ideas.contexts(['c1', 'c2', 'c3']);

		expect(Object.keys(contextMap).sort()).toEqual(['c1', 'c2', 'c3']);
		expect(contextMap).toEqual(await ideas.contexts(['c1', 'c2', 'c3']));

		await ideas.contexts(['c1', 'c2', 'c3']);
		await ideas.contexts(['c4', 'c5', 'c6']);

		await expect(ideas.contexts(['c1', 'c2', 'c3'])).resolves.toEqual(contextMap);
	});

	test('createGraph', async () => {
		expect.assertions(3);
		const verts = {
			fruit: { value: 'fruit' },
			apple: { value: 'apple' },
			banana: { value: 'banana' },
			red: { value: 'red' },
			yellow: { value: 'yellow' },
		};
		const edges = [
			['apple', 'typeOf', 'fruit'],
			['banana', 'typeOf', 'fruit'],
			['apple', 'property', 'red'],
			['banana', 'property', 'yellow'],
		];

		expect(io.units.database).toMatchSnapshot();
		jest.spyOn(ideas, 'save');

		await ideas.createGraph(verts, edges);

		expect(io.units.database).toMatchSnapshot();
		expect(ideas.save.mock.calls.length).toBe(5);
	});

	test.todo('createGraph attached to existing');
});