/*
	this doesn't test the actual code
	this ensure that we have all the test files that we need
*/

const fs = require('fs');
const path = require('path');

function listFiles(dir) {
	const list = [];

	fs.readdirSync(dir).forEach((filename) => {
		const filepath = path.join(dir, filename);
		const stats = fs.statSync(filepath);
		if (stats.isDirectory()) {
			Array.prototype.push.apply(list, listFiles(filepath));
		}
		else if (stats.isFile()) {
			list.push(filepath);
		}
	});

	return list;
}

describe('test completeness', () => {
	let srcFiles;
	beforeAll(() => {
		srcFiles = listFiles('src');
	});

	test('integration', () => {
		expect.assertions(0);
		srcFiles.forEach((srcFilepath) => {
			const testFilepath = srcFilepath
				.replace(/^src/, 'test')
				.replace(/\.js$/, '.test.js');

			if ([
				'test/database/ideas/CoreIdea.test.js', // XXX not exported in any way
				'test/database/ideas/index.test.js', // this helps defined the 'public api'
				'test/database/ideas/memory.test.js', // XXX not exported in any way
				'test/database/subgraphs/index.test.js', // this helps defined the 'public api'
				'test/database/subgraphs/LazyCopyMap.test.js', // XXX not exported in any way
			].includes(testFilepath)) {
				return;
			}

			if (!fs.existsSync(testFilepath)) {
				throw new Error(`missing test file "${testFilepath}"`);
			}
		});
	});

	test('unit', () => {
		expect.assertions(0);
		srcFiles.forEach((srcFilepath) => {
			const unitFilepath = srcFilepath
				.replace(/^src/, 'unit')
				.replace(/\.js$/, '.test.js');

			if ([
				'unit/database/ideas/index.test.js', // this helps defined the 'public api'
				'unit/database/subgraphs/index.test.js', // this helps defined the 'public api'
			].includes(unitFilepath)) {
				return;
			}

			if (!fs.existsSync(unitFilepath)) {
				throw new Error(`missing test file "${unitFilepath}"`);
			}
		});
	});
});