const utils = require('../src/utils');
const ids = require('../src/ids'); // this is the only impl; we could write another example instead of using this

describe('utils', () => {
	test('utils.integration', () => {
		expect.assertions(1);
		expect(Object.keys(utils)).toEqual([
			'isEmpty',
			'findByValue',
			'transaction',
			'join',
		]);
	});

	describe('isEmpty', () => {
		test('valid uses', () => {
			expect.assertions(2);
			expect(utils.isEmpty({})).toBe(true);
			expect(utils.isEmpty({ a: 0 })).toBe(false);
		});

		test('invalid uses', () => {
			expect.assertions(5);
			expect(utils.isEmpty(false)).toBe(true);
			expect(utils.isEmpty(1)).toBe(true);
			expect(utils.isEmpty(null)).toBe(true);
			expect(utils.isEmpty(undefined)).toBe(true);
			expect(utils.isEmpty()).toBe(true);
		});
	});

	describe('findByValue', () => {
		test('basic', () => {
			expect.assertions(3);
			const map = new Map();
			map.set('a', 1);
			map.set('b', 2);
			expect(utils.findByValue(map, (v) => v === 1)).toBe('a');
			expect(utils.findByValue(map, (v) => v === 2)).toBe('b');
			expect(utils.findByValue(map, (v) => v === 3)).toBe(undefined);
		});
	});

	describe('transaction', () => {
		const key = 'test.ids.utils.transaction';

		test('serial', async () => {
			expect.assertions(3);
			await expect(ids.next(key)).resolves.toEqual('1');
			await expect(ids.next(key)).resolves.toEqual('2');
			await expect(ids.next(key)).resolves.toEqual('3');
		});

		test('chained', async () => {
			expect.assertions(2);
			let promise = ids.next(key) // 1
				.then(() => ids.next(key)) // 2
				.then(() => ids.next(key)); // 3
			await expect(promise).resolves.toEqual('3');
			promise = ids.next(key) // 4
				.then(() => ids.next(key)) // 5
				.then(() => ids.next(key)); // 6
			await expect(promise).resolves.toEqual('6');
		});

		test('parallel', async () => {
			expect.assertions(3);

			const promise1 = ids.next(key);
			const promise2 = ids.next(key);
			const promise3 = ids.next(key);

			await expect(promise1).resolves.toEqual('1');
			await expect(promise2).resolves.toEqual('2');
			await expect(promise3).resolves.toEqual('3');
		});
	});

	describe('join', () => {
		let resolveCount;
		let rejectCount;
		let resolves = [];
		let rejects = [];
		const countResolution = utils.join(() => {
			const promise = new Promise((resolve, reject) => {
				resolves.push(resolve);
				rejects.push(reject);
			}).then((value) => { resolveCount += 1; return value; }, (value) => { rejectCount += 1; return Promise.reject(value); });
			return [promise, resolves[resolves.length - 1], rejects[rejects.length - 1]];
		});

		beforeEach(() => {
			resolveCount = 0;
			rejectCount = 0;
			resolves = [];
			rejects = [];
		});

		describe('just one', () => {
			test('resolve', async () => {
				expect.assertions(7);
				const promise = countResolution().then(
					(v) => { expect(v).toBe('a'); },
					() => { throw new Error('should resolve'); },
				);

				expect(resolves.length).toBe(1);
				expect(rejects.length).toBe(1);
				expect(resolveCount).toBe(0);
				expect(rejectCount).toBe(0);

				resolves[0]('a');

				await promise;

				expect(resolveCount).toBe(1);
				expect(rejectCount).toBe(0);
			});

			test('reject', async () => {
				expect.assertions(7);
				const promise = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('b'); },
				);

				expect(resolves.length).toBe(1);
				expect(rejects.length).toBe(1);
				expect(resolveCount).toBe(0);
				expect(rejectCount).toBe(0);

				rejects[0]('b');

				await promise;

				expect(resolveCount).toBe(0);
				expect(rejectCount).toBe(1);
			});
		});

		describe('a few', () => {
			test('resolve', async () => {
				expect.assertions(9);
				const promise0 = countResolution().then(
					(v) => { expect(v).toBe('h'); },
					() => { throw new Error('should resolve'); },
				);
				const promise1 = countResolution().then(
					(v) => { expect(v).toBe('h'); },
					() => { throw new Error('should resolve'); },
				);
				const promise2 = countResolution().then(
					(v) => { expect(v).toBe('h'); },
					() => { throw new Error('should resolve'); },
				);

				expect(resolves.length).toBe(3);
				expect(rejects.length).toBe(3);
				expect(resolveCount).toBe(0);
				expect(rejectCount).toBe(0);

				resolves[1]('h');

				await promise0;

				expect(resolveCount).toBe(3);
				expect(rejectCount).toBe(0);

				await promise1;
				await promise2;
			});

			test('reject', async () => {
				expect.assertions(9);
				const promise0 = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('k'); },
				);
				const promise1 = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('k'); },
				);
				const promise2 = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('k'); },
				);

				expect(resolves.length).toBe(3);
				expect(rejects.length).toBe(3);
				expect(resolveCount).toBe(0);
				expect(rejectCount).toBe(0);

				rejects[1]('k');

				await promise2;

				expect(resolveCount).toBe(0);
				expect(rejectCount).toBe(3);

				await promise0;
				await promise1;
			});
		});

		describe('separate', () => {
			test('resolve', async () => {
				expect.assertions(4);
				const promise0 = countResolution().then(
					(v) => { expect(v).toBe('a'); },
					() => { throw new Error('should resolve'); },
				);
				const promise1 = countResolution().then(
					(v) => { expect(v).toBe('a'); },
					() => { throw new Error('should resolve'); },
				);

				resolves[0]('a');

				await promise0;
				await promise1;

				const promise2 = countResolution().then(
					(v) => { expect(v).toBe('b'); },
					() => { throw new Error('should resolve'); },
				);
				const promise3 = countResolution().then(
					(v) => { expect(v).toBe('b'); },
					() => { throw new Error('should resolve'); },
				);

				resolves[3]('b');

				await promise2;
				await promise3;
			});

			test('reject', async () => {
				expect.assertions(4);
				const promise0 = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('c'); },
				);
				const promise1 = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('c'); },
				);

				rejects[1]('c');

				await promise0;
				await promise1;

				const promise2 = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('d'); },
				);
				const promise3 = countResolution().then(
					() => { throw new Error('should reject'); },
					(v) => { expect(v).toBe('d'); },
				);

				rejects[2]('d');

				await promise2;
				await promise3;
			});
		});
	});
});