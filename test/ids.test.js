const config = require('../src/config');

const ids = require('../src/ids');

describe('ids', () => {
	test('ids.integration', () => {
		expect.assertions(1);
		expect(Object.keys(ids)).toEqual([
			'next',
		]);
	});

	const key = 'ids.someID';

	describe('next', () => {
		test('single, new', async () => {
			expect.assertions(2);
			expect(config.units.data).not.toHaveProperty(key);
			await expect(ids.next(key)).resolves.toEqual('1');
		});

		test('single, new specified', async () => {
			expect.assertions(2);
			expect(config.units.data).not.toHaveProperty(key);
			await expect(ids.next(key, '100')).resolves.toEqual('101');
		});

		test('single, existing', async () => {
			expect.assertions(1);
			config.units.data[key] = 'a';
			await expect(ids.next(key)).resolves.toEqual('b');
		});

		test('single, existing specified', async () => {
			expect.assertions(1);
			config.units.data[key] = 'i';
			await expect(ids.next(key, 'a')).resolves.toEqual('j');
		});

		test('multiple, serial', async () => {
			expect.assertions(3);
			await expect(ids.next(key)).resolves.toEqual('1');
			await expect(ids.next(key)).resolves.toEqual('2');
			await expect(ids.next(key)).resolves.toEqual('3');
		});

		test('multiple, chained', async () => {
			expect.assertions(2);
			let promise = ids.next(key) // 1
				.then(() => ids.next(key)) // 2
				.then(() => ids.next(key)); // 3
			await expect(promise).resolves.toEqual('3');
			promise = ids.next(key) // 4
				.then(() => ids.next(key)) // 5
				.then(() => ids.next(key)); // 6
			await expect(promise).resolves.toEqual('6');
		});

		test('multiple, parallel', async () => {
			expect.assertions(3);

			const promise1 = ids.next(key);
			const promise2 = ids.next(key);
			const promise3 = ids.next(key);

			await expect(promise1).resolves.toEqual('1');
			await expect(promise2).resolves.toEqual('2');
			await expect(promise3).resolves.toEqual('3');
		});
	});
});