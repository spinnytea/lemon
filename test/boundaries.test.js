const boundaries = require('../src/boundaries');

describe('boundaries', () => {
	beforeEach(() => {
		boundaries.setTimeout.mockRestore();
		boundaries.clearTimeout.mockRestore();
		boundaries.existsSync.mockRestore();
		boundaries.readFileSync.mockRestore();
		boundaries.readFile.mockRestore();
		boundaries.writeFile.mockRestore();
		boundaries.deleteFile.mockRestore();
		boundaries.mkdirp.mockRestore();
	});

	test('boundaries.integration', () => {
		expect.assertions(1);
		expect(Object.keys(boundaries)).toEqual([
			'setTimeout',
			'clearTimeout',
			'existsSync',
			'readFileSync',
			'readFile',
			'writeFile',
			'deleteFile',
			'mkdirp',
		]);
	});

	// setTimeout - nothing to test, we just need to be able to mock it for testing

	// clearTimeout - nothing to test, we just need to be able to mock it for testing

	test('existsSync', () => {
		expect.assertions(2);
		expect(boundaries.existsSync('test_data/_settings_testing.json')).toBe(true);
		expect(boundaries.existsSync('test_data/_settings_testing__does_not_exist.json')).toBe(false);
		// existsSync cannot fail (it returns false if anything goes wrong)
	});

	describe('readFileSync', () => {
		test('success', () => {
			expect.assertions(1);
			expect(boundaries.readFileSync('test_data/_settings_testing.json', { encoding: 'utf8' })).toEqual('{\n  "peanutbutter": "jelly"\n}');
		});

		test('failure', () => {
			expect.assertions(1);
			expect(() => boundaries.readFileSync('test_data/_settings_testing__does_not_exist.json', { encoding: 'utf8' }))
				.toThrow(/^ENOENT: no such file or directory, open 'test_data\/_settings_testing__does_not_exist.json'$/);
		});
	});

	describe('readFile', () => {
		test('resolve', () => {
			expect.assertions(1);
			// REVIEW one of the times this ran, returned data was ""
			//  - figure out why
			//  - did the file not exist?
			return boundaries.readFile('test_data/_settings_testing.json', { encoding: 'utf8' })
				.then((data) => { expect(data).toEqual('{\n  "peanutbutter": "jelly"\n}'); });
		});

		test('reject', () => {
			expect.assertions(1);
			return boundaries.readFile('test_data/_settings_testing__does_not_exist.json', { encoding: 'utf8' })
				.catch((err) => { expect(err.message).toEqual("ENOENT: no such file or directory, open 'test_data/_settings_testing__does_not_exist.json'"); });
		});
	});

	describe('writeFile / deleteFile', () => {
		test('resolve', () => {
			expect.assertions(2);
			return Promise.resolve()
				.then(() => (
					boundaries.writeFile(
						'test_data/_temp_write_delete.json',
						'this file will be deleted immediately',
						{ encoding: 'utf8' },
					)
				))
				.then(
					(data) => { expect(data).toBe(undefined); },
					() => { throw new Error('should resolve'); },
				)
				.then(() => (
					boundaries.deleteFile('test_data/_temp_write_delete.json')
				))
				.then(
					(data) => { expect(data).toBe(undefined); },
					() => { throw new Error('should resolve'); },
				);
		});

		test('write reject', () => {
			expect.assertions(1);

			// cannot write to a folder
			return boundaries.writeFile(
				'test_data',
				'{\n  "peanutbutter": "jelly"\n}',
				{ encoding: 'utf8' },
			).then(
				() => { throw new Error('should reject'); },
				(err) => { expect(err.message).toBe("EISDIR: illegal operation on a directory, open 'test_data'"); },
			);
		});

		test('delete reject', () => {
			expect.assertions(1);

			// cannot delete a file that does not exist
			return boundaries.deleteFile(
				'test_data/_settings_testing__does_not_exist.json',
			).then(
				() => { throw new Error('should reject'); },
				(err) => { expect(err.message).toBe("ENOENT: no such file or directory, unlink 'test_data/_settings_testing__does_not_exist.json'"); },
			);
		});
	});

	describe('mkdirp', () => {
		test('resolve', () => {
			// granted, this folder already exists
			// *long rambling about what we actually need to test and if this will actually prevent all integration errors*
			// ... and therefore, this is just a trivial figurehead test to prove we can call the api
			expect.assertions(1);
			boundaries.mkdirp('test_data/_t/es');
			expect(boundaries.existsSync('test_data/_t/es')).toBe(true);
		});

		test('reject', () => {
			expect.assertions(1);
			expect(() => boundaries.mkdirp(null)).toThrow(/^The "path" argument must be of type string. Received null$/);
		});
	});
});