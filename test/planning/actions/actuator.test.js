const config = require('../../../src/config');
const ideas = require('../../../src/database/ideas');
const io = require('../../../src/database/ideas/io');
const memory = require('../../../src/database/ideas/memory');
const { Subgraph } = require('../../../src/database/subgraphs');
const actuator = require('../../../src/planning/actions/actuator');
const blueprint = require('../../../src/planning/primitives/blueprint');

const { ActuatorAction } = actuator;
const { BlueprintState } = blueprint;

/**
	justCountFunction, justCountValue
	this function lets us change something in the world
	which is just the value in this variable
*/
let justCountValue;
config.onInit((first) => {
	if (first) {
		actuator.rawCallbacks.justCountFunction = () => { justCountValue += 1; };
	}
});

describe('actuator', () => {
	beforeEach(() => {
		justCountValue = 0;
	});

	/** @example `/example/planning/ApplePie.test.js` */
	test.todo('examples');

	describe('run (replace)', () => {
		let from;
		let action;
		beforeEach(async () => {
			from = new BlueprintState(new Subgraph(), ['`action`, technically'/* action */]);
			from.state.addEdge(
				from.state.addVertex('id', ideas.proxy('_test')),
				'has',
				from.state.addVertex('id', ideas.proxy('_test2'), { name: 'theValue', transitionable: true }),
			);
			from.state.setData('theValue', { material: 'lead' });
			from.state.concrete = true;

			action = new ActuatorAction({ rawCallback: 'justCountFunction' });
			action.requirements.addEdge(
				action.requirements.addVertex('id', ideas.proxy('_test')),
				'has',
				action.requirements.addVertex('exact', { material: 'lead' }, { name: 'theValue', transitionable: true }),
			);
			action.transitions = [{ vertexId: 'theValue', replace: { material: 'gold' } }];

			// prereq: action needs to have been glued to this state
			action = (await action.calcGlue(from))[0]; // eslint-disable-line prefer-destructuring
		});

		test('verify setup', () => {
			expect.assertions(2);

			expect(action).toMatchSnapshot();
			expect(action.isGlued()).toBe(true);
		});

		test('runAbstract', async () => {
			expect.assertions(6);
			expect(justCountValue).toBe(0);

			const nextState = await action.runAbstract(from);

			// nextState has the new value
			expect(await nextState.state.getData('theValue')).toEqual({ material: 'gold' });
			expect(nextState.availableActions).toEqual(['`action`, technically']);

			// from is unchanged
			expect(await from.state.getData('theValue')).toEqual({ material: 'lead' });
			expect(from.availableActions).toEqual(['`action`, technically']);

			// we are only thinking about the world, so it shouldn't do anything
			expect(justCountValue).toBe(0);
		});

		test('runActual', async () => {
			expect.assertions(4);
			expect(justCountValue).toBe(0);

			const context = from.state;
			const nextState = await action.runActual(context);

			// nothing is returned
			expect(nextState).toBe(undefined);

			// instead, from is actually updated
			expect(await context.getData('theValue')).toEqual({ material: 'gold' });

			// we DID change the world this time
			expect(justCountValue).toBe(1);
		});

		test.todo('scheduleActual');
	});

	test('save / load', async () => {
		expect.assertions(6);

		// NOTE config.init is only run once per file
		//  - each file gets it's own sandbox
		//  - by the time we get here, the memory/database have been reset
		//  - we need to call this again to ensure it's available for this test, since we care about it here
		await blueprint.boundaries.doInit();

		const created = new ActuatorAction({ rawCallback: 'justCountFunction' });
		created.requirements.addEdge(
			created.requirements.addVertex('id', ideas.proxy('_test')),
			'has',
			created.requirements.addVertex('exact', { material: 'lead' }, { name: 'theValue', transitionable: true }),
		);
		created.transitions = [{ vertexId: 'theValue', replace: { material: 'gold' } }];

		expect(memory.units.map).toMatchSnapshot('before memory');
		expect(io.units.database).toMatchSnapshot('before database');

		await created.save();
		await ideas.save(created.idea);
		await ideas.save(blueprint.boundaries.context); // REVIEW we need to ensure this gets saved too!
		expect(created.idea.id).toEqual('102');

		expect(memory.units.map).toMatchSnapshot('saved memory');
		expect(io.units.database).toMatchSnapshot('saved database');
		const loaded = await blueprint.load({ id: '102' });
		expect(loaded).toEqual(created);
	});
});