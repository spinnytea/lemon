const { NumberSlideState, NumberSlideAction } = require('../../../example/planning/primitives/NumberSlide');

describe('path', () => {
	/** @see astar */
	test.todo('Path');

	/**
		PathAction and PathState are abstract classes, you aren't supposed to use them directly
		NumberSlideAction and NumberSlideState are very simple impls that show how you use them

		so, the integration tests will look at those
	*/
	describe('NumberSlide', () => {
		test('NumberSlideAction', async () => {
			expect.assertions(3);

			const action = new NumberSlideAction({ dir: 'right' });

			// it's a number! ... all actions have some kind of cost
			// NumberSlideAction is so simple that it's cost is always 1
			expect(await action.cost()).toBe(1);

			// given a starting point
			const state = new NumberSlideState({
				numbers: [
					[1, 2, 3],
					[4, 5, 6],
					[0, 7, 8],
				],
			});

			// given a starting state, (if) we can run this action on it
			// it produces another state
			expect(await action.runAbstract(state)).toMatchSnapshot();

			expect(state).toMatchSnapshot('state should be unchanged');
		});

		describe('NumberSlideState', () => {
			const goal = new NumberSlideState({
				numbers: [
					[1, 2, 3],
					[4, 5, 6],
					[7, 8, 0],
				],
			});
			// state0 is the same as the goal
			const state0 = new NumberSlideState({
				numbers: [
					[1, 2, 3],
					[4, 5, 6],
					[7, 8, 0],
				],
			});
			// state1 has the zero slide to the left once
			const state1 = new NumberSlideState({
				numbers: [
					[1, 2, 3],
					[4, 5, 6],
					[7, 0, 8],
				],
			});
			// state2 has the zero slide to the left twice
			const state2 = new NumberSlideState({
				numbers: [
					[1, 2, 3],
					[4, 5, 6],
					[0, 7, 8],
				],
			});

			test('matches', async () => {
				expect.assertions(3);

				// obvious checks are obvious

				// by definition, state0 matches the goal
				expect(await state0.matches(goal)).toBe(true);

				// by definition, state1 and state2 do not
				expect(await state1.matches(goal)).toBe(false);
				expect(await state2.matches(goal)).toBe(false);
			});

			test('distance', async () => {
				expect.assertions(3);

				// state0 is the same, so the distance is zero
				expect(await state0.distance(goal)).toBe(0);

				// state1 and state2 are different
				// state1 is closer to the goal than state2
				expect(await state1.distance(goal)).toBe(2); // manhattan distance = 1 + 1
				expect(await state2.distance(goal)).toBe(4); // manhattan distance = 2 + 1 + 1
			});

			test('actions', async () => {
				expect.assertions(6);

				// state2 is in the corner; zero can move up and right
				expect(await state2.actions()).toMatchSnapshot('state2');
				expect((await state2.actions()).map((a) => a.dir).sort()).toEqual(['right', 'up']);

				// state1 is in the middle bottom; zero can move left, up, and right
				expect(await state1.actions()).toMatchSnapshot('state1');
				expect((await state1.actions()).map((a) => a.dir).sort()).toEqual(['left', 'right', 'up']);

				// state0 matches the goal, but it still has actions that it can perform (goal met or otherwise)
				// in practice, we shouldn't ever expand this (since we are at the goal, there isn't a need to do anything else)
				// but it is important to note that it can/should/will produce actions
				//     it is important to note that actions do not depend on the goal
				// ----
				// another way of saying it is
				// the Path, and the algorithms for getting to the goals care about which paths to pick next
				// PathState and PathActions are two half of a whole
				// from a given state, here are all the actions that are possible
				// then it's up to the algorithm to prune them
				// PathState.actions just computes the available ones
				// (separation of concerns)
				expect(await state0.actions()).toMatchSnapshot('state0');
				expect((await state0.actions()).map((a) => a.dir).sort()).toEqual(['left', 'up']);
			});
		});
	});
});