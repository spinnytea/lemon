const ideas = require('../../../src/database/ideas');
const { search, Subgraph } = require('../../../src/database/subgraphs');
const subgraphs = require('../../../src/database/subgraphs/subgraphs');
const blueprint = require('../../../src/planning/primitives/blueprint');

const { BlueprintState } = blueprint;

describe('blueprint', () => {
	test('blueprint.integration', () => {
		expect.assertions(1);
		expect(Object.keys(blueprint)).toEqual([
			'BlueprintAction',
			'BlueprintState',
			'load',
			'fromContext',
			'createActionParser',
			'createStartAndGoal',
		]);
	});

	/** @example `/example/planning/ApplePie.test.js` */
	test.todo('BlueprintAction');

	/** @example `/example/planning/ApplePie.test.js` */
	test.todo('BlueprintState');

	test.todo('load');

	test.todo('fromContext');

	test.todo('createActionParser');

	test.todo('createStartAndGoal');

	describe('integration bugs', () => {
		test('start uses orData to match goal', async () => {
			expect.assertions(6);

			// TODO simplify this test, move it to units?
			//  - getVertexDataForDiff needs to handle an inconcrete goal when we can't get actual inner data
			const ideaMap = {
				mark: { name: 'mark' },
				store: { name: 'store' },
				school: { name: 'school' },
				home: { name: 'home' },
				location_mark: { name: 'home' },
				place: { name: 'place' },
			};
			await ideas.createGraph(ideaMap, [
				['location_mark', 'has', 'mark'],
				['store', 'typeOf', 'place'],
				['school', 'typeOf', 'place'],
				['home', 'typeOf', 'place'],
				['location_mark', 'typeOf', 'place'],
			]);

			const sg = new Subgraph();
			sg.addVertex('id', ideaMap.mark, { name: 'mark' });
			sg.addVertex('id', ideaMap.store, { name: 'store' });
			sg.addVertex('id', ideaMap.school, { name: 'school' });
			sg.addVertex('id', ideaMap.home, { name: 'home' });
			sg.addVertex('exact', [{ name: 'store' }, { name: 'school' }, { name: 'home' }], { name: 'location_mark', orData: true, transitionable: true });
			sg.addVertex('id', ideaMap.place, { name: 'place' });
			sg.addEdge('location_mark', 'has', 'mark');
			sg.addEdge('store', 'typeOf', 'place');
			sg.addEdge('school', 'typeOf', 'place');
			sg.addEdge('home', 'typeOf', 'place');
			sg.addEdge('location_mark', 'typeOf', 'place');
			const searchResults = await search(sg);
			expect(searchResults).toEqual(expect.any(Array));
			expect(searchResults.length).toBe(1);
			const worldContext = searchResults[0];
			expect(worldContext.concrete).toBe(true);

			const start = new BlueprintState(worldContext, []);
			const goal = new BlueprintState(new Subgraph(), []);
			goal.state.addVertex('exact', { name: 'home' }, { name: 'location_mark', transitionable: true });
			goal.state.addVertex('id', ideaMap.mark, { name: 'mark' });
			goal.state.addVertex('id', ideaMap.place, { name: 'place' });
			goal.state.addEdge('location_mark', 'has', 'mark');
			goal.state.addEdge('location_mark', 'typeOf', 'place');

			expect(await start.distance(goal)).toEqual(0);

			goal.state.getVertex('location_mark').data.name = 'store';

			expect(await start.distance(goal)).toEqual(1);

			goal.state.getVertex('location_mark').matcher = subgraphs.units.matchers.filler;
			goal.state.getVertex('location_mark').data.name = undefined;

			expect(await start.distance(goal)).toEqual(0);
		});
	});
});