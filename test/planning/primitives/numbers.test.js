const numbers = require('../../../src/planning/primitives/numbers');

describe('numbers', () => {
	test('numbers.integration', () => {
		expect.assertions(1);
		expect(Object.keys(numbers)).toEqual([
			'isNumber',
			'cast',
			'match',
			'value',
			'combine',
			'remove',
			'difference',
		]);
	});

	test('some examples', () => {
		expect.assertions(4);

		const num = numbers.cast({
			value: numbers.value(10),
			$unit: '_test',
		});

		expect(num).toMatchSnapshot('num');
		expect(numbers.isNumber(num)).toBe(true);

		const two = numbers.combine(num, num);
		expect(two).toMatchSnapshot('two');

		const one = numbers.remove(two, num);
		expect(one).toMatchSnapshot('one');
	});

	test('difference example', async () => {
		expect.assertions(1);

		const one = numbers.cast({
			value: numbers.value(10),
			$unit: '_test',
		});
		const two = numbers.combine(one, one);

		expect(await numbers.difference(one, two)).toBe(10);
	});
});