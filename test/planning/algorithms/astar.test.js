const { NumberSlideState } = require('../../../example/planning/primitives/NumberSlide');
const astar = require('../../../src/planning/algorithms/astar');

describe('astar', () => {
	test('astar.integration', () => {
		expect.assertions(1);
		expect(Object.keys(astar)).toEqual([
			'search',
		]);
	});

	/**
		NumberSlide is simple enough that we can easily see the answer
		this proves to us that astar produces a good result
	*/
	test('NumberSlide puzzle', async () => {
		expect.assertions(5);

		const goal = new NumberSlideState({
			numbers: [
				[1, 2, 3],
				[4, 5, 6],
				[7, 8, 0],
			],
		});
		const start = new NumberSlideState({
			numbers: [
				[2, 5, 0],
				[1, 6, 3],
				[4, 7, 8],
			],
		});

		jest.spyOn(console, 'log').mockImplementation(() => {});

		astar.boundaries.debugLogs = true;
		const path = await astar.search(start, goal);
		astar.boundaries.debugLogs = false;

		expect(path).toMatchSnapshot();

		// spot check a few things, just to make the point
		expect(path.states[0].numbers).toEqual(start.numbers);
		expect(path.last.numbers).toEqual(goal.numbers);

		// verify actions, just to make the point
		expect(path.actions.map((a) => a.dir))
			.toEqual(['down', 'left', 'up', 'left', 'down', 'down', 'right', 'right']);

		expect(console.log.mock.calls).toMatchSnapshot(); // eslint-disable-line no-console
	});

	/**
		Blueprints are inherently complex, so this example may be hard to follow
		but it proves that astar can be used with a Blueprint / a Blueprint can be used with astar
	*/
	test.todo('Blueprint example');
});