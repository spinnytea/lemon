const boundaries = require('./src/boundaries');
const config = require('./src/config');

beforeEach(async () => {
	Object.keys(boundaries).forEach((key) => {
		jest.spyOn(boundaries, key).mockImplementation(() => { throw new Error(`must mock behavior for "${key}"`); });
	});

	await config.init({
		in_memory: true,
		// location: 'test_data',
		filename: '_settings_testing',
	});
});