const _ = require('lodash');
const ideas = require('../../src/database/ideas');
const io = require('../../src/database/ideas/io');
const memory = require('../../src/database/ideas/memory');
const { createSubgraph, search, Subgraph, match } = require('../../src/database/subgraphs');
const numbers = require('../../src/planning/primitives/numbers');
const { Path } = require('../../src/planning/primitives/path');
const { BlueprintState } = require('../../src/planning/primitives/blueprint');
const astar = require('../../src/planning/algorithms/astar');
const { ActuatorAction, rawCallbacks } = require('../../src/planning/actions/actuator');
const planner = require('../../src/planning/planner');
const { SerialAction } = require('../../src/planning/actions/serial');
const { StubAction } = require('../../src/planning/actions/stub');

function simplifyPathSnapshot(path) {
	if (path) {
		path = Object.assign(new Path(path.states, path.actions, path.goals), path);
		['$goalsMet', 'goals', 'states'].forEach((prop) => { path[prop] = path[prop].length; });
		path.actions = path.actions.map((a) => a.prettyNames());
	}
	return path;
}

// REVIEW this is probably something we are going to want a lot for human feedback, maybe it should be in Subgraph?
function getWorldContextData(worldContext) {
	const vertices = Array.from(worldContext.$vertexAliases.keys()).filter((name) => worldContext.getVertex(name).transitionable).sort();
	return Promise.all(vertices.map((name) => (
		worldContext.getData(name)
	))).then((data) => _.zipObject(vertices, data));
}

/**
	1. Go to the store
	2. Buy apples, Buy apples, But crust
	3. Go to the cooking school
	4. Make apple pie
	5. Go home
	TODO 6. Share it (terminal goal, because someone wanted it)

	REVIEW we need to have extra rules for some types of numbers
	 - if there is a "real number", then it cannot go below 0
	 - e.g. money less than zero is simply unallowed, less than zero apples
	 - or does that just need to be part of all the goals?
	 - sure we are allowed to _think about_ what might happen, but then it's not _real_, what's the difference?
*/
describe('Example: Apple Pie', () => {
	let ideaMap;
	let worldContext;
	let goActuator;
	let buyActuator;
	let makePieActuator;
	let mockedActionCalls;
	beforeEach(async () => {
		// NOTE config.init is only run once per file
		//  - each file gets it's own sandbox
		//  - by the time we get here, the memory/database have been reset
		//  - we need to call this again to ensure it's available for this test, since we care about it here
		await ideas.context('blueprint');

		mockedActionCalls = jest.fn();

		/*
			Setup the world
			----
			This is our world model. It's something we've developed over time.
			This toy model we hard code for this test, it's really stupid simple.
			But really, this is supposed to be stored, saved and loaded, and built up over time.

			# things
			store --has-> apple --property-> money_apple --typeOf-> money
																		--property-> per --typeOf-> apple
			store --has-> banana --...->
			mark --has-> apple
			mark --has-> banana

			# places
			mark --property-> location --typeOf-> place
			store --typeOf-> place
			school --typeOf-> place
			home --typeOf-> place
		*/
		const apple = await ideas.create({ name: 'apple' });
		const banana = await ideas.create({ name: 'banana' });
		const crust = await ideas.create({ name: 'pie crust' });
		const applePie = await ideas.create({ name: 'apple pie' });
		const money = await ideas.create({ name: 'money' });
		ideaMap = {
			apple,
			banana,
			crust,
			applePie,
			mark: { name: 'mark' },
			store: { name: 'store' },
			school: { name: 'school' },
			home: { name: 'home' },
			location_mark: { name: 'home' },
			place: { name: 'place' },

			money,
			money_mark: numbers.cast({ value: numbers.value(100), $unit: money.id }),
			money_store: numbers.cast({ value: numbers.value(1000), $unit: money.id }),
			money_apple: numbers.cast({ value: numbers.value(10), $unit: money.id }),
			money_banana: numbers.cast({ value: numbers.value(8), $unit: money.id }),
			money_crust: numbers.cast({ value: numbers.value(15), $unit: money.id }),
			money_pie: numbers.cast({ value: numbers.value(100), $unit: money.id }),
			money_per_apple: numbers.cast({ value: numbers.value(2), $unit: apple.id }),
			money_per_banana: numbers.cast({ value: numbers.value(3), $unit: banana.id }),
			money_per_crust: numbers.cast({ value: numbers.value(1), $unit: crust.id }),
			money_per_pie: numbers.cast({ value: numbers.value(1), $unit: applePie.id }),

			mark_apple: numbers.cast({ value: numbers.value(0), $unit: apple.id }),
			mark_banana: numbers.cast({ value: numbers.value(0), $unit: banana.id }),
			mark_crust: numbers.cast({ value: numbers.value(0), $unit: crust.id }),
			mark_pie: numbers.cast({ value: numbers.value(0), $unit: applePie.id }),
			store_apple: numbers.cast({ value: numbers.value(10), $unit: apple.id }),
			store_banana: numbers.cast({ value: numbers.value(10), $unit: banana.id }),
			store_crust: numbers.cast({ value: numbers.value(10), $unit: crust.id }),
			store_pie: numbers.cast({ value: numbers.value(10), $unit: applePie.id }),

			pie_per_apple: numbers.cast({ value: numbers.value(3), $unit: apple.id }),
			pie_per_crust: numbers.cast({ value: numbers.value(1), $unit: crust.id }),

			// TODO add more things to that aren't necessarily in the context
		};
		await ideas.createGraph(ideaMap, [
			['mark', 'property', 'location_mark'],
			['mark', 'property', 'money_mark'],
			['store', 'property', 'money_store'],
			['mark', 'has', 'mark_apple'],
			['mark', 'has', 'mark_banana'],
			['mark', 'has', 'mark_crust'],
			['mark', 'has', 'mark_pie'],
			['store', 'has', 'store_apple'],
			['store', 'has', 'store_banana'],
			['store', 'has', 'store_crust'],
			['store', 'has', 'store_pie'],
			['store', 'typeOf', 'place'],
			['school', 'typeOf', 'place'],
			['home', 'typeOf', 'place'],
			['location_mark', 'typeOf', 'place'],
			['apple', 'property', 'money_apple'],
			['banana', 'property', 'money_banana'],
			['crust', 'property', 'money_crust'],
			['applePie', 'property', 'money_pie'],
			['money_apple', 'property', 'money_per_apple'],
			['money_banana', 'property', 'money_per_banana'],
			['money_crust', 'property', 'money_per_crust'],
			['money_pie', 'property', 'money_per_pie'],
			['applePie', 'property', 'pie_per_apple'],
			['applePie', 'property', 'pie_per_crust'],
			// XXX type of links are at the bottom because I want to remove them eventually
			['money_mark', 'typeOf', 'money'],
			['money_store', 'typeOf', 'money'],
			['mark_apple', 'typeOf', 'apple'],
			['mark_banana', 'typeOf', 'banana'],
			['mark_crust', 'typeOf', 'crust'],
			['mark_pie', 'typeOf', 'applePie'],
			['store_apple', 'typeOf', 'apple'],
			['store_banana', 'typeOf', 'banana'],
			['store_crust', 'typeOf', 'crust'],
			['store_pie', 'typeOf', 'applePie'],
			['money_apple', 'typeOf', 'money'],
			['money_banana', 'typeOf', 'money'],
			['money_crust', 'typeOf', 'money'],
			['money_pie', 'typeOf', 'money'],
			['money_per_apple', 'typeOf', 'apple'],
			['money_per_banana', 'typeOf', 'banana'],
			['money_per_crust', 'typeOf', 'crust'],
			['money_per_pie', 'typeOf', 'applePie'],
			['pie_per_apple', 'typeOf', 'apple'],
			['pie_per_crust', 'typeOf', 'crust'],
		]);

		/*
			Setup Context
			----
			This is one of our early models where everything is defined up front, and nothing is discovered.
			Eventually, we'll start with an empty context graph, and build it up by observing.
		*/
		const sg = createSubgraph([
			['id', ideaMap.apple, { name: 'apple' }],
			['id', ideaMap.banana, { name: 'banana' }],
			['id', ideaMap.crust, { name: 'crust' }],
			['id', ideaMap.applePie, { name: 'applePie' }],
			['id', ideaMap.mark, { name: 'mark' }],
			['id', ideaMap.store, { name: 'store' }],
			['id', ideaMap.school, { name: 'school' }],
			['id', ideaMap.home, { name: 'home' }],
			// REVIEW can the location_mark vertex be id/pointer instead?
			['exact', [{ name: 'store' }, { name: 'school' }, { name: 'home' }], { name: 'location_mark', orData: true, transitionable: true }],
			['id', ideaMap.place, { name: 'place' }],
			['id', ideaMap.money, { name: 'money' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: money.id }, { name: 'money_apple' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: money.id }, { name: 'money_banana' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: money.id }, { name: 'money_crust' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: money.id }, { name: 'money_pie' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: apple.id }, { name: 'money_per_apple' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: banana.id }, { name: 'money_per_banana' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: crust.id }, { name: 'money_per_crust' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: applePie.id }, { name: 'money_per_pie' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: apple.id }, { name: 'pie_per_apple' }],
			['numbers', { value: numbers.value(0, Infinity), $unit: crust.id }, { name: 'pie_per_crust' }],
			['numbers', { value: numbers.value(), $unit: apple.id }, { name: 'mark_apple', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: banana.id }, { name: 'mark_banana', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: crust.id }, { name: 'mark_crust', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: applePie.id }, { name: 'mark_pie', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: apple.id }, { name: 'store_apple', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: banana.id }, { name: 'store_banana', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: crust.id }, { name: 'store_crust', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: applePie.id }, { name: 'store_pie', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: money.id }, { name: 'money_mark', transitionable: true }],
			['numbers', { value: numbers.value(), $unit: money.id }, { name: 'money_store', transitionable: true }],
		], [
			['mark', 'property', 'location_mark'],
			['mark', 'property', 'money_mark'],
			['store', 'property', 'money_store'],
			['mark', 'has', 'mark_apple'],
			['mark', 'has', 'mark_banana'],
			['mark', 'has', 'mark_crust'],
			['mark', 'has', 'mark_pie'],
			['store', 'has', 'store_apple'],
			['store', 'has', 'store_banana'],
			['store', 'has', 'store_crust'],
			['store', 'has', 'store_pie'],
			['store', 'typeOf', 'place'],
			['school', 'typeOf', 'place'],
			['home', 'typeOf', 'place'],
			['location_mark', 'typeOf', 'place'],
			['apple', 'property', 'money_apple'],
			['banana', 'property', 'money_banana'],
			['crust', 'property', 'money_crust'],
			['applePie', 'property', 'money_pie'],
			['money_apple', 'property', 'money_per_apple'],
			['money_banana', 'property', 'money_per_banana'],
			['money_crust', 'property', 'money_per_crust'],
			['money_pie', 'property', 'money_per_pie'],
			['applePie', 'property', 'pie_per_apple'],
			['applePie', 'property', 'pie_per_crust'],
			// XXX type of links are at the bottom because I want to remove them eventually
			['money_mark', 'typeOf', 'money'],
			['money_store', 'typeOf', 'money'],
			['mark_apple', 'typeOf', 'apple'],
			['mark_banana', 'typeOf', 'banana'],
			['mark_crust', 'typeOf', 'crust'],
			['mark_pie', 'typeOf', 'applePie'],
			['store_apple', 'typeOf', 'apple'],
			['store_banana', 'typeOf', 'banana'],
			['store_crust', 'typeOf', 'crust'],
			['store_pie', 'typeOf', 'applePie'],
			['money_apple', 'typeOf', 'money'],
			['money_banana', 'typeOf', 'money'],
			['money_crust', 'typeOf', 'money'],
			['money_pie', 'typeOf', 'money'],
			['money_per_apple', 'typeOf', 'apple'],
			['money_per_banana', 'typeOf', 'banana'],
			['money_per_crust', 'typeOf', 'crust'],
			['money_per_pie', 'typeOf', 'applePie'],
			['pie_per_apple', 'typeOf', 'apple'],
			['pie_per_crust', 'typeOf', 'crust'],
		]);

		const searchResults = await search(sg);
		expect(searchResults).toEqual(expect.any(Array));
		expect(searchResults.length).toBe(1);
		([worldContext] = searchResults);
		expect(worldContext.concrete).toBe(true);

		/*
			Setup Go Actuators
		*/

		// REVIEW I'd like to say "you cannot go here and then go there without doing anything in between"
		//  - because that doesn't make sense
		//  - but this might be a special case (e.g. i WANT to buy things back to back - do I? maybe I want to fill a cart and buy once?)
		//  - is this a special case or is there a general rule?
		rawCallbacks.goActuator = (args) => mockedActionCalls('goActuator', args);
		goActuator = new ActuatorAction({
			rawCallback: 'goActuator',
			rawCallbackArgs: ['pointB'],
			transitions: [
				// IDEA if the "go" cost is too low, we keep trying to go anywhere before/after/during _everything_
				//  - how do we balance the costs?
				//  - do we need to add, like, "energy" to each action? (or "unaccountedCost" or something)
				//  - "energy" would be something we learn an adjust over time, and represents factors that we do not yet know about
				//  - e.g. the cost of going somewhere is time, and fuel (by car, etc)
				//  - so like, yes, i moved to a new "location" and that accounts for didly (cost of 1 for difference is fine)
				//  - but this action is missing SO MUCH depth, it doesn't match the real world meaning of "go"
				{ vertexId: 'location_mark', replaceId: 'pointB', cost: 100 },
			],
			requirements: createSubgraph([
				['id', ideaMap.location_mark, { name: 'location_mark', transitionable: true }],
				['id', ideaMap.mark, { name: 'mark' }],
				['id', ideaMap.place, { name: 'place' }],
				['filler', undefined, { name: 'pointB' }],
			], [
				['mark', 'property', 'location_mark'],
				['location_mark', 'typeOf', 'place'],
				['pointB', 'typeOf', 'place'],
			]),
		});

		const checkGoActuatorRequirements = await match(worldContext, goActuator.requirements);
		// expect(checkGoActuatorRequirements).toMatchSnapshot('check goActuator.requirements');
		expect(await Promise.all(checkGoActuatorRequirements.map(({ innerToOuterVertexMap }) => (
			worldContext.getData(innerToOuterVertexMap.get(goActuator.requirements.$refVertex('pointB')))
		)))).toEqual([{ name: 'store' }, { name: 'school' }, { name: 'home' }]);

		/*
			Setup Buy Actuators
		*/

		rawCallbacks.buyActuator = (args) => mockedActionCalls('buyActuator', args);
		buyActuator = new ActuatorAction({
			rawCallback: 'buyActuator',
			rawCallbackArgs: ['someone', 'item'],
			transitions: [
				{ vertexId: 'store_item', numbersRemoveId: 'money_per_item' },
				{ vertexId: 'someone_item', numbersCombineId: 'money_per_item' },
				{ vertexId: 'money_store', numbersCombineId: 'money_item' },
				{ vertexId: 'money_someone', numbersRemoveId: 'money_item' },
			],
			requirements: createSubgraph([
				['id', ideaMap.store, { name: 'store' }],
				['id', ideaMap.place, { name: 'place' }],
				['id', ideaMap.money, { name: 'money' }],
				['filler', undefined, { name: 'someone' }],
				['exact', 'store', { name: 'location_someone', pointer: true }],
				['filler', undefined, { name: 'item' }],
				['filler', undefined, { name: 'store_item' }],
				['filler', undefined, { name: 'someone_item' }],
				['filler', undefined, { name: 'money_item' }],
				['filler', undefined, { name: 'money_per_item' }],
				['numbers', { value: numbers.value(), $unit: money.id }, { name: 'money_someone' }],
				['numbers', { value: numbers.value(), $unit: money.id }, { name: 'money_store' }],
			], [
				['someone', 'property', 'location_someone'],
				['location_someone', 'typeOf', 'place'],
				['store', 'has', 'store_item'],
				['someone', 'has', 'someone_item'],
				['store_item', 'typeOf', 'item'],
				['someone_item', 'typeOf', 'item'],
				['item', 'property', 'money_item'],
				['money_item', 'property', 'money_per_item'],
				['money_item', 'typeOf', 'money'],
				['money_per_item', 'typeOf', 'item'],
				['someone', 'property', 'money_someone'],
				['store', 'property', 'money_store'],
				['money_someone', 'typeOf', 'money'],
				['money_store', 'typeOf', 'money'],
			]),
		});

		// cannot buy at home; if we set the state to be at the store, then we can buy apples and bananas
		const checkBuyActuatorRequirements = await match(worldContext, buyActuator.requirements);
		expect(await Promise.all(checkBuyActuatorRequirements.map(({ innerToOuterVertexMap }) => (
			worldContext.getData(innerToOuterVertexMap.get(buyActuator.requirements.$refVertex('item')))
		)))).toEqual([]);

		/*
			Setup Make Pie Actuators
		*/

		rawCallbacks.makePieActuator = (args) => mockedActionCalls('makePieActuator', args);
		makePieActuator = new ActuatorAction({
			rawCallback: 'makePieActuator',
			transitions: [
				// REVIEW should there be a pie recipe instead of hardcoding the numbers?
				//  - what's the difference between storing the value directly in the requirements vs somewhere in the graph?
				//  - what does it really mean to access and use the values
				//  - (here it _feels_ like a one-off and in the graph it _feels_ more reusable, but how do we learn/use each of them?)
				{ vertexId: 'someone_apple', numbersRemove: numbers.cast({ value: numbers.value(3), $unit: apple.id }) },
				{ vertexId: 'someone_crust', numbersRemove: numbers.cast({ value: numbers.value(1), $unit: crust.id }) },
				{ vertexId: 'someone_pie', numbersCombine: numbers.cast({ value: numbers.value(1), $unit: applePie.id }) },
			],
			requirements: createSubgraph([
				['id', ideaMap.school, { name: 'school' }],
				['id', ideaMap.place, { name: 'place' }],
				['filler', undefined, { name: 'someone' }],
				['exact', 'school', { name: 'location_someone', pointer: true }],
				['id', apple.id, { name: 'apple' }],
				['id', crust.id, { name: 'crust' }],
				['id', applePie.id, { name: 'pie' }],
				['numbers', numbers.cast({ value: numbers.value(3, Infinity), $unit: apple.id }), { name: 'someone_apple' }],
				['numbers', numbers.cast({ value: numbers.value(1, Infinity), $unit: crust.id }), { name: 'someone_crust' }],
				['numbers', numbers.cast({ value: numbers.value(), $unit: applePie.id }), { name: 'someone_pie' }],
			], [
				['someone', 'property', 'location_someone'],
				['location_someone', 'typeOf', 'place'],
				['someone', 'has', 'someone_apple'],
				['someone', 'has', 'someone_crust'],
				['someone', 'has', 'someone_pie'],
				['someone_apple', 'typeOf', 'apple'],
				['someone_crust', 'typeOf', 'crust'],
				['someone_pie', 'typeOf', 'pie'],
			]),
		});

		// missing reqs to make pie
		// set the location to school, apples to 3 and crust to 1
		const checkMakePieActuatorRequirements = await match(worldContext, makePieActuator.requirements);
		expect(await Promise.all(checkMakePieActuatorRequirements.map(({ innerToOuterVertexMap }) => (
			Promise.all([
				worldContext.getData(innerToOuterVertexMap.get(makePieActuator.requirements.$refVertex('location_someone'))),
				worldContext.getData(innerToOuterVertexMap.get(makePieActuator.requirements.$refVertex('someone_apple'))),
				worldContext.getData(innerToOuterVertexMap.get(makePieActuator.requirements.$refVertex('someone_crust'))),
				worldContext.getData(innerToOuterVertexMap.get(makePieActuator.requirements.$refVertex('someone_pie'))),
			])
		)))).toEqual([]);
	});

	test('setup', () => {
		expect({ memory: memory.units.map, database: io.units.database }).toMatchSnapshot('memory/database');
		expect(worldContext).toMatchSnapshot('worldContext');
		return expect(getWorldContextData(worldContext)).resolves.toMatchSnapshot('worldContext data');
	});

	function makeGoal({ locationMark, markApple, markCrust, markPie }) {
		const { apple, crust, applePie } = ideaMap;

		const goal = new BlueprintState(new Subgraph(), []);
		goal.state.addVertex('exact', locationMark, { name: 'location_mark', transitionable: true });
		goal.state.addVertex('id', ideaMap.mark, { name: 'mark' });
		goal.state.addVertex('id', ideaMap.place, { name: 'place' });
		goal.state.addVertex('id', apple, { name: 'apple' });
		goal.state.addVertex('id', crust, { name: 'crust' });
		goal.state.addVertex('id', applePie, { name: 'pie' });
		goal.state.addVertex('numbers', markApple, { name: 'mark_apple', transitionable: true });
		goal.state.addVertex('numbers', markCrust, { name: 'mark_crust', transitionable: true });
		goal.state.addVertex('numbers', markPie, { name: 'mark_pie', transitionable: true });
		goal.state.addEdge('mark', 'property', 'location_mark');
		goal.state.addEdge('location_mark', 'typeOf', 'place');
		goal.state.addEdge('mark', 'has', 'mark_apple');
		goal.state.addEdge('mark', 'has', 'mark_crust');
		goal.state.addEdge('mark', 'has', 'mark_pie');
		goal.state.addEdge('mark_apple', 'typeOf', 'apple');
		goal.state.addEdge('mark_crust', 'typeOf', 'crust');
		goal.state.addEdge('mark_pie', 'typeOf', 'pie');

		return goal;
	}

	// as you can see, we've broken this up into stages; which is the whole point of 'smaller goals' // TODO hopefully I can pull that out
	// this is why we need stubs or milestones and to from the plan from the top down
	// TODO but still, planning from the end backwards will also help
	test('astar and actuators', async () => {
		const { apple, crust, applePie } = ideaMap;

		const start = new BlueprintState(worldContext, [goActuator, buyActuator, makePieActuator]);
		const goal = makeGoal({
			locationMark: { name: 'store' }, // temp goal
			markApple: { value: numbers.value(3, 5), $unit: apple.id }, // TODO we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(1, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(0, 3), $unit: applePie.id }, // temp goal
		});

		const checkDistFromStart = await start.distance(goal);
		expect(checkDistFromStart).toEqual(5); // just missing a pie

		jest.spyOn(astar.boundaries, 'log').mockImplementation(() => {});
		astar.boundaries.debugLogs = true;
		const path = await astar.search(start, goal);
		astar.boundaries.debugLogs = false;
		expect(astar.boundaries.log.mock.calls).toMatchSnapshot('astar trace');

		expect(simplifyPathSnapshot(path)).toMatchSnapshot('final path summary');

		/* new goal! */

		goal.state.setData('location_mark', { name: 'school' });
		goal.state.setData('mark_apple', { value: numbers.value(0, 5), $unit: apple.id });
		goal.state.setData('mark_crust', { value: numbers.value(0, 2), $unit: crust.id });
		goal.state.setData('mark_pie', { value: numbers.value(1, 3), $unit: applePie.id });

		astar.boundaries.log.mockReset();
		astar.boundaries.debugLogs = true;
		const path2 = await astar.search(path.last, goal); // start from where we left off
		astar.boundaries.debugLogs = false;
		expect(astar.boundaries.log.mock.calls).toMatchSnapshot('astar trace');

		expect(simplifyPathSnapshot(path2)).toMatchSnapshot('final path summary');

		/* final goal! */

		goal.state.setData('location_mark', { name: 'home' });
		goal.state.setData('mark_apple', { value: numbers.value(0, 5), $unit: apple.id });
		goal.state.setData('mark_crust', { value: numbers.value(0, 2), $unit: crust.id });
		goal.state.setData('mark_pie', { value: numbers.value(1, 3), $unit: applePie.id });

		astar.boundaries.log.mockReset();
		astar.boundaries.debugLogs = true;
		const path3 = await astar.search(path2.last, goal); // start from where we left off
		astar.boundaries.debugLogs = false;
		expect(astar.boundaries.log.mock.calls).toMatchSnapshot('astar trace');

		expect(simplifyPathSnapshot(path3)).toMatchSnapshot('final path summary');
	});

	// no matter how good you are at planning, breaking down the goal into milestones will always help
	// you can just work towards a smaller, short term goal first
	test('smaller goals', async () => {
		const { apple, crust, applePie } = ideaMap;

		const start = new BlueprintState(worldContext, [goActuator, buyActuator, makePieActuator]);
		const goalBuyStuff = makeGoal({
			locationMark: { name: 'store' },
			markApple: { value: numbers.value(3, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(1, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(0, 3), $unit: applePie.id },
		});
		const goalMakeStuff = makeGoal({
			locationMark: { name: 'school' },
			markApple: { value: numbers.value(0, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(0, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(1, 3), $unit: applePie.id },
		});
		const goalGetHomeNStuff = makeGoal({
			locationMark: { name: 'home' },
			markApple: { value: numbers.value(0, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(0, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(1, 3), $unit: applePie.id },
		});

		const action = await planner.create(start, [goalBuyStuff, goalMakeStuff, goalGetHomeNStuff]);
		expect(action.prettyNames()).toMatchSnapshot('final action summary');

		expect(mockedActionCalls).not.toHaveBeenCalled();
		await action.runActual(worldContext);
		expect(mockedActionCalls.mock.calls).toEqual([
			['goActuator', { pointB: { name: 'store' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'apple' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'apple' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'pie crust' } }],
			['goActuator', { pointB: { name: 'school' } }],
			['makePieActuator', {}],
			['goActuator', { pointB: { name: 'home' } }],
		]);

		expect(await getWorldContextData(worldContext)).toMatchSnapshot('worldContext data');
	});

	// REVIEW why does this take 4 times as long as 'astar and actuators'? (500 ms vs 2000 ms)
	//  - obviously because it's trying to do the whole thing at once
	//  - but like, once it meets the first goal, can we avoid backtracking to resolve it?
	//  - or what else is happening here
	test('serial action', async () => {
		// NOTE this is BASICALLY what I had to do in 'astar and actuators' above (should we try to remove those tests?)
		// this is a more formalized verison of that
		const { apple, crust, applePie } = ideaMap;

		// this particular serial plan has a problem that it says "buy anything, three times"
		// there are 4 things to buy, so it makes 4^3 = 64 plans, which then de-dup down to about 20
		const buyItAllPlan = new SerialAction({ steps: [buyActuator, buyActuator, buyActuator] });

		const start = new BlueprintState(worldContext, [goActuator, buyItAllPlan, makePieActuator]);
		const goalBuyStuff = makeGoal({
			locationMark: { name: 'store' },
			markApple: { value: numbers.value(3, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(1, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(0, 3), $unit: applePie.id },
		});
		const goalMakeStuff = makeGoal({
			locationMark: { name: 'school' },
			markApple: { value: numbers.value(0, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(0, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(1, 3), $unit: applePie.id },
		});
		const goalGetHomeNStuff = makeGoal({
			locationMark: { name: 'home' },
			markApple: { value: numbers.value(0, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(0, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(1, 3), $unit: applePie.id },
		});

		jest.spyOn(astar.boundaries, 'log').mockImplementation(() => {});
		astar.boundaries.debugLogs = true;
		const action = await planner.create(start, [goalBuyStuff, goalMakeStuff, goalGetHomeNStuff]);
		astar.boundaries.debugLogs = false;
		expect(astar.boundaries.log.mock.calls).toMatchSnapshot('astar trace');

		expect(action?.prettyNames()).toMatchSnapshot('final action summary');

		expect(mockedActionCalls).not.toHaveBeenCalled();
		await action.runActual(worldContext);
		expect(mockedActionCalls.mock.calls).toEqual([
			['goActuator', { pointB: { name: 'store' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'apple' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'apple' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'pie crust' } }],
			['goActuator', { pointB: { name: 'school' } }],
			['makePieActuator', {}],
			['goActuator', { pointB: { name: 'home' } }],
		]);

		expect(await getWorldContextData(worldContext)).toMatchSnapshot('worldContext data');
	});

	test('with stubs (create)', async () => {
		const { apple, crust, applePie } = ideaMap;
		const buyItAllStub = new StubAction({
			solveAt: 'create',
			transitions: [
				{ vertexId: 'someone_apple', numbersCombine: numbers.cast({ value: numbers.value(3, 5), $unit: apple.id }) },
				{ vertexId: 'someone_crust', numbersCombine: numbers.cast({ value: numbers.value(1, 2), $unit: crust.id }) },
			],
			requirements: createSubgraph([
				['id', ideaMap.store, { name: 'store' }],
				['id', ideaMap.place, { name: 'place' }],
				['filler', undefined, { name: 'someone' }],
				['exact', 'store', { name: 'location_someone', pointer: true }],
				['id', apple.id, { name: 'apple' }],
				['id', crust.id, { name: 'crust' }],
				['numbers', { value: numbers.value(), $unit: apple.id }, { name: 'someone_apple' }],
				['numbers', { value: numbers.value(), $unit: crust.id }, { name: 'someone_crust' }],
			], [
				['someone', 'property', 'location_someone'],
				['location_someone', 'typeOf', 'place'],
				['someone', 'has', 'someone_apple'],
				['someone', 'has', 'someone_crust'],
				['someone_apple', 'typeOf', 'apple'],
				['someone_crust', 'typeOf', 'crust'],
			]),
		});

		const start = new BlueprintState(worldContext, [buyItAllStub, goActuator, buyActuator, makePieActuator]);
		const goalBuyStuff = makeGoal({
			locationMark: { name: 'store' },
			markApple: { value: numbers.value(3, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(1, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(0, 3), $unit: applePie.id },
		});
		// REVIEW for some reason, this second goal takes a LOT of steps
		const goalMakeStuff = makeGoal({
			locationMark: { name: 'school' },
			markApple: { value: numbers.value(0, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(0, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(1, 3), $unit: applePie.id },
		});
		const goalGetHomeNStuff = makeGoal({
			locationMark: { name: 'home' },
			markApple: { value: numbers.value(0, 5), $unit: apple.id }, // XXX we have to have a boundary (or else it'll go negative)
			markCrust: { value: numbers.value(0, 2), $unit: crust.id }, // XXX we have to have a boundary (or else it'll go negative)
			markPie: { value: numbers.value(1, 3), $unit: applePie.id },
		});

		jest.spyOn(astar.boundaries, 'log').mockImplementation(() => {});
		astar.boundaries.debugLogs = true;
		const action = await planner.create(start, [goalBuyStuff, goalMakeStuff, goalGetHomeNStuff]);
		astar.boundaries.debugLogs = false;
		expect(astar.boundaries.log.mock.calls).toMatchSnapshot('astar trace');

		expect(action?.prettyNames()).toMatchSnapshot('final action summary');

		expect(mockedActionCalls).not.toHaveBeenCalled();
		await action.runActual(worldContext);
		expect(mockedActionCalls.mock.calls).toEqual([
			['goActuator', { pointB: { name: 'store' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'apple' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'apple' } }],
			['buyActuator', { someone: { name: 'mark' }, item: { name: 'pie crust' } }],
			['goActuator', { pointB: { name: 'school' } }],
			['makePieActuator', {}],
			['goActuator', { pointB: { name: 'home' } }],
		]);

		expect(await getWorldContextData(worldContext)).toMatchSnapshot('worldContext data');
	});

	/**
		Right now, this isn't possible
		Maybe when we do a "units only" match, we can skip the transitionable edges (not sure what that looks like, but it's worth trying)
		This may result in many-multiple states (e.g. if you could be at 3 locations, you'll get 3 result (times how ever many other options can shift too))
		(maybe just skip the ones we can't find - if an edge does match, use it to lock down the options; if not then just ignore it)

		@see {match}
	*/
	// TODO test('location via edge transition');
});