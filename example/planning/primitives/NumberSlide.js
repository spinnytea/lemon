/* eslint-disable max-classes-per-file */

/**
	this implements State and Action so we can perform a plan with A* (astar)
	this isn't a Blueprint (doesn't work with the knowledge graph directly), it's a simple impl of Path for astar

	here we have a number slide game
	0 is the position we can slide around
*/
const _ = require('lodash');
const { PathAction, PathState } = require('../../../src/planning/primitives/path');

const DIRECTIONS = Object.freeze({
	up: 'up',
	down: 'down',
	left: 'left',
	right: 'right',
});

class NumberSlideAction extends PathAction {
	constructor({ dir, ...rest }) {
		super(rest);

		this.dir = dir;
	}

	/**
		@override
	*/
	prettyNames() {
		return `NumberSlideAction -> ${this.dir}`;
	}

	/**
		@override
	*/
	async cost() {
		return Promise.resolve(1);
	}

	/**
		@override
		@param {NumberSlideState} from
		@returns {NumberSlideState}
	*/
	async runAbstract(from) {
		// 1. clone
		const state = new NumberSlideState({ numbers: _.cloneDeep(from.numbers) });

		// 2. update
		await state.find(0).then(([y, x]) => {
			let dy = 0;
			let dx = 0;
			switch (this.dir) {
			case DIRECTIONS.up:    dy = -1; break; // eslint-disable-line no-multi-spaces
			case DIRECTIONS.down:  dy =  1; break; // eslint-disable-line no-multi-spaces
			case DIRECTIONS.left:  dx = -1; break; // eslint-disable-line no-multi-spaces
			case DIRECTIONS.right: dx =  1; break; // eslint-disable-line no-multi-spaces
			default: throw new RangeError(`invalid dir: "${this.dir}"`); // should/can not happen
			}
			// swap 0 with the adjacent number
			state.numbers[y][x] = state.numbers[y + dy][x + dx];
			state.numbers[y + dy][x + dx] = 0;
		});

		// 3. return
		return state;
	}
}

class NumberSlideState extends PathState {
	/**
		@param {Array<Array<number>>} numbers - a matrix of values; must be at least 1x1
	*/
	constructor({ numbers, ...rest }) {
		super(rest);

		this.numbers = numbers;
	}

	/**
		sum the manhattan distance of all the numbers

		@override
		@param {NumberSlideState} to
	*/
	async distance(to) {
		return Promise.all(this.numbers.map((row, y) => (
			Promise.all(row.map((num, x) => (
				to.find(num).then(([ty, tx]) => (
					Math.abs(tx - x) + Math.abs(ty - y)
				))
			))).then((totals) => _.sum(totals))
		))).then((totals) => _.sum(totals));
	}

	/**
		@override
		@return {Promise<Array<NumberSlideAction>>} - the directions we can move in
	*/
	async $calcActions() {
		return this.find(0).then(([y, x]) => {
			const ret = [];

			if (y > 0) {
				ret.push(new NumberSlideAction({ dir: DIRECTIONS.up }));
			}
			if (y < this.numbers.length - 1) {
				ret.push(new NumberSlideAction({ dir: DIRECTIONS.down }));
			}
			if (x > 0) {
				ret.push(new NumberSlideAction({ dir: DIRECTIONS.left }));
			}
			if (x < this.numbers[y].length - 1) {
				ret.push(new NumberSlideAction({ dir: DIRECTIONS.right }));
			}

			return ret;
		});
	}

	/**
		@override
		@param {NumberSlideState} state
		@return {Promise<boolean>}
	*/
	async matches(state) {
		// number slide is not have an approximation
		return Promise.resolve(_.isEqual(this.numbers, state.numbers));
	}

	/**
		@override
		@param {NumberSlideState} state
		@return {boolean}
	*/
	equals(state) {
		return _.isEqual(this.numbers, state.numbers);
	}

	/**
		helper function to find the value within the array
		when found, we will call the callback (this.numbers[y][x] ~= function(y, x))
		this will only call the callback once, on the first value it finds

		this doesn't need to return a promise for this toy example,
		but it helps reinforce the realism for other impls of path (i.e. blueprints)

		@param {number} val - a number on the puzzle
		@return {Promise<[number, number]>} - return the found [y, x]
	*/
	find(val) {
		let y;
		let x;

		this.numbers.every((row, yy) => (
			row.every((num, xx) => {
				if (val === num) {
					y = yy;
					x = xx;
					return false; // exit early
				}
				return true; // keep going
			})
		));

		return Promise.resolve([y, x]);
	}
}

exports.NumberSlideAction = NumberSlideAction;
exports.NumberSlideState = NumberSlideState;
